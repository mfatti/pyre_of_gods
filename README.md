# Pyre of Gods Client

This is the client application for my old game project Pyre of Gods. It is a 3D voxel game inspired by Minecraft and Terraria.

**Note the code isn't the neatest or best designed**

I wrote this ~4-5 years ago when I was teaching myself OpenGL, networking and threading in C++, so things might be a bit messy.


**To compile**

I have this compiling under Pop!_OS 20.04 (so Ubuntu 20.04), but the original work was done under Windows in VS2015.

The dependencies are in the makefile but for the client you'll need:
- zlib
- pthread
- GLEW
- GL
- libpng
- GLFW

Everything is in the makefile, note it's not a very good makefile so any changes to headers will need a clean compile. Just run

> make

to compile release and

> make debug

to compile debug.

> make clean

will clean everything.

**Videos**

Want to just see the game in action? I've recorded some short clips demonstrating some aspects of the game:

[![Demonstration](https://img.youtube.com/vi/DtX8xCy3nt8/0.jpg)](https://youtu.be/DtX8xCy3nt8)

[![Water Test](https://img.youtube.com/vi/t4w_z90yHQ0/0.jpg)](https://youtu.be/t4w_z90yHQ0)

[![Cave Lighting Test](https://img.youtube.com/vi/laOQKxzm438/0.jpg)](https://youtu.be/laOQKxzm438)
