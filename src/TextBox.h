#ifndef _TEXTBOX_H_
#define _TEXTBOX_H_

#include "Text.h"

class TextBox : public GUIElement {
	public:
		TextBox(string text, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size);

		void SetCharLimit(GLuint limit);
		void SetNumeric(bool numbers);
		void SetHint(string hint);
		void UnFocus();
		void TextEntered(GLuint codepoint);

		void SetText(string text);
		string GetText();

		void SetCharFunction(void(*fn)(GUIElement*));

		virtual void Update(bool bForce = false);
		virtual void MouseDownFunction(GLuint button);
		virtual void Release(GLuint button);

		static void DefaultFunction(GUIElement* e);

	protected:

	private:
		Text*	m_Text;
		Text*	m_Hint;
		GLfloat	m_Counter;
		bool	m_bNumbersOnly;
		GLuint	m_CharLimit;
		void	(*m_CharFunction)(GUIElement*);

		void SetCursor();
};

#endif