#ifndef _INI_FILE_H_
#define _INI_FILE_H_

#include <string>
#include <fstream>

using std::string;
using std::fstream;

class INIFile {
	public:
		INIFile(string file);
		~INIFile();

		void Close();

		bool IsEmpty();
		bool WriteDefaults(string defaults);

		bool FindSection(string section);

		string ReadSetting(string setting);

		void Clear();
		void WriteSection(string section);
		void WriteSetting(string setting, string value);
		void WriteSpace();
		bool Write();

	protected:

	private:
		string			m_strFileContents;
		string			m_strLastLine;
		string			m_strFileName;
		fstream			m_INIFile;
		bool			m_bIsEmpty;
		unsigned int	m_Position;

		bool _Search(string str, unsigned int pos = 0);
};

#endif