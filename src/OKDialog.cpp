#include "OKDialog.h"
#include "GUIManager.h"

OKDialog::OKDialog(Manager* parent, string title, string text, void(*fn)(GUIElement*))
	: GUIElement(parent, NULL, vector2df(), parent->GetRoot()->GetScreenSize())
{
	// create the background fade
	AddFade();
	CreateBufferObjects();

	// create the actual pop-up
	vector2di size = GetSize();
	vector2df sized(460, 240);
	Pane* pane = GetGUIManager()->CreatePane(string("^8").append(title).c_str(), vector2df(size) * 0.5f, sized, GetGUIManager()->GetFont("font20b"), this, 255, false);
	GetGUIManager()->CreateText(text.c_str(), vector2df(30, 40), vector2di((GLint)sized.x - 60, (GLint)sized.y - 64), NULL, pane)->SplitByWord(true);
	GetGUIManager()->CreateButton("OK", vector2df((sized.x - 100) * 0.5f, sized.y - 44), vector2di(100, 24), (fn ? fn : GUIElement::Close), NULL, pane);

	// set this as menu so it will work in menus
	SetMenu();

	// set up the animation
	SetAlpha(0);
	pane->SetScale(vector3df());
	FadeTo(1);
	ScaleTo(vector2df(1));
	pane->MoveTo((vector2df(size) - sized) * 0.5f);
}

void OKDialog::MouseDownFunction(GLuint button)
{

}

void OKDialog::ScaleTo(vector2df scale, bool hide, bool kill)
{
	GetChild(0)->ScaleTo(scale, hide, kill);
}