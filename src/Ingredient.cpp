#include "Ingredient.h"
#include "GUIManager.h"
#include "Item.h"
#include <sstream>

Ingredient::Ingredient(Manager* parent, GUIElement* guiParent, vector3df pos, const ItemClass* ic, GLuint amount, GLuint amountOwned)
	: GUIElement(parent, guiParent, pos, vector2di(166, 34))
{
	// create the quads
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(0, 0, 0, 100));
	CreateBufferObjects();

	// name of the ingredient
	m_FullName = ic->strName;
	m_Name = GetGUIManager()->CreateText(m_FullName.c_str(), vector2df(34, 0), vector2di(132, 0), GetGUIManager()->GetFont("font14b"), this);
	if (m_FullName.length() > 12)
	{
		string sname = m_FullName.substr(0, 9).append("...");
		m_Name->SetText(sname);
	}

	// quantity of ingredient
	std::stringstream ss;
	ss << "^8x" << amount << " (";
	if (amountOwned >= amount)
		ss << "^2";
	else
		ss << "^1";
	ss << amountOwned << "^8)";
	m_Amount = GetGUIManager()->CreateText(ss.str().c_str(), vector2df(34, 16), vector2di(132, 0), NULL, this);

	// and finally the mesh view
	m_MeshView = GetGUIManager()->CreateMeshView(ic->Meshes[0], ic->strTexturePath, vector2df(0, 1), vector2di(32), this);
	if (ic->Type == BLOCK)
		m_MeshView->SetBlockModelView(ic->BlockSize);
	else if (ic->TypeSub == INGOT)
		m_MeshView->SetIngotModelView();
}

void Ingredient::MouseDownFunction(GLuint button)
{
	// do nothing
}