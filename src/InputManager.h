#ifndef _INPUTMANAGER_H_
#define _INPUTMANAGER_H_

#include "Misc.h"

class Game;

class InputManager {
	public:
		static InputManager& GetInstance();		
		static void MouseCallback(GLFWwindow* window, int button, int action, int mods);
		static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void CharCallback(GLFWwindow* window, unsigned int codepoint);
		static void ScrollCallback(GLFWwindow* window, double x, double y);
		
		void AssignToGame(Game* game);
	
	protected:
	
	private:
		InputManager() : m_pGame(NULL) {}
		~InputManager() {}
		
		InputManager(InputManager const&);
		void operator=(InputManager const&);
		
		Game* m_pGame;
};

#endif
