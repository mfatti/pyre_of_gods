#ifndef _CLIENT_MANAGER_H_
#define _CLIENT_MANAGER_H_

#include "Client.h"
#include "SceneManager.h"

class NetworkManager;
class Text;

class ClientManager {
	public:
		ClientManager(SceneManager* sceneman, NetworkManager* netman);

		void Update();
		void NewClient(int id, string name);
		void DeleteClient(int id);
		string GetClientName(int id);

		bool DoesClientExist(int id);
		void UpdateClientPosition(int id, PlayerData& data);
		void UpdateClientInventory(int id, int type, unsigned char* data, int len);
		void UpdateClientStats(int id, const Stats& stats);

	protected:

	private:
		struct CharacterGUI {
			Text* nameplate;
			int width;
		};

		map<int, Client*>		m_Clients;
		map<int, CharacterGUI>	m_GUIClientNames;
		SceneManager*			m_pSceneMan;
		NetworkManager*			m_pNetworkMan;
		int						m_NewClientID;
		string					m_NewClientName;
		int						m_DisconnectID;
};

#endif