#ifndef _CHAT_TEXT_H_
#define _CHAT_TEXT_H_

#include "Text.h"
#include "Scrollbar.h"
#include <sstream>

struct ChatLine {
	// text for this line
	string text;

	// timer
	GLint timer;
};

class ChatText : public Text {
	public:
		ChatText(GLuint maxlines, Font* font, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size);

		void AddChatMessage(const string& text);
		void ToggleChat(bool visible);

		virtual void SetPosition(const vector2df& pos);
		virtual void Update(bool bForce = false);
		virtual bool IsHovered();

	protected:

	private:
		GLuint				m_MaxLineCount;
		GLuint				m_LineCount;
		Scrollbar*			m_Scrollbar;
		vector<ChatLine>	m_Lines;
		bool				m_bNeedsCreation;
		std::stringstream	m_TextStream;
		bool				m_bChatVisible;
};

#endif
