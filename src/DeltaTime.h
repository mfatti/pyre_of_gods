#ifndef _DELTA_TIME_H_
#define _DELTA_TIME_H_

#include <chrono>

class DeltaTime {
public:
	DeltaTime(DeltaTime const&) = delete;
	void operator=(DeltaTime const&) = delete;

	static void CalculateDelta();
	static double GetDelta();

protected:

private:
	static DeltaTime& _Instance()
	{
		static DeltaTime instance;
		return instance;
	}

	DeltaTime();

	std::chrono::time_point<std::chrono::system_clock>	m_Time;
	std::chrono::duration<double>						m_Delta;
};

#endif