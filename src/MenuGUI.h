#ifndef _MENU_GUI_H_
#define _MENU_GUI_H_

#include "SceneManager.h"
#include "GUIManager.h"
#include "SettingsManager.h"

enum {
	initialisation,
	connection_failed,
	connection_successful,
	terrain_download,
	starting_game
};

struct ServerDetails {
	string name;
	string address;
	string port;
};

class MenuGUI {
	public:
		MenuGUI(GUIManager* gui, SceneManager* scene, SettingsManager* settings);
		~MenuGUI();

		void GotoPage(GLuint page);
		void ReturnToPreviousPage();
		void ForwardPage();
		void ReversePage();

		void ShowPlayMenu();
		void HidePlayMenu();

		void ConnectionFailed();
		void ConnectionSuccessful();

		int GetStatus();
		void SetStatus(int status);

		void WaitForChange();

		void NewPlayer();
		void DeletePlayer();
		string GetPlayerName();

		void LoadServers();
		void AddServer(string name);
		void RemoveServer();
		void EditServer(string name, string address, string port);
		void SaveServerList();

		static void StartNetworkManager(GUIElement* e);
		static void AddNewServer(GUIElement* e);
		static void StartEditServer(GUIElement* e);

	protected:

	private:
		GUIManager*				m_pGUIMan;
		SceneManager*			m_pSceneMan;
		SettingsManager*		m_Settings;

		vector2di				m_PaneSize;

		ListBox*				m_GUICharacters;
		vector<string>			m_PlayerFiles;

		TextBox*				m_GUICharacterName;

		TextBox*				m_GUIIPAddress;
		TextBox*				m_GUIPortNumber;
		Table*					m_GUIServers;
		vector<ServerDetails>	m_Servers;

		Text*					m_GUIConnectionStatus;

		vector<Pane*>			m_Pages;
		GLuint					m_CurrentPage;
		GLuint					m_PreviousPage;

		int						m_Status;
};

#endif