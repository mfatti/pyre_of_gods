#include "Menu.h"
#include "GUIManager.h"
#include "Pane.h"
#include "Button.h"
#include "YesNoDialog.h"

Menu::Menu(Manager* parent, vector2di page)
	: GUIElement(parent, NULL, vector2df(), parent->GetRoot()->GetScreenSize())
	, m_PageSize(page)
{
	AddFade();
	CreateBufferObjects();

	m_bMenu = true;

	vector2di size = parent->GetRoot()->GetScreenSize();
	Pane* pane = GetGUIManager()->CreatePane("Menu", vector2df((GLfloat)(size.x - m_PageSize.x) * 0.5f, (GLfloat)(size.y - m_PageSize.y) * 0.5f), m_PageSize, GetGUIManager()->GetFont("font20b"), this, 140, false);
}

void Menu::MouseDownFunction(GLuint button)
{

}

void Menu::ButtonClicked(GUIElement* e)
{
	Button* b = dynamic_cast<Button*>(e);
	Menu* m = dynamic_cast<Menu*>(e->GetParent()->GetParent());

	if (b && m)
	{
		m->SwitchPage(b->GetText());
	}
}

void Menu::ReturnToMainScreen(GUIElement* e)
{
	Menu* m = e->GetGUIManager()->GetMenu();

	if (m)
		m->SwitchPage("Menu");

	// if this is a pop-up dialog then close it
	YesNoDialog* yn = dynamic_cast<YesNoDialog*>(e->GetRoot());
	if (yn)
		GUIElement::Close(e);
}

Pane* Menu::AddPage(string name, vector2df buttonPos, vector2di buttonSize, void(*fn)(GUIElement* e))
{
	Button* b = GetGUIManager()->CreateButton(name.c_str(), buttonPos, buttonSize, Menu::ButtonClicked, GetGUIManager()->GetFont("font14b"), GetPageByName("Menu"));

	vector2di size = GetManager()->GetRoot()->GetScreenSize();
	Pane* pane = GetGUIManager()->CreatePane(name.c_str(), vector2df((GLfloat)(size.x - m_PageSize.x) * 0.5f, (GLfloat)(size.y - m_PageSize.y) * 0.5f), m_PageSize, GetGUIManager()->GetFont("font20b"), this, 140, false);
	pane->SetAlpha(0);

	m_PageLoadFunctions[name] = fn;

	return pane;
}

Pane* Menu::GetPageByName(string name)
{
	vector<GUIElement*> children = GetChildren();
	vector<GUIElement*>::iterator it = children.begin();

	while (it != children.end())
	{
		Pane* p = dynamic_cast<Pane*>(*it);

		if (p && p->GetName() == name)
			return p;

		++it;
	}

	return NULL;
}

Pane* Menu::GetActivePage()
{
	vector<GUIElement*> children = GetChildren();
	vector<GUIElement*>::iterator it = children.begin();

	while (it != children.end())
	{
		Pane* p = dynamic_cast<Pane*>(*it);

		if (p && p->IsVisible())
			return p;

		++it;
	}

	return NULL;
}

vector2di Menu::GetPageSize()
{
	return m_PageSize;
}

void Menu::SetMenuPosition(const vector2df &pos)
{
	vector<GUIElement*> children = GetChildren();
	vector<GUIElement*>::iterator it = children.begin();

	while (it != children.end())
	{
		(*it)->SetOffset(pos);

		++it;
	}
}

void Menu::SwitchPage(string name)
{
	Pane* p1 = GetActivePage();
	Pane* p2 = GetPageByName(name);

	if (p1 && p2)
	{
		p1->FadeTo(0, true);
		p2->SetVisible(true);
		p2->FadeTo(1);

		if (m_PageLoadFunctions[p2->GetName()])
			m_PageLoadFunctions[p2->GetName()](p2);
	}
}
