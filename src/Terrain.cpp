#include "Terrain.h"
#include "SceneManager.h"
#include "Game.h"
#include <random>
#include <fstream>
#include <sstream>
#include <sys/stat.h>

void ThreadTask(Terrain* t)
{
	t->TerrainThreadTask();
}

bool Terrain::m_bThreadRunning(true);

Terrain::Terrain(Manager* manager)
	: GLObject(manager, vector3df(), vector3di())
	//, m_Noise(1024)
	, m_bDoSunlightUpdate(false)
	, m_pChunkToUpdate(NULL)
	, m_ChunkDistance(12)
	, m_ChunksCount(0)
{
	m_pPlayer = GetSceneManager()->GetFirstInstance<Player>();
	std::chrono::duration<double> time = std::chrono::system_clock::now().time_since_epoch();
	m_TerrainSeed = time.count();
	//m_Noise.SetSeed(m_TerrainSeed);

	//RunInThread(ThreadTask, this, &m_bThreadRunning, &m_pChunkToUpdate);
	m_bThreadRunning = true;
	m_pTerrainThread = new std::thread(ThreadTask, this);
	m_pTerrainThread->detach();

	GetSceneManager()->AddTransparencyRenderFunction(this, Terrain::RenderWater);

	//cout << sizeof(TerrainBlock) << endl;
}

Terrain::~Terrain()
{
	// remove the transparency render function
	GetSceneManager()->RemoveTransparencyRenderFunction(this, Terrain::RenderWater);
	
	// delete the thread
	m_bThreadRunning = false;

	std::this_thread::sleep_for(std::chrono::milliseconds(50));

	//m_pTerrainThread->join();
	delete m_pTerrainThread;

	// wait for the thread to finish
	//std::this_thread::sleep_for(std::chrono::milliseconds(50));
	
	// clean up the terrain
	TerrainMap::iterator it = m_Terrain.begin();
	while (it != m_Terrain.end())
	{
		if (it->second)
		{
			delete it->second;
			it->second = NULL;
		}
		++it;
	}
	m_Terrain.clear();
}

void Terrain::TerrainThreadTask()
{
	// this is the terrain worker thread which will create terrain chunks
	// this is done in a separate thread so that the main thread remains undisturbed by the lag created duration creation
	NetworkManager* n = GetSceneManager()->GetRoot()->CastTo<Game>()->GetNetworkManager();
	
	while (m_bThreadRunning)
	{
		vector<TerrainChunk*> chunks;
		GetChunksAroundPlayer(chunks, false);
		vector<vector3di> updates;
		const size_t num = chunks.size();

		for (GLuint i = 0; i < num && m_bThreadRunning; ++i) {
			if (!chunks[i]->IsCreated() && !chunks[i]->IsCreating())
			{
				chunks[i]->SetCreating();
			}

			if (!chunks[i]->IsInitialised())
			{
				vector<Bytef> buf;
				if (n->GetTerrainData(chunks[i]->GetChunkPos(), buf))
				{
					chunks[i]->UnpackTerrain(&buf[0], buf.size());
					UpdateChunksAroundChunk(chunks[i]->GetChunkPos());
					//break;
				}
				else
				{
					chunks[i]->SetCreating(false);
				}
			}
			if (!chunks[i]->IsWaterInitialised())
			{
				vector<Bytef> buf;
				if (n->GetWaterData(chunks[i]->GetChunkPos(), buf))
				{
					chunks[i]->UnpackWater(&buf[0], buf.size());
				}
			}

			if (chunks[i]->IsCreating())
				chunks[i]->CreateMesh();
		}

		if (m_bThreadRunning)
			UpdateTerrainWater(chunks);

		if (m_bThreadRunning)
			ManageUnseenChunks();

		std::this_thread::sleep_for(std::chrono::milliseconds(20));
		
		/*if (*pChunk != NULL)
		{
			if (!(*pChunk)->IsInitialised())
			{
				vector<Bytef> buf;
				if (n->GetTerrainData((*pChunk)->GetChunkPos(), buf))
				{
					(*pChunk)->UnpackTerrain(&buf[0], buf.size());
					t->UpdateChunksAroundChunk((*pChunk)->GetChunkPos());
					//cout << (*pChunk)->GetChunkPos().x << ", " << (*pChunk)->GetChunkPos().y << ", " << (*pChunk)->GetChunkPos().z << endl;
				}
				else
					(*pChunk)->SetCreating(false);
				//vector2di pos((*pChunk)->GetChunkPos().x, (*pChunk)->GetChunkPos().z);
				//(*pChunk)->GenerateTerrain(t->GetNoise(pos));
			}
			if (!(*pChunk)->IsWaterInitialised())
			{
				vector<Bytef> buf;
				if (n->GetWaterData((*pChunk)->GetChunkPos(), buf))
				{
					(*pChunk)->UnpackWater(&buf[0], buf.size());
				}
			}

			if ((*pChunk)->IsCreating())
				(*pChunk)->CreateMesh();

			*pChunk = NULL;

			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
		else
		{
			t->ManageUnseenChunks();

			std::this_thread::sleep_for(std::chrono::milliseconds(5));
		}

		// we will update terrain chunk water here rather than in the main thread
		t->UpdateTerrainWater();*/
	}
}

void Terrain::StopThreads()
{
	m_bThreadRunning = false;
}

bool PointInTriangle(vector3df& p, vector3df& p1, vector3df& p2, vector3df& p3)
{
	// note this checks against a 2d triangle and is mainly used for the GetHeightBelow when dealing with half blocks
	GLfloat a = 0.5f * (-p2.z * p3.x + p1.z * (-p2.x + p3.x) + p1.x * (p2.z - p3.z) + p2.x * p3.z);
	GLfloat sign = a < 0 ? -1.f : 1.f;
	GLfloat s = (p1.z * p3.x - p1.x * p3.z + (p3.z - p1.z) * p.x + (p1.x - p3.x) * p.z) * sign;
	GLfloat t = (p1.x * p2.z - p1.z * p2.x + (p1.z - p2.z) * p.x + (p2.x - p1.x) * p.z) * sign;

	return (s > 0 && t > 0 && s + t < 2 * a * sign);
}

GLfloat GetHeightOnTriangle(vector3df& p, vector3df& p1, vector3df& p2, vector3df& p3)
{
	// gets the height on this triangle - used for when colliding with half-block slopes as the GetHeightOnFace doesn't work well for those situations
	/*float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);

	float h1 = ((p2.z - p3.z) * (p.x - p3.x) + (p3.x - p2.x) * (p.z - p3.z)) / det;
	float h2 = ((p3.z - p1.z) * (p.x - p3.x) + (p1.x - p3.x) * (p.z - p3.z)) / det;
	float h3 = 1.f - h1 - h2;

	return (h1 * p1.y + h2 * p2.y + h3 * p3.y);*/
	triangle3df t(p1, p2, p3);
	vector3df n = -t.Normal().Normal();

	GLfloat d = -((n.x * p1.x) + (n.y * p1.y) + (n.z * p1.z));

	return -((n.x * p.x) + (n.z * p.z) + d) / (n.y);
}

GLfloat GetHeightOnFace(vector3df& p, vector3df& p1, vector3df& p2, vector3df& p3, vector3df& p4)
{
	//           p4 - - - p3 
	//           |         |
	//           |         |
	//           |         |
	//           p2 - - - p1
	
	// get the y-coordinate on a face at point (p.x, p.z)
	vector2df offset((p.x - p1.x) * ONE_OVER_BLOCK_SIZE, (p.z - p1.z) * ONE_OVER_BLOCK_SIZE);
	GLfloat a = (p4.y * offset.x + p3.y * (1 - offset.x)) * offset.y;
	GLfloat b = (p2.y * offset.x + p1.y * (1 - offset.x)) * (1 - offset.y);

	return a + b;
}

GLfloat Terrain::GetHeightBelow(vector3df pos)
{
	// make sure that a chunk exists in this location
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		// if the chunk isn't initialised, return the position passed in
		if (!pChunk->IsInitialised())
			return pos.y - 16.f;
		
		// the chunk exists
		int x = blockPos.x;
		int y = blockPos.y;
		int z = blockPos.z;

		//bool searching = true;
		int height = (int)(pos.y * ONE_OVER_BLOCK_SIZE);
		if (pos.y < 0) height--;
		while (true)
		{
			for (; y >= 0; y--, height--) {
				TerrainBlock block = pChunk->GetBlock(x, y, z);
				if (block.Solid)
				{
					// if this is a full block just get the height on top
					if (!block.XX && !block.XY && !block.YX && !block.YY)
						return (height + 1) * BLOCK_SIZE;

					// if this is a half block (ie half slope corner) then check if we're in the visible triangle and if so, get the height on the triangle otherwise fall through to the next block below
					vector3df p(chunkPos.x * CHUNK_SIZE * BLOCK_SIZE + x * BLOCK_SIZE, chunkPos.y * CHUNK_SIZE * BLOCK_SIZE + (y + 1) * BLOCK_SIZE, chunkPos.z * CHUNK_SIZE * BLOCK_SIZE + z * BLOCK_SIZE);
					if (block.Half)
					{
						vector3df p1, p2, p3;

						if (!block.XX && (block.XY && block.YX && block.YY))
						{
							p1.Set(p + vector3df(0, -BLOCK_SIZE, BLOCK_SIZE));
							p2.Set(p);
							p3.Set(p + vector3df(BLOCK_SIZE, -BLOCK_SIZE, 0));
						}
						else if (!block.XY && (block.XX && block.YX && block.YY))
						{
							p1.Set(p + vector3df(0, -BLOCK_SIZE, 0));
							p2.Set(p + vector3df(BLOCK_SIZE, 0, 0));
							p3.Set(p + vector3df(BLOCK_SIZE, -BLOCK_SIZE, BLOCK_SIZE));
						}
						else if (!block.YX && (block.XY && block.XX && block.YY))
						{
							p1.Set(p + vector3df(BLOCK_SIZE, -BLOCK_SIZE, BLOCK_SIZE));
							p2.Set(p + vector3df(0, 0, BLOCK_SIZE));
							p3.Set(p + vector3df(0, -BLOCK_SIZE, 0));
						}
						else
						{
							p1.Set(p + vector3df(BLOCK_SIZE, -BLOCK_SIZE, 0));
							p2.Set(p + vector3df(BLOCK_SIZE, 0, BLOCK_SIZE));
							p3.Set(p + vector3df(0, -BLOCK_SIZE, BLOCK_SIZE));
						}

						if (PointInTriangle(pos, p1, p2, p3))
							return GetHeightOnTriangle(pos, p1, p2, p3);
						else
							continue;
					}

					// if the block has any raised corners we'll need to get the height on the face
					vector3df p1(p + vector3df(0, -(GLfloat)block.XX * BLOCK_SIZE, 0));
					vector3df p2(p + vector3df(BLOCK_SIZE, -(GLfloat)block.XY * BLOCK_SIZE, 0));
					vector3df p3(p + vector3df(0, -(GLfloat)block.YX * BLOCK_SIZE, BLOCK_SIZE));
					vector3df p4(p + vector3df(BLOCK_SIZE, -(GLfloat)block.YY * BLOCK_SIZE, BLOCK_SIZE));
					bool bFlipped = block.XY + block.YX > block.XX + block.YY;

					if (bFlipped)
					{
						if (PointInTriangle(pos, p1, p2, p4))
							return GetHeightOnTriangle(pos, p1, p2, p4);
						else
							return GetHeightOnTriangle(pos, p4, p3, p1);
					}
					else
					{
						if (PointInTriangle(pos, p1, p2, p3))
							return GetHeightOnTriangle(pos, p1, p2, p3);
						else
							return GetHeightOnTriangle(pos, p2, p3, p4);
					}
				}
			}

			y += CHUNK_SIZE;
			chunkPos.y--;

			LockMutex();
			TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
			if (it == m_Terrain.end())
			{
				UnlockMutex();
				return pos.y - 16.f;
			}

			pChunk = it->second;
			UnlockMutex();

			if (!pChunk)
				return pos.y - 16.f;
		}

		// if we were unsuccessful, return 0
		return pos.y - 16.f;
	}
	else
	{
		// a chunk doesn't exist here, just return 0
		return pos.y - 16.f;
	}
}

GLfloat Terrain::GetFreeSpaceBelow(vector3df pos)
{
	// make sure that a chunk exists in this location
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		// if the chunk isn't initialised, return the position passed in
		if (!pChunk->IsInitialised())
			return pos.y - 16;

		// the chunk exists
		int x = blockPos.x;
		int y = blockPos.y;
		int z = blockPos.z;

		//bool searching = true;
		int height = (int)(pos.y * ONE_OVER_BLOCK_SIZE);
		if (pos.y < 0) height--;
		while (true)
		{
			for (; y >= 0; y--, height--) {
				TerrainBlock block = pChunk->GetBlock(x, y, z);

				if (!block.Solid)
					return height * BLOCK_SIZE;
			}

			y += CHUNK_SIZE;
			chunkPos.y--;

			LockMutex();
			TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
			if (it == m_Terrain.end())
			{
				UnlockMutex();
				return 0;
			}
			UnlockMutex();

			pChunk = it->second;

			if (!pChunk)
				return 0;
		}

		// if we were unsuccessful, return 0
		return pos.y;
	}
	else
	{
		// a chunk doesn't exist here, just return 0
		return pos.y;
	}
}

GLfloat Terrain::GetDistanceAbove(vector3df pos)
{
	// make sure that a chunk exists in this location
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		// the chunk exists
		int x = blockPos.x;
		int y = blockPos.y;
		int z = blockPos.z;

		//bool searching = true;
		int height = (int)(pos.y * ONE_OVER_BLOCK_SIZE);
		if (pos.y < 0) height--;
		while (true)
		{
			for (; y < CHUNK_SIZE; ++y, ++height) {
				TerrainBlock block = pChunk->GetBlock(x, y, z);
				if (block.Solid)
				{
					vector3df p(chunkPos.x * CHUNK_SIZE * BLOCK_SIZE + x * BLOCK_SIZE, chunkPos.y * CHUNK_SIZE * BLOCK_SIZE + (y + 1) * BLOCK_SIZE, chunkPos.z * CHUNK_SIZE * BLOCK_SIZE + z * BLOCK_SIZE);
					if (block.Half)
					{
						vector3df p1, p2, p3;

						if (!block.XX && (block.XY && block.YX && block.YY))
						{
							p1.Set(p + vector3df(BLOCK_SIZE, 0, 0));
							p2.Set(p + vector3df(BLOCK_SIZE, 0, BLOCK_SIZE));
							p3.Set(p + vector3df(0, 0, BLOCK_SIZE));
						}
						else if (!block.XY && (block.XX && block.YX && block.YY))
						{
							p1.Set(p + vector3df(BLOCK_SIZE, 0, BLOCK_SIZE));
							p2.Set(p + vector3df(0, 0, BLOCK_SIZE));
							p3.Set(p);
						}
						else if (!block.YX && (block.XY && block.XX && block.YY))
						{
							p1.Set(p);
							p2.Set(p + vector3df(BLOCK_SIZE, 0, 0));
							p3.Set(p + vector3df(BLOCK_SIZE, 0, BLOCK_SIZE));
						}
						else
						{
							p1.Set(p + vector3df(0, 0, BLOCK_SIZE));
							p2.Set(p);
							p3.Set(p + vector3df(BLOCK_SIZE, 0, 0));
						}

						if (PointInTriangle(pos, p1, p2, p3))
							continue;
					}
					
					return (int)(height * BLOCK_SIZE) - pos.y;
				}
			}

			y = 0;
			++chunkPos.y;

			LockMutex();
			TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
			if (it == m_Terrain.end())
			{
				UnlockMutex();
				return 9001;
			}
			UnlockMutex();

			pChunk = it->second;

			if (!pChunk)
				return 9001;
		}

		// if we were unsuccessful, return 0
		return 9001;
	}
	else
	{
		// a chunk doesn't exist here, just return 0
		return 9001;
	}
}

/*vector<GLfloat>& Terrain::GetNoise(vector2di pos)
{
	if (m_TerrainNoise.find(pos) == m_TerrainNoise.end())
	{
		// if this noise hasn't yet been generated then generate it!
		vector<GLfloat> noise;
		GLint X = pos.x * CHUNK_SIZE;
		GLint Y = pos.y * CHUNK_SIZE;
		for (GLint i = -1; i <= CHUNK_SIZE; i++) {
			for (GLint j = -1; j <= CHUNK_SIZE; j++) {
				double _x = (double)(X + i) / 9.0;
				double _y = (double)(Y + j) / 9.0;
				noise.push_back((float)m_Noise.GetNoise(_x, _y, 0.5) * 14);
			}
		}
		noise.push_back(0);

		m_TerrainNoise[pos] = noise;
	}

	return m_TerrainNoise[pos];
}*/

bool Terrain::DoesPointCollide(vector3df& p)
{
	return (GetBlock(p).BlockType != 0);
}

bool Terrain::DoesRayCollide(line3df& l, vector3df& oo, GLfloat& out)
{
	for (GLuint i = 0; i < m_ChunksCount; ++i) {
		if (m_Chunks[i]->DoesRayCollide(l, oo, out))
			return true;
	}
	
	return false;
}

void TerrainBlockToByte(TerrainBlock& in, unsigned char* out)
{
	unsigned char* buf = reinterpret_cast<unsigned char*>(&in);
	memcpy(out, buf, 8);
}

void ByteToTerrainBlock(unsigned char* in, TerrainBlock& out)
{
	unsigned char* buf = reinterpret_cast<unsigned char*>(&out);
	memcpy(buf, in, 8);
}

bool Terrain::ChangeBlock(vector3df pos, TerrainBlock type)
{
	/*
		Essentially, the change_block message will be packed as follows:

		[##][i]|[tttttttt][x][y][z][cx__][cy__][cz__]|[repeat from | to | for more blocks]

		where each char ^ represents one byte.

		# = reserved bytes for data (we don't need to populate that here, SendPacket does it for us)
		i = client id (for inventory management)
		t = terrain block
		x = x position in chunk
		y = y position in chunk
		z = z position in chunk
		cx = chunk x position
		cy = chunk y position
		cz = chunk z position

		This message COULD be shrunk by 3 bytes per block, but I want to reduce calculations done on the server - 
		if we calculate the chunk position and position in chunk for each block, the server doesn't need to, saving
		clock cycles that could be used elsewhere. This might not be a necessary optimisation, but it WILL help in
		server processing speed and load, hopefully reducing strain on the server.

		This function will only ever handle single-block messages, or multi-block structure messages. The server can
		send other messages independently which can affect multiple non-related multi-block/single-block structures,
		which can be mixed and matched - the format should easily allow this.
	*/
	
	// check we're connected to a server / network manager is running
	NetworkManager* n = GetSceneManager()->GetRoot()->CastTo<Game>()->GetNetworkManager();
	if (!n)
		return false;

	// function for placing terrain blocks in the buffer - ensure 23 bytes free exist
	auto terrainblock_to_buffer = [](TerrainBlock& in, vector3di& blockPos, vector3di& chunkPos, unsigned char* buf, int start) {
		// convert the terrainblock to bytes
		TerrainBlockToByte(in, &buf[start]);

		// place the block position in the chunk
		unsigned char _x = (unsigned char)blockPos.x;
		unsigned char _y = (unsigned char)blockPos.y;
		unsigned char _z = (unsigned char)blockPos.z;
		buf[start + 8] = _x;
		buf[start + 9] = _y;
		buf[start + 10] = _z;

		// place the chunk position in the buffer
		IntToByte(chunkPos.x, &buf[start + 11]);
		IntToByte(chunkPos.y, &buf[start + 15]);
		IntToByte(chunkPos.z, &buf[start + 19]);
	};

	// send a change block packet to the server
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		// the chunk exists
		int x = blockPos.x;
		int y = blockPos.y;
		int z = blockPos.z;

		const ItemClass *ic = Item::GetItemClass(type);
		TerrainBlock block = pChunk->GetBlock(x, y, z);
		uLongf _size = 3000; // (5 * 5 * 5 * 23) rounded up to nearest 1000

		// deal with multi-block structures
		if (ic && ic->BlockSize != vector3di(1))
		{
			// we are dealing with a multi-block structure so let's place more than one block
			vector3di size = ic->BlockSize;
			if (type.RotZ == 0 && type.RotY == 0)
			{
				int tmp = size.x;
				size.x = size.z;
				size.z = tmp;
			}

			// create the buffer
			int slen = (size.x * size.y * size.z) * 23;
			unsigned char* buf = new unsigned char[slen];
			//buf[0] = n->GetClientID();

			for (int i = 0; i < size.x; ++i) {
				for (int j = 0; j < size.z; ++j) {
					for (int k = 0; k < size.y; ++k) {
						// set up the terrain block
						TerrainBlock t = type;

						if (i != 0 || j != 0 || k != 0)
							t.Multi = 1;

						// figure out the chunk and position
						vector3di cpos(chunkPos), bpos(blockPos + vector3di(i, k, j));
						PositionCorrect(bpos, cpos);

						// put the data into the buffer
						int position = ((i * size.z * size.y) + (j * size.y) + k) * 23;
						terrainblock_to_buffer(t, bpos, cpos, buf, position);
					}
				}
			}

			// compress the data
			unsigned char* ubuf = new unsigned char[_size];
			compress(ubuf, &_size, buf, slen);
			memmove(&ubuf[1], ubuf, _size);
			ubuf[0] = n->GetClientID();

			// send the data and clean up
			n->SendPacket(change_block, ubuf, _size + 1, true);
			delete[] buf;
			delete[] ubuf;
			return true;
		}
		else
		{
			// we are dealing with a single-block structure so let's just send the one block
			unsigned char buf[23];
			//buf[0] = n->GetClientID();

			// put the data in
			terrainblock_to_buffer(type, blockPos, chunkPos, buf, 0);

			// compress the data
			unsigned char* ubuf = new unsigned char[_size];
			compress(ubuf, &_size, buf, 23);
			memmove(&ubuf[1], ubuf, _size);
			ubuf[0] = n->GetClientID();

			// send the data
			n->SendPacket(change_block, ubuf, _size + 1, true);
			delete[] ubuf;
			return true;
		}
	}

	// doesn't exist on our client, don't allow it
	return false;
}

void Terrain::ProcessChangeBlock(unsigned char* buf, int size)
{
	// uncompress the data received
	uLongf _size = 3000; // (5 * 5 * 5 * 23) rounded up to nearest 1000
	unsigned char* ubuf = new unsigned char[_size];
	uncompress(ubuf, &_size, buf, size);
	
	// process a block_change message (as sent from above) and change the terrain
	// note the above message could be a return message that we sent to the server
	// so we could be processing our own block_change request
	int pos = 0;
	vector<TerrainChunk*> updates;

	// function used in lighting updates
	// function used in lighting updates
	auto compare_block_rgb = [&](bool& highest, vector3di blockPos, vector3di chunkPos, GLuint& r, GLuint& g, GLuint& b) {
		if (!highest)
		{
			PositionCorrect(blockPos, chunkPos);
			TerrainBlock tb = _GetBlock(blockPos, chunkPos);
			if (tb.LightRed > r || tb.LightGreen > g || tb.LightBlue > b)
			{
				r = tb.LightRed;
				g = tb.LightGreen;
				b = tb.LightBlue;
				if (r == 15 || g == 15 || b == 15)
					highest = true;
				return true;
			}
		}

		return false;
	};

	// function used in lighting updates
	auto compare_block_sky = [&](bool& highest, vector3di blockPos, vector3di chunkPos, GLuint& v) {
		if (!highest)
		{
			PositionCorrect(blockPos, chunkPos);
			TerrainBlock tb = _GetBlock(blockPos, chunkPos);
			if (tb.SkyLight > v)
			{
				v = tb.SkyLight;
				if (v == 15)
					highest = true;
				return true;
			}
		}

		return false;
	};

	// function to update block lighting
	auto update_lighting = [&](vector3di blockPos, vector3di chunkPos, bool bUpdate = true) {
		if (bUpdate && !m_BlockLightCreation.empty())
		{
			UpdateLightCreation();
			//return;
		}

		vector3di cpos, bpos;
		TerrainBlock tb;
		bool bHighest = false;
		GLuint r = 0;
		GLuint g = 0;
		GLuint b = 0;

		if (compare_block_rgb(bHighest, blockPos + vector3di(1, 0, 0), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(1, 0, 0);
			cpos = chunkPos;
		}
		if (compare_block_rgb(bHighest, blockPos + vector3di(-1, 0, 0), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(-1, 0, 0);
			cpos = chunkPos;
		}
		if (compare_block_rgb(bHighest, blockPos + vector3di(0, 1, 0), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(0, 1, 0);
			cpos = chunkPos;
		}
		if (compare_block_rgb(bHighest, blockPos + vector3di(0, -1, 0), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(0, -1, 0);
			cpos = chunkPos;
		}
		if (compare_block_rgb(bHighest, blockPos + vector3di(0, 0, 1), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(0, 0, 1);
			cpos = chunkPos;
		}
		if (compare_block_rgb(bHighest, blockPos + vector3di(0, 0, -1), chunkPos, r, g, b))
		{
			bpos = blockPos + vector3di(0, 0, -1);
			cpos = chunkPos;
		}

		PositionCorrect(bpos, cpos);
		SetBlockLight(bpos, cpos, r, g, b, bUpdate);
	};

	// function to update skylighting
	auto update_skylighting = [&](vector3di blockPos, vector3di chunkPos, bool bUpdate = true) {
		if (bUpdate && !m_SunLightCreation.empty())
		{
			UpdateSunlightCreation();
			//return;
		}

		vector3di cpos, bpos;
		TerrainBlock tb;
		bool bHighest = false;
		GLuint v = 0;

		if (compare_block_sky(bHighest, blockPos + vector3di(1, 0, 0), chunkPos, v))
		{
			bpos = blockPos + vector3di(1, 0, 0);
			cpos = chunkPos;
		}
		if (compare_block_sky(bHighest, blockPos + vector3di(-1, 0, 0), chunkPos, v))
		{
			bpos = blockPos + vector3di(-1, 0, 0);
			cpos = chunkPos;
		}
		if (compare_block_sky(bHighest, blockPos + vector3di(0, -1, 0), chunkPos, v))
		{
			bpos = blockPos + vector3di(0, -1, 0);
			cpos = chunkPos;
		}
		if (compare_block_sky(bHighest, blockPos + vector3di(0, 0, 1), chunkPos, v))
		{
			bpos = blockPos + vector3di(0, 0, 1);
			cpos = chunkPos;
		}
		if (compare_block_sky(bHighest, blockPos + vector3di(0, 0, -1), chunkPos, v))
		{
			bpos = blockPos + vector3di(0, 0, -1);
			cpos = chunkPos;
		}

		{
			vector3di c(chunkPos), b(blockPos + vector3di(0, 1, 0));
			PositionCorrect(b, c);
			tb = _GetBlock(b, c);
			if (tb.SkyLight > 0)
				SetSunLight(b, c, tb.SkyLight);
		}

		PositionCorrect(bpos, cpos);
		SetSunLight(bpos, cpos, v, bUpdate);
	};

	auto adjacent_helper = [&](vector3di blockPos, vector3di chunkPos, TerrainChunk* t) {
		PositionCorrect(blockPos, chunkPos);

		TerrainChunk* chunk = NULL;

		TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
		if (it != m_Terrain.end())
			chunk = it->second;
		else
			return;

		if (chunk == t)
			return;

		// push this chunk back in the updates
		if (std::find(updates.begin(), updates.end(), chunk) == updates.end())
			updates.push_back(chunk);
	};

	auto update_adjacent_chunks = [&](vector3di blockPos, vector3di chunkPos, TerrainChunk* t) {
		LockMutex();

		adjacent_helper(blockPos + vector3di(1, 0, 0), chunkPos, t);
		adjacent_helper(blockPos + vector3di(-1, 0, 0), chunkPos, t);
		adjacent_helper(blockPos + vector3di(0, 1, 0), chunkPos, t);
		adjacent_helper(blockPos + vector3di(0, -1, 0), chunkPos, t);
		adjacent_helper(blockPos + vector3di(0, 0, 1), chunkPos, t);
		adjacent_helper(blockPos + vector3di(0, 0, -1), chunkPos, t);

		UnlockMutex();
	};

	while (pos + 22 <= _size)
	{
		// extract the block data
		TerrainBlock b;
		ByteToTerrainBlock(&ubuf[pos], b);

		// extract the position in chunk
		int x = ubuf[pos + 8];
		int y = ubuf[pos + 9];
		int z = ubuf[pos + 10];
		vector3di blockPos(x, y, z);

		// extract the chunk position
		ByteToInt(&ubuf[pos + 11], x);
		ByteToInt(&ubuf[pos + 15], y);
		ByteToInt(&ubuf[pos + 19], z);
		vector3di chunkPos(x, y, z);

		// check if this chunk is loaded in memory or not
		bool bTemp = false;
		TerrainChunk* t = NULL;
		NetworkManager* n = GetSceneManager()->GetRoot()->CastTo<Game>()->GetNetworkManager();

		LockMutex();
		TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
		if (it != m_Terrain.end())
			t = it->second;
		else
		{
			// this chunk isn't currently loaded, ask the network manager if the chunk is in its memory
			vector<unsigned char> data;
			if (n->GetTerrainData(chunkPos, data, false))
			{
				// the chunk is in the network manager's memory
				// we need to create a new temporary chunk, calculate changes made to that chunk and then save the data back to the network manager
				bTemp = true;
				t = new TerrainChunk(GetSceneManager(), this, chunkPos);
				t->UnpackTerrain(&data[0], data.size());
			}
		}
		UnlockMutex();

		// set the block at this position
		if (t)
		{
			// terrain chunk exists, process the block change
			TerrainBlock block = t->GetBlock(blockPos);
			const ItemClass *ic = Item::GetItemClass(b);

			// clear any slopes underneath this block
			//ClearSlopes(_pos - vector3df(0, 10, 0));

			// determine whether we are removing light
			bool _l = !b.Ignored;
			if (ic && ic->LightColour != vector3di(0))
				_l = false;

			// remove block light / sky light
			if (_l || (block.LightRed > 0 || block.LightBlue > 0 || block.LightGreen > 0))
				RemoveBlockLight(blockPos, chunkPos);
			if (!b.Ignored)
				RemoveSunLight(blockPos, chunkPos);

			// set the block and sort the particle emitter
			t->SetBlock(blockPos, b);
			t->SortParticleEmitter(blockPos.x, blockPos.y, blockPos.z, b);

			// update the lighting / set the block light at this block
			if (!ic || ic->LightColour == vector3di(0))
				update_lighting(blockPos, chunkPos);
			else if (ic)
				SetBlockLight(blockPos, chunkPos, ic->LightColour.x, ic->LightColour.y, ic->LightColour.z);
			update_skylighting(blockPos, chunkPos);

			// push this chunk back in the updates
			if (std::find(updates.begin(), updates.end(), t) == updates.end())
				updates.push_back(t);

			// update any adjacent chunks
			update_adjacent_chunks(blockPos, chunkPos, t);
		}

		// if this was a temporary chunk, save the chunk data to the network manager and delete the chunk
		if (bTemp)
		{
			unsigned char* tmp;
			uLongf len;
			if (t->CompressTerrain(&tmp, len))
				n->SaveTerrainData(chunkPos, &tmp, len);

			delete t;
		}

		// continue to the next block
		pos += 23;
	}

	// now update all the terrain chunks affected by this update
	for (GLuint i = 0; i < updates.size(); ++i) {
		updates[i]->ClearMesh();
	}

	// clean up
	delete[] ubuf;
}

void Terrain::ClearSlopes(vector3df pos)
{
	// make sure that a chunk exists in this location
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		TerrainBlock block = pChunk->GetBlock(blockPos);
		block.XX = 0; block.XY = 0; block.YX = 0; block.YY = 0;

		pChunk->SetBlock(blockPos, block);
	}
}

TerrainBlock Terrain::GetBlock(vector3df pos)
{
	// make sure that a chunk exists in this location
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		// the chunk exists
		int x = blockPos.x;
		int y = blockPos.y;
		int z = blockPos.z;

		return pChunk->GetBlock(x, y, z);
	}

	return TerrainBlock(0);
}

GLfloat Terrain::GetWater(vector3df pos)
{
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		return pChunk->GetWater(blockPos);
	}

	return 0.f;
}

bool Terrain::Underwater(vector3df pos)
{
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		if (pChunk->GetBlock(blockPos).Water)
		{
			GLfloat level = pChunk->GetWater(blockPos);
			GLfloat depth = (chunkPos.y * CHUNK_SIZE + blockPos.y + level) * BLOCK_SIZE;

			return pos.y < depth;
		}
	}

	return false;
}

void Terrain::_DEBUG_SetBlock(vector3df pos, TerrainBlock block)
{
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		pChunk->SetBlock(blockPos, block);
	}
}

void Terrain::_DEBUG_SetWater(vector3df pos, GLfloat water)
{
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		pChunk->AddWater(blockPos, water);
	}
}

TerrainBlock Terrain::GetBlockBelow(vector3df pos, GLfloat& height, bool bWater)
{
	// make sure that a chunk exists in this location
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		// the chunk exists
		int x = blockPos.x;
		int y = blockPos.y;
		int z = blockPos.z;

		height = (int)(pos.y * ONE_OVER_BLOCK_SIZE);
		if (pos.y < 0) height--;
		while (true)
		{
			for (; y >= 0; y--, height--) {
				TerrainBlock block = pChunk->GetBlock(x, y, z);
				if (block.Solid || (bWater && block.Water > 0))
				{
					height = (height + 1) * BLOCK_SIZE;
					return pChunk->GetBlock(x, y, z);
				}
			}

			y += CHUNK_SIZE;
			chunkPos.y--;

			LockMutex();
			TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
			if (it == m_Terrain.end())
			{
				UnlockMutex();
				return 0;
			}
			UnlockMutex();
			
			pChunk = it->second;

			if (!pChunk)
				return 0;
		}

		return pChunk->GetBlock(x, y, z);
	}

	return 0;
}

TerrainBlock Terrain::_GetBlock(int x, int y, int z, const vector3di& chunkPos)
{
	// make sure that a chunk exists in this location
	LockMutex();
	TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
	if (it != m_Terrain.end())
	{
		TerrainChunk *pChunk = it->second;
		UnlockMutex();

		if (pChunk)
		{
			if (!pChunk->IsInitialised() && chunkPos.y < 0)
			{
				TerrainBlock block;
				block.Ignored = 0;
				return block;
			}

			return pChunk->GetBlock(x, y, z);
		}
	}
	UnlockMutex();

	// the chunk doesn't exist, just return air with sun light = 15
	static TerrainBlock block(0, 0, 0, 0, 0, chunkPos.y >= 0, 0, 0, 0, 0, 0, 0, 0, 15);
	return block;
}

GLfloat Terrain::_GetWater(int x, int y, int z, const vector3di& chunkPos)
{
	// make sure that a chunk exists in this location
	LockMutex();
	TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
	if (it != m_Terrain.end())
	{
		TerrainChunk *pChunk = it->second;
		UnlockMutex();

		if (pChunk)
		{
			if (!pChunk->IsInitialised() && chunkPos.y < 0)
			{
				return 0.f;
			}

			return pChunk->GetWater(vector3di(x, y, z));
		}
	}
	UnlockMutex();

	// just return 0 otherwise
	return 0.f;
}

TerrainBlock Terrain::_GetBlock(const vector3di& blockPos, const vector3di& chunkPos)
{
	// get the block at this position
	TerrainChunk* pChunk = GetChunk(chunkPos);
	if (pChunk)
		return pChunk->GetBlock(blockPos);

	// there is no chunk at this position, return air with sun light = 15
	static TerrainBlock block(0, 0, 0, 0, 0, chunkPos.y >= 0, 0, 0, 0, 0, 0, 0, 0, 15);
	return block;
}

TerrainBlock Terrain::_GetBlock(int x, int y, int z)
{
	// figure out which chunk we are in
	vector3di chunkPos((GLint)(x * ONE_OVER_CHUNK_SIZE), (GLint)(y * ONE_OVER_CHUNK_SIZE), (GLint)(z * ONE_OVER_CHUNK_SIZE));
	if (x < 0) chunkPos.x -= 1;
	if (y < 0) chunkPos.y -= 1;
	if (z < 0) chunkPos.z -= 1;

	// make sure that a chunk exists in this location
	LockMutex();
	TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
	if (it != m_Terrain.end())
	{
		TerrainChunk *pChunk = it->second;
		UnlockMutex();

		if (pChunk)
		{
			// the chunk exists, let's get the next block below the current position
			int X = x - chunkPos.x * CHUNK_SIZE;
			int Y = y - chunkPos.y * CHUNK_SIZE;
			int Z = z - chunkPos.z * CHUNK_SIZE;

			// deal with negative chunks
			if (x < 0) X -= 1;
			if (y < 0) Y -= 1;
			if (z < 0) Z -= 1;

			// get the block
			return pChunk->GetBlock(X, Y, Z);
		}
	}
	UnlockMutex();

	// the chunk doesn't exist, just return air with sun light = 15
	static TerrainBlock block(0, 0, 0, 0, 0, chunkPos.y >= 0, 0, 0, 0, 0, 0, 0, 0, 15);
	return block;
}

TerrainBlock& Terrain::_BlockAt(const vector3di& blockPos, const vector3di& chunkPos)
{
	// get the block at this position
	TerrainChunk* pChunk = GetChunk(chunkPos);
	if (pChunk)
		return pChunk->BlockAt(blockPos);

	// there is no chunk at this position, return air with sun light = 15
	static TerrainBlock block(0, 0, 0, 0, 0, chunkPos.y >= 0, 0, 0, 0, 0, 0, 0, 0, 15);
	return block;
}

TerrainBlock* Terrain::_BlockAt(vector3di& blockPos, vector3di& chunkPos, TerrainChunk** out, bool bLockMutex)
{
	// correct the position
	PositionCorrect(blockPos, chunkPos);
	
	// get the block at this position
	*out = GetChunk(chunkPos, bLockMutex);
	if (*out)
		return &(*out)->BlockAt(blockPos);

	return NULL;
}

void Terrain::Interact(unsigned char* buf)
{
	// we have received a block interact packet from the server, so animate a block
	// we can ignore the id byte as that's used for the server to determine locks on blocks by players (ie to figure out who's used a block)
	vector3di blockPos, chunkPos;

	// load block position in chunk
	blockPos.x = buf[1];
	blockPos.y = buf[2];
	blockPos.z = buf[3];

	// load chunk position in world
	ByteToInt(&buf[4], chunkPos.x);
	ByteToInt(&buf[8], chunkPos.y);
	ByteToInt(&buf[12], chunkPos.z);

	// load open/close switch
	bool open = buf[16] > 0;

	// check this chunk exists
	LockMutex();
	TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
	if (it != m_Terrain.end())
	{
		// the chunk exists, tell it to perform an interaction
		UnlockMutex();
		it->second->Interact(blockPos, open);

		// if we are opening a door, set the solid bit to false
		const ItemClass* ic = Item::GetItemClass(m_Terrain.at(chunkPos)->GetBlock(blockPos));
		if (ic)
		{
			if (ic->sValues[4] == 0)
			{
				// this block doesn't have an inventory so (for now) assume it is a door
				if (ic->BlockSize != vector3di(1))
				{
					for (int x = 0; x < ic->BlockSize.x; ++x) {
						for (int y = 0; y < ic->BlockSize.y; ++y) {
							for (int z = 0; z < ic->BlockSize.z; ++z) {
								_BlockAt(blockPos + vector3di(x, y, z), chunkPos).Solid = !open;
							}
						}
					}
				}
				else
				{
					LockMutex();
					m_Terrain.at(chunkPos)->BlockAt(blockPos).Solid = !open;
					UnlockMutex();
				}
			}
		}
	}
}

void Terrain::SendInteraction(vector3df pos, bool open)
{
	// check we're connected to a server / network manager is running
	NetworkManager* n = GetSceneManager()->GetRoot()->CastTo<Game>()->GetNetworkManager();
	if (!n)
		return;
	
	// send a block interaction packet to the server
	TerrainChunk *pChunk;
	vector3di blockPos, chunkPos;
	if (GetPositionInChunk(pos, pChunk, blockPos, chunkPos))
	{
		// position is valid, create the buffer
		unsigned char buf[17];
		buf[0] = n->GetClientID();
		
		// place the block position in the chunk
		unsigned char _x = (unsigned char)blockPos.x;
		unsigned char _y = (unsigned char)blockPos.y;
		unsigned char _z = (unsigned char)blockPos.z;
		buf[1] = _x;
		buf[2] = _y;
		buf[3] = _z;

		// place the chunk position in the buffer
		IntToByte(chunkPos.x, &buf[4]);
		IntToByte(chunkPos.y, &buf[8]);
		IntToByte(chunkPos.z, &buf[12]);

		// place whether we are opening/closing the interactable
		buf[16] = (open ? 1 : 0);

		// send the packet
		n->SendPacket(block_interaction, buf, 17, true);
	}
}

GLuint Terrain::GetSeed() const
{
	return m_TerrainSeed;
}

void TerrainIndices(TerrainChunk* chunk, GLint dir)
{
	GLubyte d = 0;

	if (dir == 360 || dir == 0)
		d = 1;
	else if (dir == 90)
		d = 2;
	else if (dir == 180)
		d = 3;

	chunk->SortIndices(d);
}

void Terrain::UpdateTerrainIndices(GLint dir)
{
	
}

void Terrain::UpdateChunksAroundChunk(const vector3di& chunkPos)
{
	// update the chunks adjacent to the given chunk - this is to correct artifacts in the terrain
	auto update = [&](const vector3di& pos) {
		TerrainMap::const_iterator it = m_Terrain.find(pos);
		if (it != m_Terrain.end() && it->second)
			it->second->ClearMesh();
	};

	LockMutex();
	update(vector3di(chunkPos) + vector3di(-1, 0, 0));
	update(vector3di(chunkPos) + vector3di(1, 0, 0));
	update(vector3di(chunkPos) + vector3di(0, 1, 0));
	update(vector3di(chunkPos) + vector3di(0, -1, 0));
	update(vector3di(chunkPos) + vector3di(0, 0, 1));
	update(vector3di(chunkPos) + vector3di(0, 0, -1));
	UnlockMutex();
}

void CreateChunk(Terrain* t, TerrainChunk* chunk)
{
	if (!chunk->IsInitialised())
	{
		vector2di pos(chunk->GetChunkPos().x, chunk->GetChunkPos().z);
		//chunk->GenerateTerrain(t->GetNoise(pos));
	}
	
	chunk->CreateMesh();
}

void UpdateChunk(TerrainChunk* chunk)
{
	// wait a bit
	//for (GLuint i = 0; i < 1000; i++) {}

	chunk->CreateMesh();
}

void Terrain::Update(bool bForce)
{
	if (!m_pPlayer)
		m_pPlayer = GetSceneManager()->GetFirstInstance<Player>();
	
	// create new chunks and hide chunks as the camera moves
	m_Chunks.clear();
	GetChunksAroundPlayer(m_Chunks, true);
	m_ChunksCount = m_Chunks.size();
	vector<vector3di> updates;

	for (GLuint i = 0; i < m_ChunksCount; ++i) {
		TerrainChunk* chunk = m_Chunks[i];
		if (chunk->IsCreated() && !chunk->IsCreating())
		{
			//chunks[i]->CalculateInView();

			//if (chunks[i]->InViewFrustum())
			chunk->Update(bForce);
		}
	}

	// delete any chunks not on screen - once saving is implemented, this will write the chunk to disk too
	if (!m_ChunksToDelete.empty())
	{
		LockMutex();

		for (GLuint i = 0; i < m_ChunksToDelete.size(); ++i) {
			delete m_ChunksToDelete.at(i);
		}

		m_ChunksToDelete.clear();
		UnlockMutex();
	}
}

void Terrain::Render()
{
	// render the chunks close to the camera
	bool bCreate = true;
	for (GLuint i = 0; i < m_ChunksCount; ++i) {
		TerrainChunk* chunk = m_Chunks[i];
		if (chunk)
		{
			chunk->Render(bCreate);
		}
	}
}

void Terrain::RenderShadows()
{
	// render the chunks close to the camera
	GLfloat shadowDist = GetSceneManager()->GetShadowDistance() + 160.f;
	vector3df pos = GetSceneManager()->GetActiveCamera()->GetPosition();
	pos.y = 0.f;

	for (GLuint i = 0; i < m_ChunksCount; ++i) {
		TerrainChunk* chunk = m_Chunks[i];
		vector3df _pos(chunk->GetPosition());
		_pos.y = 0.f;

		if (chunk && pos.Distance(_pos) <= shadowDist)
		{
			chunk->RenderShadows();
		}
	}
}

void Terrain::RenderWater(GLObject* o)
{
	Terrain* t = dynamic_cast<Terrain*>(o);

	if (t)
	{
		for (GLuint i = 0; i < t->m_ChunksCount; ++i) {
			TerrainChunk* chunk = t->m_Chunks[i];
			if (chunk)
			{
				chunk->RenderWater();
			}
		}
	}
}

void Terrain::DoChunk(const vector3di& c)
{
	TerrainChunk* t = m_Terrain[c];
	if (t)
		t->ClearMesh();
}

void Terrain::UpdateChunks(vector3di& chunk, int x, int y, int z)
{
	// x
	if (x == 0)
	{
		DoChunk(chunk + vector3di(-1, 0, 0));

		if (y == 0)
			DoChunk(chunk + vector3di(-1, -1, 0));
		else if (y == CHUNK_SIZE - 1)
			DoChunk(chunk + vector3di(-1, 1, 0));

		if (z == 0)
			DoChunk(chunk + vector3di(-1, 0, -1));
		else if (z == CHUNK_SIZE - 1)
			DoChunk(chunk + vector3di(-1, 0, 1));
	}
	else if (x == CHUNK_SIZE - 1)
	{
		DoChunk(chunk + vector3di(1, 0, 0));

		if (y == 0)
			DoChunk(chunk + vector3di(1, -1, 0));
		else if (y == CHUNK_SIZE - 1)
			DoChunk(chunk + vector3di(1, 1, 0));

		if (z == 0)
			DoChunk(chunk + vector3di(1, 0, -1));
		else if (z == CHUNK_SIZE - 1)
			DoChunk(chunk + vector3di(1, 0, 1));
	}

	// y
	if (y == 0)
	{
		DoChunk(chunk + vector3di(0, -1, 0));

		if (z == 0)
			DoChunk(chunk + vector3di(0, -1, -1));
		else if (z == CHUNK_SIZE - 1)
			DoChunk(chunk + vector3di(0, -1, 1));
	}
	else if (y == CHUNK_SIZE - 1)
	{
		DoChunk(chunk + vector3di(0, 1, 0));

		if (z == 0)
			DoChunk(chunk + vector3di(0, 1, -1));
		else if (z == CHUNK_SIZE - 1)
			DoChunk(chunk + vector3di(0, 1, 1));
	}

	// z
	if (z == 0)
	{
		DoChunk(chunk + vector3di(0, 0, -1));
	}
	else if (z == CHUNK_SIZE - 1)
	{
		DoChunk(chunk + vector3di(0, 0, 1));
	}
}

void Terrain::GetChunksAroundPlayer(vector<TerrainChunk*>& v, bool bCreateNew, bool bForRender)
{
	vector3df cameraPos = m_pPlayer->GetPosition();
	vector3di startPos((GLint)(cameraPos.x * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(cameraPos.y * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(cameraPos.z * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE));
	if (cameraPos.x < 0) startPos.x -= 1;
	if (cameraPos.y < 0) startPos.y -= 1;
	if (cameraPos.z < 0) startPos.z -= 1;
	
	auto ChunkCreation = [&](vector3di pos) {
		TerrainMap::const_iterator it = m_Terrain.find(pos);
		if (bCreateNew && it == m_Terrain.end())
		{
			m_Terrain.emplace(pos, new TerrainChunk(GetManager(), this, pos));
			it = m_Terrain.find(pos);
		}

		if (it == m_Terrain.end())
		{
			std::cout << "Terrain::GetChunksAroundPlayer::ChunkCreation iterator is not valid" << std::endl;
			return;
		}

		TerrainChunk* chunk = it->second;
		if (chunk)
		{
			// determine if we want to add this chunk
			chunk->CalculateInView();

			// if we are below 0, check if we can be seen
			bool bAdd(true);
			vector3di _where = pos + vector3di(0, 1, 0);
			if (pos.y < min(startPos.y, 0))
			{
				TerrainChunk* _chunk = m_Terrain[_where];
				if (_chunk)
				{
					if (chunk->GetChunkPos().y < min(startPos.y - 1, 0))
						bAdd = _chunk->CanFaceSeeFace(5, 4);
					else
						bAdd = _chunk->CanSeeFace(4);
				}
				else
					m_Terrain.emplace(_where, new TerrainChunk(GetManager(), this, _where));
			}
			
			if (bAdd && chunk->InViewFrustum())
				v.push_back(chunk);
		}
	};

	// change chunks based off of camera rotation - this order affects how the blending looks
	LockMutex();
	for (int k = 0; k < 4; ++k) {
		ChunkCreation(vector3di(startPos.x, startPos.y + k, startPos.z));

		if (k > 0)
		{
			ChunkCreation(vector3di(startPos.x, startPos.y - k, startPos.z));
		}
	}

	for (int i = 1; i < m_ChunkDistance; ++i) {
		int num = i * 2 + 1;
		int start = num / 2;
		for (int j = 0; j < num; ++j) {
			for (int k = 0; k < 4; ++k) {
				ChunkCreation(vector3di(startPos.x - start + j, startPos.y + k, startPos.z - start));
				ChunkCreation(vector3di(startPos.x - start + j, startPos.y + k, startPos.z + start));

				if (j > 1)
				{
					ChunkCreation(vector3di(startPos.x - start, startPos.y + k, startPos.z - start + j - 1));
					ChunkCreation(vector3di(startPos.x + start, startPos.y + k, startPos.z - start + j - 1));
				}

				if (k > 0)
				{
					ChunkCreation(vector3di(startPos.x - start + j, startPos.y - k, startPos.z - start));
					ChunkCreation(vector3di(startPos.x - start + j, startPos.y - k, startPos.z + start));

					if (j > 1)
					{
						ChunkCreation(vector3di(startPos.x - start, startPos.y - k, startPos.z - start + j - 1));
						ChunkCreation(vector3di(startPos.x + start, startPos.y - k, startPos.z - start + j - 1));
					}
				}
			}
		}
	}
	UnlockMutex();
}

void Terrain::GetChunksNotAroundPlayer(vector<TerrainChunk*>& v)
{
	// loop through all chunks and see which aren't in the comp vector (usually the chunks around the player)
	vector3df cameraPos = m_pPlayer->GetPosition();
	vector3di startPos((GLint)(cameraPos.x * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(cameraPos.y * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(cameraPos.z * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE));
	if (cameraPos.x < 0) startPos.x -= 1;
	if (cameraPos.y < 0) startPos.y -= 1;
	if (cameraPos.z < 0) startPos.z -= 1;

	LockMutex();
	TerrainMap::iterator it = m_Terrain.begin();
	while (it != m_Terrain.end())
	{
		if (abs(it->first.x - startPos.x) > m_ChunkDistance || abs(it->first.y - startPos.y) > 4 || abs(it->first.z - startPos.z) > m_ChunkDistance)
			v.push_back(it->second);

		++it;
	}
	UnlockMutex();
}

std::list<vector3di>* Terrain::GetMultiBlockList(vector3di block, vector3di chunk, TerrainChunk*& pChunk)
{
	// we need to find the list with the given block position in
	std::list<vector3di>* list = m_Terrain[chunk]->GetMultiBlockList(block);
	pChunk = m_Terrain[chunk];

	// if this block's list is actually in another chunk (it will be in an adjacent chunk in the negative direction) then find it
	if (list == NULL)
	{
		vector3di b = block + vector3di(CHUNK_SIZE, 0, 0);
		vector3di p = chunk + vector3di(-1, 0, 0);
		pChunk = m_Terrain[p];

		if (pChunk)
			list = pChunk->GetMultiBlockList(b);

		if (!list)
		{
			b = block + vector3di(0, 0, CHUNK_SIZE);
			p = chunk + vector3di(0, 0, -1);
			pChunk = m_Terrain[p];

			if (pChunk)
				list = pChunk->GetMultiBlockList(b);

			if (!list)
			{
				b = block + vector3di(0, CHUNK_SIZE, 0);
				p = chunk + vector3di(0, -1, 0);
				pChunk = m_Terrain[p];

				if (pChunk)
					list = pChunk->GetMultiBlockList(b);

				if (!list)
				{
					b = block + vector3di(CHUNK_SIZE, 0, CHUNK_SIZE);
					p = chunk + vector3di(-1, 0, -1);
					pChunk = m_Terrain[p];

					if (pChunk)
						list = pChunk->GetMultiBlockList(b);

					if (!list)
					{
						b = block + vector3di(CHUNK_SIZE, CHUNK_SIZE, CHUNK_SIZE);
						p = chunk + vector3di(-1, -1, -1);
						pChunk = m_Terrain[p];

						if (pChunk)
							list = pChunk->GetMultiBlockList(b);

						if (!list)
						{
							b = block + vector3di(CHUNK_SIZE, CHUNK_SIZE, 0);
							p = chunk + vector3di(-1, -1, 0);
							pChunk = m_Terrain[p];

							if (pChunk)
								list = pChunk->GetMultiBlockList(b);

							if (!list)
							{
								b = block + vector3di(0, CHUNK_SIZE, CHUNK_SIZE);
								p = chunk + vector3di(0, -1, -1);
								pChunk = m_Terrain[p];

								if (pChunk)
									list = pChunk->GetMultiBlockList(b);

								// ok that should cover all the possibilities so if we haven't found it it doesn't exist
							}
						}
					}
				}
			}
		}
	}

	return list;
}

bool Terrain::GetPositionInChunk(vector3df& pos, TerrainChunk*& chunk, vector3di& out, vector3di& chunkPos)
{
	// figure out which chunk we are in
	chunkPos.Set((GLint)(pos.x * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(pos.y * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(pos.z * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE));
	if (pos.x < 0) chunkPos.x -= 1;
	if (pos.y < 0) chunkPos.y -= 1;
	if (pos.z < 0) chunkPos.z -= 1;

	// make sure that a chunk exists in this location
	LockMutex();
	TerrainMap::const_iterator it = m_Terrain.find(chunkPos);
	if (it != m_Terrain.end())
	{
		chunk = it->second;
		UnlockMutex();

		/*if (!pChunk)
		{
		pChunk = new TerrainChunkSceneNode(m_pSceneMan->getRootSceneNode(), m_pSceneMan, this, chunkPos);
		m_World[chunkPos] = pChunk;
		}*/

		if (chunk)
		{
			// the chunk exists, let's get the next block below the current position
			int x = (int)(pos.x * ONE_OVER_BLOCK_SIZE) - chunkPos.x * CHUNK_SIZE;
			int y = (int)(pos.y * ONE_OVER_BLOCK_SIZE) - chunkPos.y * CHUNK_SIZE;
			int z = (int)(pos.z * ONE_OVER_BLOCK_SIZE) - chunkPos.z * CHUNK_SIZE;

			// deal with negative chunks
			if (pos.x < 0) x -= 1;
			if (pos.y < 0) y -= 1;
			if (pos.z < 0) z -= 1;

			// output
			out.Set(x, y, z);
			return true;
		}
	}
	UnlockMutex();

	return false;
}

TerrainChunk* Terrain::GetChunk(const vector3di& pos, bool bLockMutex)
{
	// see if a chunk exists at this position
	if (bLockMutex)
		LockMutex();

	TerrainMap::const_iterator it = m_Terrain.find(pos);
	if (it != m_Terrain.end())
	{
		if (bLockMutex)
			UnlockMutex();

		return it->second;
	}

	if (bLockMutex)
		UnlockMutex();

	return NULL;
}

void Terrain::PositionCorrect(vector3di& blockPos, vector3di& chunkPos)
{
	// function to correct position errors
	auto position_correct = [](int& in1, int& in2) {
		if (in1 < 0)
		{
			in1 += CHUNK_SIZE;
			--in2;
		}
		else if (in1 >= CHUNK_SIZE)
		{
			in1 -= CHUNK_SIZE;
			++in2;
		}
	};

	// correct block / chunk positions
	position_correct(blockPos.x, chunkPos.x);
	position_correct(blockPos.y, chunkPos.y);
	position_correct(blockPos.z, chunkPos.z);
}

void Terrain::ConvertToChunkBlockPosition(const vector3df& pos, vector3di& blockPos, vector3di& chunkPos) const
{
	// chunk position
	chunkPos.Set((GLint)(pos.x * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(pos.y * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE),
		(GLint)(pos.z * ONE_OVER_BLOCK_SIZE * ONE_OVER_CHUNK_SIZE));
	if (pos.x < 0) chunkPos.x -= 1;
	if (pos.y < 0) chunkPos.y -= 1;
	if (pos.z < 0) chunkPos.z -= 1;

	// block position
	int x = (int)(pos.x * ONE_OVER_BLOCK_SIZE) - chunkPos.x * CHUNK_SIZE;
	int y = (int)(pos.y * ONE_OVER_BLOCK_SIZE) - chunkPos.y * CHUNK_SIZE;
	int z = (int)(pos.z * ONE_OVER_BLOCK_SIZE) - chunkPos.z * CHUNK_SIZE;

	// deal with negative chunks
	if (pos.x < 0) x -= 1;
	if (pos.y < 0) y -= 1;
	if (pos.z < 0) z -= 1;

	// output
	blockPos.Set(x, y, z);
}

void Terrain::ManageUnseenChunks()
{
	// delete any chunks not on screen - once saving is implemented, this will write the chunk to disk too
	if (!GetSceneManager() || !GetSceneManager()->GetRoot() || !m_bThreadRunning)
		return;

	vector<TerrainChunk*> todelete;
	vector<TerrainChunk*> chunks;
	GetChunksNotAroundPlayer(todelete);
	NetworkManager* n = GetSceneManager()->GetRoot()->CastTo<Game>()->GetNetworkManager();
	for (GLuint i = 0; i < todelete.size() && m_bThreadRunning; ++i) {
		if (m_pChunkToUpdate == todelete[i])
			continue;

		// we don't want to delete this if we are in the middle of creating it
		if (todelete[i]->IsCreating() || !todelete[i]->IsCreated())
			continue;

		// first save the chunk's data to the network manager
		unsigned char* tmp;
		uLongf len;
		if (todelete[i]->CompressTerrain(&tmp, len))
			n->SaveTerrainData(todelete[i]->GetChunkPos(), &tmp, len);
		if (todelete[i]->CompressWater(&tmp, len))
			n->SaveWaterData(todelete[i]->GetChunkPos(), &tmp, len);

		// then we can delete the chunk
		LockMutex();
		m_Terrain.erase(todelete[i]->GetChunkPos());
		m_ChunksToDelete.push_back(todelete[i]);
		UnlockMutex();
		//delete todelete[i];
	}
}

void Terrain::UpdateTerrainWater(const vector<TerrainChunk*>& chunks)
{
	const size_t num = chunks.size();
	for (GLuint i = 0; i < num && m_bThreadRunning; ++i) {
		chunks[i]->UpdateWater();
	}
}

void Terrain::SetBlockLight(const vector3di& blockPos, const vector3di& chunkPos, GLubyte red, GLuint green, GLuint blue, bool bUpdate)
{
	// set the block light at this position and push back a new light creation node
	_SetBlockLight(blockPos, chunkPos, red, green, blue);
	m_BlockLightCreation.emplace(blockPos, chunkPos, red, green, blue);

	// update light creation
	UpdateLightCreation(bUpdate);
}

void Terrain::RemoveBlockLight(const vector3di& blockPos, const vector3di& chunkPos, bool bUpdate)
{
	// get the block here already
	TerrainBlock b = _GetBlock(blockPos, chunkPos);
	m_BlockLightDeletion.emplace(blockPos, chunkPos, (int)b.LightRed, (int)b.LightGreen, (int)b.LightBlue);

	// set the block light here
	_SetBlockLight(blockPos, chunkPos, 0, 0, 0);

	// update light deletion
	UpdateLightDeletion(bUpdate);
	//UpdateLightCreation(m_BlockLightCreation, a);
}

void Terrain::SetSunLight(const vector3di& blockPos, const vector3di& chunkPos, GLubyte val, bool bUpdate)
{
	// set the sky light at this position and push back a new light creation node
	_SetSunLight(blockPos, chunkPos, val);
	m_SunLightCreation.emplace(blockPos, chunkPos, val);

	// update light creation
	UpdateSunlightCreation(bUpdate);
}

void Terrain::RemoveSunLight(const vector3di& blockPos, const vector3di& chunkPos, bool bUpdate)
{
	// get the block already here
	TerrainBlock b = _GetBlock(blockPos, chunkPos);
	m_SunLightDeletion.emplace(blockPos, chunkPos, (int)b.SkyLight);

	// set the sky light here
	_SetSunLight(blockPos, chunkPos, 0);

	// update light deletion
	UpdateSunlightDeletion(bUpdate);
}

void Terrain::DoSunlightUpdate()
{
	m_bDoSunlightUpdate = true;
}

TerrainChunk* Terrain::_SetBlockLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint red, GLuint green, GLuint blue)
{
	// set the block light at the given position
	TerrainChunk *pChunk = GetChunk(chunkPos);
	if (pChunk)
		pChunk->SetBlockLight(blockPos, red, green, blue);

	return pChunk;
}

void Terrain::UpdateLightCreation(bool bUpdate)
{
	// update light creation
	vector3di chunkPos, blockPos;
	GLuint r, g, b;
	vector<TerrainChunk*> a;

	// function to compare light levels
	auto light_compare = [](GLuint v, GLuint c) -> bool {
		return v + 2 <= c;
	};

	// function to add the chunk to the update list
	auto add_chunk = [&](vector3di& chunkPos) {
		TerrainMap::iterator it = m_Terrain.find(chunkPos);
		if (it == m_Terrain.end())
			return;

		TerrainChunk* t = it->second;
		if (t && std::find(a.begin(), a.end(), t) == a.end())
			a.push_back(t);
	};

	// function to update adjacent chunk
	auto update_adjacent = [&](vector3di& blockPos, vector3di chunkPos) {
		if (blockPos.x == 0)
		{
			--chunkPos.x;
			add_chunk(chunkPos);
		}
		else if (blockPos.x == CHUNK_SIZE - 1)
		{
			++chunkPos.x;
			add_chunk(chunkPos);
		}

		if (blockPos.y == 0)
		{
			--chunkPos.y;
			add_chunk(chunkPos);
		}
		else if (blockPos.y == CHUNK_SIZE - 1)
		{
			++chunkPos.y;
			add_chunk(chunkPos);
		}

		if (blockPos.z == 0)
		{
			--chunkPos.z;
			add_chunk(chunkPos);
		}
		else if (blockPos.z == CHUNK_SIZE - 1)
		{
			++chunkPos.z;
			add_chunk(chunkPos);
		}
	};

	// function to check a light value at a given point
	auto check_light = [&](vector3di chunkPos, vector3di blockPos) {
		// if this block position is out of this chunk, correct it
		PositionCorrect(blockPos, chunkPos);

		// get the block at this position
		TerrainBlock tb = _GetBlock(blockPos, chunkPos);

		// compare lighting values
		bool _r = light_compare(tb.LightRed, r);
		bool _g = light_compare(tb.LightGreen, g);
		bool _b = light_compare(tb.LightBlue, b);

		// if a lighting check passed, set block lights and push back a new node
		if ((!tb.Solid || tb.Ignored || (tb.XX || tb.XY || tb.YX || tb.YY)) && (_r || _g || _b))
		{
			// set the block light
			TerrainChunk* t = _SetBlockLight(blockPos, chunkPos, _r ? r - 1 : tb.LightRed, _g ? g - 1 : tb.LightGreen, _b ? b - 1 : tb.LightBlue);

			if (t)
			{
				// for updates, check if the chunk is in there already and if not push it back
				if (std::find(a.begin(), a.end(), t) == a.end())
					a.push_back(t);

				// if we're on the edge of the chunk border we'll want to update the adjacent chunk
				update_adjacent(blockPos, chunkPos);

				// create a new light creation node
				m_BlockLightCreation.emplace(blockPos, chunkPos, _r ? r - 1 : tb.LightRed, _g ? g - 1 : tb.LightGreen, _b ? b - 1 : tb.LightBlue);
			}
		}
	};

	// loop through the creation list
	while (!m_BlockLightCreation.empty())
	{
		// get the light node details at this point
		LightNode& light = m_BlockLightCreation.front();
		chunkPos = light.chunkPos;
		blockPos = light.blockPos;
		r = light.red;
		g = light.green;
		b = light.blue;
		m_BlockLightCreation.pop();

		// if for whatever reason there's a solid block here, continue
		TerrainBlock block = _GetBlock(blockPos, chunkPos);
		if (block.Solid && !block.Ignored && !(block.XX || block.XY || block.YX || block.YY))
			continue;

		// check lights adjacent to this block
		check_light(chunkPos, blockPos + vector3di(-1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(0, -1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 0, -1));
		check_light(chunkPos, blockPos + vector3di(0, 0, 1));
	}

	// if we are updating, clear the meshes of the chunks affected
	if (bUpdate)
	{
		for (GLuint i = 0; i < a.size(); ++i) {
			if (a[i])
				a[i]->ClearMesh();
		}
	}
}

void Terrain::UpdateLightDeletion(bool bUpdate)
{
	// update light deletion
	vector3di chunkPos, blockPos;
	GLuint r, g, b;
	vector<TerrainChunk*> a;

	// function to compare light levels
	auto light_compare = [](GLuint v, GLuint c) -> bool {
		return v > 0 && v < c;
	};

	// function to add the chunk to the update list
	auto add_chunk = [&](vector3di& chunkPos) {
		TerrainMap::iterator it = m_Terrain.find(chunkPos);
		if (it == m_Terrain.end())
			return;

		TerrainChunk* t = it->second;
		if (t && std::find(a.begin(), a.end(), t) == a.end())
			a.push_back(t);
	};

	// function to update adjacent chunk
	auto update_adjacent = [&](vector3di& blockPos, vector3di chunkPos) {
		if (blockPos.x == 0)
		{
			--chunkPos.x;
			add_chunk(chunkPos);
		}
		else if (blockPos.x == CHUNK_SIZE - 1)
		{
			++chunkPos.x;
			add_chunk(chunkPos);
		}

		if (blockPos.y == 0)
		{
			--chunkPos.y;
			add_chunk(chunkPos);
		}
		else if (blockPos.y == CHUNK_SIZE - 1)
		{
			++chunkPos.y;
			add_chunk(chunkPos);
		}

		if (blockPos.z == 0)
		{
			--chunkPos.z;
			add_chunk(chunkPos);
		}
		else if (blockPos.z == CHUNK_SIZE - 1)
		{
			++chunkPos.z;
			add_chunk(chunkPos);
		}
	};

	// function to check a light value at a given point
	auto check_light = [&](vector3di chunkPos, vector3di blockPos) {
		// if this block position is out of this chunk, correct it
		PositionCorrect(blockPos, chunkPos);

		// get the block at this position
		TerrainBlock tb = _GetBlock(blockPos, chunkPos);

		// compare lighting values
		bool _r = light_compare(tb.LightRed, r);
		bool _g = light_compare(tb.LightGreen, g);
		bool _b = light_compare(tb.LightBlue, b);

		// if a lighting check passed, set block lights and push back a new node
		if (_r || _g || _b)
		{
			// set the block light
			TerrainChunk* t = _SetBlockLight(blockPos, chunkPos, _r ? 0 : tb.LightRed, _g ? 0 : tb.LightGreen, _b ? 0 : tb.LightBlue);

			if (t)
			{
				// for updates, check if the chunk is in there already and if not push it back
				if (std::find(a.begin(), a.end(), t) == a.end())
					a.push_back(t);

				// if we're on the edge of the chunk border we'll want to update the adjacent chunk
				update_adjacent(blockPos, chunkPos);

				// create a new light deletion node
				m_BlockLightDeletion.emplace(blockPos, chunkPos, (int)tb.LightRed, (int)tb.LightGreen, (int)tb.LightBlue);
			}
		}
		else if (m_Terrain.find(chunkPos) != m_Terrain.end())
			m_BlockLightCreation.emplace(blockPos, chunkPos, (int)tb.LightRed, (int)tb.LightGreen, (int)tb.LightBlue);
	};

	// loop through the deletion list
	while (!m_BlockLightDeletion.empty())
	{
		// get the light node details at this point
		LightNode& light = m_BlockLightDeletion.front();
		chunkPos = light.chunkPos;
		blockPos = light.blockPos;
		r = light.red;
		g = light.green;
		b = light.blue;
		m_BlockLightDeletion.pop();

		// check lights adjacent to this block
		check_light(chunkPos, blockPos + vector3di(-1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(0, -1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 0, -1));
		check_light(chunkPos, blockPos + vector3di(0, 0, 1));
	}

	// if we are updating, clear the meshes of the chunks affected
	if (bUpdate/* && m_BlockLightCreation.empty()*/)
	{
		for (GLuint i = 0; i < a.size(); ++i) {
			if (a[i])
				a[i]->ClearMesh();
		}
	}
}

TerrainChunk* Terrain::_SetSunLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint val)
{
	// set the sky/sun light at the given position
	TerrainChunk *pChunk = GetChunk(chunkPos);
	if (pChunk)
		pChunk->SetSunLight(blockPos, val);

	return pChunk;
}

void Terrain::UpdateSunlightCreation(bool bUpdate)
{
	// update light creation
	vector3di chunkPos, blockPos;
	GLuint val;
	vector<TerrainChunk*> a;

	// function to compare light levels
	auto light_compare = [](GLuint v, GLuint c, bool vertical) -> bool {
		if (vertical && c == 15)
			return true;
		else
			return v + 2 <= c;
	};

	// function to add the chunk to the update list
	auto add_chunk = [&](vector3di& chunkPos) {
		TerrainMap::iterator it = m_Terrain.find(chunkPos);
		if (it == m_Terrain.end())
			return;

		TerrainChunk* t = it->second;
		if (t && std::find(a.begin(), a.end(), t) == a.end())
			a.push_back(t);
	};

	// function to update adjacent chunk
	auto update_adjacent = [&](vector3di& blockPos, vector3di chunkPos) {
		if (blockPos.x == 0)
		{
			--chunkPos.x;
			add_chunk(chunkPos);
		}
		else if (blockPos.x == CHUNK_SIZE - 1)
		{
			++chunkPos.x;
			add_chunk(chunkPos);
		}

		if (blockPos.y == 0)
		{
			--chunkPos.y;
			add_chunk(chunkPos);
		}
		else if (blockPos.y == CHUNK_SIZE - 1)
		{
			++chunkPos.y;
			add_chunk(chunkPos);
		}

		if (blockPos.z == 0)
		{
			--chunkPos.z;
			add_chunk(chunkPos);
		}
		else if (blockPos.z == CHUNK_SIZE - 1)
		{
			++chunkPos.z;
			add_chunk(chunkPos);
		}
	};

	// function to check a light value at a given point
	auto check_light = [&](vector3di chunkPos, vector3di blockPos, bool vertical = false) {
		// if this block position is out of this chunk, correct it
		PositionCorrect(blockPos, chunkPos);

		// get the block at this position
		TerrainBlock tb = _GetBlock(blockPos, chunkPos);

		// compare lighting values
		bool _l = light_compare(tb.SkyLight, val, vertical);

		// if the light check passed
		if ((!tb.Solid || tb.Ignored || (tb.XX || tb.XY || tb.YX || tb.YY)) && _l)
		{
			// set the sky light
			GLubyte l = vertical && val == 15 ? 15 : val - 1;
			TerrainChunk* t = _SetSunLight(blockPos, chunkPos, l);

			if (t)
			{
				// for updates, check if the chunk is in there already and if not push it back
				if (std::find(a.begin(), a.end(), t) == a.end())
					a.push_back(t);

				// if we're on the edge of the chunk border we'll want to update the adjacent chunk
				update_adjacent(blockPos, chunkPos);

				// create a new sky light creation node
				m_SunLightCreation.emplace(blockPos, chunkPos, l);
			}
		}
	};

	// loop through the creation list
	while (!m_SunLightCreation.empty())
	{
		// get the light node details at this point
		SunlightNode& light = m_SunLightCreation.front();
		chunkPos = light.chunkPos;
		blockPos = light.blockPos;
		val = light.val;// GetBlock(pos).SkyLight;
		m_SunLightCreation.pop();

		// check lights adjacent to this block
		check_light(chunkPos, blockPos + vector3di(-1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(0, -1, 0), true);
		check_light(chunkPos, blockPos + vector3di(0, 1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 0, -1));
		check_light(chunkPos, blockPos + vector3di(0, 0, 1));
	}

	// if we are updating, clear the meshes of the chunks affected
	if (bUpdate)
	{
		for (GLuint i = 0; i < a.size(); ++i) {
			if (a[i])
				a[i]->ClearMesh();
		}
	}
}

void Terrain::UpdateSunlightDeletion(bool bUpdate)
{
	// update light deletion
	vector3di chunkPos, blockPos;
	GLuint val;
	vector<TerrainChunk*> a;

	// function to compare light levels
	auto light_compare = [](GLuint v, GLuint c, bool vertical) -> bool {
		if (vertical && c == 15)
			return true;
		else
			return v > 0 && v < c;
	};

	// function to add the chunk to the update list
	auto add_chunk = [&](vector3di& chunkPos) {
		TerrainMap::iterator it = m_Terrain.find(chunkPos);
		if (it == m_Terrain.end())
			return;

		TerrainChunk* t = it->second;
		if (t && std::find(a.begin(), a.end(), t) == a.end())
			a.push_back(t);
	};

	// function to update adjacent chunk
	auto update_adjacent = [&](vector3di& blockPos, vector3di chunkPos) {
		if (blockPos.x == 0)
		{
			--chunkPos.x;
			add_chunk(chunkPos);
		}
		else if (blockPos.x == CHUNK_SIZE - 1)
		{
			++chunkPos.x;
			add_chunk(chunkPos);
		}

		if (blockPos.y == 0)
		{
			--chunkPos.y;
			add_chunk(chunkPos);
		}
		else if (blockPos.y == CHUNK_SIZE - 1)
		{
			++chunkPos.y;
			add_chunk(chunkPos);
		}

		if (blockPos.z == 0)
		{
			--chunkPos.z;
			add_chunk(chunkPos);
		}
		else if (blockPos.z == CHUNK_SIZE - 1)
		{
			++chunkPos.z;
			add_chunk(chunkPos);
		}
	};

	// function to check a light value at a given point
	auto check_light = [&](vector3di chunkPos, vector3di blockPos, bool vertical = false) {
		// if this block position is out of this chunk, correct it
		PositionCorrect(blockPos, chunkPos);

		// get the block at this position
		TerrainBlock tb = _GetBlock(blockPos, chunkPos);

		// compare lighting values
		bool _l = light_compare(tb.SkyLight, val, vertical);

		// if the light check passed
		if ((!tb.Solid || tb.Ignored) && _l)
		{
			// set the sky light
			TerrainChunk* t = _SetSunLight(blockPos, chunkPos, 0);

			if (t)
			{
				// for updates, check if the chunk is in there already and if not push it back
				if (std::find(a.begin(), a.end(), t) == a.end())
					a.push_back(t);

				// if we're on the edge of the chunk border we'll want to update the adjacent chunk
				update_adjacent(blockPos, chunkPos);

				// create a new sky light deletion node
				m_SunLightDeletion.emplace(blockPos, chunkPos, 15);
			}
		}
		else if (tb.BlockType == 0 && m_Terrain.find(chunkPos) != m_Terrain.end())
			m_SunLightCreation.emplace(blockPos, chunkPos, (int)tb.SkyLight);
	};

	// loop through the deletion list
	while (!m_SunLightDeletion.empty())
	{
		// get the light node details at this point
		SunlightNode& light = m_SunLightDeletion.front();
		chunkPos = light.chunkPos;
		blockPos = light.blockPos;
		val = light.val;
		m_SunLightDeletion.pop();

		// check lights adjacent to this block
		check_light(chunkPos, blockPos + vector3di(-1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(1, 0, 0));
		check_light(chunkPos, blockPos + vector3di(0, -1, 0), true);
		check_light(chunkPos, blockPos + vector3di(0, 1, 0));
		check_light(chunkPos, blockPos + vector3di(0, 0, -1));
		check_light(chunkPos, blockPos + vector3di(0, 0, 1));
	}

	// if we are updating, clear the meshes of the chunks affected
	if (bUpdate/* && m_SunLightCreation.empty()*/)
	{
		for (GLuint i = 0; i < a.size(); ++i) {
			if (a[i])
				a[i]->ClearMesh();
		}
	}
}
