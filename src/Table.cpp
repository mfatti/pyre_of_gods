#include "Table.h"
#include "GUIManager.h"
#include <sstream>

Table::Table(Manager* parent, GUIElement* guiParent, vector2df pos, vector2di size, GLuint columns)
	: GUIElement(parent, guiParent, pos, size)
	, m_ColumnCount(columns)
{
	// create the headers
	AddQuad(GLVertex(vector3df(2, 2, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x - 2, 2, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x - 2, 24, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(2, 24, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()));

	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df(0, 25), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 26), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df(0, 1), vector2df(0.5, 0.5), vector2df(1, 25), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df((GLfloat)size.x - 1, 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 25), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));

	// set up default columns
	GLuint width = size.x / columns;
	for (GLuint i = 0; i < columns; i++) {
		m_ColumnNames.push_back("^8Column");
		m_ColumnWidths.push_back(width);

		if (i > 0)
			AddQuad(vector2df((GLfloat)(i * width), 1), vector2df(0.5, 0.5), vector2df((GLfloat)(i * width + 1), (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
		GetGUIManager()->CreateText(m_ColumnNames[i].c_str(), vector2df((GLfloat)(i * width + 6), 4), vector2di(m_ColumnWidths[i], 24), NULL, this);
	}
	CreateBufferObjects();

	// create the list box
	m_ListBox = GetGUIManager()->CreateListBox(vector2df(0, 25), size - vector2di(0, 25), this);
	m_ListBox->SetBackgroundAlpha(120);
}

void Table::MouseDownFunction(GLuint button)
{

}

void Table::SetColumnName(GLuint c, string name)
{
	m_ColumnNames[c] = name;
	Text* t = dynamic_cast<Text*>(GetChild(c + 1));
	if (t)
		t->SetText(string("^8").append(name), true);
}

void Table::SetColumnWidth(GLuint c, GLuint width)
{
	m_ColumnWidths[c] = width;
	GLuint w = GetWidthUptoColumn(c);
	if (c > 0)
		ChangeQuadCoords(c + 4, vector2df((GLfloat)w, 1), vector2df((GLfloat)(w + 1), (GLfloat)GetSize().y - 1));
	Text* t = dynamic_cast<Text*>(GetChild(c + 1));
	if (t)
	{
		t->SetPosition(vector2df((GLfloat)(w + 6), 4));
		t->SetSize(vector3di(width, 26, 0));
	}
}

void Table::AddRow(vector<string> values)
{
	// add a row of values
	std::stringstream ss;
	string row;
	
	for (GLuint i = 0; i < m_ColumnCount; ++i) {
		ss.str(std::string());
		ss << "%" << GetWidthUptoColumn(i);
		ss << values[i];

		row.append(ss.str());
	}

	m_ListBox->AddItem(row);
}

void Table::ChangeRow(GLuint row, vector<string> values)
{
	// change a row of values
	std::stringstream ss;
	string trow;

	for (GLuint i = 0; i < m_ColumnCount; ++i) {
		ss.str(std::string());
		ss << "%" << GetWidthUptoColumn(i);
		ss << values[i];

		trow.append(ss.str());
	}

	m_ListBox->ChangeItem(row, trow);
}

void Table::RemoveRow(GLuint row)
{
	m_ListBox->RemoveItem(row);
	m_ListBox->Select(0);
}

GLuint Table::GetSelectedRow()
{
	return m_ListBox->GetSelected();
}

string Table::GetCell(GLuint column, GLuint row)
{
	string val = m_ListBox->GetItem(row);
	std::stringstream ss;
	GLuint n = 0;
	char c;
	bool bSpacer = false;

	for (GLuint i = 0; i < val.length(); ++i) {
		c = val[i];

		if (bSpacer)
		{
			if (isdigit(c))
				continue;
			else
			{
				bSpacer = false;
				ss.str(std::string());
			}
		}
		if (c == '%')
		{
			bSpacer = true;

			if (n == column + 1)
			{
				return ss.str();
			}
			else
				++n;

			continue;
		}

		ss << c;
	}

	return ss.str();
}

GLuint Table::GetWidthUptoColumn(GLuint c)
{
	GLuint ret = 0;

	for (GLuint i = 0; i < c; ++i) {
		ret += m_ColumnWidths[i];
	}

	return ret;
}