#include "INIFile.h"
#include <sstream>

INIFile::INIFile(string file)
	: m_INIFile(file.c_str(), std::ios::in | std::ios::out)
	, m_strFileContents("")
	, m_strFileName(file)
	, m_bIsEmpty(false)
{
	// read the contents of the file
	string line;
	while (getline(m_INIFile, line))
	{
		m_strFileContents.append(line.append("\n"));
	}

	// if the file is empty then set the empty flag so the user can write their own INI file
	if (m_strFileContents.empty())
	{
		m_bIsEmpty = true;

		std::ofstream ofile(file.c_str());
		ofile.close();

		m_INIFile.open(file.c_str(), std::ios::in);
	}
}

INIFile::~INIFile()
{
	Close();
}

void INIFile::Close()
{
	if (m_INIFile.is_open())
		m_INIFile.close();
}

bool INIFile::IsEmpty()
{
	return m_bIsEmpty;
}

bool INIFile::WriteDefaults(string defaults)
{
	// should never happen really as the file will be open constantly until this object is destroyed
	if (!m_INIFile.is_open())
		return false;

	// write the given defaults string to the file
	m_strFileContents = defaults;
	m_INIFile.seekg(0);
	m_INIFile.write(defaults.c_str(), defaults.length());
	//m_INIFile << defaults;

	// set the empty flag to false
	m_bIsEmpty = false;

	// we have completed successfully
	return true;
}

bool INIFile::FindSection(string section)
{
	// should never happen really as the file will be open constantly until this object is destroyed
	if (!m_INIFile.is_open())
		return false;
	
	// get the section name in section tags
	string s = "[";
	s.append(section.append("]"));

	// search through the file contents for this section
	return _Search(s);
}

string INIFile::ReadSetting(string setting)
{
	// should never happen really as the file will be open constantly until this object is destroyed
	if (!m_INIFile.is_open())
		return "0";
	
	// first see if we can find the string
	if (!_Search(setting, m_Position))
		return "0";

	// read the string to get the value of the setting
	unsigned int i = m_strLastLine.find("=");
	if (i == string::npos)
		return "0";

	return m_strLastLine.substr(i + 1, m_strLastLine.length() - (i + 1));
}

void INIFile::Clear()
{
	m_strFileContents.clear();
}

void INIFile::WriteSection(string section)
{
	m_strFileContents.append("[");
	m_strFileContents.append(section);
	m_strFileContents.append("]\n");
}

void INIFile::WriteSetting(string setting, string value)
{
	m_strFileContents.append(setting);
	m_strFileContents.append("=");
	m_strFileContents.append(value);
	m_strFileContents.append("\n");
}

void INIFile::WriteSpace()
{
	m_strFileContents.append("\n");
}

bool INIFile::Write()
{
	// should never happen really as the file will be open constantly until this object is destroyed
	if (!m_INIFile.is_open())
		m_INIFile.close();

	// open the file for writing
	std::ofstream ofile(m_strFileName.c_str());
	
	// write the file
	ofile.write(m_strFileContents.c_str(), m_strFileContents.length());

	// close the file and open it again for reading
	ofile.close();
	m_INIFile.open(m_strFileName.c_str(), std::ios::in);

	return true;
}

bool INIFile::_Search(string str, unsigned int pos)
{
	// search through the file contents for the given string, starting from position pos
	std::stringstream file(m_strFileContents);
	m_Position = pos;
	while (getline(file, m_strLastLine, '\n'))
	{
		if (m_Position < pos)
		{
			++m_Position;
			continue;
		}
		
		if (m_strLastLine.find(str) != string::npos)
			return true;
	}

	// we have failed to find the string
	return false;
}