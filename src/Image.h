#ifndef _IMAGE_H_
#define _IMAGE_H_

#include "GUIElement.h"

class Image : public GUIElement {
	public:
		Image(string texture, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size, GLColour col = GLColour(255, 255, 255, 255));

		void SetUV(vector2df one, vector2df two);
		void Centre();

		void SetHoverable(bool bHoverable = true);

		virtual void MouseDownFunction(GLuint button);
		virtual bool IsHovered();

	protected:

	private:
		bool	m_bCentred;
		bool	m_bHoverable;
};

#endif