#ifndef _SCROLLBAR_H_
#define _SCROLLBAR_H_

#include "GUIElement.h"

enum {
	vertical_scroll = 0x01,
	horizontal_scroll = 0x02
};

class Scrollbar : public GUIElement {
	public:
		Scrollbar(GUIElement* scrollee, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size, GLubyte mode = vertical_scroll);

		virtual void Update(bool bForce = false);
		virtual bool IsHovered();
		virtual void MouseDownFunction(GLuint button);

		void AddHeight(GLfloat height);
		void RemoveHeight(GLfloat height);
		void ResetHeight();
		GLfloat GetHeight();

		void AddWidth(GLfloat width);
		void RemoveWidth(GLfloat width);
		void ResetWidth();
		GLfloat GetWidth();

		void ScrollToTop();
		void ScrollToBottom();
		void Reset();
		bool IsHorizontal();
		bool IsVertical();

		vector2df GetScrollAmount();

	protected:

	private:
		vector3dd	m_PrevMousePos;
		GUIElement*	m_Scrollee;
		GLfloat		m_Height;
		GLfloat		m_Width;
		vector2df	m_ScrollbarPos;
		vector2df	m_ScrollbarSize;
		vector2df	m_ScrollScale;
		GLubyte		m_ScrollMode;
		bool		m_bVertical;
		bool		m_bHorizontal;

		void UpdateGUI();
		void CalculateScrollbarSize();
};

#endif