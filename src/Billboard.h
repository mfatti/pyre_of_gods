#ifndef _BILLBOARD_H_
#define _BILLBOARD_H_

#include "GLObject.h"

struct Board {
	vector3df pos;
	vector2df size;
	GLfloat frame;
	GLfloat animspeed;
	GLfloat speed;
	GLuint anim;
	bool dead;

	Board(vector3df p, vector2df s, GLuint a, GLfloat as, GLfloat sp) : pos(p), size(s), frame(0), anim(a), dead(false), animspeed(as), speed(sp) {}
};

class Billboard : public GLObject {
	public:
		Billboard(Manager* parent, vector2df size = vector2df(64));

		virtual void Update(bool bForce = false);
		virtual void Render();
		virtual void RenderShadows();

		virtual void SetTexture(const char* fileName);

		GLuint AddBoard(Board& board, bool bCreate = true);

		void SetAnimationSpeed(GLfloat speed);
		float GetAnimationSpeed();

		GLuint AddAnimation(vector2di start, vector2di end);
		void RemakeAnimations();

		void SetAnimation(GLuint pos, GLuint anim);
		GLuint GetAnimation(GLuint pos);

		void SetCurrentFrame(GLuint pos, GLfloat frame);

		virtual void OnAnimationFinish(GLuint pos);

	protected:
		vector<Board>				m_Boards;

	private:
		vector<vector<vector2df> >	m_Animations;
		vector<vector<vector2di> >  m_AnimationFrames;
		GLfloat						m_fFrameWidth;
		GLfloat						m_fFrameHeight;
		GLfloat						m_fAnimSpeed;
		GLint						m_iXFrames;
		vector2df					m_FrameSize;
};

#endif