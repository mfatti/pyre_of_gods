#ifndef _PANE_H_
#define _PANE_H_

#include "Text.h"

class Pane : public GUIElement {
	public:
		Pane(const char* text, Font* font, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size, GLubyte alpha = 100, bool movable = true);

		void SetMovable(bool movable);
		bool IsMovable();

		string GetName();
		
		virtual void Update(bool bForce = false);
		
		virtual void MouseDownFunction(GLuint button);
		
	protected:
	
	private:
		vector3dd	m_PrevMousePos;
		bool		m_bMovable;
};

#endif
