#ifndef _TEXT_H_
#define _TEXT_H_

#include "GUIElement.h"
#include "Font.h"

class Text : public GUIElement {
	public:
		Text(string text, Font* font, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size);
		
		void SetText(string text, bool remake = false);
		void AppendText(string text, bool remake = true);
		void ClearText(bool clearVBO = true);
		string GetText();
		string GetReadableText();
		int GetTextLength();
		void SetLineSpacing(GLfloat spacing);
		GLfloat GetLineSpacing();
		GLuint GetHeight();
		void SetShadowed(bool shadow = true);

		virtual bool IsHovered();
		
		virtual void MouseDownFunction(GLuint button);
		
		void SetFont(Font* font);
		Font* GetFont();
		
		void CentreText(bool centre = true);
		void AlignRight(bool align = true);
		void SplitByWord(bool split = true);
		void CreateCharacterBuffers(bool remake);
	
	protected:
	
	private:
		string	m_Text;
		Font*	m_Font;
		bool	m_bCentered;
		bool	m_bSplitByWord;
		GLfloat	m_LineSpacing;
		GLuint	m_LineHeight;
		bool	m_bShadowed;
		bool	m_bRightAlign;
		
		bool GetColour(GLColour& col, char c);
		void GetLineLengths(std::vector<int>& out);
};

#endif
