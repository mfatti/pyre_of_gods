#include "NetworkManager.h"
#include "Game.h"
#include <iostream>

bool NetworkManager::m_Initialised(false);

NetworkManager::NetworkManager(Game* game, std::string ip, short port)
	: m_pGame(game)
	, m_IPAddress(ip)
	, m_PortNumber(port)
	, m_pConnectThread(NULL)
	, m_pListenThread(NULL)
	, m_ReplyReceived(false)
	, m_CanRequestChunkData(true)
	, m_TxCounters(max_message_number, 0)
	, m_RxCounters(max_message_number, 0)
{
	// create a connection to the server specified by ip and port
	// by the time this object is created a server ip/port should be known
	// a separate class exists to find details of servers (LAN games/manually entered)
	m_Initialised = false;

#ifdef _WIN32
	WSAData wsa_data;
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
		std::cout << "Error initialising: " << WSAGetLastError() << std::endl;
	else
	{
#endif
		m_Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (m_Socket == SOCKET_ERROR)
#ifdef _WIN32
			std::cout << "Failure creating socket: " << WSAGetLastError() << std::endl;
#else
			std::cout << "Failure creating socket: " << errno << std::endl;
#endif
		else
		{
			memset(&m_Server, 0, sizeof(m_Server));
			m_Server.sin_family = AF_INET;
			m_Server.sin_port = htons(m_PortNumber);
#ifdef _WIN32
			m_Server.sin_addr.S_un.S_addr = inet_addr(m_IPAddress.c_str());
#else
			m_Server.sin_addr.s_addr = inet_addr(m_IPAddress.c_str());
#endif
			m_Initialised = true;
			
			// set the timeout of the socket to 3 seconds
			// to prevent us from getting stuck waiting on the recvfrom call
/*			const unsigned int timeout = 3; // seconds
#ifdef _WIN32
			DWORD timeout = timeout * 1000;
			setsockopt(m_Socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof timeout);
#else
			struct timeval tv;
			tv.tv_sec = timeout;
			tv.tv_usec = 0;
			setsockopt(m_Socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
#endif*/
		}
#ifdef _WIN32
	}
#endif

	// on creation, send a new connection packet to the server
	if (m_Initialised)
	{
		// send our player name in a connection packet
		string name = m_pGame->GetMenuGUI()->GetPlayerName();
		unsigned char* buf = new unsigned char[name.length() + 1];
		buf[0] = 0; // just send a clientid of 0
		for (GLuint i = 0; i < name.length(); ++i) {
			buf[i + 1] = (unsigned char)name[i];
		}
		
		SendPacket(new_connection, buf, name.length() + 1);
		delete[] buf;

		// create a new thread to await for response from server
		m_pConnectThread = new std::thread(NetworkManager::WaitForConnectionReply, this);
		m_pConnectThread->detach();

		// start a timer for timeout
		Timer(5000, true, NetworkManager::ConnectionTimeout, this);

		// create the ack messages
		for (GLuint i = 0; i < ACK_MSG_COUNT; ++i) {
			AckMsg msg;
			msg.isfree = true;
			msg.buf = new unsigned char[BUFFER_SIZE];
			msg.len = 0;

			m_AckMessages[i] = msg;
		}
	}
}

NetworkManager::~NetworkManager()
{
	Finish();
}

void NetworkManager::Finish()
{
	// clean up
	m_Initialised = false;
#ifdef _WIN32
	closesocket(m_Socket);
	WSACleanup();
#else
	close(m_Socket);
#endif

	if (m_pConnectThread)
	{
		//m_pConnectThread->join();
		delete m_pConnectThread;
		m_pConnectThread = NULL;
	}

	if (m_pListenThread)
	{
		//m_pListenThread->join();
		delete m_pListenThread;
		m_pListenThread = NULL;
	}

	if (m_pPlayerUpdateThread)
	{
		//m_pPlayerUpdateThread->join();
		delete m_pPlayerUpdateThread;
		m_pPlayerUpdateThread = NULL;
	}

	for (GLuint i = 0; i < ACK_MSG_COUNT; ++i) {
		delete[] m_AckMessages[i].buf;
	}
}

bool NetworkManager::SendPacket(unsigned char type, unsigned char* buf, int size, bool ack, bool req)
{
	// send a packet over the socket to the server
	char* sendbuf;
	if (buf)
	{
		// if the user has supplied data, copy the data to a char buffer
		sendbuf = new char[size + HEADER_SIZE];
		sendbuf[0] = 0;
		sendbuf[1] = type;

		if (req)
			SetBit(sendbuf[0], 2, 1);

		memcpy(&sendbuf[HEADER_SIZE], buf, size);
		size += HEADER_SIZE;
	}
	else
	{
		// if the user has supplied no data (ie for a request packet), send 2 bytes
		sendbuf = new char[HEADER_SIZE];
		sendbuf[0] = 0;
		sendbuf[1] = type;
		SetBit(sendbuf[0], 2, 1);
		size = HEADER_SIZE;
	}

	// add the counter
	UIntToByte(NextTxCounter(type), (unsigned char*)&sendbuf[2]);

	// send the packet and clean up
	int ret = sendto(m_Socket, sendbuf, size, 0, (sockaddr*)&m_Server, sizeof(m_Server));

	// add this to the ack messages
	if (ack)
		AddAckMsg(MessageHeader::FromBuffer((unsigned char*)sendbuf), (unsigned char*)sendbuf, size);

	// clean up
	delete[] sendbuf;

	if (ret == SOCKET_ERROR)
		// if there was a socket error alert us (for DEBUG only)
#ifdef _WIN32
		std::cout << "SOCKET ERROR: " << WSAGetLastError() << std::endl;
#else
		std::cout << "SOCKET ERROR: " << errno << std::endl;
#endif

	return (ret >= 0);
}

bool NetworkManager::SendAck(unsigned char* buf, int size)
{
	// send an ack response back to the server
	SetBit(buf[0], 0, 0);
	SetBit(buf[0], 1, 1);

	// send the packet
	int ret = sendto(m_Socket, (char*)buf, HEADER_SIZE + size, 0, (sockaddr*)&m_Server, sizeof(m_Server));

	if (ret == SOCKET_ERROR)
		// if there was a socket error alert us (for DEBUG only)
#ifdef _WIN32
		std::cout << "SOCKET ERROR: " << WSAGetLastError() << std::endl;
#else
		std::cout << "SOCKET ERROR: " << errno << std::endl;
#endif

	return (ret >= 0);
}

void ResetFlag(bool* flag)
{
	*flag = true;
}

bool NetworkManager::GetTerrainData(const vector3di& pos, std::vector<unsigned char>& out, bool request)
{
	if (m_ChunkBuffers.find(pos) != m_ChunkBuffers.end())
	{
		ChunkData* data = &m_ChunkBuffers.at(pos);
		if (data->created)
		{
			//out.reserve(data->blocks.size());
			out.resize(data->data.size());
			memcpy(out.data(), data->data.data(), data->data.size());
			/*for (GLuint i = 0; i < data->blocks.size(); i++) {
				out.push_back(data->blocks[i]);
			}*/
			//memcpy(&out[0], &data->blocks[0], data->blocks.size());
			//std::copy(data->blocks[0], data->blocks[data->blocks.size() - 1], out);
			return true;
		}
	}
	else if (request && m_CanRequestChunkData)
	{
		// we don't have any information about this chunk, request the data from the server
		unsigned char buf[12];

		// place the chunk coordinates
		IntToByte(pos.x, &buf[0]);
		IntToByte(pos.y, &buf[4]);
		IntToByte(pos.z, &buf[8]);

		// send the request
		SendPacket(terrain_block_data, buf, 12, false, true);

		// clean up
		//delete[] buf;

		// stop us from sending too many chunk requests
		//m_CanRequestChunkData = false;
		//Timer(60, true, ResetFlag, &m_CanRequestChunkData);
	}

	return false;
}

bool NetworkManager::GetWaterData(const vector3di& pos, std::vector<unsigned char>& out, bool request)
{
	if (m_WaterBuffers.find(pos) != m_WaterBuffers.end())
	{
		ChunkData* data = &m_WaterBuffers.at(pos);
		if (data->created)
		{
			out.resize(data->data.size());
			memcpy(out.data(), data->data.data(), data->data.size());
			return true;
		}
	}
	else if (request && m_CanRequestChunkData)
	{
		// we don't have any information about this chunk, request the data from the server
		unsigned char buf[12];

		// place the chunk coordinates
		IntToByte(pos.x, &buf[0]);
		IntToByte(pos.y, &buf[4]);
		IntToByte(pos.z, &buf[8]);

		// send the request
		SendPacket(terrain_water_data, buf, 12, false, true);
	}

	return false;
}

void NetworkManager::SaveTerrainData(const vector3di& pos, unsigned char** buf, int len)
{
	// save the chunk to memory
	// this is done either on updating a chunk that's outside the rendering window, or saving a chunk as it becomes deleted
	// this will stop the client from seeing old chunk data for chunks they leave and return to
	if (m_ChunkBuffers.find(pos) != m_ChunkBuffers.end())
	{
		ChunkData* data = &m_ChunkBuffers.at(pos);

		data->data.clear();
		data->data.resize(len);
		memcpy(&data->data[0], *buf, len);
	}

	// delete the buffer as it is no longer needed
	delete[] *buf;
	*buf = NULL;
}

void NetworkManager::SaveWaterData(const vector3di& pos, unsigned char** buf, int len)
{
	// save the water to memory
	// this is done either on updating a chunk that's outside the rendering window, or saving a chunk as it becomes deleted
	// this will stop the client from seeing old chunk data for chunks they leave and return to
	if (m_WaterBuffers.find(pos) != m_WaterBuffers.end())
	{
		ChunkData* data = &m_WaterBuffers.at(pos);

		data->data.clear();
		data->data.resize(len);
		memcpy(&data->data[0], *buf, len);
	}

	// delete the buffer as it is no longer needed
	delete[] * buf;
	*buf = NULL;
}

void NetworkManager::StartPlayerUpdateThread()
{
	m_pPlayerUpdateThread = new std::thread(NetworkManager::SendPlayerData, this);
	m_pPlayerUpdateThread->detach();
}

int NetworkManager::GetClientID()
{
	return m_ClientID;
}

/*NetworkItem CreateItemFromBuffer(unsigned char* buf, int len)
{
	// safety first!
	if (len <= 0)
		return NetworkItem();
	
	// uncompress the data
	uLongf ulen = 1024;
	unsigned char* ubuf = new unsigned char[ulen];
	uncompress(ubuf, &ulen, buf, len);
	
	// read the length of the name
	short length;
	ByteToShort(&ubuf[0], length);

	// read in the name
	char* name = new char[length + 1];
	for (short i = 0; i < length; i++) {
		name[i] = (char)ubuf[i + 2];
	}
	name[length] = '\0';

	// read in the stack size
	short amount;
	ByteToShort(&ubuf[length + 2], amount);

	// read in the position
	float x, y, z;
	ByteToFloat(&ubuf[length + 4], x);
	ByteToFloat(&ubuf[length + 8], y);
	ByteToFloat(&ubuf[length + 12], z);

	// create the network item
	NetworkItem item;
	item.pos.Set(x, y, z);
	item.name = string(name);
	item.amount = amount;

	// clean up
	delete[] ubuf;
	delete[] name;

	// return the object
	return item;
}*/

AckMsg* NetworkManager::FindFreeAckMsg()
{
	for (GLuint i = 0; i < ACK_MSG_COUNT; ++i) {
		if (m_AckMessages[i].isfree)
			return &m_AckMessages[i];
	}

	return NULL;
}

void NetworkManager::AddAckMsg(const MessageHeader& header, unsigned char* buffer, int len)
{
	AckMsg* msg = FindFreeAckMsg();
	if (msg)
	{
		memset(msg->buf, 0, BUFFER_SIZE);
		memcpy(msg->buf, &buffer[1], len - 1);
		msg->len = len - 1;
		msg->isfree = false;
		msg->count = 6;
		msg->header = header;
	}
}

void NetworkManager::AcknowledgeMessage(const MessageHeader& other)
{
	// a message has been acknowledged from the server
	for (GLuint i = 0; i < ACK_MSG_COUNT; ++i) {
		if (m_AckMessages[i].Compare(other))
		{
			// mark this ack message as free
			m_AckMessages[i].isfree = true;
			//std::cout << "Server has acknowledged message " << (int)other.message << std::endl;
			return;
		}
	}
}

GLuint NetworkManager::NextTxCounter(int message)
{
	return m_TxCounters.at(message)++;
}

GLuint NetworkManager::GetTxCounter(int message) const
{
	return m_TxCounters.at(message);
}

bool NetworkManager::SetTxCounter(int message, GLuint counter)
{
	if (counter > GetTxCounter(message))
	{
		m_TxCounters.at(message) = counter;
		return true;
	}

	return false;
}

void NetworkManager::ResetTxCounter(int message)
{
	m_TxCounters.at(message) = 0;
}

GLuint NetworkManager::GetRxCounter(int message) const
{
	return m_RxCounters.at(message);
}

bool NetworkManager::SetRxCounter(int message, GLuint counter)
{
	if (counter > GetRxCounter(message))
	{
		m_RxCounters.at(message) = counter;
		return true;
	}

	return false;
}

void NetworkManager::ResetRxCounter(int message)
{
	m_RxCounters.at(message) = 0;
}

void NetworkManager::Listen(NetworkManager* n)
{
	// create the buffer
	char buf[BUFFER_SIZE];
	
	// start the listen loop
	while (n->m_Initialised)
	{
		// wait for incoming data and process it
		memset(buf, 0, BUFFER_SIZE);
#ifdef _WIN32
		int slen = sizeof(n->m_Server);
#else
		unsigned int slen = sizeof(n->m_Server);
#endif
		int rlen = recvfrom(n->m_Socket, buf, BUFFER_SIZE, 0, (sockaddr*)&n->m_Server, &slen);

		if (rlen > 0)
		{
			unsigned char* buffer = (unsigned char*)buf;

			if (!GetBit(buffer[0], 0))
				// if this message wasn't received from server ignore it
				continue;

			// extract the message header
			MessageHeader& header = MessageHeader::FromBuffer(buffer);

			bool ack = GetBit(header.info, 1);
			bool req = GetBit(header.info, 2);

			switch (header.message)
			{
				case server_packet:
				{
					// the server has sent us some important information
					switch (buffer[HEADER_SIZE])
					{
						case sp_server_update:
						{
							// extract information from this server tick message
							GLfloat time;
							ByteToFloat(&buffer[HEADER_SIZE + 1], time);

							n->m_pGame->GetSceneManager()->SetTimeOfDay(time);
							break;
						}
						case sp_give_item:
						{
							// an item has been given to us, woohoo
							//NetworkItem item = CreateItemFromBuffer(&buffer[HEADER_SIZE + 2], rlen - HEADER_SIZE - 2);
							ItemData item = ItemDataFromBuffer(&buffer[HEADER_SIZE + 2]);
							if (buffer[HEADER_SIZE + 1] == n->m_ClientID)
							{
								n->m_pGame->GetSceneManager()->GiveItem(item);
							}

							// get the client manager
							ClientManager* pClientMan = n->m_pGame->GetClientManager();

							if (!pClientMan)
								break;

							// print out the message
							std::stringstream ss;
							ss << item.amount;
							//n->m_pGame->GetGameGUI()->AddChatMessage("SERVER: Giving " + ss.str() + " of " + item.index + " to " + pClientMan->GetClientName(buffer[HEADER_SIZE + 1]));
							break;
						}
						case sp_kick_player:
						{
							// a player has been kicked from the server! if it is us, return to the main menu
							int id = buffer[HEADER_SIZE + 1];

							if (id == n->GetClientID())
							{
								// we have been kicked
								n->m_pGame->GetGameGUI()->GoToMainMenu("You have been kicked from the server.");
							}
							else
							{
								// get the client manager
								ClientManager* pClientMan = n->m_pGame->GetClientManager();

								if (!pClientMan)
									break;
								
								// another player has been kicked, add a message to the chat
								n->m_pGame->GetGameGUI()->AddChatMessage(pClientMan->GetClientName(buffer[HEADER_SIZE + 1]) + " has been kicked from the server");
							}
							break;
						}
						case sp_move_player:
						{
							// we have been moved by the server, so let us obey and move ourself
							int id = buffer[HEADER_SIZE + 1];
							if (id == n->GetClientID())
							{
								// this is definitely us being told to move
								vector3df pos;
								ByteToFloat(&buffer[HEADER_SIZE + 2], pos.x);
								ByteToFloat(&buffer[HEADER_SIZE + 6], pos.y);
								ByteToFloat(&buffer[HEADER_SIZE + 10], pos.z);

								Player* p = n->m_pGame->GetSceneManager()->GetFirstInstance<Player>();

								if (p)
								{
									p->SetPosition(pos);

									// acknowledge the message
									n->SendAck(buffer, 1);
								}
							}
							break;
						}
						case sp_knockback:
						{
							// we might have been knocked back by an enemy, so knockback the player
							int id = buffer[HEADER_SIZE + 1];
							if (id == n->GetClientID())
							{
								// this is definitely us being knocked back
								GLfloat dir, speed;
								ByteToFloat(&buffer[HEADER_SIZE + 2], dir);
								ByteToFloat(&buffer[HEADER_SIZE + 6], speed);

								n->m_pGame->GetSceneManager()->GetFirstInstance<Player>()->KnockBack(dir, speed);

								// acknowledge the message
								n->SendAck(buffer, 1);
							}
							break;
						}
						case sp_new_biome:
						{
							// we've moved into a new biome, print a message on our screen
							GameGUI* g = n->m_pGame->GetGameGUI();
							if (g)
							{
								int id = buffer[HEADER_SIZE + 1];
								if (id == n->GetClientID())
								{
									// alert our Game GUI we've moved biome
									string biome;
									int i = 2;
									char c = static_cast<char>(buffer[HEADER_SIZE + i]);
									while (c != '\0')
									{
										biome += c;

										c = static_cast<char>(buffer[HEADER_SIZE + ++i]);
									}

									g->NewBiome(biome);

									// send an ack
									n->SendAck(buffer, 1);
								}
							}
							break;
						}
					}
					break;
				}
				case new_connection:
				{
					if (!ack)
					{
						// create a new client
						ClientManager* pClientMan = n->m_pGame->GetClientManager();

						if (!pClientMan)
							break;

						string text;
						int i = HEADER_SIZE + 1;
						int id = buffer[HEADER_SIZE];
						while (buf[i] != '\0')
						{
							// add to the text
							text += buf[i++];

							if (i == rlen)
								// this is here as a fail-safe incase something went wrong with the transmission
								break;
						}

						pClientMan->NewClient(id, text);

						// acknowledge the message
						n->SendAck(buffer);
						//n->SendPacket(new_connection, &buffer[HEADER_SIZE], rlen - HEADER_SIZE, true);

						// print this in chat
						n->m_pGame->GetGameGUI()->AddChatMessage(string("^7") + text + string(" has connected!"));
					}
					break;
				}
				case disconnect:
				{
					// a client has disconnected, delete them
					ClientManager* pClientMan = n->m_pGame->GetClientManager();

					if (!pClientMan)
						break;

					int id = buffer[HEADER_SIZE];
					pClientMan->DeleteClient(id);

					// reply with an acknowledge
					n->SendAck(buffer);
					//n->SendPacket(disconnect, &buffer[HEADER_SIZE], rlen - HEADER_SIZE, true);

					// print this in chat
					n->m_pGame->GetGameGUI()->AddChatMessage(string("^7") + pClientMan->GetClientName(id) + string(" has disconnected."));
					break;
				}
				case terrain_block_data:
				{
					// figure out which chunk this data is for
					int x, y, z;
					ByteToInt(&buffer[HEADER_SIZE], x);
					ByteToInt(&buffer[HEADER_SIZE + 4], y);
					ByteToInt(&buffer[HEADER_SIZE + 8], z);
					vector3di pos(x, y, z);

					// figure out how many packets are for this chunk
					char size = buffer[HEADER_SIZE + 12];
					char i = buffer[HEADER_SIZE + 13];

					// create a terrain chunk buffer object
					ChunkData* tdata = NULL;
					TerrainData::iterator it = n->m_ChunkBuffers.find(pos);
					if (it == n->m_ChunkBuffers.end())
					{
						ChunkData data;
						n->m_ChunkBuffers.emplace(pos, data);
						tdata = &n->m_ChunkBuffers.at(pos);

						// fill the data with empty data
						tdata->data.resize(size * 450);
					}
					else
						tdata = &it->second;

					// push back the data
					memcpy(&tdata->data.data()[(i - 1) * 450], &buffer[HEADER_SIZE + 14], rlen - HEADER_SIZE - 14);

					// reply with an acknowledgement for this chunk and data combination
					n->SendAck(buffer);

					// see if we are fully created
					if (++tdata->count >= (int)size - 1)
					{
						// we have received all data for this chunk, set the flag
						tdata->created = true;

						// if we have received enough data and are in the connecting phase, switch to gameplay
						if (n->m_pGame->GetMenuGUI() && n->m_ChunkBuffers.size() >= 50)
						{
							n->m_pGame->GetMenuGUI()->SetStatus(starting_game);
						}
					}

					break;
				}
				case terrain_water_data:
				{
					// figure out which chunk this data is for
					int x, y, z;
					ByteToInt(&buffer[HEADER_SIZE], x);
					ByteToInt(&buffer[HEADER_SIZE + 4], y);
					ByteToInt(&buffer[HEADER_SIZE + 8], z);
					vector3di pos(x, y, z);

					// figure out how many packets are for this chunk
					char size = buffer[HEADER_SIZE + 12];
					char i = buffer[HEADER_SIZE + 13];

					// create a terrain chunk buffer object
					ChunkData* tdata = NULL;
					TerrainData::iterator it = n->m_WaterBuffers.find(pos);
					if (it == n->m_WaterBuffers.end())
					{
						ChunkData data;
						n->m_WaterBuffers.emplace(pos, data);
						tdata = &n->m_WaterBuffers.at(pos);

						// fill the data with empty data
						tdata->data.resize(size * 450);
					}
					else
						tdata = &it->second;

					// push back the data
					memcpy(&tdata->data.data()[(i - 1) * 450], &buffer[HEADER_SIZE + 14], rlen - HEADER_SIZE - 14);

					// reply with an acknowledgement for this chunk and data combination
					n->SendAck(buffer);

					// see if we are fully created
					if (++tdata->count >= (int)size - 1)
					{
						// we have received all data for this chunk, set the flag
						tdata->created = true;
					}

					break;
				}
				case player_data:
				{
					// if we don't know about this client create a new client
					ClientManager* pClientMan = n->m_pGame->GetClientManager();

					if (!pClientMan)
						break;

					int id = buffer[HEADER_SIZE];
					if (pClientMan->DoesClientExist(id))
					{
						// extract the position data
						PlayerData data = PlayerDataFromBuffer(&buffer[HEADER_SIZE + 1]);
						
						pClientMan->UpdateClientPosition(id, data);
					}
					break;
				}
				case player_hotbar_data:
				case player_equipment_data:
				{
					// if this is an acknowledge, acknowledge
					if (ack)
					{
						n->AcknowledgeMessage(header);
					}
					
					if (req && !ack)
					{
						// the server is requesting our data, send it to them
						if (header.message == player_hotbar_data)
							n->SendPlayerInventory(player_hotbar_data, n->m_pGame->GetSceneManager()->GetFirstInstance<Player>()->GetHotbar());
						else
							n->SendPlayerInventory(player_equipment_data, n->m_pGame->GetSceneManager()->GetFirstInstance<Player>()->GetEquipment());
					}
					else
					{
						// pass on this data to the given client, if it exists
						ClientManager* pClientMan = n->m_pGame->GetClientManager();

						if (!pClientMan)
							break;

						int id = buf[HEADER_SIZE];
						if (id != n->GetClientID() && pClientMan->DoesClientExist(id))
						{
							pClientMan->UpdateClientInventory(id, header.message, &buffer[HEADER_SIZE + 1], rlen - HEADER_SIZE - 1);
						}

						// acknowledge the message
						n->SendAck(buffer);
					}
					break;
				}
				case player_stat_data:
				{
					// if this is an acknowledge, acknowledge
					if (ack)
					{
						n->AcknowledgeMessage(header);
						break;
					}
					
					// we have received stat data about a player, it could be us or another client
					int id = buf[HEADER_SIZE];
					if (id == n->GetClientID())
					{
						// this is information about our player, so update our stats
						n->m_pGame->GetSceneManager()->GetFirstInstance<Player>()->SetStats(Stats::FromBuffer(&buffer[HEADER_SIZE + 1]));

						if (n->m_pGame->GetGameGUI())
							n->m_pGame->GetGameGUI()->StatsUpdated();
					}
					else
					{
						// this is information about another player, so update their stats
						ClientManager* pClientMan = n->m_pGame->GetClientManager();

						if (!pClientMan)
							break;

						if (pClientMan->DoesClientExist(id))
							pClientMan->UpdateClientStats(id, Stats::FromBuffer(&buffer[HEADER_SIZE + 1]));
					}

					// acknowledge the message
					n->SendAck(buffer);

					break;
				}
				case player_action:
				{
					// firstly acknowledge the message
					if (ack)
					{
						n->AcknowledgeMessage(header);
					}

					// if this isn't us, move the client
					if (buffer[HEADER_SIZE] != n->GetClientID())
					{

					}
					
					break;
				}
				case change_block:
				{
					// if this is an acknowledge, acknowledge
					if (ack)
					{
						n->AcknowledgeMessage(header);
						break;
					}
					
					// pass on this data to the currently active terrain, if it exists
					Terrain* t = n->m_pGame->GetSceneManager()->GetFirstInstance<Terrain>();

					if (t)
						t->ProcessChangeBlock(&buffer[HEADER_SIZE + 1], rlen - HEADER_SIZE - 1);

					// acknowledge the message
					n->SendAck(buffer);
					//n->SendPacket(change_block, &buffer[2], rlen - 2, true);
					break;
				}
				case create_item:
				{
					if (ack)
					{
						// this is the server acknowledging our create_item message, we can clear the currently held item
						n->m_pGame->GetGameGUI()->GetInventory()->RemoveHeldItem();
						n->AcknowledgeMessage(header);
					}
					
					// create the item in the world
					n->m_pGame->GetSceneManager()->CreateNetworkItem(ItemDataFromBuffer(&buffer[HEADER_SIZE + 1]));
					break;
				}
				case destroy_item:
				{
					ItemData item = ItemDataFromBuffer(&buffer[HEADER_SIZE + 1]);

					if (ack)
					{
						// this is the server telling us "yes YOU'RE the one that has destroyed the item"
						// this basically means we're the client that has picked up the item
						n->m_pGame->GetSceneManager()->PickUpItem(item);
						n->AcknowledgeMessage(header);
					}
					
					// destroy the item in the world
					n->m_pGame->GetSceneManager()->DeleteNetworkItem(item);
					break;
				}
				case chat_message:
				{
					// if this is an acknowledge, acknowledge
					if (ack)
					{
						n->AcknowledgeMessage(header);
						break;
					}
					
					// a client or the server has sent a chat message, add it to the chat!
					ClientManager* pClientMan = n->m_pGame->GetClientManager();

					if (!pClientMan)
						break;

					string text("");
					int id = buffer[HEADER_SIZE];
					int i = HEADER_SIZE + 1;
					while (buf[i] != '\0')
					{
						// add to the text
						text += buf[i++];

						if (i == rlen)
							// this is here as a fail-safe incase something went wrong with the transmission
							break;
					}

					n->m_pGame->GetGameGUI()->AddChatMessage(string("^6") + pClientMan->GetClientName(id) + string("^7: ") + text);

					// acknowledge the message
					n->SendAck(buffer);
					//n->SendPacket(chat_message, &buffer[2], rlen - 2, true);

					// print the message
					//cout << id << endl;
					//cout << "Client " << id << ": " << text << endl;
					break;
				}
				case block_interaction:
				{
					// if this is an acknowledge, acknowledge
					if (ack)
					{
						n->AcknowledgeMessage(header);
						break;
					}

					// send an ack
					n->SendAck(buffer);
					
					// process a block interaction message
					Terrain* t = n->m_pGame->GetSceneManager()->GetFirstInstance<Terrain>();

					if (t)
						t->Interact(&buffer[HEADER_SIZE]);
					break;
				}
				case block_inventory:
				{
					// if this is an acknowledge, acknowledge
					if (ack)
					{
						n->AcknowledgeMessage(header);
						break;
					}

					// send an ack
					n->SendAck(buffer);
					
					// process this block (ie chest) inventory
					GameGUI* g = n->m_pGame->GetGameGUI();

					if (g)
						g->ProcessBlockInventory(&buffer[HEADER_SIZE], rlen - HEADER_SIZE);
					break;
				}
				case mob_updates:
				{
					// send this data to the mob manager to deal with
					n->m_pGame->GetSceneManager()->GetMobManager()->ReceiveData(&buffer[HEADER_SIZE], rlen - HEADER_SIZE);
					break;
				}
				case new_projectile:
				{
					// create a new projectile as someone has used a ranged weapon/magic (including mobs)
					ProjectileData data = ProjectileDataFromBuffer(&buffer[HEADER_SIZE + 1]);
					int id = buffer[HEADER_SIZE];
					const size_t plen = sizeof(ProjectileData);

					n->m_pGame->GetSceneManager()->CreateProjectile(data, id == 255);

					// if it was us who fired, tell the player they have used some ammo
					if (id == n->GetClientID())
						n->m_pGame->GetSceneManager()->GetFirstInstance<Player>()->UseAmmo(data.index);

					// send an ack
					//n->SendAck(buffer);
					break;
				}
			}
		}
		else
#ifdef _WIN32
			std::cout << "SOCKET ERROR RECV: " << WSAGetLastError() << std::endl;
#else
			std::cout << "SOCKET ERROR RECV: " << errno << std::endl;
#endif
	}
}

void NetworkManager::WaitForConnectionReply(NetworkManager* n)
{
	// wait to see if we get a reply from the server
	char buf[BUFFER_SIZE];
#ifdef _WIN32
		int slen = sizeof(n->m_Server);
#else
		unsigned int slen = sizeof(n->m_Server);
#endif
	int rlen = recvfrom(n->m_Socket, buf, BUFFER_SIZE, 0, (sockaddr*)&n->m_Server, &slen);

	if (rlen != SOCKET_ERROR)
	{
		// we have received a reply from the server, so begin the join sequence
		n->m_ReplyReceived = true;
		n->m_pGame->GetMenuGUI()->ConnectionSuccessful();

		// set up our client ID
		n->m_ClientID = buf[HEADER_SIZE];

		// delete the connection timeout thread
		/*if (n->m_pConnectThread)
		{
			delete n->m_pConnectThread;
			n->m_pConnectThread = NULL;
		}*/

		// start the listen thread
		n->m_pListenThread = new std::thread(NetworkManager::Listen, n);
		n->m_pListenThread->detach();
	}
}

void NetworkManager::ConnectionTimeout(NetworkManager* n)
{
	// time's up, the server hasn't responded in time so shutdown
	if (n->m_pConnectThread)
	{
		delete n->m_pConnectThread;
		n->m_pConnectThread = NULL;

		if (!n->m_ReplyReceived)
		{
			n->Finish();
			n->m_pGame->GetMenuGUI()->ConnectionFailed();
		}
	}
}

void NetworkManager::SendPlayerData(NetworkManager* n)
{
	// transmit player data to the server
	while (n->m_Initialised)
	{
		if (n->m_pGame && n->m_pGame->GetSceneManager())
		{
			Player* p = n->m_pGame->GetSceneManager()->GetFirstInstance<Player>();
			if (p)
			{
				// fill the data
				const size_t plen = sizeof(PlayerData);
				unsigned char buf[plen + 1];

				// client id
				buf[0] = n->m_ClientID;

				// fill the data
				PlayerData data;
				data.pos = p->GetPosition();
				data.rot = -p->GetLookDirection() * DEG_TO_RAD + vector2df(0, PI);
				data.swing = p->IsSwinging();
				data.selected = p->GetSelected();
				data.sprinting = p->IsSprinting();

				// put it in the buffer
				memcpy(&buf[1], PlayerDataToBuffer(data), plen);

				// send the packet
				n->SendPacket(player_data, buf, plen + 1);
			}
		}

		// loop through any messages that need acknowledging and re-send them (doesn't matter if they've been acknowledged
		// and that ack hasn't reached us yet)
		char buf[BUFFER_SIZE];
		for (GLuint j = 0; j < ACK_MSG_COUNT; ++j) {
			if (!n->m_AckMessages[j].isfree)
			{
				if (--n->m_AckMessages[j].count == 0)
				{
					// create the message
					memset(buf, 0, BUFFER_SIZE);
					SetBit(buf[0], 0, 1);
					memcpy(&buf[1], (char*)n->m_AckMessages[j].buf, n->m_AckMessages[j].len);

					// transmit
					sendto(n->m_Socket, buf, n->m_AckMessages[j].len + 1, 0, (sockaddr*)&n->m_Server, sizeof(n->m_Server));

					// reset the counter
					n->m_AckMessages[j].count = 6;
				}
			}
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}

void NetworkManager::SendPlayerInventory(int type, vector<Item*>* inv)
{
	// get the hotbar item details and compress the data
	vector<unsigned char> items;

	for (GLuint i = 0; i < inv->size(); ++i) {
		if (inv->at(i))
		{
			// there is an item here, write the item
			items.push_back(1);

			// create the item data
			ItemData data;
			data.index = inv->at(i)->GetIndex();
			data.amount = inv->at(i)->GetStackSize();
			data.pos = inv->at(i)->GetPosition();
			unsigned char* itemdata = ItemDataToBuffer(data);

			// push back the data
			for (GLuint i = 0; i < sizeof(ItemData); ++i) {
				items.push_back(itemdata[i]);
			}
		}
		else
		{
			// nothing is here, write 0
			items.push_back(0);
		}
	}

	// now we have the data, compress it so it isn't too big to send in one packet
	uLongf size = items.size();
	unsigned char* cbuf = new unsigned char[size];
	compress(cbuf, &size, items.data(), items.size());

	// we now need a buffer 1 byte bigger than the size of the data
	unsigned char* buf = new unsigned char[size + 1];
	buf[0] = GetClientID();
	memcpy(&buf[1], cbuf, size);

	// send the data and clean up
	SendPacket(type, buf, size + 1, true);
	delete[] cbuf;
	delete[] buf;
}

void NetworkManager::SendPlayerStats()
{
	// send the player's stats to the server so the server has up to date information on the player
	// this should only happen when the player is newly connected - the server will ignore these packets after the first is received
	Player* p = m_pGame->GetSceneManager()->GetFirstInstance<Player>();
	if (!p)
		return;

	// create a buffer to send the information
	const size_t len = sizeof(Stats) + 1;
	unsigned char buf[len];
	buf[0] = GetClientID();

	// put the stats into the buffer
	memcpy(&buf[1], Stats::ToBuffer(p->GetStats()), len - 1);

	// send the data
	SendPacket(player_stat_data, buf, len, true);
}

void NetworkManager::SendPlayerAction(int type)
{
	// send an action to the server - this gets sent when the player does something, like rolling, that should affect their stats / other things
	Player* p = m_pGame->GetSceneManager()->GetFirstInstance<Player>();
	if (!p)
		return;

	// reate a buffer to send the information
	unsigned char buf[2];
	buf[0] = GetClientID();
	buf[1] = type;

	// send the data
	SendPacket(player_action, buf, 2, true);
}

void NetworkManager::SendItem(unsigned char type, Item* item)
{
	// create the item
	ItemData data;
	data.index = item->GetIndex();
	data.amount = item->GetStackSize();
	data.pos = item->GetPosition();

	// add the client id on the front
	const size_t ilen = sizeof(ItemData);
	unsigned char* sbuf = new unsigned char[ilen + 1];
	sbuf[0] = GetClientID();
	memcpy(&sbuf[1], ItemDataToBuffer(data), ilen);

	// send the packet and clean up
	SendPacket(type, sbuf, ilen + 1, true);
	delete[] sbuf;
}

string NetworkManager::GetPlayerName()
{
	// used by client manager to get name
	return m_pGame->GetSceneManager()->GetFirstInstance<Player>()->GetName();
}
