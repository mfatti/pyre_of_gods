#include "Animator.h"

Animator::Animator(Mesh* mesh)
	: m_Mesh(mesh)
	, m_CurrentAnim(NULL)
	, m_CurrentAnimPos(0)
	, m_bMoveToStart(false)
	, m_bAnimRunningOver(false)
	, m_AnimSpeed(1)
	, m_bNotLooping(false)
	, m_bRunning(false)
	, m_CurrentAnimName("")
	, m_Status(animation_failed)
{

}

void Animator::SetMesh(Mesh* mesh)
{
	m_Mesh = mesh;
}

AnimateResult Animator::Animate(AnimFrame& lower, AnimFrame& upper, GLfloat& t, bool force)
{
	// to call this function requires a mesh having been set - it does NOT work if no mesh (ie AddTriangles called instead)
	if (!m_Mesh || !m_CurrentAnim)
	{
		m_Status = animation_failed;
		return m_Status;
	}

	// if we aren't looping and we have finished, let the user know
	if (!force && m_bNotLooping && !m_bRunning)
	{
		m_Status = animation_ended;
		return m_Status;
	}

	// we will now sort our bone transformations based on the current animation
	if (force || !m_bMoveToStart)
	{
		if (!m_CurrentAnim->frames.empty())
		{
			if (m_CurrentAnim->frames.size() > 1)
			{
				// calculate frames if needs be
				if (force || m_CurrentAnimPos == 0.f)
					CalculateFrames();

				// if we have a lower and higher, calculate the time between the two
				GLfloat diff = 0.f, current = m_CurrentAnimPos;
				if (m_bAnimRunningOver)
				{
					if (m_LowerFrame.time > m_UpperFrame.time && m_CurrentAnimPos < m_LowerFrame.time)
					{
						current += m_CurrentAnim->length;
						diff = (m_CurrentAnim->length - m_UpperFrame.time) + m_LowerFrame.time;
					}
					else
						diff = (m_CurrentAnim->length - m_LowerFrame.time) + m_UpperFrame.time;
				}
				else
					diff = m_UpperFrame.time - m_LowerFrame.time;

				// now set the model to be lerped between both keyframes
				t = (current - m_LowerFrame.time) / diff;

				lower = m_LowerFrame;
				upper = m_UpperFrame;
			}
			else
			{
				// we only have one keyframe so set the model to this
				lower = m_CurrentAnim->frames.begin()->second;

				m_Status = animation_still;
				return m_Status;
			}
		}
		else
		{
			// we have no keyframes set, set the model to be the default bind pose
			m_Status = animation_failed;
			return m_Status;
		}

		// now let us move onto the next part of the animation
		m_CurrentAnimPos += m_AnimSpeed * DeltaTime::GetDelta();

		// if we have reached the upper (or lower) frame, recalculate frames
		if ((m_AnimSpeed > 0.f && m_CurrentAnimPos - (m_UpperFrame.time < m_LowerFrame.time ? m_CurrentAnim->length : 0) > m_UpperFrame.time) ||
			(m_AnimSpeed < 0.f && m_CurrentAnimPos + (m_LowerFrame.time > m_UpperFrame.time ? m_CurrentAnim->length : 0) < m_LowerFrame.time))
			CalculateFrames();

		if (m_CurrentAnimPos > m_CurrentAnim->length)
		{
			m_CurrentAnimPos = 0.f;

			if (m_bNotLooping)
			{
				m_bRunning = false;
			}
		}
		else if (m_CurrentAnimPos < 0.f)
		{
			m_CurrentAnimPos = m_CurrentAnim->length;

			if (m_bNotLooping)
			{
				m_bRunning = false;
			}
		}

		m_Status = animation_running;
		return m_Status;
	}
	else
	{
		// if we get close to the animation, stop there
		m_CurrentAnimPos = 0.f;

		// get the first frame and animate towards that
		lower = m_CurrentAnim->frames.begin()->second;

		// resume normal playback (if movetostart is called again this won't matter, but it will allow us to continue normally after)
		m_bMoveToStart = false;
		m_Status = animation_resetting;
		return m_Status;
	}
}

void Animator::SetAnimation(string anim, GLfloat pos)
{
	// if string is empty, set animation to NULL
	if (anim == "")
	{
		m_CurrentAnim = NULL;
		return;
	}

	// if we are already using this animation, return
	if (anim == m_CurrentAnimName)
		return;

	// if we have no mesh, we cannot use this
	if (!m_Mesh)
		return;

	// see if we can find the animation in our mesh's animation list
	map<string, Animation>* anims = m_Mesh->GetAnimationsPointer();
	if (anims->empty())
		return;

	if (anims->find(anim) == anims->end())
		return;

	// all checks have passed, set the current animation
	m_CurrentAnim = &(*anims)[anim];
	m_CurrentAnimPos = pos;
	m_CurrentAnimName = anim;

	// calculate the starting frames
	CalculateFrames();
}

void Animator::SetAnimationSpeed(GLfloat speed)
{
	// this we can do without a mesh, although it won't do much
	m_AnimSpeed = speed;
}

void Animator::SetAnimationPosition(GLfloat pos)
{
	// we can't do this without a mesh or current animation
	if (!m_Mesh || !m_CurrentAnim)
		return;

	// ensure the position isn't outside our animation range
	m_CurrentAnimPos = max(min(pos, m_CurrentAnim->length), 0.f);
}

void Animator::MoveToStart()
{
	// repeatedly call this to force an animation to the start, to reset an animation smoothly
	m_bMoveToStart = true;
}

void Animator::SetAnimationLoop(bool loop)
{
	// toggle looping
	m_bNotLooping = !loop;
}

bool Animator::IsLooping()
{
	// are we looping?
	return !m_bNotLooping;
}

void Animator::Start()
{
	// if we aren't looping the animation, use this to start the animation
	m_bRunning = true;
}

AnimateResult Animator::GetAnimationStatus()
{
	// get the status of this animation
	return m_Status;
}

GLfloat Animator::GetCurrentPosition()
{
	// get the current position in the animation
	return m_CurrentAnimPos;
}

GLfloat Animator::GetAnimationLength()
{
	// get the length of the animation
	return m_CurrentAnim->length;
}

void Animator::CalculateFrames()
{
	// if this is the start of an animation, set the frames to be the first and second
	m_bAnimRunningOver = false;
	if (m_CurrentAnimPos == 0.f)
	{
		// ensure we start at the beginning - just because
		m_CurrentAnimPos = 0.f;

		if (m_CurrentAnim->frames.begin()->first > 0.f)
		{
			m_LowerFrame = (--m_CurrentAnim->frames.end())->second;
			m_bAnimRunningOver = true;
		}
		else
			m_LowerFrame = m_CurrentAnim->frames.begin()->second;

		m_UpperFrame = (++m_CurrentAnim->frames.begin())->second;
		return;
	}

	// calculate the next frames to use based on direction of animation
	if (m_AnimSpeed > 0.f)
		m_LowerFrame = m_UpperFrame;
	else
		m_UpperFrame = m_LowerFrame;

	GLfloat current = m_UpperFrame.time;
	GLfloat lower = -1.f, upper = m_CurrentAnim->length + 1.f, lowest = m_CurrentAnim->length, highest = 0.f;
	map<GLfloat, AnimFrame>::iterator it = m_CurrentAnim->frames.begin();
	while (it != m_CurrentAnim->frames.end())
	{
		if (it->first < current && it->first > lower)
			lower = it->first;
		else if (it->first > current && it->first < upper)
			upper = it->first;

		// figure out the lowest and highest in the animation
		if (it->first < lowest)
			lowest = it->first;
		if (it->first > highest)
			highest = it->first;

		++it;
	}

	// now set the next frame sequence
	if (m_AnimSpeed > 0.f)
	{
		if (upper > m_CurrentAnim->length)
		{
			upper = lowest;
			m_bAnimRunningOver = true;
		}

		m_UpperFrame = m_CurrentAnim->frames[upper];
	}
	else
	{
		if (lower < 0.f)
		{
			lower = highest;
			m_bAnimRunningOver = true;
		}

		m_LowerFrame = m_CurrentAnim->frames[lower];
	}
}