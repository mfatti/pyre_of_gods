#ifndef _TABLE_H_
#define _TABLE_H_

#include "GUIElement.h"
#include "ListBox.h"

class Table : public GUIElement {
	public:
		Table(Manager* parent, GUIElement* guiParent, vector2df pos, vector2di size, GLuint columns);

		virtual void MouseDownFunction(GLuint button);

		void SetColumnName(GLuint c, string name);
		void SetColumnWidth(GLuint c, GLuint width);

		void AddRow(vector<string> values);
		void ChangeRow(GLuint row, vector<string> values);
		void RemoveRow(GLuint row);

		GLuint GetSelectedRow();
		string GetCell(GLuint column, GLuint row);

	protected:

	private:
		vector<string>	m_Cells;
		vector<string>	m_ColumnNames;
		vector<GLuint>	m_ColumnWidths;
		GLuint			m_ColumnCount;
		ListBox*		m_ListBox;

		GLuint GetWidthUptoColumn(GLuint c);
};

#endif