#ifndef _GUIELEMENT_H_
#define _GUIELEMENT_H_

#include <vector>
#include "GLObject.h"

enum {
	left_mouse = 0,
	right_mouse,

	mouse_fns
};

class GUIManager;
class Label;

class GUIElement : public GLObject {
	public:
		GUIElement(Manager* parent, GUIElement* guiParent, vector2df pos, vector3di size);
		virtual ~GUIElement();
		
		virtual void Update(bool bForce = false);
		virtual void Render();
		
		virtual void MouseDownFunction(GLuint button) = 0;

		virtual bool InViewFrustum() const;
		
		GUIElement* GetParent();
		GUIElement* GetRoot();
		GUIManager* GetGUIManager();
		const vector<GUIElement*>& GetChildren();
		void KillChildren();
		void Kill();
		virtual void AddChild(GUIElement* child);
		virtual void RemoveChild(GUIElement* child);
		GUIElement* GetChild(GLuint index);
		vector2df GetAbsolutePosition();
		void TranslatePosition(const vector2df& translate);
		
		void SetFunction(GLuint button, void (*fn)(GUIElement*));
		void SetUpdateFunction(void(*fn)(GUIElement*));
		void ExecuteFunction(GLuint button);
		
		bool MouseRelease(GLuint button);
		bool MouseDown(GLuint button);
		virtual bool IsHovered();
		bool IsPressed(GLuint button);
		bool IsAnyChildPressed(GLuint button);
		void Press(GLuint button);
		virtual void Release(GLuint button);

		void SetClickable(bool clickable);
		bool IsClickable();
		
		virtual void SetPosition(const vector2df& pos);
		void SetOffset(const vector2df& offset);
		vector2df GetOffset();

		virtual void SetDepth(GLfloat depth);
		GLfloat GetDepth();

		bool IsVisible();

		virtual void MoveTo(vector2df pos, bool hide = false, bool kill = false);
		virtual void FadeTo(GLfloat alpha, bool hide = false, bool kill = false, bool children = false);
		virtual void ScaleTo(vector2df scale, bool hide = false, bool kill = false);

		void SetAlpha(GLfloat alpha, bool changeset = false);
		GLfloat GetAlpha();

		void SetMenu();
		bool IsMenu();

		void SetEnabled(bool enabled = true);
		bool IsEnabled();

		void SetFollowMouse(bool follow = true);

		void SetLabel(Label* l);
		Label* GetLabel();

		virtual void SetTexture(const char* fileName);
		virtual void SetTexture(Texture tex);

		virtual bool KeyRelease(int key);
		virtual bool KeyPress(int key);

		bool IsAnimating();
		void SetAnimationSmoothness(GLfloat smooth);

		void AnchorToWorld(const vector3df& pos);

		virtual void SetRenderBoundary(vector4df boundary);

		const bool operator<(const GUIElement& o) const;
		
		// some basic button functionality
		static void Close(GUIElement* e);
		static void Hide(GUIElement* e);
		static void CloseGame(GUIElement* e);

		static GLuint GetSizeUniPosition();
	
	protected:
		vector2df 		m_Offset;
		GLfloat			m_Depth;
		vector4df		m_RenderBoundary;
		vector4df		m_OriginalRenderBoundary;
		bool			m_bMenu;
		
		virtual void UpdateTransformationMatrix();
		void RenderBoundaryToShader();
		void AddFade();
	
	private:
		GUIElement*					m_pGUIParent;
		std::vector<GUIElement*>	m_GUIChildren;
		GLfloat						m_fAlpha;
		GLfloat						m_fDestAlpha;
		GLfloat						m_fSetAlpha;
		GLfloat						m_fAnimationSmooth;
		bool						m_bEnabled;
		bool						m_bFollowMouse;
		
		void (*m_Function[mouse_fns])(GUIElement*);
		void(*m_UpdateFunction)(GUIElement*);
		bool 				m_bHovered[mouse_fns];
		bool				m_bPressed[mouse_fns];
		bool				m_bClickable;

		vector2df			m_AnimationPosition;
		vector3df			m_AnimationScale;
		bool				m_bHideOnAnimationEnd;
		bool				m_bKillOnAnimationEnd;
		bool				m_bAnimating;

		vector3df			m_WorldAnchor;
		bool				m_bAnchored;

		Label*				m_Label;

		static GLuint		m_SizeUni;
		static GLuint		m_AlphaUni;

		inline void ReverseChildren()
		{
			if (m_GUIChildren.size())
				std::reverse(m_GUIChildren.begin(), m_GUIChildren.end());
		}
};

#endif
