#ifndef _MATRIX_H_
#define _MATRIX_H_

#include "Vector.h"

// 4x4 matrix class
template<typename T>
class Matrix4 {
	public:
		Matrix4() { Identity(); }
		Matrix4(const Matrix4<T> &other) { Set(other); }
		Matrix4(T vals[]) { Set(vals); }
		Matrix4(T val) { Set(val); }
		//Matrix4(T left, T right, T bottom, T top, T near, T far) { Orthographic(left, right, bottom, top, near, far); }
		
		void Reset()
		{
			for (int i = 0; i < SIZE; ++i) {
				matrix[i] = 0;
			}
		}
		void Identity()
		{
			for (int i = 0; i < SIZE; ++i) {
				matrix[i] = 0;
			}
			matrix[0] = 1;
			matrix[5] = 1;
			matrix[10] = 1;
			matrix[15] = 1;
		}
		void Orthographic(T left, T right, T bottom, T top, T near, T far)
		{
			Identity();
			
			T tx = -(right + left) / (right - left);
        	T ty = -(top + bottom) / (top - bottom);
        	T tz = -(far + near) / (far - near);

        	matrix[0] = 2 / (right - left);
        	matrix[5] = 2 / (top - bottom);
        	matrix[10] = -2 / (far - near);
        	matrix[3] = tx;
        	matrix[7] = ty;
        	matrix[11] = tz;
		}
		void Perspective(T angle, T ratio, T near, T far)
		{
			Reset();

			T tanHalf = (T)tan((angle * DEG_TO_RAD) / 2);

			matrix[0] = 1 / (ratio * tanHalf);
			matrix[5] = 1 / tanHalf;
			matrix[10] = -(far + near) / (far - near);
			matrix[11] = -(2 * far * near) / (far - near);
			matrix[14] = -1;
		}
		void Flip()
		{
			T temp;
			for (int i = 0; i < SIZE / 2; ++i) {
				temp = matrix[i];
				matrix[i] = matrix[SIZE - 1 - i];
				matrix[SIZE - 1 - i] = temp;
			}
		}
		void Print()
		{
			for (int i = 0; i < SIZE; ++i) {
				std::cout << matrix[i] << " ";
				if ((i + 1) % 4 == 0)
					std::cout << std::endl;
			}
		}
		void Transpose()
		{
			T temp[SIZE];

			for (int i = 0; i < 4; ++i) {
				temp[i] = matrix[i * 4];
				temp[i + 4] = matrix[i * 4 + 1];
				temp[i + 8] = matrix[i * 4 + 2];
				temp[i + 12] = matrix[i * 4 + 3];
			}

			Set(temp);
		}
		bool Inverse()
		{
			double inv[SIZE], det;
			int i;

			inv[0] = matrix[5] * matrix[10] * matrix[15] -
				matrix[5] * matrix[11] * matrix[14] -
				matrix[9] * matrix[6] * matrix[15] +
				matrix[9] * matrix[7] * matrix[14] +
				matrix[13] * matrix[6] * matrix[11] -
				matrix[13] * matrix[7] * matrix[10];

			inv[4] = -matrix[4] * matrix[10] * matrix[15] +
				matrix[4] * matrix[11] * matrix[14] +
				matrix[8] * matrix[6] * matrix[15] -
				matrix[8] * matrix[7] * matrix[14] -
				matrix[12] * matrix[6] * matrix[11] +
				matrix[12] * matrix[7] * matrix[10];

			inv[8] = matrix[4] * matrix[9] * matrix[15] -
				matrix[4] * matrix[11] * matrix[13] -
				matrix[8] * matrix[5] * matrix[15] +
				matrix[8] * matrix[7] * matrix[13] +
				matrix[12] * matrix[5] * matrix[11] -
				matrix[12] * matrix[7] * matrix[9];

			inv[12] = -matrix[4] * matrix[9] * matrix[14] +
				matrix[4] * matrix[10] * matrix[13] +
				matrix[8] * matrix[5] * matrix[14] -
				matrix[8] * matrix[6] * matrix[13] -
				matrix[12] * matrix[5] * matrix[10] +
				matrix[12] * matrix[6] * matrix[9];

			inv[1] = -matrix[1] * matrix[10] * matrix[15] +
				matrix[1] * matrix[11] * matrix[14] +
				matrix[9] * matrix[2] * matrix[15] -
				matrix[9] * matrix[3] * matrix[14] -
				matrix[13] * matrix[2] * matrix[11] +
				matrix[13] * matrix[3] * matrix[10];

			inv[5] = matrix[0] * matrix[10] * matrix[15] -
				matrix[0] * matrix[11] * matrix[14] -
				matrix[8] * matrix[2] * matrix[15] +
				matrix[8] * matrix[3] * matrix[14] +
				matrix[12] * matrix[2] * matrix[11] -
				matrix[12] * matrix[3] * matrix[10];

			inv[9] = -matrix[0] * matrix[9] * matrix[15] +
				matrix[0] * matrix[11] * matrix[13] +
				matrix[8] * matrix[1] * matrix[15] -
				matrix[8] * matrix[3] * matrix[13] -
				matrix[12] * matrix[1] * matrix[11] +
				matrix[12] * matrix[3] * matrix[9];

			inv[13] = matrix[0] * matrix[9] * matrix[14] -
				matrix[0] * matrix[10] * matrix[13] -
				matrix[8] * matrix[1] * matrix[14] +
				matrix[8] * matrix[2] * matrix[13] +
				matrix[12] * matrix[1] * matrix[10] -
				matrix[12] * matrix[2] * matrix[9];

			inv[2] = matrix[1] * matrix[6] * matrix[15] -
				matrix[1] * matrix[7] * matrix[14] -
				matrix[5] * matrix[2] * matrix[15] +
				matrix[5] * matrix[3] * matrix[14] +
				matrix[13] * matrix[2] * matrix[7] -
				matrix[13] * matrix[3] * matrix[6];

			inv[6] = -matrix[0] * matrix[6] * matrix[15] +
				matrix[0] * matrix[7] * matrix[14] +
				matrix[4] * matrix[2] * matrix[15] -
				matrix[4] * matrix[3] * matrix[14] -
				matrix[12] * matrix[2] * matrix[7] +
				matrix[12] * matrix[3] * matrix[6];

			inv[10] = matrix[0] * matrix[5] * matrix[15] -
				matrix[0] * matrix[7] * matrix[13] -
				matrix[4] * matrix[1] * matrix[15] +
				matrix[4] * matrix[3] * matrix[13] +
				matrix[12] * matrix[1] * matrix[7] -
				matrix[12] * matrix[3] * matrix[5];

			inv[14] = -matrix[0] * matrix[5] * matrix[14] +
				matrix[0] * matrix[6] * matrix[13] +
				matrix[4] * matrix[1] * matrix[14] -
				matrix[4] * matrix[2] * matrix[13] -
				matrix[12] * matrix[1] * matrix[6] +
				matrix[12] * matrix[2] * matrix[5];

			inv[3] = -matrix[1] * matrix[6] * matrix[11] +
				matrix[1] * matrix[7] * matrix[10] +
				matrix[5] * matrix[2] * matrix[11] -
				matrix[5] * matrix[3] * matrix[10] -
				matrix[9] * matrix[2] * matrix[7] +
				matrix[9] * matrix[3] * matrix[6];

			inv[7] = matrix[0] * matrix[6] * matrix[11] -
				matrix[0] * matrix[7] * matrix[10] -
				matrix[4] * matrix[2] * matrix[11] +
				matrix[4] * matrix[3] * matrix[10] +
				matrix[8] * matrix[2] * matrix[7] -
				matrix[8] * matrix[3] * matrix[6];

			inv[11] = -matrix[0] * matrix[5] * matrix[11] +
				matrix[0] * matrix[7] * matrix[9] +
				matrix[4] * matrix[1] * matrix[11] -
				matrix[4] * matrix[3] * matrix[9] -
				matrix[8] * matrix[1] * matrix[7] +
				matrix[8] * matrix[3] * matrix[5];

			inv[15] = matrix[0] * matrix[5] * matrix[10] -
				matrix[0] * matrix[6] * matrix[9] -
				matrix[4] * matrix[1] * matrix[10] +
				matrix[4] * matrix[2] * matrix[9] +
				matrix[8] * matrix[1] * matrix[6] -
				matrix[8] * matrix[2] * matrix[5];

			det = matrix[0] * inv[0] + matrix[1] * inv[4] + matrix[2] * inv[8] + matrix[3] * inv[12];

			if (det == 0)
				return false;

			det = 1.0 / det;

			for (i = 0; i < 16; ++i) {
				matrix[i] = static_cast<T>(inv[i] * det);
			}

			return true;
		}
		void LookAt(Vector3<T>& start, Vector3<T>& end)
		{
			Identity();

			Vector3<T> zaxis = (start - end);
			zaxis.Normal();

			Vector3<T> xaxis(0, 1, 0);
			xaxis.Cross(zaxis);
			xaxis.Normal();

			Vector3<T> yaxis = zaxis;
			yaxis.Cross(xaxis);

			Set(0, xaxis.x);
			Set(1, xaxis.y);
			Set(2, xaxis.z);
			Set(3, -xaxis.Dot(start));
			Set(4, yaxis.x);
			Set(5, yaxis.y);
			Set(6, yaxis.z);
			Set(7, -yaxis.Dot(start));
			Set(8, zaxis.x);
			Set(9, zaxis.y);
			Set(10, zaxis.z);
			Set(11, -zaxis.Dot(start));
		}
		void SetFromQuaternion(const quaternion& q)
		{
			Set(0, (T)(1 - 2 * q.y * q.y - 2 * q.z * q.z));
			Set(1, (T)(2 * q.x * q.y - 2 * q.w * q.z));
			Set(2, (T)(2 * q.x * q.z + 2 * q.w * q.y));
			Set(3, (T)(0));
			Set(4, (T)(2 * q.x * q.y + 2 * q.w * q.z));
			Set(5, (T)(1 - 2 * q.x * q.x - 2 * q.z * q.z));
			Set(6, (T)(2 * q.y * q.z - 2 * q.w * q.x));
			Set(7, (T)(0));
			Set(8, (T)(2 * q.x * q.z - 2 * q.w * q.y));
			Set(9, (T)(2 * q.y * q.z + 2 * q.w * q.x));
			Set(10, (T)(1 - 2 * q.x * q.x - 2 * q.y * q.y));
			Set(11, (T)(0));
			Set(12, (T)(0));
			Set(13, (T)(0));
			Set(14, (T)(0));
			Set(15, (T)(1));
		}
		
		void Set(const Matrix4<T> &other)
		{
			for (int i = 0; i < SIZE; ++i) {
				matrix[i] = other.matrix[i];
			}
		}
		void Set(T vals[])
		{
			for (int i = 0; i < SIZE; ++i) {
				matrix[i] = vals[i];
			}
		}
		void Set(T val)
		{
			for (int i = 0; i < SIZE; ++i) {
				matrix[i] = val;
			}
		}
		void Set(int pos, T val)
		{
			matrix[pos] = val;
		}
		void Add(const Matrix4<T>& other)
		{
			for (int i = 0; i < SIZE; ++i) {
				matrix[i] += other.matrix[i];
			}
		}
		void Subtract(const Matrix4<T>& other)
		{
			for (int i = 0; i < SIZE; ++i) {
				matrix[i] += other.matrix[i];
			}
		}
		Matrix4<T> Multiply(const Matrix4<T>& other) const
		{
			T vals[SIZE];
			for (int i = 0; i < SIZE; ++i) {
				vals[i] = 0;
				for (int j = 0; j < 4; ++j) {
					vals[i] += matrix[(i / 4) * 4 + j] * other.matrix[(i % 4) + (j * 4)];
				}
			}
			
			return Matrix4<T>(vals);
		}
		Vector4<T> Multiply(const Vector4<T>& other) const
		{
			T vals[4];
			for (int i = 0; i < 4; ++i) {
				vals[i] = 0;
				vals[i] += other.x * matrix[i * 4] + other.y * matrix[i * 4 + 1] + other.z * matrix[i * 4 + 2] + other.w * matrix[i * 4 + 3];
			}

			//cout << vals[0] << ", " << vals[1] << ", " << vals[2] << ", " << vals[3] << endl;
			return Vector4<T>(vals);
		}
		void MultiplicationByProduct(const Matrix4<T> &m1, const Matrix4<T> &m2)
		{
			// quicker that calling matrix = matrix1 * matrix2
			for (int i = 0; i < SIZE; ++i) {
				matrix[i] = 0;
				for (int j = 0; j < 4; ++j) {
					matrix[i] += m1.matrix[(i / 4) * 4 + j] * m2.matrix[(i % 4) + (j * 4)];
				}
			}
		}
		
		void Translate(const vector3df& pos)
		{
			matrix[3] += pos.x;
			matrix[7] += pos.y;
			matrix[11] += pos.z;
		}
		void SetTranslation(const vector3df& pos)
		{
			matrix[3] = pos.x;
			matrix[7] = pos.y;
			matrix[11] = pos.z;
		}

		void Scale(const vector3df& scale)
		{
			matrix[0] *= scale.x;
			matrix[5] *= scale.y;
			matrix[10] *= scale.z;
		}

		void SetRotation(const Vector3<T>& vec, const Vector3<T>& off = Vector3<T>())
		{
			//Identity();

			// z rotation matrix
			if (vec.z != 0)
			{
				Matrix4<T> m;
				m.Set(0, cos(vec.z));
				m.Set(1, -sin(vec.z));
				m.Set(4, sin(vec.z));
				m.Set(5, cos(vec.z));
				m.Set(3, off.x - m.matrix[0] * off.x - m.matrix[1] * off.y - m.matrix[2] * off.z);
				m.Set(7, off.y - m.matrix[4] * off.x - m.matrix[5] * off.y - m.matrix[6] * off.z);
				m.Set(11, off.z - m.matrix[8] * off.x - m.matrix[9] * off.y - m.matrix[10] * off.z);
				Set(Multiply(m));
			}

			// x rotation matrix
			if (vec.x != 0)
			{
				Matrix4<T> m;
				m.Set(5, cos(vec.x));
				m.Set(6, -sin(vec.x));
				m.Set(9, sin(vec.x));
				m.Set(10, cos(vec.x));
				m.Set(3, off.x - m.matrix[0] * off.x - m.matrix[1] * off.y - m.matrix[2] * off.z);
				m.Set(7, off.y - m.matrix[4] * off.x - m.matrix[5] * off.y - m.matrix[6] * off.z);
				m.Set(11, off.z - m.matrix[8] * off.x - m.matrix[9] * off.y - m.matrix[10] * off.z);
				Set(Multiply(m));
			}

			// y rotation matrix
			if (vec.y != 0)
			{
				Matrix4<T> m;
				m.Set(0, cos(vec.y));
				m.Set(2, sin(vec.y));
				m.Set(8, -sin(vec.y));
				m.Set(10, cos(vec.y));
				m.Set(3, off.x - m.matrix[0] * off.x - m.matrix[1] * off.y - m.matrix[2] * off.z);
				m.Set(7, off.y - m.matrix[4] * off.x - m.matrix[5] * off.y - m.matrix[6] * off.z);
				m.Set(11, off.z - m.matrix[8] * off.x - m.matrix[9] * off.y - m.matrix[10] * off.z);
				Set(Multiply(m));
			}
		}
		
		const T *GetDataPointer() const { return &matrix[0]; }
		
		void operator=(const Matrix4<T> &other) { Set(other); }
		//Matrix4<T> operator+(const Matrix4<T> &other) { return Matrix4<T>(x + other.x, y + other.y); }
		//Matrix4<T> operator-(const Matrix4<T> &other) { return Matrix4<T>(x - other.x, y - other.y); }
		void operator+=(const Matrix4<T> &other) { Add(other); }
		void operator-=(const Matrix4<T> &other) { Subtract(other); }
		void operator*=(const Matrix4<T> &other) { Set(Multiply(other)); }
		Matrix4<T> operator*(const Matrix4<T> &other) { return Multiply(other); }
		bool operator==(const Matrix4<T>& other) const
		{
			for (int i = 0; i < SIZE; ++i) {
				if (matrix[i] != other.matrix[i])
					return false;
			}

			return true;
		}
		bool operator!=(const Matrix4<T>& other) const { return !(*this == other); }
	
	protected:
	
	private:
		static const int SIZE = 16;
		T matrix[SIZE];
};

typedef Matrix4<GLint>		matrix4i;
typedef Matrix4<GLshort>	matrix4s;
typedef Matrix4<GLfloat>	matrix4f;

#endif
