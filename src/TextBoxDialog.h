#ifndef _TEXTBOX_DIALOG_H_
#define _TEXTBOX_DIALOG_H_

#include "GUIElement.h"

class TextBoxDialog : public GUIElement {
public:
	TextBoxDialog(Manager* parent, string title, string text, void(*fny)(GUIElement*) = NULL, void(*fnn)(GUIElement*) = NULL);

	virtual void MouseDownFunction(GLuint button);

	virtual void ScaleTo(vector2df scale, bool hide = false, bool kill = false);

protected:

private:

};

#endif