#ifndef _ITEM_H_
#define _ITEM_H_

#include "GLObject.h"
#include "Mesh.h"

//----------------------------------------------------------------------------\\
// Items can be customised by the player through the editing of the XML file. \\
// Each item entry will have a number of things listed, varying depending on  \\
// what type of item it is. This should allow for easy item modding into the  \\
// game - simply copy an existing item, tweak the values and the player will  \\
// be able to start finding the item in game (based on spawn conditions).     \\
//----------------------------------------------------------------------------\\

enum ItemID {
	NONE,
	SELECTABLE
};

enum ItemType {
	POINTLESS,		// useless crap
	BLOCK,			// placeable blocks
	TOOL,			// tools
	WEAPON,			// weapons
	ARMOUR,			// armour
	EQUIPPABLE,		// equippables - stuff like rings
	CONSUMABLE,		// consumables - stuff like potions
	MISC,			// misc - stuff like ingots
	AMMO,			// ammo - arrows, bullets, potatoes
	PROJECTILE,		// projectile - misc projectiles, magic missiles

	NO_TYPE
};

enum Rarity {
	JUNK,			// grey
	NORMAL,			// white
	UNCOMMON,		// blue
	SPECIAL,		// cyan
	RARE,			// green
	SET,			// yellow
	LEGENDARY,		// orange
	GODLIKE,		// crimson
	INCONCEIVABLE	// black (inverted)
};

enum SpawnType {
	MOB_DROP,		// will be dropped by mobs
	BLOCK_DROP,		// will be dropped by blocks
	CHEST,			// can be found in chests
	CRAFTED,		// can be crafted
	BOSS_DROP,		// will be dropped by bosses
	QUEST_REWARD	// will be given as a quest reward
};

enum SubType {
	TERRAIN,
	TERRAIN_GRAVITY,
	TREE,
	WOOD,
	PICKAXE,
	AXE,
	MELEE,
	RANGED,
	MAGIC,
	SUMMON,
	INGOT,
	DOOR,
	ARROW,
	BULLET,
	HELMET,
	CHESTPIECE,
	GLOVES,
	GREAVES,
	BOOTS,

	NO_SUBTYPE
};

// apparently VS2012 (was it?) allowed us to forward declare an enum
// but trying to compile on Linux was a no, so I've moved this here...
enum InventoryResult {
	// we failed to add the item to the inventory
	INV_FAIL,

	// we managed to add part of the stack
	INV_PARTIAL,

	// we managed to add all the item
	INV_SUCCESS
};

struct ItemClass {
	string						strName;
	string						strDescription;
	ItemType					Type;
	short						sStackSize;
	Rarity						rRarity;
	short						sValues[10];
	float						fValues[10];
	vector<Mesh*>				Meshes;
	SpawnType					SpawnCondition;
	string						strTexturePath;
	SubType						TypeSub;
	vector4df					TerrainUV[6];
	bool						bBlockHasMesh;
	vector3di					BlockSize;
	vector3di					LightColour;
	bool						bSolid;
	bool						bAnimated;
	vector<ParticleDefinition>	Particles;
	vector<pair<string, short>>	Drops;
	vector<AnimDetails>			anims;
	unsigned int				index;
	SubType						AmmoType;
	string						strProjectileType;

	ItemClass()
	{
		strName = "NULL_ITEM";
		strDescription = " ";
		Type = POINTLESS;
		sStackSize = 0;
		rRarity = JUNK;
		SpawnCondition = MOB_DROP;
		strTexturePath = "";
		TypeSub = TERRAIN;
		bBlockHasMesh = false;
		BlockSize = vector3di(1);
		LightColour = vector3di(0);
		bSolid = true;
		bAnimated = false;
		Particles.clear();
		index = 0;
		AmmoType = NO_SUBTYPE;
		strProjectileType = "";

		for (int i = 0; i < 10; i++) {
			sValues[i] = 0;
			fValues[i] = 0.f;
		}
	}

	void operator=(ItemClass* other)
	{
#ifdef _WIN32
		_ASSERT(other != NULL);
#endif

		strName = other->strName;
		strDescription = other->strDescription;
		Type = other->Type;
		sStackSize = other->sStackSize;
		rRarity = other->rRarity;
		SpawnCondition = other->SpawnCondition;
		strTexturePath = other->strTexturePath;
		TypeSub = other->TypeSub;
		bBlockHasMesh = other->bBlockHasMesh;
		BlockSize = other->BlockSize;
		LightColour = other->LightColour;
		bSolid = other->bSolid;
		bAnimated = other->bAnimated;
		index = other->index;
		AmmoType = other->AmmoType;
		strProjectileType = other->strProjectileType;

		for (GLuint i = 0; i < other->Meshes.size(); ++i) {
			Meshes.push_back(other->Meshes[i]);
		}

		Particles.clear();
		for (GLuint i = 0; i < other->Particles.size(); i++) {
			Particles.push_back(other->Particles[i]);
		}

		for (int i = 0; i < 6; i++) {
			TerrainUV[i] = other->TerrainUV[i];
		}

		for (int i = 0; i < 10; i++) {
			sValues[i] = other->sValues[i];
			fValues[i] = other->fValues[i];
		}

		anims.clear();
		for (GLuint i = 0; i < other->anims.size(); i++) {
			anims.push_back(other->anims[i]);
		}
	}
};

struct TerrainBlock;
class Player;

class Item : public GLObject {
	public:
		Item(Manager* parent, string type, short amount = 1, vector3df pos = vector3df());
		Item(Manager* parent, unsigned int type, short amount = 1, vector3df pos = vector3df());
		Item(Item* other, bool visible = true);

		static bool	LoadItems(string file, string type, SceneManager* pSceneMan);
		static void SortItems();
		static void	UnloadItems();
		static bool	ReloadItems(string file, string type, SceneManager* pSceneMan);
		static const ItemClass*	GetItemClass(string name);
		static const ItemClass* GetItemClass(unsigned int n);
		static const ItemClass* GetItemClass(const TerrainBlock& n);
		static const GLuint GetBlockFromName(string name);
		static const GLColour GetBlockMapColour(TerrainBlock b);

		bool				SetStackSize(short size = -1);
		bool				IncreaseStackSize(short amount);
		bool				DecreaseStackSize(short amount);
		short				GetStackSize();
		short				GetMaxStackSize();
		short				GetDamage();

		unsigned short		GetBlockType();

		string				GetWeaponType() const;
		AnimDetails			GetAnimation() const;
		string				GetWeaponSpeed() const;
		string				GetKnockbackStrength() const;

		ItemType			GetType() const;
		SubType				GetSubType() const;

		bool operator==(Item& other);
		bool operator==(const ItemClass& other);

		bool IsServerItem(const ItemData& other);

		string GetName();
		string GetDescription();
		unsigned int GetIndex();

		void WriteTo(vector<unsigned char>& out);

		virtual void Update(bool bForce = false);

	protected:
		static std::map<string, ItemClass*, StringCompare>		ListOfItems;
		static std::vector<ItemClass*>							VectorOfItems;
		static std::vector<ItemClass*>							ListOfBlocks;
		static std::vector<GLColour>							ListOfBlockColours;

	private:
		ItemClass	m_ItemClass;
		short		m_sCurrentStack;
		Player*		m_pPlayer;
};

#endif
