#ifndef _PROJECTILE_H_
#define _PROJECTILE_H_

#include "Item.h"
#include "Misc.h"

class ParticleEmitter;

class Projectile : public GLObject {
	public:
		Projectile(Manager* parent, const ProjectileData& data, bool mob = false);

		virtual void Update(bool bForce = false);

	protected:

	private:
		ProjectileData				m_Data;
		const ItemClass*			m_ItemClass;
		GLfloat						m_fGravity;
		quaternion					m_Rotation;
		vector<ParticleEmitter*>	m_Emitters;
		string						m_TextureToLoad;
		bool						m_bMobCreated;
		GLfloat						m_fLife;
};

#endif