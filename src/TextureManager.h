#ifndef _TEXTUREMANAGER_H_
#define _TEXTUREMANAGER_H_

#include <libpng/png.h>
#include <map>
#include <GL/glew.h>
#include <string>

using std::string;
using std::map;

struct StringCompare {
	bool operator()(const string& a, const string& b) const
	{
		const int al = a.length();
		const int bl = b.length();
		if (al != bl)
			return al < bl;

		return a < b;
	}
};

struct Texture {
	GLuint gltex;
	GLuint width;
	GLuint height;

	Texture() : gltex(0), width(0), height(0) {}
	Texture(GLuint t, GLuint w, GLuint h) : gltex(t), width(w), height(h) {}
};

class TextureManager {
	public:
		Texture GetTexture(string fileName);
		Texture CreateTexture(string name, GLint width, GLint height, GLubyte* data);
		
		TextureManager();
		~TextureManager();
		
		void CleanUp();
		void DeleteTexture(string name);
	
	protected:
	
	private:
		map<string, Texture, StringCompare>	m_Textures;
};

#endif
