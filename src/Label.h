#ifndef _LABEL_H_
#define _LABEL_H_

#include "GUIElement.h"

class Label : public GUIElement {
	public:
		Label(Manager* parent, string title, string text, GUIElement* e);

		void SetElement(GUIElement* e);

		virtual void MouseDownFunction(GLuint button);

		virtual void Update(bool bForce = false);

	protected:

	private:
		GUIElement*	m_Element;
};

#endif