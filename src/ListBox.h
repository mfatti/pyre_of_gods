#ifndef _LIST_BOX_H_
#define _LIST_BOX_H_

#include "GUIElement.h"
#include "Text.h"
#include "Scrollbar.h"

class ListBox : public GUIElement {
	public:
		ListBox(Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size);

		virtual bool IsHovered();
		virtual void MouseDownFunction(GLuint button);

		void AddItem(string text, bool remake = true);
		void ChangeItem(GLuint pos, string text);
		void RemoveItem(GLuint pos);
		string GetSelectedText();
		GLuint GetSelected();
		string GetItem(GLuint pos);

		void ClearList();
		void SetDepth(GLfloat depth);

		void SetVisible(bool visible);
		void SetBackgroundAlpha(GLubyte alpha);

		void Search(string s);
		void ClearSearch();

		void Create();
		void Select(GLuint i);

		bool ItemExists(string item);
		GLuint GetFirstItem(string item);

		static void DefaultFunction(GUIElement* e);

	protected:
		virtual void SetRenderBoundary(vector4df boundary);

	private:
		GLuint		m_Selected;
		GLuint		m_ItemCount;
		Scrollbar*	m_Scrollbar;
		Text*		m_Items;
		GLfloat		m_ItemSpacing;
		string		m_ItemsString;
		GLfloat		m_ListHeight;
};

#endif