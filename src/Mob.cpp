#include "Mob.h"
#include "SceneManager.h"
#include "XMLReader.h"
#include "ProgressBar.h"

std::map<string, MobClass*> Mob::ListOfMobs;
std::vector<MobClass*> Mob::IntListOfMobs;

bool Mob::LoadMobs(string file, string type, SceneManager* pSceneMan)
{
	// create an XML file reader
	XMLReader XML(file);
	if (!XML.IsOpen())
		return false;

	// read the file
	const string strMobsTag("mobs");
	string strCurrentSection, strNodeName;
	MobClass *mob = NULL;
	bool bLoaded = false;

	while (XML.ReadLine())
	{
		if (XML.IsNode(strMobsTag))
			strCurrentSection = strMobsTag;
		else if (strCurrentSection == strMobsTag && XML.IsNode(type))
			strCurrentSection = type;
		else if (strCurrentSection == type)
		{
			// check if we have reached the end of the item
			if (XML.IsEndOfNode(strCurrentSection))
			{
				strCurrentSection = strMobsTag;

				continue;
			}

			// look for what we need
			strNodeName = XML.GetNodeName();
			if (strNodeName == "name")
			{
				mob = new MobClass();
				mob->strName = XML.GetAttributeValue("value");

				// make sure this item doesn't already exist
				if (ListOfMobs[mob->strName] != NULL)
				{
					delete mob;
					mob = NULL;

					strCurrentSection = strMobsTag;
				}
				else
				{
					ListOfMobs[mob->strName] = mob;
					bLoaded = true; // we have loaded at least one so return true
					mob->index = IntListOfMobs.size();
					IntListOfMobs.push_back(mob);
				}
			}
			else if (strNodeName == "mesh_path")
			{
				string strMesh = "";
				strMesh.append(XML.GetAttributeValue("value"));

				if (strMesh.substr(strMesh.length() - 4, 4) == ".obj")
					mob->pMesh = pSceneMan->LoadMeshFromOBJ(strMesh);
				else
					mob->pMesh = pSceneMan->LoadMeshFromAOBJ(strMesh);
			}
			else if (strNodeName == "texture_path")
			{
				string strTexture = "";
				strTexture.append(XML.GetAttributeValue("value"));
				mob->strTexturePath = strTexture;
			}
			else if (strNodeName == "damage")
			{
				mob->sDamage = atoi(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "health")
			{
				mob->iHealth = atoi(XML.GetAttributeValue("value").c_str());
			}
			else if (strNodeName == "scale")
			{
				float x = (float)atof(XML.GetAttributeValue("x").c_str());
				float y = (float)atof(XML.GetAttributeValue("y").c_str());
				float z = (float)atof(XML.GetAttributeValue("z").c_str());
				mob->Scale.Set(x, y, z);
			}
			else if (strNodeName == "animation")
			{
				string evt = XML.GetAttributeValue("event");
				string name = XML.GetAttributeValue("name");
				GLfloat speed = (float)atof(XML.GetAttributeValue("speed").c_str());
				if (!evt.empty() && !name.empty())
				{
					if (mob->anims.find(evt) == mob->anims.end())
					{
						vector<AnimDetails> evtanims;
						mob->anims.emplace(evt, evtanims);
					}
					
					AnimDetails details;
					details.name = name;
					details.speed = (speed > 0 ? speed : 1.f);
					mob->anims.at(evt).push_back(details);
				}
			}
			else if (strNodeName == "class")
			{
				string style = XML.GetAttributeValue("value");
				if (style == "ranged")
					mob->style = ranged;
			}
		}
	}

	return bLoaded;
}

void Mob::UnloadMobs()
{
	for (std::map<string, MobClass*>::iterator it = ListOfMobs.begin(); it != ListOfMobs.end(); ++it) {
		delete it->second;
	}
	ListOfMobs.clear();
	IntListOfMobs.clear();
}

bool Mob::ReloadMobs(string file, string type, SceneManager* pSceneMan)
{
	UnloadMobs();
	return LoadMobs(file, type, pSceneMan);
}

const MobClass* Mob::GetMobClass(string name)
{
	return ListOfMobs[name];
}

const MobClass* Mob::GetMobClass(int id)
{
	return IntListOfMobs[id];
}

bool Mob::MobsLoaded()
{
	return !IntListOfMobs.empty();
}

Mob::Mob(Manager* parent, string type, vector3df pos)
	: GLObject(parent, pos, vector3di())
	, m_bCreated(false)
	, m_Animation(NULL)
	, m_CurrentEvtAnim("")
	, m_GUIHealthBar(NULL)
	, m_fTimeout(0.f)
{
	// set the mob class
	m_MobClass = ListOfMobs[type];
	SetUseBones(true);

	SetAABB(vector3df(-5.f, -5.f, -5.f), vector3df(5.f, 5.f, 5.f));
	
	m_pTerrain = GetSceneManager()->GetFirstInstance<Terrain>();
}

Mob::Mob(Manager* parent, int type, vector3df pos)
	: GLObject(parent, pos, vector3di())
	, m_bCreated(false)
	, m_Animation(NULL)
	, m_CurrentEvtAnim("")
	, m_GUIHealthBar(NULL)
	, m_fTimeout(0.f)
	, m_fServerTimeout(0.f)
{
	// set the mob class
	m_MobClass = IntListOfMobs[type];
	SetUseBones(true);

	SetAABB(vector3df(-5.f, -5.f, -5.f), vector3df(5.f, 5.f, 5.f));
	
	m_pTerrain = GetSceneManager()->GetFirstInstance<Terrain>();
}

MobData& Mob::GetMobData()
{
	return m_MobData;
}

void Mob::AttachHealthBar(ProgressBar* hpbar)
{
	m_GUIHealthBar = hpbar;
}

string Mob::GetName() const
{
	return m_MobClass.strName;
}

void Mob::MobSeen()
{
	m_fServerTimeout = 0.f;
}

void Mob::Update(bool bForce)
{
	// set the mesh / texture if needs be
	if (!m_bCreated)
	{
		SetMesh(m_MobClass.pMesh, false);
		SetTexture(m_MobClass.strTexturePath.c_str());
		SetScale(m_MobClass.Scale);

		m_Animation.SetMesh(GetMesh());

		m_bCreated = true;
	}

	// update our position based on our data position
	vector3df pos = GetPosition();

	// rotate the mob
	GLfloat rot = -GetRotation().y;
	if (m_MobData.rot - rot > PI)
		rot += TWO_PI;
	else if (rot - m_MobData.rot > PI)
		rot -= TWO_PI;
	Interpolate(rot, m_MobData.rot, 4.5f, 0.001f);
	SetRotation(vector3df(0, 1, 0), -rot);

	// interpolate position
	Interpolate(pos.x, m_MobData.pos.x, 8.5f, 0.01f);
	Interpolate(pos.y, m_MobData.pos.y, 8.5f, 0.01f);
	Interpolate(pos.z, m_MobData.pos.z, 8.5f, 0.01f);
	SetPosition(pos);

	// visibility
	m_fServerTimeout += 100.f * DeltaTime::GetDelta();
	if (IsVisible())
	{
		if (pos.QuickDistance(GetSceneManager()->GetActiveCamera()->GetPosition()) > 50000.f)
		{
			SetVisible(false);
			m_GUIHealthBar->SetVisible(false);
		}
	}
	else
	{
		m_fTimeout += 100.f * DeltaTime::GetDelta();

		if (m_fTimeout >= 100.f || m_fServerTimeout >= 100.f)
		{
			// we haven't seen this mob in a long time, kill it
			m_MobData.health = 0;
			return;
		}
		
		if (pos.QuickDistance(GetSceneManager()->GetActiveCamera()->GetPosition()) <= 50000.f)
		{
			SetVisible(true);
			m_GUIHealthBar->SetVisible(true);

			m_fTimeout = 0.f;
		}
	}

	// health bar visibility
	if (m_GUIHealthBar->IsVisible() && m_MobData.health == m_MobData.maxhealth)
	{
		m_GUIHealthBar->SetVisible(false);
	}
	else if (!m_GUIHealthBar->IsVisible() && m_MobData.health < m_MobData.maxhealth)
	{
		m_GUIHealthBar->SetVisible(IsVisible());
	}

	// set health bar
	if (m_GUIHealthBar)
		m_GUIHealthBar->SetPosition(GetSceneManager()->ConvertToScreenCoordinates(pos + vector3df(0, 18, 0)) - vector2df(50, 0));

	// update our animations
	bool bAttacking = GetBit(m_MobData.flags, 0);
	if (!bAttacking)
	{
		// do the movement animation
		ChooseAnimation("move");
	}
	else
	{
		// choose an attacking animation
		ChooseAnimation("attack", false);
	}

	// animate ourselves
	Animate(&m_Animation);

	// call GLObject::Update()
	GLObject::Update(bForce);

	// lighting
	SceneManager* pSceneMan = GetSceneManager();
	TerrainBlock b = m_pTerrain->GetBlock(pos + vector3df(0, 15.f, 0));
	GLfloat sun = pSceneMan->GetSun();
	SetLightAmount(vector3df(max((GLfloat)b.LightRed, b.SkyLight * sun) * 0.066667f, max((GLfloat)b.LightGreen, b.SkyLight * sun) * 0.066667f, max((GLfloat)b.LightBlue, b.SkyLight * sun) * 0.066667f));
}

void Mob::ChooseAnimation(string evt, bool loop)
{
	// choose an animation from the possible list
	if (!evt.empty() && m_MobClass.anims.find(evt) != m_MobClass.anims.end())
	{
		if (m_Animation.GetAnimationStatus() != animation_running || (!loop && m_CurrentEvtAnim != evt))
		{
			vector<AnimDetails>& anims = m_MobClass.anims.at(evt);
			int index = 0;
			if (anims.size() > 1)
				index = rand() % anims.size();

			m_Animation.SetAnimation(anims.at(index).name);
			m_Animation.SetAnimationSpeed(anims.at(index).speed);

			m_Animation.SetAnimationLoop(loop);
			m_Animation.Start();
			m_CurrentEvtAnim = evt;
		}
	}
}
