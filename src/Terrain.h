#ifndef _TERRAIN_H_
#define _TERRAIN_H_

#include "TerrainChunk.h"

typedef std::unordered_map<vector3di, TerrainChunk*> TerrainMap;

class Terrain : public GLObject {
	public:
		Terrain(Manager* parent);
		~Terrain();

		void TerrainThreadTask();

		GLfloat GetHeightBelow(vector3df pos);
		GLfloat GetFreeSpaceBelow(vector3df pos);
		GLfloat GetDistanceAbove(vector3df pos);

		//vector<GLfloat>& GetNoise(vector2di pos);

		bool DoesPointCollide(vector3df& p);
		bool DoesRayCollide(line3df& l, vector3df& oo, GLfloat& out);

		//bool SetBlock(vector3df pos, TerrainBlock type, bool bConstruct = true);
		bool ChangeBlock(vector3df pos, TerrainBlock type);
		void ProcessChangeBlock(unsigned char* buf, int size);
		void ClearSlopes(vector3df pos);
		TerrainBlock GetBlock(vector3df pos);
		TerrainBlock GetBlockBelow(vector3df pos, GLfloat& height, bool bWater = false);
		GLfloat GetWater(vector3df pos);
		bool Underwater(vector3df pos);

		TerrainBlock _GetBlock(int x, int y, int z, const vector3di& chunkPos);
		TerrainBlock _GetBlock(const vector3di& blockPos, const vector3di& chunkPos);
		TerrainBlock _GetBlock(int x, int y, int z);
		TerrainBlock& _BlockAt(const vector3di& blockPos, const vector3di& chunkPos);
		TerrainBlock* _BlockAt(vector3di& blockPos, vector3di& chunkPos, TerrainChunk** out, bool bLockMutex = true);
		GLfloat _GetWater(int x, int y, int z, const vector3di& chunkPos);

		void Interact(unsigned char* buf);
		void SendInteraction(vector3df pos, bool open);

		GLuint GetSeed() const;

		void SetBlockLight(const vector3di& blockPos, const vector3di& chunkPos, GLubyte red, GLuint green, GLuint blue, bool bUpdate = true);
		void RemoveBlockLight(const vector3di& blockPos, const vector3di& chunkPos, bool bUpdate = true);
		void SetSunLight(const vector3di& blockPos, const vector3di& chunkPos, GLubyte val, bool bUpdate = true);
		void RemoveSunLight(const vector3di& blockPos, const vector3di& chunkPos, bool bUpdate = true);
		void DoSunlightUpdate();

		void UpdateTerrainIndices(GLint dir);

		void UpdateChunksAroundChunk(const vector3di& chunkPos);

		virtual void Update(bool bForce = false);
		virtual void Render();
		virtual void RenderShadows();

		static void RenderWater(GLObject* o);

		void ConvertToChunkBlockPosition(const vector3df& pos, vector3di& blockPos, vector3di& chunkPos) const;

		void ManageUnseenChunks();
		void UpdateTerrainWater(const vector<TerrainChunk*>& chunks);

		void _DEBUG_SetBlock(vector3df pos, TerrainBlock block);
		void _DEBUG_SetWater(vector3df pos, GLfloat water);

		static void StopThreads();

	protected:
		void DoChunk(const vector3di& c);
		void UpdateChunks(vector3di& chunk, int x, int y, int z);

	private:
		Player*							m_pPlayer;
		//PerlinNoise						m_Noise;

		TerrainMap						m_Terrain;
		map<vector2di, vector<GLfloat>> m_TerrainNoise;
		vector<TerrainChunk*>			m_ChunksToDelete;

		bool							m_bDoSunlightUpdate;

		std::thread*					m_pTerrainThread;
		static bool						m_bThreadRunning;
		TerrainChunk*					m_pChunkToUpdate;
		GLuint							m_TerrainSeed;
		vector<TerrainChunk*>			m_Chunks;
		GLuint							m_ChunkDistance;
		size_t							m_ChunksCount;

		void GetChunksAroundPlayer(vector<TerrainChunk*>& v, bool bCreateNew = false, bool bForRender = false);
		void GetChunksNotAroundPlayer(vector<TerrainChunk*>& v);
		std::list<vector3di>* GetMultiBlockList(vector3di block, vector3di chunk, TerrainChunk*& pChunk);

		bool GetPositionInChunk(vector3df& pos, TerrainChunk*& chunk, vector3di& out, vector3di& chunkPos);
		TerrainChunk* GetChunk(const vector3di& pos, bool bLockMutex = true);
		void PositionCorrect(vector3di& blockPos, vector3di& chunkPos);

		// lighting
		struct LightNode {
			//vector3df pos;
			vector3di blockPos;
			vector3di chunkPos;
			GLuint red;
			GLuint green;
			GLuint blue;

			LightNode(vector3di bp, vector3di cp, GLuint r, GLuint g, GLuint b) : blockPos(bp), chunkPos(cp), red(r), green(g), blue(b) {}
		};

		struct SunlightNode {
			//vector3df pos;
			vector3di blockPos;
			vector3di chunkPos;
			GLuint val;

			SunlightNode(vector3di bp, vector3di cp, GLuint v) : blockPos(bp), chunkPos(cp), val(v) {}
		};

		queue<LightNode>	m_BlockLightCreation;
		queue<LightNode>	m_BlockLightDeletion;
		queue<SunlightNode>	m_SunLightCreation;
		queue<SunlightNode>	m_SunLightDeletion;

		TerrainChunk* _SetBlockLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint red, GLuint green, GLuint blue);
		void UpdateLightCreation(bool bUpdate = true);
		void UpdateLightDeletion(bool bUpdate = true);
		TerrainChunk* _SetSunLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint val);
		void UpdateSunlightCreation(bool bUpdate = true);
		void UpdateSunlightDeletion(bool bUpdate = true);
};

#endif
