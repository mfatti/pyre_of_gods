#ifndef _MOB_MANAGER_H_
#define _MOB_MANAGER_H_

#define MOB_BUFFER_SIZE 1000

#include "Mob.h"

class SceneManager;
class ProgressBar;

class MobManager {
	public:
		MobManager(SceneManager* sceneman);
		~MobManager();

		void Update();

		void ReceiveData(unsigned char* buffer, int rlen);

		const vector<Mob*>* GetMobs() const;

	protected:

	private:
		vector<Mob*>			m_Mobs;
		vector<ProgressBar*>	m_GUIMobHealthBars;
		SceneManager*			m_pSceneMan;

		unsigned char*			m_MsgBuffer;
		unsigned char*			m_Buffer;
		int						m_RLen;
};

#endif