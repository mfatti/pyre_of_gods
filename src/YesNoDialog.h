#ifndef _YES_NO_DIALOG_H_
#define _YES_NO_DIALOG_H_

#include "GUIElement.h"

class YesNoDialog : public GUIElement {
	public:
		YesNoDialog(Manager* parent, string title, string text, void(*fny)(GUIElement*) = NULL, void(*fnn)(GUIElement*) = NULL);
		
		virtual void MouseDownFunction(GLuint button);

		virtual void ScaleTo(vector2df scale, bool hide = false, bool kill = false);

	protected:

	private:

};

#endif