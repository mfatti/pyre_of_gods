#ifndef _RECIPE_BOOK_H_
#define _RECIPE_BOOK_H_

#include "Item.h"

class ListBox;
class DropDownMenu;

struct Recipe {
	string							name;
	map<const ItemClass*, int>		ingredients;
	const ItemClass*				produce;
	int								produceAmount;
	unsigned short					craftingStation;

	Recipe()
	{
		name = "";
		produceAmount = 0;
		craftingStation = 0;
	}
};

class RecipeBook {
	public:
		RecipeBook();
		~RecipeBook();

		bool LoadRecipes(string file);
		bool LoadRecipePages(string file, DropDownMenu* menu);

		void AddRecipesToListBox(string page, ListBox* lb);

		Recipe* GetRecipe(string name);

	protected:

	private:
		map<string, map<string, bool>>	m_RecipePages;
		map<string, Recipe*>			m_Recipes;
};

#endif