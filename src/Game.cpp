#include <iostream>
//#include <unistd.h>
#include "Game.h"
#include "InputManager.h"
#include "Pane.h"
#include "ShaderStrings.h"
#include <sstream>
#include "Mesh.h"

Game::Game()
	: m_pWindow(NULL)
	, m_pSceneMan(NULL)
	, m_pGUIMan(NULL)
	, m_pClientMan(NULL)
	, m_pTexMan(NULL)
	, m_pGameGUI(NULL)
	, m_pMenuGUI(NULL)
	, m_Settings("configofgods.ini")
	, m_pNetworkMan(NULL)
	, m_bStartingGame(false)
	, m_bStartingMenu(true)
	, m_PlayerFilename("")
	, m_PlayerName("")
	, m_MenuMessage("")
	, m_bPerPixel(false)
	, m_bNormalMapping(false)
	, m_bShadows(false)
{
	// key double press states
	for (GLuint i = 0; i < GLFW_KEY_LAST; ++i) {
		m_bDoublePress[i] = false;
		m_fKeyReset[i] = 0.f;
	}
}

Game::~Game()
{
	// destroy the window if it exists
	if (m_pWindow)
		glfwDestroyWindow(m_pWindow);

	// clear up shaders
	GLObject::ClearShaderPrograms();

	// end glfw
	glfwTerminate();
}

bool Game::Create()
{
	// initialise glfw
	if (!glfwInit())
		return false;

	// read in the screen resolution settings
	int WIDTH = m_Settings.GetValueAsInt("width");
	int HEIGHT = m_Settings.GetValueAsInt("height");
	SetScreenSize(WIDTH, HEIGHT);

	// decide whether we are fullscreen or not
	bool bFullscreen = m_Settings.GetValueAsBool("fullscreen");

	// create the window
	glfwWindowHint(GLFW_RESIZABLE, 0);
	glfwWindowHint(GLFW_DECORATED, !m_Settings.GetValueAsBool("borderless"));
	glfwWindowHint(GLFW_VERSION_MAJOR, 1);
	glfwWindowHint(GLFW_VERSION_MINOR, 3);
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	m_pWindow = glfwCreateWindow(WIDTH, HEIGHT, "Pyre Of Gods", bFullscreen ? glfwGetPrimaryMonitor() : NULL, NULL);
	if (!m_pWindow)
	{
		glfwTerminate();
		return false;
	}
	
	// set up input callback functions
	InputManager::GetInstance().AssignToGame(this);
	glfwSetMouseButtonCallback(m_pWindow, InputManager::MouseCallback);
	glfwSetKeyCallback(m_pWindow, InputManager::KeyCallback);
	glfwSetCharCallback(m_pWindow, InputManager::CharCallback);
	glfwSetScrollCallback(m_pWindow, InputManager::ScrollCallback);

	// make the window's context current
	glfwMakeContextCurrent(m_pWindow);

	// vsync
	glfwSwapInterval(m_Settings.GetValueAsBool("vsync"));

	// advanced video settings
	m_bPerPixel = m_Settings.GetValueAsBool("perpixel");
	m_bNormalMapping = m_Settings.GetValueAsBool("normalmaps");
	m_bShadows = m_Settings.GetValueAsInt("shadows") > 0;

	// set the window position if we're not in fullscreen mode
	if (!bFullscreen)
	{
		GLFWmonitor* primary = glfwGetPrimaryMonitor();
		if (primary)
		{
			const GLFWvidmode* mode = glfwGetVideoMode(primary);
			if (mode)
			{
				glfwSetWindowPos(m_pWindow, (int)((mode->width - WIDTH) * 0.5), (int)((mode->height - HEIGHT) * 0.5));
			}
		}
	}

	// attempt to initialise glew
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
		return false;
		
	// allow transparency in the textures
    glEnable(GL_BLEND);
	//glEnable(GL_DEPTH);
	glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthFunc(GL_LEQUAL);

	// create the shader programs - the first one is the shader for 3d rendering, the second is the GUI shader, the third is for shadows, the fourth is for animated 3d rendering, the fifth is for animated shadows
	GLShaderProgram *shader = new GLShaderProgram(ShaderStrings::GetVertexShader(false, m_bPerPixel, m_bNormalMapping, m_bShadows), ShaderStrings::GetFragmentShader(false, m_bPerPixel, m_bNormalMapping, m_bShadows));
	GLObject::AddShaderProgram(shader);
	
	// get position for matrices
	shader->SetPerspectiveProjection(85, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 3800);

	// sort out the multiple textures
	GLObject::GetShaderProgram(0)->Bind();
	glUniform1i(GLObject::GetShaderProgram(0)->GetUniformLocation("texImage"), 0);
	glUniform1i(GLObject::GetShaderProgram(0)->GetUniformLocation("shadowMap"), 1);
	glUniform1i(GLObject::GetShaderProgram(0)->GetUniformLocation("normalMap"), 2);
	GLObject::GetShaderProgram(0)->Unbind();

	// GUI shader program
	const GLchar *strVertexShaderGUI =
		"#version 130\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"uniform mat4 model;\n"
		"uniform mat4 projection;\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    gl_Position = projection * model * vec4(position, 1.0);\n"
		"}";

	const GLchar *strFragmentShaderGUI =
		"#version 130\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"uniform sampler2D texImage;\n"
		"uniform vec4 size;\n"
		"uniform float alpha;\n"
		"void main() {\n"
		"    if (gl_FragCoord.x < size.x || gl_FragCoord.x > size.z || gl_FragCoord.y < size.y || gl_FragCoord.y > size.w) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    textureColour.a *= alpha;\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    gl_FragColor = vertexColour * textureColour;\n"
		"}";

	GLShaderProgram* shaderGUI = new GLShaderProgram(strVertexShaderGUI, strFragmentShaderGUI);
	GLObject::AddShaderProgram(shaderGUI);

	// get position for matrices
	shaderGUI->SetOrthoProjection(WIDTH, HEIGHT);

	// shadow shader program
	const GLchar *strVertexShaderShadow =
		"#version 130\n"
		"attribute vec3 position;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 bones;\n"
		"attribute vec4 weights;\n"
		"uniform mat4 lightSpace;\n"
		"uniform mat4 model;\n"
		"varying vec2 textureCoord;\n"
		"void main() {\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    gl_Position = lightSpace * model * vec4(_pos.xyz, 1.0);\n"
		"}";

	const GLchar *strFragmentShaderShadow =
		"#version 130\n"
		"uniform sampler2D texImage;\n"
		"varying vec2 textureCoord;\n"
		"void main() {\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    gl_FragDepth = gl_FragCoord.z;\n"
		"}";

	GLShaderProgram* shaderShadow = new GLShaderProgram(strVertexShaderShadow, strFragmentShaderShadow);
	GLObject::AddShaderProgram(shaderShadow);

	// create the shader programs
	GLShaderProgram *shaderAnimated = new GLShaderProgram(ShaderStrings::GetVertexShader(true, m_bPerPixel, m_bNormalMapping, m_bShadows), ShaderStrings::GetFragmentShader(true, m_bPerPixel, m_bNormalMapping, m_bShadows));
	GLObject::AddShaderProgram(shaderAnimated);

	// get position for matrices
	shaderAnimated->SetPerspectiveProjection(85, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 3800);

	// sort out the multiple textures
	GLObject::GetShaderProgram(3)->Bind();
	glUniform1i(GLObject::GetShaderProgram(3)->GetUniformLocation("texImage"), 0);
	glUniform1i(GLObject::GetShaderProgram(3)->GetUniformLocation("shadowMap"), 1);
	GLObject::GetShaderProgram(3)->Unbind();

	// animated shadow shader program
	const GLchar *strVertexShaderShadowAnimated =
		"#version 130\n"
		"attribute vec3 position;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 bones;\n"
		"attribute vec4 weights;\n"
		"uniform mat4 lightSpace;\n"
		"uniform mat4 model;\n"
		"uniform mat4 skeleton[64];\n"
		"varying vec2 textureCoord;\n"
		"void main() {\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    if (bones.x > -1) _pos = (skeleton[int(bones.x)] * vec4(position.xyz, 1.0)) * weights.x;"
		"    if (bones.y > -1) _pos += (skeleton[int(bones.y)] * vec4(position.xyz, 1.0)) * weights.y;"
		"    if (bones.z > -1) _pos += (skeleton[int(bones.z)] * vec4(position.xyz, 1.0)) * weights.z;"
		"    if (bones.w > -1) _pos += (skeleton[int(bones.w)] * vec4(position.xyz, 1.0)) * weights.w;"
		"    gl_Position = lightSpace * model * vec4(_pos.xyz, 1.0);\n"
		"}";

	const GLchar *strFragmentShaderShadowAnimated =
		"#version 130\n"
		"uniform sampler2D texImage;\n"
		"varying vec2 textureCoord;\n"
		"void main() {\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    gl_FragDepth = gl_FragCoord.z;\n"
		"}";

	GLShaderProgram* shaderShadowAnimated = new GLShaderProgram(strVertexShaderShadowAnimated, strFragmentShaderShadowAnimated);
	GLObject::AddShaderProgram(shaderShadowAnimated);
        
    // create the texture manager
    m_pTexMan = new TextureManager();
        
    // create the scene manager
    m_pSceneMan = CreateSceneManager();

	// sort out the shadow framebuffer object
	SetShadowParameters();
	GenerateFramebufferObjects();

	// create the GUI manager
	m_pGUIMan = CreateGUIManager();
	m_pGUIMan->SetDefaultFont(m_pGUIMan->GetFont("font14"));

	// create the menu
	//StartMenu();
	//StartGame("save/player/player.sav", "Player");
	
	// we have successfully initialised, return true
	return true;
}

void Game::Run()
{
	// the main game loop
	while (!glfwWindowShouldClose(m_pWindow))
	{
		// update delta time
		DeltaTime::CalculateDelta();

		// check for a change in game state
		CheckForStateChange();
		
		// update time
		if (m_pGameGUI)
		{
			m_pGameGUI->UpdateMinimapTexture();
			m_pGameGUI->WaitForUpdate();
		}
		if (m_pMenuGUI)
			m_pMenuGUI->WaitForChange();
		if (m_pClientMan)
			m_pClientMan->Update();

		m_pSceneMan->Update();
		m_pGUIMan->Update();

		// render the shadows
		if (m_bShadows)
		{
			glViewport(0, 0, m_ShadowSize.x, m_ShadowSize.y);
			glBindFramebuffer(GL_FRAMEBUFFER, m_ShadowFBO);
			glClear(GL_DEPTH_BUFFER_BIT);
			m_pSceneMan->RenderShadows();
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}

		// render the game
		if (m_pGameGUI)
			glClearColor(0.25f * m_pSceneMan->GetSun(), 0.65f * m_pSceneMan->GetSun(), 0.85f * m_pSceneMan->GetSun(), 1);
		else
			glClearColor(0.5f, 0.5f, 0.5f, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		// do all the scene rendering
		glViewport(0, 0, GetScreenSize().x, GetScreenSize().y);
		m_pSceneMan->Render();

		// draw the GUI
		glClear(GL_DEPTH_BUFFER_BIT);
		glDisable(GL_CULL_FACE);
		m_pGUIMan->Render();
		glEnable(GL_CULL_FACE);

		// swap the buffers
		glfwSwapBuffers(m_pWindow);

#ifdef _DEBUG
		// output any errors
		GLenum err = glGetError();
		if (err != 0)
		{
			cout << "OPENGL ERROR: " << err << endl;

			std::ofstream file("pyreoflogs.txt", std::ofstream::out | std::ofstream::app);
			file << "OPENGL ERROR: " << err << endl;
			file.close();
		}
#endif

		// poll for events
		glfwPollEvents();
		HandleDoublePress();
	}
	
	// clean up
	DeleteNetworkManager();
	m_pGUIMan->CleanUp();
	m_pSceneMan->CleanUp();
	delete m_pGameGUI;
	delete m_pTexMan;
	if (m_pClientMan)
		delete m_pClientMan;
	Item::UnloadItems();
	Mob::UnloadMobs();
}

void Game::StartMenu(string message)
{
	// start the main menu
	m_MenuMessage = message;
	m_bStartingMenu = true;
}

void Game::StartGame(string filename, string name)
{
	// start the game
	m_PlayerFilename = filename;
	m_PlayerName = name;
	m_bStartingGame = true;
}

void Game::CheckForStateChange()
{
	// are we starting the menu?
	if (m_bStartingMenu)
	{
		// kill the game gui object if it exists
		if (m_pGameGUI)
		{
			delete m_pGameGUI;
			m_pGameGUI = NULL;
		}

		if (m_pClientMan)
		{
			delete m_pClientMan;
			m_pClientMan = NULL;
		}

		DeleteNetworkManager();

		std::this_thread::sleep_for(std::chrono::milliseconds(100));

		// clean up
		m_pGUIMan->CleanUp();
		m_pSceneMan->CleanUp();

		// set the default font
		m_pGUIMan->SetDefaultFont(m_pGUIMan->GetFont("font14"));

		// create the main menu object
		m_pMenuGUI = new MenuGUI(m_pGUIMan, m_pSceneMan, &m_Settings);

		// if we have a message to display, display it
		if (!m_MenuMessage.empty())
		{
			m_pGUIMan->ShowOKDialog("Message", m_MenuMessage);
			m_MenuMessage = "";
		}

		// reset flags
		m_bStartingMenu = false;
		m_bStartingGame = false;
	}

	if (m_bStartingGame)
	{
		// kill the menu gui objects if it exists
		if (m_pMenuGUI)
		{
			delete m_pMenuGUI;
			m_pMenuGUI = NULL;
		}

		// clean up
		m_pGUIMan->CleanUp();
		m_pSceneMan->CleanUp();

		// set the default font
		m_pGUIMan->SetDefaultFont(m_pGUIMan->GetFont("font14"));

		// load up stuff
		Item::UnloadItems();
		Mob::UnloadMobs();
		Item::LoadItems("xml/weapons.xml", "weapon", m_pSceneMan);
		Item::LoadItems("xml/armour.xml", "armour", m_pSceneMan);
		Item::LoadItems("xml/tools.xml", "tool", m_pSceneMan);
		Item::LoadItems("xml/ingots.xml", "ingot", m_pSceneMan);
		Item::LoadItems("xml/blocks.xml", "block", m_pSceneMan);
		Item::LoadItems("xml/ammo.xml", "ammo", m_pSceneMan);
		Item::LoadItems("xml/projectiles.xml", "projectile", m_pSceneMan);
		Item::SortItems();
		Mob::LoadMobs("xml/enemies.xml", "enemy", m_pSceneMan);

		// create the player and send their hotbar data
		Player* p = m_pSceneMan->CreatePlayer(vector3df(0, 120, 0), m_PlayerFilename, m_PlayerName);
		m_pNetworkMan->SendPlayerInventory(player_hotbar_data, p->GetHotbar());
		m_pNetworkMan->SendPlayerInventory(player_equipment_data, p->GetEquipment());
		m_pNetworkMan->SendPlayerStats();

		// reset strings
		m_PlayerFilename = "";
		m_PlayerName = "";

		// create the terrain object
		m_pSceneMan->CreateTerrain();
		p->AttachTerrain(m_pSceneMan->GetFirstInstance<Terrain>());
		//m_pSceneMan->CreateTerrain();

		// create the game gui
		m_pGameGUI = new GameGUI(m_pGUIMan, m_pSceneMan, &m_Settings);
		p->AttachGameGUI(m_pGameGUI);

		// create the client manager
		m_pClientMan = new ClientManager(m_pSceneMan, m_pNetworkMan);
		m_pNetworkMan->StartPlayerUpdateThread();

		// reset flags
		m_bStartingGame = false;
		m_bStartingMenu = false;
	}
}

void Game::HandleDoublePress()
{
	// sort out key double presses
	double delta = DeltaTime::GetDelta();
	for (GLuint i = 0; i < GLFW_KEY_LAST; ++i) {
		if (m_fKeyReset[i] > 0.f)
		{
			m_fKeyReset[i] -= 5.f * delta;
		}
		else
		{
			m_bDoublePress[i] = false;
		}
	}
}

void Game::MouseRelease(GLuint button)
{
	if (!m_pGUIMan->MouseRelease(button))
	{
		// the GUI manager didn't register any GUI event so we are free to pass mouse events to the scene manager
		m_pSceneMan->Input(INPUT_MOUSE, button, GLFW_RELEASE);
	}
}

void Game::MouseDown(GLuint button)
{
	if (!m_pGUIMan->MouseDown(button))
	{
		// the GUI manager didn't register any GUI event so we are free to pass mouse events to the scene manager
		m_pSceneMan->Input(INPUT_MOUSE, button, GLFW_PRESS);
	}
}

void Game::KeyRelease(GLuint key)
{
	if (!m_pGUIMan->KeyRelease(key))
	{
		// the GUI manager didn't register any GUI event so we are free to pass mouse events to the scene manager
		m_pSceneMan->Input(INPUT_KEY, key, GLFW_RELEASE);
	}
}

void Game::KeyDown(GLuint key)
{
	// handle double-presses of keys
	if (m_fKeyReset[key] > 0.f)
	{
		m_bDoublePress[key] = true;
		m_fKeyReset[key] = 0.5f;
	}
	else
	{
		m_fKeyReset[key] = 1.f;
	}
	
	if (!m_pGUIMan->KeyPress(key))
	{
		// the GUI manager didn't register any GUI event so we are free to pass mouse events to the scene manager
		m_pSceneMan->Input(INPUT_KEY, key, GLFW_PRESS);
	}
}

bool Game::KeyDoublePressed(GLuint key)
{
	return m_bDoublePress[key];
}

void Game::CharCallback(GLuint codepoint)
{
	// char callbacks are only ever handled by the GUI
	m_pGUIMan->CharCallback(codepoint);
}

void Game::ScrollCallback(GLdouble x, GLdouble y)
{
	//if (!m_pGUIMan->Scroll(x, y))
	{
		m_pSceneMan->ScrollInput(x, y);
	}
}

GameGUI* Game::GetGameGUI()
{
	return m_pGameGUI;
}

MenuGUI* Game::GetMenuGUI()
{
	return m_pMenuGUI;
}

SceneManager* Game::GetSceneManager()
{
	return m_pSceneMan;
}

ClientManager* Game::GetClientManager()
{
	return m_pClientMan;
}

NetworkManager* Game::GetNetworkManager()
{
	return m_pNetworkMan;
}

NetworkManager* Game::CreateNetworkManager(std::string ip, short port)
{
	m_pNetworkMan = new NetworkManager(this, ip, port);
	return m_pNetworkMan;
}

void Game::DeleteNetworkManager()
{
	if (!m_pNetworkMan)
		return;
	
	delete m_pNetworkMan;
	m_pNetworkMan = NULL;
}

GLuint Game::GetDepthMap() const
{
	return m_DepthMap;
}

void Game::ApplySettings(GUIElement* e)
{
	Game* g = e->GetManager()->GetRoot()->CastTo<Game>();
	if (!g)
		return; // something has gone terribly, terribly wrong if this is ever hit

	g->ChangeDisplaySettings();
}

SceneManager* Game::CreateSceneManager()
{
	SceneManager* sceneMan = new SceneManager(this, m_pWindow, m_pTexMan);
	AddManager(sceneMan);
	return sceneMan;
}

GUIManager* Game::CreateGUIManager()
{
	GUIManager* guiMan = new GUIManager(this, m_pWindow, m_pTexMan);
	AddManager(guiMan);
	return guiMan;
}

void Game::SetShadowParameters()
{
	// set the size based on the shadow setting
	GLuint val = m_Settings.GetValueAsInt("shadows");
	switch (val)
	{
		case 0:
			m_ShadowSize.Set(0, 0);
			break;
		case 1:
			m_ShadowSize.Set(1024, 1024);
			break;
		case 2:
			m_ShadowSize.Set(2048, 2048);
			break;
		case 3:
			m_ShadowSize.Set(3072, 3072);
			break;
		case 4:
			m_ShadowSize.Set(4096, 4096);
			break;
	}

	// change the scene manager's shadow settings too
	m_pSceneMan->SetShadowRenderSize(val);
}

void Game::ChangeDisplaySettings()
{
	// we might need to create a new display
	GLFWmonitor* current = glfwGetWindowMonitor(m_pWindow);
	bool bCurrentlyFullscreen = (current != NULL);
	bool bFullscreen = m_Settings.GetValueAsBool("fullscreen");
	bool bBorderless = m_Settings.GetValueAsBool("borderless");

	// advanced video settings
	m_bPerPixel = m_Settings.GetValueAsBool("perpixel");
	m_bNormalMapping = m_Settings.GetValueAsBool("normalmaps");
	m_bShadows = m_Settings.GetValueAsInt("shadows") > 0;

	// read in width and height
	int WIDTH = m_Settings.GetValueAsInt("width");
	int HEIGHT = m_Settings.GetValueAsInt("height");
	SetScreenSize(WIDTH, HEIGHT);

	// we need to create a new window
	glfwWindowHint(GLFW_DECORATED, !bBorderless);
	GLFWwindow* newWindow = glfwCreateWindow(WIDTH, HEIGHT, "Pyre Of Gods", bFullscreen ? glfwGetPrimaryMonitor() : NULL, m_pWindow);
	glfwDestroyWindow(m_pWindow);
	m_pWindow = newWindow;

	// assign the new window to the scene and GUI managers
	m_pSceneMan->SetWindow(m_pWindow);
	m_pGUIMan->SetWindow(m_pWindow);

	// re-assign input callback
	glfwSetMouseButtonCallback(m_pWindow, InputManager::MouseCallback);
	glfwSetKeyCallback(m_pWindow, InputManager::KeyCallback);
	glfwSetCharCallback(m_pWindow, InputManager::CharCallback);
	glfwSetScrollCallback(m_pWindow, InputManager::ScrollCallback);

	// sort out opengl context
	glfwMakeContextCurrent(m_pWindow);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_BLEND);
	glEnable(GL_DEPTH);
	glEnable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthFunc(GL_LEQUAL);

	glViewport(0, 0, WIDTH, HEIGHT);

	// set the window position if we're not in fullscreen mode
	if (!bFullscreen)
	{
		GLFWmonitor* primary = glfwGetPrimaryMonitor();
		if (primary)
		{
			const GLFWvidmode* mode = glfwGetVideoMode(primary);
			if (mode)
			{
				glfwSetWindowPos(m_pWindow, (int)((mode->width - WIDTH) * 0.5), (int)((mode->height - HEIGHT) * 0.5));
			}
		}
	}

	// sort out the shaders
	(*GLObject::GetPointerToShaderProgram(0))->SetShaders(ShaderStrings::GetVertexShader(false, m_bPerPixel, m_bNormalMapping, m_bShadows), ShaderStrings::GetFragmentShader(false, m_bPerPixel, m_bNormalMapping, m_bShadows));
	(*GLObject::GetPointerToShaderProgram(3))->SetShaders(ShaderStrings::GetVertexShader(true, m_bPerPixel, m_bNormalMapping, m_bShadows), ShaderStrings::GetFragmentShader(true, m_bPerPixel, m_bNormalMapping, m_bShadows));
	(*GLObject::GetPointerToShaderProgram(0))->RecompileShaders();
	(*GLObject::GetPointerToShaderProgram(3))->RecompileShaders();

	(*GLObject::GetPointerToShaderProgram(0))->SetPerspectiveProjection(85, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 3800);
	(*GLObject::GetPointerToShaderProgram(1))->SetOrthoProjection(WIDTH, HEIGHT);
	(*GLObject::GetPointerToShaderProgram(3))->SetPerspectiveProjection(85, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 3800);

	// sort out the multiple textures
	GLObject::GetShaderProgram(0)->Bind();
	glUniform1i(GLObject::GetShaderProgram(0)->GetUniformLocation("texImage"), 0);
	glUniform1i(GLObject::GetShaderProgram(0)->GetUniformLocation("shadowMap"), 1);
	glUniform1i(GLObject::GetShaderProgram(0)->GetUniformLocation("normalMap"), 2);
	GLObject::GetShaderProgram(0)->Unbind();

	GLObject::GetShaderProgram(3)->Bind();
	glUniform1i(GLObject::GetShaderProgram(3)->GetUniformLocation("texImage"), 0);
	glUniform1i(GLObject::GetShaderProgram(3)->GetUniformLocation("shadowMap"), 1);
	glUniform1i(GLObject::GetShaderProgram(3)->GetUniformLocation("normalMap"), 2);
	GLObject::GetShaderProgram(3)->Unbind();

	GLObject::UpdateShaderPrograms();

	// update the scene manager's uniforms
	m_pSceneMan->UpdateUniforms();

	// recreate framebufferobjects
	SetShadowParameters();
	GenerateFramebufferObjects();

	// vsync can be set here in the same way
	glfwSwapInterval(m_Settings.GetValueAsBool("vsync"));

	// update the GUI
	m_pGameGUI->ResolutionUpdate();
}

void Game::GenerateFramebufferObjects()
{
	// delete the buffer if it exists
	if (m_ShadowFBO > 0)
	{
		glDeleteFramebuffers(1, &m_ShadowFBO);
		glDeleteTextures(1, &m_DepthMap);
		cout << "Deleting FBO" << endl;
		m_ShadowFBO = 0;
		m_DepthMap = 0;
	}
	
	if (m_bShadows)
	{
		// sort out the shadow framebuffer object
		glGenFramebuffers(1, &m_ShadowFBO);

		// create the shadow depth map
		glGenTextures(1, &m_DepthMap);
		glBindTexture(GL_TEXTURE_2D, m_DepthMap);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, m_ShadowSize.x, m_ShadowSize.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		GLfloat borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

		// attach the texture to the framebuffer object
		glBindFramebuffer(GL_FRAMEBUFFER, m_ShadowFBO);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_DepthMap, 0);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}
