#ifndef _PLANE_H_
#define _PLANE_H_

// implementation of planes - thanks to the Irrlicht engine for help :)
#include "Line.h"

template<typename T>
class Plane3 {
	public:
		Plane3() : normal(0, 1, 0) { CalculateD(Vector3<T>()); }
		Plane3(const Vector3<T>& n, Vector3<T>& p) : normal(n) { CalculateD(p); }
		Plane3(T nx, T ny, T nz, T px, T py, T pz) : normal(nx, ny, nz) { CalculateD(Vector3<T>(px, py, pz)); }
		Plane3(const Vector3<T>& n, T _d) : normal(n), d(_d) {}

		bool operator==(const Plane3<T>& other) { return (normal == other.normal && d == other.d); }
		bool operator!=(const Plane3<T>& other) { return !(*this == other); }

		void CalculateD(const Vector3<T>& p)
		{
			d = -p.Dot(normal);
		}

		bool IntersectWithLine(Vector3<T>& p, Vector3<T>& l, Vector3<T>& out)
		{
			T intersect = normal.Dot(l);

			if (intersect == 0)
				return false;

			T t = -(normal.Dot(p) + d) / intersect;
			out = p + (l * t);
			return true;
		}
		bool IntersectWithPlane(Plane3<T>& p, Vector3<T>& outP, Vector3<T>& outV)
		{
			const T f00 = normal.Magnitude();
			const T f01 = normal.Dot(p.normal);
			const T f11 = p.normal.Magnitude();
			const double det = (f00 * f11) - (f01 * f01);

			if (abs(det) < 0.00000001)
				return false;

			const double invdet = 1.0 / det;
			const double fc0 = ((f11 * -d) + (f01 * p.d)) * invdet;
			const double fc1 = ((f00 * -p.d) + (f01 * d)) * invdet;

			outV.Set(normal);
			outV.Cross(p.normal);
			outP = (normal * (T)fc0) + (p.normal * (T)fc1);
			return true;
		}
		bool IntersectWithPlanes(Plane3<T>& p1, Plane3<T>& p2, Vector3<T>& out)
		{
			Vector3<T> p, v;
			if (IntersectWithPlane(p1, p, v))
				return p2.IntersectWithLine(p, v, out);

			return false;
		}
		bool PointInFront(const Vector3<T>& p) const
		{
			return vector3df(normal).Dot(p) + d >= 0;
		}

		Vector3<T> normal;
		T d;

	protected:

	private:

};

typedef Plane3<GLint>	plane3di;
typedef Plane3<GLshort>	plane3ds;
typedef Plane3<GLfloat>	plane3df;

#endif
