#ifndef _SCROLL_PANE_H_
#define _SCROLL_PANE_H_

#include "GUIElement.h"
#include "Scrollbar.h"

class ScrollPane : public GUIElement {
	public:
		ScrollPane(Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size, GLubyte mode = vertical_scroll | horizontal_scroll);

		virtual bool IsHovered();
		virtual void MouseDownFunction(GLuint button);

		virtual void AddChild(GUIElement* child);
		void ClearPane();

	protected:

	private:
		Scrollbar*	m_Scrollbar;
};

#endif