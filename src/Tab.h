#ifndef _TAB_H_
#define _TAB_H_

#include "GUIElement.h"

class TabManager;

class Tab : public GUIElement {
	public:
		Tab(string name, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size);

		virtual void MouseDownFunction(GLuint button);

		string GetName();

	protected:

	private:
		string	m_strName;
};

class TabManager : public GUIElement {
	public:
		TabManager(Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size);

		virtual void MouseDownFunction(GLuint button);

		Tab* AddTab(string name);
		Tab* GetTab(string name);
		Tab* GetTab(GLuint pos);
		Tab* GetActiveTab();

	protected:

	private:
		GLuint	m_TabCount;

		GLuint GetWidthUpToTab(GLuint tab);
		void UpdateGUI(GLuint tab);
};

#endif