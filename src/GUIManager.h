#ifndef _GUIMANAGER_H_
#define _GUIMANAGER_H_

#include <list>
#include "Misc.h"
#include "GUIElement.h"
#include "Text.h"
#include "Button.h"
#include "Pane.h"
#include "Inventory.h"
#include "Image.h"
#include "Tab.h"
#include "ListBox.h"
#include "Scrollbar.h"
#include "DropDownMenu.h"
#include "TextBox.h"
#include "ScrollPane.h"
#include "Ingredient.h"
#include "Menu.h"
#include "GroupBox.h"
#include "CheckBox.h"
#include "OKDialog.h"
#include "YesNoDialog.h"
#include "Table.h"
#include "KeyChange.h"
#include "Label.h"
#include "TextBoxDialog.h"
#include "ProgressBar.h"
#include "ChatText.h"
#include "ServerDetailsDialog.h"

using std::list;

class GUIManager : public Manager {
	public:
		GUIManager(Root* root, GLFWwindow* window, TextureManager* texMan);
		~GUIManager();

		void SetWindow(GLFWwindow* window);
		
		void CleanUp();
		void Update();
		void Render();
		bool MouseRelease(GLuint button);
		bool MouseDown(GLuint button);
		bool KillElement(GUIElement* e);

		bool KeyRelease(int key);
		bool KeyPress(int key);

		bool IsHovered();

		int GetKeyState(int key);
		int GetMouseState(int button);
		
		Font* GetFont(const char* fileName);
		void SetDefaultFont(Font* font);
		const Font* GetDefaultFont();
		
		vector2dd& GetMousePosition();
		void Sort();

		void SetCharCallback(TextBox* tb);
		TextBox* GetCharCallback();
		bool IsKeyboardFree();
		void CharCallback(GLuint codepoint);
		GLint GetLastKey();

		Menu* GetMenu();

		GLFWwindow* GetWindow();
		
		Text* CreateText(const char* text, vector2df pos, vector2di size = vector2di(1), Font* font = NULL, GUIElement* parent = NULL);
		Button* CreateButton(const char* text, vector2df pos, vector2di size, void(*fn)(GUIElement*), Font* font = NULL, GUIElement* parent = NULL);
		Pane* CreatePane(const char* text, vector2df pos, vector2di size, Font* font = NULL, GUIElement* parent = NULL, GLubyte alpha = 100, bool movable = true);
		MeshView* CreateMeshView(Mesh* mesh, string texture, vector2df pos, vector2di size, GUIElement* parent = NULL);
		Inventory* CreateInventory(vector2df pos, vector2di size, vector<Item*>* inventory, GLuint slots, bool bBackground = true, GUIElement* parent = NULL);
		Image* CreateImage(string texture, vector2df pos, vector2di size, GLColour col = GLColour(255, 255, 255, 255), GUIElement* parent = NULL);
		TabManager* CreateTabManager(vector2df pos, vector2di size, GUIElement* parent = NULL);
		ListBox* CreateListBox(vector2df pos, vector2di size, GUIElement* parent = NULL);
		Scrollbar* CreateScrollbar(GUIElement* scrollee, vector2df pos, vector2di size, GLubyte mode = vertical_scroll, GUIElement* parent = NULL);
		DropDownMenu* CreateDropDownMenu(vector2df pos, vector2di size, GLfloat height = 80, GUIElement* parent = NULL);
		TextBox* CreateTextBox(string text, vector2df pos, vector2di size, string hint = "", GUIElement* parent = NULL);
		ScrollPane* CreateScrollPane(vector2df pos, vector2di size, GLubyte mode = vertical_scroll | horizontal_scroll, GUIElement* parent = NULL);
		Ingredient* CreateIngredient(vector2df pos, const ItemClass* ic, GLuint amount, GLuint amountOwned, GUIElement* parent = NULL);
		Menu* CreateMenu(vector2di size);
		GroupBox* CreateGroupBox(vector2df pos, vector2di size, string label, GUIElement* parent = NULL);
		CheckBox* CreateCheckBox(vector2df pos, vector2di size, string label, GUIElement* parent = NULL);
		Table* CreateTable(vector2df pos, vector2di size, GLuint columncount, GUIElement* parent = NULL);
		ProgressBar* CreateProgressBar(vector2df pos, vector2di size, GUIElement* parent = NULL);
		ChatText* CreateChatText(GLuint maxlines, vector2df pos, vector2di size, Font* font = NULL, GUIElement* parent = NULL);

		OKDialog* ShowOKDialog(string title, string text, void(*fn)(GUIElement*) = NULL);
		YesNoDialog* ShowYesNoDialog(string title, string text, void(*fny)(GUIElement*) = NULL, void(*fnn)(GUIElement*) = NULL);
		TextBoxDialog* ShowTextBoxDialog(string title, string text, void(*fny)(GUIElement*) = NULL, void(*fnn)(GUIElement*) = NULL);
		ServerDetailsDialog* ShowServerDetailsDialog(string title, string text1, string text2, string text3, void(*fny)(GUIElement*) = NULL, void(*fnn)(GUIElement*) = NULL);
		KeyChange* ShowKeyChange(string control);
		Label* ShowLabel(string title, string text, GUIElement* e);
	
	protected:
	
	private:
		list<GUIElement*> 					m_Elements;
		list<GUIElement*>					m_DeadElements;
		map<string, Font*, StringCompare>	m_Fonts;
		
		GLFWwindow* m_pWindow;
		Font*		m_DefaultFont;
		Texture		m_Texture;
		vector2dd	m_CursorPos;

		TextBox*	m_CharCallback;
		Menu*		m_Menu;
		GLint		m_LastKey;
		
		inline void ProcessObject(GUIElement* gui, GUIElement* parent, bool texture = true)
		{
			if (parent)
			{
				parent->AddChild(gui);
				if (parent->IsMenu())
					gui->SetMenu();
			}
			else
			{
				m_Elements.push_back(gui);
			}
			if (texture)
			{
				gui->SetTexture(m_Texture);
			}
			//gui->UseShaderProgram(1);

			Sort();
		}
		
		Font* SortFont(Font* font);

		inline void ReverseElements()
		{
			if (m_Elements.size())
				std::reverse(m_Elements.begin(), m_Elements.end());
		}
};

#endif
