#ifndef _GLOBJECT_H_
#define _GLOBJECT_H_

#include "Animator.h"
#include "GLShaderProgram.h"
#include "AABB.h"
#include <mutex>

class SceneManager;
struct Texture;

// GLShaderDetails struct - contains locations of attributes and uniforms needed by this game
struct GLShaderDetails {
	GLint 				PositionAttr;
	GLint 				ColourAttr;
	GLint 				TextureAttr;
	GLint				NormalAttr;
	GLint				LightAttr;
	GLint				TangentAttr;
	GLint				BonesAttr;
	GLint				WeightsAttr;
	GLint				ModelUni;
	GLint				LightUni;
	GLint				LightPosUni;
	GLint				SkeletonUni;
	GLShaderProgram*	Program;
};

// GLObject class - the base class for anything that needs to be rendered
class GLObject {
	public:
		GLObject(Manager* parent, vector3df pos, vector3di size);
		virtual ~GLObject();
		
		void CreateBufferObjects();
		void UseShaderProgram(GLuint index);
		void UseShadowShaderProgram(GLuint index);
		
		virtual void Update(bool bForce = false);
		virtual void Render();
		virtual void RenderShadows();

		virtual bool DoesPointCollide(vector3df& p);
		virtual bool DoesRayCollide(line3df& l, vector3df& oo, GLfloat& out);
		
		vector3df GetPosition();
		vector3di GetSize();
		vector3df GetRotation();
		vector3df GetScale();
		const GLfloat* GetTransformationMatrixDataPointer();

		void SetAABB(const vector3df& bottomLeft, const vector3df& topRight);
		const AABB& GetAABB() const;
		virtual bool InViewFrustum() const;
		void CalculateInView();
		void CalculateInView(const vector3df& _where);
		
		void SetPosition(const vector3df& pos);
		void TranslatePosition(const vector3df& translate);
		void TranslateWithRotation(GLfloat amount, const vector3df& axis);

		void SetSize(const vector3di& size);

		void SetRotation(const vector3df& axis, GLfloat angle, const vector3df& off = vector3df());
		void Rotate(const vector3df& axis, GLfloat angle);

		void SetScale(const vector3df& scale);
		
		virtual void SetTexture(const char* fileName);
		virtual void SetTexture(Texture tex);
		virtual void SetNormalTexture(const char* fileName);
		Texture GetTexture();
		GLuint GetTextureWidth();
		GLuint GetTextureHeight();
		
		void SetVisible(bool visible);
		bool IsVisible();
		
		void SetManager(Manager* man);
		Manager* GetManager();
		SceneManager* GetSceneManager();

		void SetMesh(Mesh* mesh, bool bChangeShaderProgram = true, bool bForceNoBones = false);
		Mesh* GetMesh();
		void SetSkeleton(Mesh* mesh);
		GLuint GetBoneCount();
		Bone& GetBone(GLuint index);

		void Animate(Animator* anim);
		void SetFrameAtTime(Animator* anim, GLfloat t);

		void SetLightAmount(vector3df l);
		vector3df GetLightAmount();

		void SetUseBones(bool use, bool bChange = true);
		bool IsAnimated();

		void AddKeyFunction(int key, int evt, bool(*fn)(GLObject*));
		void AddMouseFunction(int key, int evt, bool(*fn)(GLObject*));
		void AddScrollFunction(bool(*fn)(GLObject*, double, double));

		virtual void SortIndices(GLubyte dir);

		static void AddShaderProgram(GLShaderProgram* program);
		static void ClearShaderPrograms();
		static void UpdateShaderPrograms();
		static GLShaderProgram* GetShaderProgram(int iIndex);
		static GLShaderProgram** GetPointerToShaderProgram(int iIndex);
	
	protected:
		void UpdateVBO();
		void Clear();
		void ClearVectors();
		GLuint GetModelUniPosition();
		GLuint GetBufferSize();
		GLuint GetVBO();
		void RenderMesh(Mesh* mesh, GLuint tex, const matrix4f &transform, const vector3df& light = vector3df(0), bool bForceNonAnimated = false);
		void RenderShadowMesh(Mesh* mesh, GLuint tex, const matrix4f &transform, bool bForceNonAnimated = false);
		void UpdateIndices();
		void SetIndicesUnsorted();

		void ReallocateVertices(GLuint size);
		void ReallocateIndices(GLuint size);

		void LockMutex();
		void UnlockMutex();

		vector<GLuint> GetIndices();
		void SetIndices(const vector<GLuint> indices);
		vector<GLVertex> GetVertices();
		
		virtual void UpdateTransformationMatrix(const vector3df& offset = vector3df(), const matrix4f& rot = matrix4f(0.f), const vector3df& scale = vector3df(1));
		matrix4f& GetTransformationMatrix();

		void Add2DTriangle(const vector2df& pos1, const vector2df& uv1, const vector2df& pos2, const vector2df& uv2, const vector3df& pos3, const vector2df& uv3, const GLColour& col = GLColour(255, 255, 255, 255));
	
		GLuint AddQuad(const vector2df& pos1, const vector2df& uv1, const vector2df& pos2, const vector2df& uv2, const GLColour& col = GLColour(255, 255, 255, 255));
		GLuint AddQuad(GLVertex ver1, GLVertex ver2, GLVertex ver3, GLVertex ver4, bool flip = false);
		void AddTriangle(GLVertex ver1, GLVertex ver2, GLVertex ver3);
		
		void ChangeQuadCoords(int pos, const vector2df& p1, const vector2df& p2, bool updateVBO = true);
		void ChangeQuadCoords(int pos, const vector3df& p1, const vector3df& p2, const vector3df& p3, const vector3df& p4, bool updateVBO = true);
		void ChangeQuadUV(int pos, const vector2df& uv1, const vector2df& uv2, bool updateVBO = true);
		void ChangeQuadColour(int pos, const GLColour& col, bool updateVBO = true);
		void ChangeQuadColour(int pos, const GLColour& col1, const GLColour& col2, const GLColour& col3, const GLColour& col4, bool updateVBO = true);

		void Bind(bool bShadows = false, bool bForceNonAnimated = false);
		void Unbind(bool bShadows = false, bool bForceNonAnimated = false);
		void SetUniforms();

		void SetClearData(bool clear);
	
	private:
		std::vector<GLVertex>	m_Vertices;
		std::vector<GLuint>		m_Indices;
		std::vector<Bone>		m_Bones;
		GLuint				m_bufVBO;
		GLuint				m_bufEBO;
		Texture				m_Texture;
		Texture				m_NormalTexture;
		GLuint				m_IndexCount;
		std::mutex			m_Mutex;
		AABB				m_AABB;
		bool				m_bInView;
		
		vector3df			m_Position;
		vector3df			m_Scale;
		vector3di			m_Size;
		vector3df			m_Rotation;
		matrix4f			m_Transformation;
		bool				m_bVisible;
		bool				m_bIndicesSorted;
		bool				m_bUseBones;
		bool				m_bClearData;
		
		GLuint				m_Shader;
		GLuint				m_ShadowShader;
		
		Manager*			m_pParent;
		SceneManager*		m_pSceneMan;
		Mesh*				m_Mesh;

		vector3df			m_LightAmount;
		vector3df			m_RotationOffset;

		static std::vector<GLShaderDetails> m_ShaderPrograms;
};

#endif
