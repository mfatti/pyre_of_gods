#ifndef _GAME_H_
#define _GAME_H_

#include "Misc.h"
#include "SceneManager.h"
#include "GUIManager.h"
#include "GameGUI.h"
#include "SettingsManager.h"
#include "MenuGUI.h"
#include "NetworkManager.h"
#include "ClientManager.h"

typedef GLuint shader;

class Game : public Root {
	public:
		Game();
		~Game();

		bool Create();
		void Run();

		void StartMenu(string message = "");
		void StartGame(string filename, string name);
		
		void MouseRelease(GLuint button);
		void MouseDown(GLuint button);

		void KeyRelease(GLuint key);
		void KeyDown(GLuint key);
		bool KeyDoublePressed(GLuint key);

		void CharCallback(GLuint codepoint);

		void ScrollCallback(GLdouble x, GLdouble y);

		GameGUI* GetGameGUI();
		MenuGUI* GetMenuGUI();

		SceneManager* GetSceneManager();
		ClientManager* GetClientManager();

		NetworkManager* GetNetworkManager();
		NetworkManager* CreateNetworkManager(std::string ip, short port);
		void DeleteNetworkManager();

		GLuint GetDepthMap() const;

		static void ApplySettings(GUIElement* e);

	protected:
		SceneManager* CreateSceneManager();
		GUIManager* CreateGUIManager();

	private:
		GLFWwindow*	m_pWindow;
		
		SceneManager*	m_pSceneMan;
		GUIManager*		m_pGUIMan;
		ClientManager*	m_pClientMan;
		TextureManager* m_pTexMan;
		GameGUI*		m_pGameGUI;
		MenuGUI*		m_pMenuGUI;
		NetworkManager*	m_pNetworkMan;

		SettingsManager	m_Settings;

		vector2di		m_ShadowSize;
		GLuint			m_ShadowFBO;
		GLuint			m_DepthMap;

		bool			m_bStartingGame;
		bool			m_bStartingMenu;
		string			m_PlayerFilename;
		string			m_PlayerName;
		string			m_MenuMessage;

		bool			m_bDoublePress[GLFW_KEY_LAST];
		GLfloat			m_fKeyReset[GLFW_KEY_LAST];

		bool			m_bPerPixel;
		bool			m_bNormalMapping;
		bool			m_bShadows;

		void ChangeDisplaySettings();
		void GenerateFramebufferObjects();
		void CheckForStateChange();
		void HandleDoublePress();
		void SetShadowParameters();
};

#endif
