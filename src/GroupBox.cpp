#include "GroupBox.h"
#include "Text.h"
#include "GUIManager.h"

GroupBox::GroupBox(Manager* parent, GUIElement* guiParent, vector2df pos, vector3di size, string label)
	: GUIElement(parent, guiParent, pos, size)
{
	// create a group box
	Text* t = GetGUIManager()->CreateText(string("^8").append(label).c_str(), vector2df(), vector2di(size.x, -1), NULL, this);
	t->SetOffset(vector2df(0, (GLfloat)-t->GetFont()->GetDetails().z / 2.f + 1));
	t->CentreText(true);
	GLfloat w = t->GetFont()->GetStringWidth(label) * 0.5f;

	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x / 2.f - w - 5, 1), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df((GLfloat)size.x / 2.f + w + 5, 0), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 1), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df(0, (GLfloat)size.y - 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df(0, 1), vector2df(0.5, 0.5), vector2df(1, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df((GLfloat)size.x - 1, 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 255));
	CreateBufferObjects();
}

void GroupBox::MouseDownFunction(GLuint button)
{

}