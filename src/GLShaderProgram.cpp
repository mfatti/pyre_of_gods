#include "GLShaderProgram.h"

GLShaderProgram::GLShaderProgram(const GLchar* vertex, const GLchar* fragment)
	: m_Vertex(vertex)
	, m_Fragment(fragment)
	, m_fAngle(85.f)
	, m_fRatio(16.f / 9.f)
	, m_fNear(0.1f)
	, m_fFar(800.f)
	, m_fCurrentAngle(m_fAngle)
{
	CompileShaders();
}

GLShaderProgram::~GLShaderProgram()
{
	Clear();
}

void GLShaderProgram::Clear()
{
	glDeleteShader(m_VertexShader);
	glDeleteShader(m_FragmentShader);
	glDeleteProgram(m_ShaderProgram);
}

void GLShaderProgram::CompileShaders()
{
	// load the given shaders (supplied as strings) and compile a shader program
	m_VertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(m_VertexShader, 1, &m_Vertex, NULL);
	glCompileShader(m_VertexShader);

	m_FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(m_FragmentShader, 1, &m_Fragment, NULL);
	glCompileShader(m_FragmentShader);

	// check that the shaders have compiled correctly
	GLint ShaderStatus(GL_FALSE);
	glGetShaderiv(m_VertexShader, GL_COMPILE_STATUS, &ShaderStatus);
	if (ShaderStatus != GL_TRUE)
	{
		//cout << "Vertex shader failed to compile!" << endl;
		GLint maxLength = 0;
		glGetShaderiv(m_VertexShader, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(m_VertexShader, maxLength, &maxLength, &errorLog[0]);

		cout << "Vertex shader failed to compile! Details: " << endl;
		for (GLint i = 0; i < maxLength; ++i) {
			cout << errorLog[i];
		}
		cout << endl;
		return;
	}

	glGetShaderiv(m_FragmentShader, GL_COMPILE_STATUS, &ShaderStatus);
	if (ShaderStatus != GL_TRUE)
	{
		//cout << "Fragment shader failed to compile!" << endl;
		GLint maxLength = 0;
		glGetShaderiv(m_VertexShader, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(m_VertexShader, maxLength, &maxLength, &errorLog[0]);

		cout << "Fragment shader failed to compile! Details: " << endl;
		for (GLint i = 0; i < maxLength; ++i) {
			cout << errorLog[i];
		}
		cout << endl;
		return;
	}

	// create the shader program
	m_ShaderProgram = glCreateProgram();
	glAttachShader(m_ShaderProgram, m_VertexShader);
	glAttachShader(m_ShaderProgram, m_FragmentShader);
	glLinkProgram(m_ShaderProgram);

	// check that the program was created successfully and use it
	glGetProgramiv(m_ShaderProgram, GL_LINK_STATUS, &ShaderStatus);
	if (ShaderStatus != GL_TRUE)
	{
		cout << "Shader program failed to link!" << endl;
		return;
	}
}

void GLShaderProgram::RecompileShaders()
{
	Clear();
	CompileShaders();
}

void GLShaderProgram::SetShaders(const GLchar* vertex, const GLchar* fragment)
{
	m_Vertex = vertex;
	m_Fragment = fragment;
}

void GLShaderProgram::Bind()
{
	glUseProgram(m_ShaderProgram);
}

void GLShaderProgram::Unbind()
{
	glUseProgram(0);
}

GLint GLShaderProgram::GetAttribLocation(const GLchar* attrib)
{
	// remember to call Bind() first!
	return glGetAttribLocation(m_ShaderProgram, attrib);
}

GLint GLShaderProgram::GetUniformLocation(const GLchar* attrib)
{
	// remember to call Bind() first!
	return glGetUniformLocation(m_ShaderProgram, attrib);
}

void GLShaderProgram::SetOrthoProjection(int width, int height)
{
	Bind();
	m_ProjMatrix.Orthographic(0, (GLfloat)width, (GLfloat)height, 0, -100, 100);
	glUniformMatrix4fv(GetUniformLocation("projection"), 1, GL_TRUE, m_ProjMatrix.GetDataPointer());
	Unbind();
}

void GLShaderProgram::SetPerspectiveProjection(float angle, float ratio, float near, float far, bool bDefaults)
{
	Bind();
	m_ProjMatrix.Perspective(angle, ratio, near, far);
	glUniformMatrix4fv(GetUniformLocation("projection"), 1, GL_TRUE, m_ProjMatrix.GetDataPointer());
	Unbind();

	// if we are setting the defaults, do so now
	if (bDefaults)
	{
		m_fAngle = angle;
		m_fRatio = ratio;
		m_fNear = near;
		m_fFar = far;
	}
}

void GLShaderProgram::SmoothFOV(float dest)
{
	// determine which way we're smoothing
	if (dest != 0.f)
	{
		Interpolate(m_fCurrentAngle, dest, 10.f, 0.1f);
	}
	else
	{
		Interpolate(m_fCurrentAngle, m_fAngle, 8.f, 0.1f);
	}

	// if we're already there, don't bother continuing
	if (m_fCurrentAngle == dest || m_fCurrentAngle == m_fAngle)
		return;

	SetPerspectiveProjection(m_fCurrentAngle, m_fRatio, m_fNear, m_fFar, false);
}

matrix4f& GLShaderProgram::GetProjectionMatrix()
{
	return m_ProjMatrix;
}