#ifndef _SCENEMANAGER_H_
#define _SCENEMANAGER_H_

#include <list>
#include <mutex>
#include "Misc.h"
#include "GLObject.h"
#include "Player.h"
#include "Camera.h"
#include "Item.h"
#include "Billboard.h"
#include "ParticleEmitter.h"
#include "Mob.h"
#include "MobManager.h"
#include "Projectile.h"
#include "Terrain.h"

using std::list;

class Game;

class SceneManager : public Manager {
	public:
		SceneManager(Root* root, GLFWwindow* window, TextureManager* texMan);
		~SceneManager();

		void SetWindow(GLFWwindow* window);
		void SetShadowRenderSize(GLuint i);
		void UpdateUniforms();
		
		void CleanUp();
		void Update();
		void Render();
		void RenderShadows();

		vector2dd& GetMousePosition();
		int GetMouseButton(int button);
		void CenterCursor();
		bool RestrictCursorToWindow();
		void ToggleMouse(bool hidden);

		int GetKeyState(int key);
		bool KeyDoublePressed(GLuint key);

		bool Input(int type, int key, int evt);
		bool ScrollInput(double x, double y);

		void AddKeyFunction(GLObject* o, int key, int evt, bool(*fn)(GLObject*));
		void RemoveKeyFunction(GLObject* o, int key, int evt);
		void AddMouseFunction(GLObject* o, int key, int evt, bool(*fn)(GLObject*));
		void AddScrollFunction(GLObject* o, bool(*fn)(GLObject*, double, double));

		void AddTransparencyRenderFunction(GLObject* o, void(*fn)(GLObject*));
		void RemoveTransparencyRenderFunction(GLObject* o, void(*fn)(GLObject*));

		Mesh* LoadMeshFromOBJ(string filename);
		Mesh* LoadMeshFromAOBJ(string filename);
		Mesh* CreateTerrainBlock(string type);

		Camera* GetActiveCamera() const;

		GLfloat GetSun() const;
		void SetSun(GLfloat sun);
		GLfloat GetShadowDistance() const;

		line3df GetRayFromScreenCoords(vector2di& pos);
		bool DoesRayCollide(GLObject* obj, line3df& ray);
		bool GetRayCollisionPoint(GLObject* obj, line3df& ray, vector3df& out, vector3df& outBefore, bool(*check)(SceneManager*, vector3df) = NULL);
		bool GetRayCollisionWithTerrain(line3df& ray, vector3df& out, vector3df& outBefore, bool(*check)(SceneManager*, vector3df, vector3df) = NULL);
		bool GetRayCollisionWithMobs(line3df& ray, vector3df& out, Mob** mob = NULL);

		vector2df ConvertToScreenCoordinates(const vector3df& pos);

		bool DeleteObject(GLObject* o);
		void AddGLObject(GLObject* o);

		void DeleteNetworkItem(const ItemData& item);
		void CreateNetworkItem(ItemData item);

		void PickUpItem(const ItemData& item);
		void GiveItem(const ItemData& item);
		bool SetItemToPickUp(Item* item);

		void SetTimeOfDay(GLfloat time);

		MobManager* GetMobManager();
		
		GLObject* CreateGLObject(vector3df pos);
		Player* CreatePlayer(vector3df pos, string filename, string name);
		Terrain* CreateTerrain();
		Item* CreateItem(string type, vector3df pos, short amount = 1);
		Item* CreateItem(TerrainBlock blocktype, vector3df pos, short amount = 1);
		Item* CreateItem(const ItemData& data);
		Item* CreateItem(vector<unsigned char>& in);
		Mob* CreateMob(string type, vector3df pos);
		Mob* CreateMob(int type, vector3df pos);
		Billboard* CreateBillboard(vector2df size = vector2df(64));
		ParticleEmitter* CreateParticleEmitter(vector3df pos, vector3df dir, GLfloat speed, GLuint amount = 50, vector2di psize = vector2di(5), GLfloat spread = 3, GLfloat alpha = 0.6f, vector2df size = vector2df(64));
		ParticleEmitter* CreateParticleEmitter(ParticleDefinition p);
		Projectile* CreateProjectile(const ProjectileData& data, bool mob);
		
		template<class T> T* GetFirstInstance()
		{
			list<GLObject*>::iterator it = m_Objects.begin();
			while (it != m_Objects.end())
			{
				T* t = dynamic_cast<T*>(*it);

				if (t)
				{
					return t;
				}

				it++;
			}

			return NULL;
		}
	
	protected:
	
	private:
		Game* m_pGame;

		std::list<GLObject*> m_Objects;
		std::list<GLObject*> m_RenderQueue;
		std::list<GLObject*> m_AnimatedRenderQueue;
		std::list<GLObject*> m_DeadObjects;
		std::map<string, Mesh*, StringCompare> m_Meshes;
		vector<std::pair<GLObject*, void(*)(GLObject*)>> m_TransparencyRenderFunctions;
		GLuint			m_CameraUni[2];
		GLuint			m_SunUni[2];
		GLuint			m_LightSpaceUni[2];
		GLuint			m_ShadowLightSpaceUni[2];
		GLuint			m_LightDirUni[2];
		GLuint			m_LightPosUni;
		Camera*			m_pCamera;
		GLfloat			m_Sun;
		GLfloat			m_SunChange;
		GLfloat			m_DestSunChange;
		GLfloat			m_fShadowDistance;

		GLFWwindow*		m_pWindow;
		vector2dd		m_CursorPos;

		matrix4f		m_LightProj;
		matrix4f		m_LightView;
		matrix4f		m_LightSpace;
		vector3df		m_LightDir;
		vector3df		m_LightPos;

		MobManager		m_MobManager;

		std::mutex				m_ItemMutex;
		std::list<ItemData>		m_ItemsToCreate;
		Item*					m_ItemToPickUp;
		bool					m_bCanPickUp;
		GLfloat					m_PickUpTimeout;
		std::list<ItemData>		m_ItemsToGive;

		map<int, map<GLObject*, bool(*[2])(GLObject*)>>		m_KeyFunctions;
		map<int, map<GLObject*, bool(*[2])(GLObject*)>>		m_MouseFunctions;
		map<GLObject*, bool(*)(GLObject*, double, double)>	m_ScrollFunctions;
		
		inline void ProcessObject(GLObject* obj)
		{
			m_Objects.push_back(obj);

			// push the object into the correct rendering queue
			if (obj->IsAnimated())
				m_AnimatedRenderQueue.push_back(obj);
			else
				m_RenderQueue.push_back(obj);
		}

		inline void ReprocessObject(GLObject* obj)
		{
			// this object has changed from animated to non-animted / other way round, re-process it
			if (obj->IsAnimated())
			{
				std::list<GLObject*>::iterator it = std::find(m_RenderQueue.begin(), m_RenderQueue.end(), obj);
				if (it != m_RenderQueue.end())
				{
					m_RenderQueue.erase(it);
					m_AnimatedRenderQueue.push_back(obj);
				}
			}
			else
			{
				std::list<GLObject*>::iterator it = std::find(m_AnimatedRenderQueue.begin(), m_AnimatedRenderQueue.end(), obj);
				if (it != m_AnimatedRenderQueue.end())
				{
					m_AnimatedRenderQueue.erase(it);
					m_RenderQueue.push_back(obj);
				}
			}
		}
};

#endif
