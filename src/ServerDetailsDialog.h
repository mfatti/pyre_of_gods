#ifndef _SERVER_DETAILS_DIALOG_H_
#define _SERVER_DETAILS_DIALOG_H_

#include "GUIElement.h"

class ServerDetailsDialog : public GUIElement {
public:
	ServerDetailsDialog(Manager* parent, string title, string text1, string text2, string text3, void(*fny)(GUIElement*) = NULL, void(*fnn)(GUIElement*) = NULL);

	virtual void MouseDownFunction(GLuint button);

	virtual void ScaleTo(vector2df scale, bool hide = false, bool kill = false);

protected:

private:

};

#endif