#ifndef _MESH_VIEW_H_
#define _MESH_VIEW_H_

#include "GUIElement.h"

class MeshView : public GUIElement {
public:
	MeshView(Mesh* mesh, string texture, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size);

	void SetMeshView(Mesh* mesh, string texture);
	void SetModelOffset(const vector2df& offset);
	void SetDefaultModelView();
	void SetBlockModelView(const vector3di& blockSize);
	void SetIngotModelView();

	void ScaleTo(const vector3df& scale);

	virtual void Update(bool bForce = false);
	virtual void Render();

	virtual void MouseDownFunction(GLuint button);

protected:

private:
	vector2df m_ModelOffset;
	vector3df m_Scale;
	vector3df m_DestScale;
};

#endif
