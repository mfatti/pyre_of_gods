#include "GLObject.h"
#include "SceneManager.h"
#include "GUIManager.h"
#include "Game.h"
#include <iostream>
#include <algorithm>

vector<GLShaderDetails> GLObject::m_ShaderPrograms;

GLObject::GLObject(Manager* parent, vector3df pos, vector3di size)
	: m_pParent(parent)
	, m_bufVBO(0)
	, m_bufEBO(0)
	, m_Position(pos)
	, m_Size(size)
	, m_Scale(1, 1, 1)
	, m_Texture()
	, m_bVisible(true)
	, m_Shader(0)
	, m_Mesh(NULL)
	, m_IndexCount(0)
	, m_bIndicesSorted(true)
	, m_LightAmount(1)
	, m_RotationOffset(0)
	, m_bUseBones(false)
	, m_AABB()
	, m_bClearData(true)
{
	UseShaderProgram(0);
	UseShadowShaderProgram(2);

	SetNormalTexture("default_normalmap.png");

	m_pSceneMan = dynamic_cast<SceneManager*>(parent);
}

GLObject::~GLObject()
{
	Clear();
	ClearVectors();
}

void GLObject::Clear()
{
	// delete the buffers
	if (m_bufVBO)
		glDeleteBuffers(1, &m_bufVBO);
	if (m_bufEBO)
		glDeleteBuffers(1, &m_bufEBO);
		
	m_bufVBO = 0;
	m_bufEBO = 0;
}

void GLObject::ClearVectors()
{
	if (m_Indices.empty() && m_Vertices.empty())
		return;
	
	// clear the vectors
	m_Mutex.lock();
	m_Indices.clear();
	m_Vertices.clear();
	m_Mutex.unlock();
}

GLuint GLObject::GetModelUniPosition()
{
	return m_ShaderPrograms[m_Shader].ModelUni;
}

GLuint GLObject::GetBufferSize()
{
	return m_IndexCount;
}

GLuint GLObject::GetVBO()
{
	return m_bufVBO;
}

void GLObject::RenderMesh(Mesh* mesh, GLuint tex, const matrix4f &transform, const vector3df& light, bool bForceNonAnimated)
{
	// if there is no mesh, don't do anything
	if (!mesh)
		return;
	
	// if the mesh is empty, don't draw it
	if (mesh->GetSize() == 0)
		return;

	GLuint vbo = mesh->GetVBO();
	GLuint ebo = mesh->GetEBO();
	if (vbo == 0 || ebo == 0)
		return;

	// draw this mesh
	glUniformMatrix4fv(m_ShaderPrograms[m_Shader].ModelUni, 1, GL_TRUE, transform.GetDataPointer());
	std::vector<Bone> bones = *mesh->GetSkeletonPointer();
	if (!bForceNonAnimated && m_bUseBones && !bones.empty())
	{
		for (unsigned int i = 0; i < bones.size(); ++i) {
			bones[i].SortTransform(bones);

			glUniformMatrix4fv(m_ShaderPrograms[m_Shader].SkeletonUni + i, 1, GL_TRUE, &bones[i].transform.GetDataPointer()[0]);
		}
	}
	if (light != vector3df(0))
		glUniform3f(m_ShaderPrograms[m_Shader].LightUni, light.x, light.y, light.z);
	glBindTexture(GL_TEXTURE_2D, tex);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	
	Bind(false, bForceNonAnimated);

	glDrawElements(GL_TRIANGLES, mesh->GetSize(), GL_UNSIGNED_INT, 0);
	
	Unbind(false, bForceNonAnimated);

	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void GLObject::RenderShadowMesh(Mesh* mesh, GLuint tex, const matrix4f &transform, bool bForceNonAnimated)
{
	// if there is no mesh, don't do anything
	if (!mesh)
		return;
	
	// if the mesh is empty, don't draw it
	if (mesh->GetSize() == 0)
		return;

	GLuint vbo = mesh->GetVBO();
	GLuint ebo = mesh->GetEBO();
	if (vbo == 0 || ebo == 0)
		return;
	
	// draw this mesh
	glUniformMatrix4fv(m_ShaderPrograms[m_ShadowShader].ModelUni, 1, GL_TRUE, transform.GetDataPointer());
	std::vector<Bone> bones = *mesh->GetSkeletonPointer();
	if (!bForceNonAnimated && m_bUseBones && !bones.empty())
	{
		for (unsigned int i = 0; i < bones.size(); ++i) {
			bones[i].SortTransform(bones);

			glUniformMatrix4fv(m_ShaderPrograms[m_ShadowShader].SkeletonUni + i, 1, GL_TRUE, &bones[i].transform.GetDataPointer()[0]);
		}
	}
	glBindTexture(GL_TEXTURE_2D, tex);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	
	Bind(true, bForceNonAnimated);

	glDrawElements(GL_TRIANGLES, mesh->GetSize(), GL_UNSIGNED_INT, 0);
	
	Unbind(true, bForceNonAnimated);

	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void GLObject::UpdateIndices()
{
	if (!m_bIndicesSorted)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufEBO);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, m_IndexCount * sizeof(GLuint), &m_Indices[0]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		m_bIndicesSorted = true;
	}
}

void GLObject::SetIndicesUnsorted()
{
	m_bIndicesSorted = false;
}

void GLObject::ReallocateVertices(GLuint size)
{
	m_Vertices.reserve(size);
}

void GLObject::ReallocateIndices(GLuint size)
{
	m_Indices.reserve(size);
}

void GLObject::LockMutex()
{
	m_Mutex.lock();
}

void GLObject::UnlockMutex()
{
	m_Mutex.unlock();
}

vector<GLuint> GLObject::GetIndices()
{
	return m_Indices;
}

void GLObject::SetIndices(const vector<GLuint> indices)
{
	m_Indices.clear();
	for (GLuint i = 0; i < indices.size(); ++i) {
		m_Indices.push_back(indices[i]);
	}
}

vector<GLVertex> GLObject::GetVertices()
{
	return m_Vertices;
}

void GLObject::UpdateTransformationMatrix(const vector3df& offset, const matrix4f& rot, const vector3df& scale)
{
	m_Transformation.Identity();
	m_Transformation.Scale(GetScale() * scale);

	if (rot != matrix4f(0.f))
		m_Transformation = rot.Multiply(m_Transformation);
	else
		m_Transformation.SetRotation(m_Rotation, m_RotationOffset);

	m_Transformation.SetTranslation(GetPosition() + offset);
}

matrix4f& GLObject::GetTransformationMatrix()
{
	return m_Transformation;
}

void GLObject::CreateBufferObjects()
{
	// create the buffers used by this GLObject
	m_Mutex.lock();
	Clear();

	// resize vectors to fit contents
	//m_Vertices.shrink_to_fit();
	//m_Indices.shrink_to_fit();

	// get size of indices
	m_IndexCount = m_Indices.size();
	if (!m_IndexCount)
	{
		m_Mutex.unlock();
		return;
	}
	
	// create the vertex buffer object
	glGenBuffers(1, &m_bufVBO);
	if (m_bufVBO > 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_bufVBO);
		glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(GLVertex), &m_Vertices[0], GL_STATIC_DRAW);

		/*GLVertex* test = (GLVertex *) glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);
		for (unsigned int i = 0; i < m_Vertices.size(); i++) {
			cout << "Vertex " << i << ": Position: " << test[i].pos.x << ", " << test[i].pos.y << ", " << test[i].pos.z << " | Colour: " << (int)test[i].col.r << ", " << (int)test[i].col.g << ", " << (int)test[i].col.b << ", " << (int)test[i].col.a;
			cout << " | UV: " << test[i].uv.x << ", " << test[i].uv.y << " | Normal: " << test[i].n.x << ", " << test[i].n.y << ", " << test[i].n.z << ", " << test[i].n.ZERO_PADDING << " == " << sizeof(test[i]) * i << endl;
		}
		glUnmapBuffer(GL_ARRAY_BUFFER);*/

		// create the element buffer object
		glGenBuffers(1, &m_bufEBO);
		if (m_bufEBO > 0)
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufEBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_IndexCount * sizeof(GLuint), &m_Indices[0], GL_STATIC_DRAW);
		}
		else
		{
			glDeleteBuffers(1, &m_bufVBO);
			m_bufVBO = 0;
		}

		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// now we have buffered the data, delete everything!
		if (m_bClearData)
		{
			m_Vertices.clear();
			m_Indices.clear();
		}
	}
	m_Mutex.unlock();
}

void GLObject::UseShaderProgram(GLuint index)
{
	m_Shader = index;
}

void GLObject::UseShadowShaderProgram(GLuint index)
{
	m_ShadowShader = index;
}

void GLObject::CalculateInView()
{
	// check if we're in the view frustum
	m_bInView = /*GetPosition().Distance(GetSceneManager()->GetActiveCamera()->GetPosition()) <= m_AABB.size ||*/
		m_AABB.QuickInViewFrustum(GetSceneManager()->GetActiveCamera()->GetViewFrustum(), GetPosition());
		//m_AABB.InViewFrustum(GetSceneManager()->GetActiveCamera()->GetViewFrustum());
}

void GLObject::CalculateInView(const vector3df& _where)
{
	// check if we're in the view frustum
	m_bInView = /*GetPosition().Distance(GetSceneManager()->GetActiveCamera()->GetPosition()) <= m_AABB.size ||*/
		m_AABB.QuickInViewFrustum(GetSceneManager()->GetActiveCamera()->GetViewFrustum(), _where);
	//m_AABB.InViewFrustum(GetSceneManager()->GetActiveCamera()->GetViewFrustum());
}

void GLObject::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible())
		return;
	
	// must be called by all base classes
	UpdateTransformationMatrix();

	// check if we're in the view frustum
	CalculateInView();
}

void GLObject::Bind(bool bShadows, bool bForceNonAnimated)
{
	size_t size = m_bUseBones && !bForceNonAnimated ? sizeof(GLBoneVertex) : sizeof(GLVertex);
	
	if (bShadows)
	{
		if (m_ShaderPrograms[m_ShadowShader].PositionAttr > -1)
			glEnableVertexAttribArray(m_ShaderPrograms[m_ShadowShader].PositionAttr);
		if (m_ShaderPrograms[m_ShadowShader].TextureAttr > -1)
			glEnableVertexAttribArray(m_ShaderPrograms[m_ShadowShader].TextureAttr);
		if (m_bUseBones)
		{
			if (m_ShaderPrograms[m_ShadowShader].BonesAttr > -1)
				glEnableVertexAttribArray(m_ShaderPrograms[m_ShadowShader].BonesAttr);
			if (m_ShaderPrograms[m_ShadowShader].WeightsAttr > -1)
				glEnableVertexAttribArray(m_ShaderPrograms[m_ShadowShader].WeightsAttr);
		}

		if (m_ShaderPrograms[m_ShadowShader].PositionAttr > -1)
			glVertexAttribPointer(m_ShaderPrograms[m_ShadowShader].PositionAttr, 3, GL_FLOAT, GL_FALSE, size, 0);
		if (m_ShaderPrograms[m_ShadowShader].TextureAttr > -1)
			glVertexAttribPointer(m_ShaderPrograms[m_ShadowShader].TextureAttr, 2, GL_SHORT, GL_FALSE, size, (GLvoid *)(16));
		if (m_bUseBones)
		{
			if (m_ShaderPrograms[m_ShadowShader].BonesAttr > -1)
				glVertexAttribPointer(m_ShaderPrograms[m_ShadowShader].BonesAttr, 4, GL_INT, GL_FALSE, size, (GLvoid *)(32));
			if (m_ShaderPrograms[m_ShadowShader].WeightsAttr > -1)
				glVertexAttribPointer(m_ShaderPrograms[m_ShadowShader].WeightsAttr, 4, GL_FLOAT, GL_FALSE, size, (GLvoid *)(48));
		}
	}
	else
	{
		if (m_ShaderPrograms[m_Shader].PositionAttr > -1)
			glEnableVertexAttribArray(m_ShaderPrograms[m_Shader].PositionAttr);
		if (m_ShaderPrograms[m_Shader].ColourAttr > -1)
			glEnableVertexAttribArray(m_ShaderPrograms[m_Shader].ColourAttr);
		if (m_ShaderPrograms[m_Shader].TextureAttr > -1)
			glEnableVertexAttribArray(m_ShaderPrograms[m_Shader].TextureAttr);
		if (m_ShaderPrograms[m_Shader].LightAttr > -1)
			glEnableVertexAttribArray(m_ShaderPrograms[m_Shader].LightAttr);
		if (m_ShaderPrograms[m_Shader].NormalAttr > -1)
			glEnableVertexAttribArray(m_ShaderPrograms[m_Shader].NormalAttr);
		if (m_ShaderPrograms[m_Shader].TangentAttr > -1)
			glEnableVertexAttribArray(m_ShaderPrograms[m_Shader].TangentAttr);
		if (m_bUseBones)
		{
			if (m_ShaderPrograms[m_Shader].BonesAttr > -1)
				glEnableVertexAttribArray(m_ShaderPrograms[m_Shader].BonesAttr);
			if (m_ShaderPrograms[m_Shader].WeightsAttr > -1)
				glEnableVertexAttribArray(m_ShaderPrograms[m_Shader].WeightsAttr);
		}

		if (m_ShaderPrograms[m_Shader].PositionAttr > -1)
			glVertexAttribPointer(m_ShaderPrograms[m_Shader].PositionAttr, 3, GL_FLOAT, GL_FALSE, size, 0);
		if (m_ShaderPrograms[m_Shader].ColourAttr > -1)
			glVertexAttribPointer(m_ShaderPrograms[m_Shader].ColourAttr, 4, GL_UNSIGNED_BYTE, GL_TRUE, size, (GLvoid *)(12));
		if (m_ShaderPrograms[m_Shader].TextureAttr > -1)
			glVertexAttribPointer(m_ShaderPrograms[m_Shader].TextureAttr, 2, GL_SHORT, GL_FALSE, size, (GLvoid *)(16));
		if (m_ShaderPrograms[m_Shader].LightAttr > -1)
			glVertexAttribPointer(m_ShaderPrograms[m_Shader].LightAttr, 4, GL_UNSIGNED_BYTE, GL_TRUE, size, (GLvoid *)(24));
		if (m_ShaderPrograms[m_Shader].NormalAttr > -1)
			glVertexAttribPointer(m_ShaderPrograms[m_Shader].NormalAttr, 4, GL_INT_2_10_10_10_REV, GL_FALSE, size, (GLvoid *)(20));
		if (m_ShaderPrograms[m_Shader].TangentAttr > -1)
			glVertexAttribPointer(m_ShaderPrograms[m_Shader].TangentAttr, 4, GL_INT_2_10_10_10_REV, GL_FALSE, size, (GLvoid *)(28));
		if (m_bUseBones)
		{
			if (m_ShaderPrograms[m_Shader].BonesAttr > -1)
				glVertexAttribPointer(m_ShaderPrograms[m_Shader].BonesAttr, 4, GL_INT, GL_FALSE, size, (GLvoid *)(32));
			if (m_ShaderPrograms[m_Shader].WeightsAttr > -1)
				glVertexAttribPointer(m_ShaderPrograms[m_Shader].WeightsAttr, 4, GL_FLOAT, GL_FALSE, size, (GLvoid *)(48));
		}
	}
}

void GLObject::Unbind(bool bShadows, bool bForceNonAnimated)
{
	if (bShadows)
	{
		if (m_ShaderPrograms[m_ShadowShader].PositionAttr > -1)
			glDisableVertexAttribArray(m_ShaderPrograms[m_ShadowShader].PositionAttr);
		if (m_ShaderPrograms[m_ShadowShader].TextureAttr > -1)
			glDisableVertexAttribArray(m_ShaderPrograms[m_ShadowShader].TextureAttr);
		if (m_bUseBones)
		{
			if (m_ShaderPrograms[m_ShadowShader].BonesAttr > -1)
				glDisableVertexAttribArray(m_ShaderPrograms[m_ShadowShader].BonesAttr);
			if (m_ShaderPrograms[m_ShadowShader].WeightsAttr > -1)
				glDisableVertexAttribArray(m_ShaderPrograms[m_ShadowShader].WeightsAttr);
		}
	}
	else
	{
		if (m_ShaderPrograms[m_Shader].PositionAttr > -1)
			glDisableVertexAttribArray(m_ShaderPrograms[m_Shader].PositionAttr);
		if (m_ShaderPrograms[m_Shader].ColourAttr > -1)
			glDisableVertexAttribArray(m_ShaderPrograms[m_Shader].ColourAttr);
		if (m_ShaderPrograms[m_Shader].TextureAttr > -1)
			glDisableVertexAttribArray(m_ShaderPrograms[m_Shader].TextureAttr);
		if (m_ShaderPrograms[m_Shader].LightAttr > -1)
			glDisableVertexAttribArray(m_ShaderPrograms[m_Shader].LightAttr);
		if (m_ShaderPrograms[m_Shader].NormalAttr > -1)
			glDisableVertexAttribArray(m_ShaderPrograms[m_Shader].NormalAttr);
		if (m_ShaderPrograms[m_Shader].TangentAttr > -1)
			glDisableVertexAttribArray(m_ShaderPrograms[m_Shader].TangentAttr);
		if (m_bUseBones)
		{
			if (m_ShaderPrograms[m_Shader].BonesAttr > -1)
				glDisableVertexAttribArray(m_ShaderPrograms[m_Shader].BonesAttr);
			if (m_ShaderPrograms[m_Shader].WeightsAttr > -1)
				glDisableVertexAttribArray(m_ShaderPrograms[m_Shader].WeightsAttr);
		}
	}
}

void GLObject::SetUniforms()
{
	glUniformMatrix4fv(m_ShaderPrograms[m_Shader].ModelUni, 1, GL_TRUE, GetTransformationMatrixDataPointer());
	if (m_bUseBones && !m_Bones.empty())
	{
		for (unsigned int i = 0; i < m_Bones.size(); ++i) {
			m_Bones[i].SortTransform(m_Bones);

			glUniformMatrix4fv(m_ShaderPrograms[m_Shader].SkeletonUni + i, 1, GL_TRUE, &m_Bones[i].transform.GetDataPointer()[0]);
		}
	}
	glUniform3f(m_ShaderPrograms[m_Shader].LightUni, m_LightAmount.x, m_LightAmount.y, m_LightAmount.z);
}

void GLObject::SetClearData(bool clear)
{
	m_bClearData = clear;
}

void GLObject::Render()
{
	// if we aren't visible then don't draw anything
	if (!IsVisible())
		return;
	
	// if there's nothing to draw then don't do anything
	if (m_Mesh && !m_Mesh->GetSize())
		return;
	else if (!m_Mesh && !GetBufferSize())
		return;

	// if we're not in view, DO NOTHING
	if (!InViewFrustum())
		return;

	// make sure we've got our indices sorted
	//UpdateIndices();

	// shadows
	SceneManager* pSceneMan = GetSceneManager();
	
	// draw this GLObject
	SetUniforms();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Texture.gltex);
	if (pSceneMan)
	{
		//glBindTexture(GL_TEXTURE_2D, pSceneMan->GetRoot()->CastTo<Game>()->GetDepthMap());
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, pSceneMan->GetRoot()->CastTo<Game>()->GetDepthMap());
	}
	if (m_ShaderPrograms[m_Shader].TangentAttr > -1 && m_NormalTexture.gltex > 0)
	{
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, m_NormalTexture.gltex);
	}
	if (m_Mesh)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_Mesh->GetVBO());
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Mesh->GetEBO());
	}
	else
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_bufVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufEBO);
	}
	
	Bind();

	if (m_Mesh)
		glDrawElements(GL_TRIANGLES, m_Mesh->GetSize(), GL_UNSIGNED_INT, 0);
	else
		glDrawElements(GL_TRIANGLES, m_IndexCount, GL_UNSIGNED_INT, 0);
	
	Unbind();

	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void GLObject::RenderShadows()
{
	// if we aren't visible then don't draw anything
	if (!IsVisible())
		return;

	// if there's nothing to draw then don't do anything
	if (m_Mesh && !m_Mesh->GetSize())
		return;
	else if (!m_Mesh && !GetBufferSize())
		return;

	// if we're not in view, DO NOTHING
	//if (!InViewFrustum())
	//	return;

	// make sure we've got our indices sorted
	UpdateIndices();

	// draw this GLObject
	glUniformMatrix4fv(m_ShaderPrograms[m_ShadowShader].ModelUni, 1, GL_TRUE, GetTransformationMatrixDataPointer());
	if (m_bUseBones && !m_Bones.empty())
	{
		for (unsigned int i = 0; i < m_Bones.size(); ++i) {
			m_Bones[i].SortTransform(m_Bones);

			glUniformMatrix4fv(m_ShaderPrograms[m_ShadowShader].SkeletonUni + i, 1, GL_TRUE, &m_Bones[i].transform.GetDataPointer()[0]);
		}
	}
	glBindTexture(GL_TEXTURE_2D, m_Texture.gltex);
	if (m_Mesh)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_Mesh->GetVBO());
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Mesh->GetEBO());
	}
	else
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_bufVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufEBO);
	}
	
	Bind(true);

	if (m_Mesh)
		glDrawElements(GL_TRIANGLES, m_Mesh->GetSize(), GL_UNSIGNED_INT, 0);
	else
		glDrawElements(GL_TRIANGLES, m_IndexCount, GL_UNSIGNED_INT, 0);
	
	Unbind(true);

	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

bool GLObject::DoesPointCollide(vector3df& p)
{
	// until we sort out bounding boxes and triangle collisions, NO
	return false;
}

bool GLObject::DoesRayCollide(line3df& l, vector3df& oo, GLfloat& out)
{
	// until we sort out bounding boxes, NO
	return false;
}

void GLObject::UpdateVBO()
{
	// update just the VBO for this GLObject
	// note if more vertices are added call CreateBufferObjects() instead
	m_Mutex.lock();
	glBindBuffer(GL_ARRAY_BUFFER, m_bufVBO);
	
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_Vertices.size() * sizeof(GLVertex), &m_Vertices[0]);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// get size of indices
	m_IndexCount = m_Indices.size();
	m_Mutex.unlock();
}

void GLObject::Add2DTriangle(const vector2df& pos1, const vector2df& uv1, const vector2df& pos2, const vector2df& uv2, const vector3df& pos3, const vector2df& uv3, const GLColour& col)
{
	// add a triangle - this is here for GUI only!
	// after adding this to a GUI element be aware ChangeQuad* won't work correctly for quads after this!
	// first vertex (0, 0)
	GLVertex ver1(vector3df(pos1.x, pos1.y, -20), col, vector2df(uv1.x, uv1.y), GLNormal());
	m_Vertices.push_back(ver1);

	// second vertex (1, 0)
	GLVertex ver2(vector3df(pos2.x, pos2.y, -20), col, vector2df(uv2.x, uv2.y), GLNormal());
	m_Vertices.push_back(ver2);

	// third vertex (1, 1)
	GLVertex ver3(vector3df(pos3.x, pos3.y, -20), col, vector2df(uv3.x, uv3.y), GLNormal());
	m_Vertices.push_back(ver3);

	// add to the indices
	GLuint iIndex = m_Vertices.size() - 3;
	m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2);
}

GLuint GLObject::AddQuad(const vector2df& pos1, const vector2df& uv1, const vector2df& pos2, const vector2df& uv2, const GLColour& col)
{
	// add a quad to this GLObject
	// returns an integer position of the new vertices added
	// NOTE: this function has been kept due to laziness - this is a 2D function only! used mainly in the GUI
	GLuint iIndex = m_Vertices.size();
	
	// first vertex (0, 0)
	GLVertex ver1(vector3df(pos1.x, pos1.y, -20), col, vector2df(uv1.x, uv2.y), GLNormal());
	m_Vertices.push_back(ver1);
	
	// second vertex (1, 0)
	GLVertex ver2(vector3df(pos2.x, pos1.y, -20), col, vector2df(uv2.x, uv2.y), GLNormal());
	m_Vertices.push_back(ver2);
	
	// third vertex (1, 1)
	GLVertex ver3(vector3df(pos2.x, pos2.y, -20), col, vector2df(uv2.x, uv1.y), GLNormal());
	m_Vertices.push_back(ver3);
	
	// fourth vertex (0, 1)
	GLVertex ver4(vector3df(pos1.x, pos2.y, -20), col, vector2df(uv1.x, uv1.y), GLNormal());
	m_Vertices.push_back(ver4);
	
	// add to the indices
	m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2);
	m_Indices.push_back(iIndex + 2); m_Indices.push_back(iIndex + 3); m_Indices.push_back(iIndex + 0);
	
	// return the position
	return iIndex / 4;
}

GLNormal CalculateTangent(const GLVertex& p1, const GLVertex& p2, const GLVertex& p3)
{
	// calculate a tangent for this triangle - bitanget will be done in the vertex shader (as we have no more space in our vbo)
	vector3df edge1 = p2.pos - p1.pos;
	vector3df edge2 = p3.pos - p1.pos;
	vector2df delta1 = p2.uv.ToVector() - p1.uv.ToVector();
	vector2df delta2 = p3.uv.ToVector() - p1.uv.ToVector();

	GLfloat f = 1.0f / (delta1.x * delta2.y - delta2.x * delta1.y);

	vector3df tangent(f * (delta2.y * edge1.x - delta1.y * edge2.x),
		f * (delta2.y * edge1.y - delta1.y * edge2.y),
		f * (delta2.y * edge1.z - delta1.y * edge2.z));
	tangent.Normal();

	vector3df n;
	p1.n.GetValues(n);
	tangent -= n * n.Dot(tangent);

	return GLNormal(tangent.Normal());
}

GLuint GLObject::AddQuad(GLVertex ver1, GLVertex ver2, GLVertex ver3, GLVertex ver4, bool flip)
{
	// add a quad to this GLObject
	// returns an integer position of the new vertices added
	GLuint iIndex = m_Vertices.size();

	// sort out the UV coordinates
	GLushort temp;
	ver1.uv.v = 8192 - ver1.uv.v;
	ver2.uv.v = 8192 - ver2.uv.v;
	ver3.uv.v = 8192 - ver3.uv.v;
	ver4.uv.v = 8192 - ver4.uv.v;

	temp = ver1.uv.v;
	ver1.uv.v = ver4.uv.v;
	ver4.uv.v = temp;

	temp = ver2.uv.v;
	ver2.uv.v = ver3.uv.v;
	ver3.uv.v = temp;

	// stop textures from having dem annoyin white lines yo
	/*ver1.uv *= 1.0000002f;
	ver2.uv *= 1.0000002f;
	ver3.uv *= 1.0000002f;
	ver4.uv *= 1.0000002f;*/

	// calculate the tangent - it WILL be the same for both faces
	GLNormal tangent = CalculateTangent(ver1, ver2, ver3);
	ver1.t = tangent; ver2.t = tangent; ver3.t = tangent; ver4.t = tangent;

	// push back the vertices
	m_Vertices.push_back(ver1); m_Vertices.push_back(ver2); m_Vertices.push_back(ver3); m_Vertices.push_back(ver4);

	// add to the indices
	if (flip)
	{
		m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2); m_Indices.push_back(iIndex + 3);
		m_Indices.push_back(iIndex + 3); m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1);
	}
	else
	{
		m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2);
		m_Indices.push_back(iIndex + 2); m_Indices.push_back(iIndex + 3); m_Indices.push_back(iIndex + 0);
	}

	// return the position
	return iIndex / 4;
}

void GLObject::AddTriangle(GLVertex ver1, GLVertex ver2, GLVertex ver3)
{
	// add a triangle to this GLObject
	GLuint iIndex = m_Vertices.size();

	// sort out the UV coordinates
	ver1.uv.v = 8192 - ver1.uv.v;
	ver2.uv.v = 8192 - ver2.uv.v;
	ver3.uv.v = 8192 - ver3.uv.v;

	// stop textures from having dem annoyin white lines yo
	/*ver1.uv *= 1.0000002f;
	ver2.uv *= 1.0000002f;
	ver3.uv *= 1.0000002f;*/

	// calculate the tangent
	GLNormal tangent = CalculateTangent(ver1, ver2, ver3);
	ver1.t = tangent; ver2.t = tangent; ver3.t = tangent;

	// push back the vertices
	m_Vertices.push_back(ver1); m_Vertices.push_back(ver2); m_Vertices.push_back(ver3);

	// add to the indices
	m_Indices.push_back(iIndex + 0); m_Indices.push_back(iIndex + 1); m_Indices.push_back(iIndex + 2);
}

void GLObject::ChangeQuadCoords(int pos, const vector2df& p1, const vector2df& p2, bool updateVBO)
{
	// changes the positions for this quad
	pos *= 4;

	m_Vertices[pos + 0].pos.x = p1.x; m_Vertices[pos + 0].pos.y = p1.y;
	m_Vertices[pos + 1].pos.x = p2.x; m_Vertices[pos + 1].pos.y = p1.y;
	m_Vertices[pos + 2].pos.x = p2.x; m_Vertices[pos + 2].pos.y = p2.y;
	m_Vertices[pos + 3].pos.x = p1.x; m_Vertices[pos + 3].pos.y = p2.y;

	if (updateVBO)
		UpdateVBO();
}

void GLObject::ChangeQuadCoords(int pos, const vector3df& p1, const vector3df& p2, const vector3df& p3, const vector3df& p4, bool updateVBO)
{
	// changes the positions for this quad
	pos *= 4;

	m_Vertices[pos + 0].pos.Set(p1);
	m_Vertices[pos + 1].pos.Set(p2);
	m_Vertices[pos + 2].pos.Set(p3);
	m_Vertices[pos + 3].pos.Set(p4);

	if (updateVBO)
		UpdateVBO();
}

void GLObject::ChangeQuadUV(int pos, const vector2df& uv1, const vector2df& uv2, bool updateVBO)
{
	// changes the UV for this quad
	pos *= 4;
	
	m_Vertices[pos + 0].uv.u = uv1.x * 8192; m_Vertices[pos + 0].uv.v = (1.0f - uv1.y) * 8192;
	m_Vertices[pos + 1].uv.u = uv2.x * 8192; m_Vertices[pos + 1].uv.v = (1.0f - uv1.y) * 8192;
	m_Vertices[pos + 2].uv.u = uv2.x * 8192; m_Vertices[pos + 2].uv.v = (1.0f - uv2.y) * 8192;
	m_Vertices[pos + 3].uv.u = uv1.x * 8192; m_Vertices[pos + 3].uv.v = (1.0f - uv2.y) * 8192;
	
	if (updateVBO)
		UpdateVBO();
}

void GLObject::ChangeQuadColour(int pos, const GLColour& col, bool updateVBO)
{
	// changes the colour for this quad
	pos *= 4;
	
	m_Vertices[pos + 0].col.r = col.r; m_Vertices[pos + 0].col.g = col.g; m_Vertices[pos + 0].col.b = col.b; m_Vertices[pos + 0].col.a = col.a;
	m_Vertices[pos + 1].col.r = col.r; m_Vertices[pos + 1].col.g = col.g; m_Vertices[pos + 1].col.b = col.b; m_Vertices[pos + 1].col.a = col.a;
	m_Vertices[pos + 2].col.r = col.r; m_Vertices[pos + 2].col.g = col.g; m_Vertices[pos + 2].col.b = col.b; m_Vertices[pos + 2].col.a = col.a;
	m_Vertices[pos + 3].col.r = col.r; m_Vertices[pos + 3].col.g = col.g; m_Vertices[pos + 3].col.b = col.b; m_Vertices[pos + 3].col.a = col.a;
	
	if (updateVBO)
		UpdateVBO();
}

void GLObject::ChangeQuadColour(int pos, const GLColour& col1, const GLColour& col2, const GLColour& col3, const GLColour& col4, bool updateVBO)
{
	// changes the colour for this quad
	pos *= 4;

	m_Vertices[pos + 0].col.r = col1.r; m_Vertices[pos + 0].col.g = col1.g; m_Vertices[pos + 0].col.b = col1.b; m_Vertices[pos + 0].col.a = col1.a;
	m_Vertices[pos + 1].col.r = col2.r; m_Vertices[pos + 1].col.g = col2.g; m_Vertices[pos + 1].col.b = col2.b; m_Vertices[pos + 1].col.a = col2.a;
	m_Vertices[pos + 2].col.r = col3.r; m_Vertices[pos + 2].col.g = col3.g; m_Vertices[pos + 2].col.b = col3.b; m_Vertices[pos + 2].col.a = col3.a;
	m_Vertices[pos + 3].col.r = col4.r; m_Vertices[pos + 3].col.g = col4.g; m_Vertices[pos + 3].col.b = col4.b; m_Vertices[pos + 3].col.a = col4.a;

	if (updateVBO)
		UpdateVBO();
}

void GLObject::AddKeyFunction(int key, int evt, bool(*fn)(GLObject*))
{
	GetSceneManager()->AddKeyFunction(this, key, evt, fn);
}

void GLObject::AddMouseFunction(int key, int evt, bool(*fn)(GLObject*))
{
	GetSceneManager()->AddMouseFunction(this, key, evt, fn);
}

void GLObject::AddScrollFunction(bool(*fn)(GLObject*, double, double))
{
	GetSceneManager()->AddScrollFunction(this, fn);
}

void GLObject::SortIndices(GLubyte dir)
{
	/*map<GLuint, vector3df> verts;

	for (GLuint i = 0; i < m_Indices.size(); i += 3) {
		vector3df point = vector3df((min(m_Vertices[m_Indices[i]].pos.x, m_Vertices[m_Indices[i + 1]].pos.x, m_Vertices[m_Indices[i + 2]].pos.x) +
									 max(m_Vertices[m_Indices[i]].pos.x, m_Vertices[m_Indices[i + 1]].pos.x, m_Vertices[m_Indices[i + 2]].pos.x)) / 2,
			(min(m_Vertices[m_Indices[i]].pos.y, m_Vertices[m_Indices[i + 1]].pos.y, m_Vertices[m_Indices[i + 2]].pos.y) +
			 max(m_Vertices[m_Indices[i]].pos.y, m_Vertices[m_Indices[i + 1]].pos.y, m_Vertices[m_Indices[i + 2]].pos.y)) / 2,
			(min(m_Vertices[m_Indices[i]].pos.z, m_Vertices[m_Indices[i + 1]].pos.z, m_Vertices[m_Indices[i + 2]].pos.z) +
			 max(m_Vertices[m_Indices[i]].pos.z, m_Vertices[m_Indices[i + 1]].pos.z, m_Vertices[m_Indices[i + 2]].pos.z)) / 2);
		verts[i] = point;
	}*/
	
	/*switch (dir)
	{
		case 0:
		case 4:
			std::sort(m_Indices.begin(), m_Indices.end(), [&](GLuint one, GLuint two) {
				//return (verts[one / 3].z < verts[two / 3].z);
				return (m_Vertices[one].pos.z < m_Vertices[two].pos.z && (m_Vertices[one].pos.x < m_Vertices[two].pos.x || m_Vertices[one].pos.y < m_Vertices[two].pos.y));
			});
			break;
		case 1:
			std::sort(m_Indices.begin(), m_Indices.end(), [&](GLuint one, GLuint two) {
				//return (verts[one / 3].x < verts[two / 3].x);
				return (m_Vertices[one].pos.x < m_Vertices[two].pos.x && (m_Vertices[one].pos.z < m_Vertices[two].pos.z || m_Vertices[one].pos.y < m_Vertices[two].pos.y));
			});
			break;
		case 2:
			std::sort(m_Indices.begin(), m_Indices.end(), [&](GLuint one, GLuint two) {
				//return (verts[two / 3].z < verts[one / 3].z);
				return (m_Vertices[two].pos.z < m_Vertices[one].pos.z && (m_Vertices[one].pos.x < m_Vertices[two].pos.x || m_Vertices[one].pos.y < m_Vertices[two].pos.y));
			});
			break;
		case 3:
			std::sort(m_Indices.begin(), m_Indices.end(), [&](GLuint one, GLuint two) {
				//return (verts[two / 3].x < verts[one / 3].x);
				return (m_Vertices[two].pos.x < m_Vertices[one].pos.x && (m_Vertices[one].pos.z < m_Vertices[two].pos.z || m_Vertices[one].pos.y < m_Vertices[two].pos.y));
			});
			break;
	}
	
	m_bIndicesSorted = false;*/
}

// getters / setters
vector3df GLObject::GetPosition()
{
	return m_Position;
}

void GLObject::SetPosition(const vector3df& pos)
{
	m_Position = pos;
}

void GLObject::TranslatePosition(const vector3df& translate)
{
	m_Position += translate;
}

void GLObject::TranslateWithRotation(GLfloat amount, const vector3df& axis)
{
	// translate this object with regards to the rotation on the given axis
	vector3df v = axis * m_Rotation;
	v.MinusOne();

	if (v.x != 0)
	{
		//
	}

	if (v.y != 0)
	{
		TranslatePosition(vector3df(amount * cos(v.y), 0, amount * sin(v.y)));
	}

	if (v.z != 0)
	{
		//
	}
}

vector3di GLObject::GetSize()
{
	return m_Size;
}

void GLObject::SetSize(const vector3di& size)
{
	m_Size = size;
}

vector3df GLObject::GetRotation()
{
	return m_Rotation;
}

void GLObject::SetRotation(const vector3df& axis, GLfloat angle, const vector3df& off)
{
	m_Rotation.Set(axis.x * angle, axis.y * angle, axis.z * angle);
	m_RotationOffset.Set(off);
}

void GLObject::Rotate(const vector3df& axis, GLfloat angle)
{
	m_Rotation.x += (axis.x * angle);
	m_Rotation.y += (axis.y * angle);
	m_Rotation.z += (axis.z * angle);
}

vector3df GLObject::GetScale()
{
	return m_Scale;
}

void GLObject::SetScale(const vector3df& scale)
{
	m_Scale = scale;
}

const GLfloat* GLObject::GetTransformationMatrixDataPointer()
{
	return m_Transformation.GetDataPointer();
}

void GLObject::SetAABB(const vector3df& bottomLeft, const vector3df& topRight)
{
	m_AABB.SetAABB(bottomLeft, topRight);
}

const AABB& GLObject::GetAABB() const
{
	return m_AABB;
}

bool GLObject::InViewFrustum() const
{
	return m_bInView;
}

void GLObject::SetTexture(const char* fileName)
{
	m_Texture = m_pParent->GetTextureManager()->GetTexture(fileName);
}

void GLObject::SetTexture(Texture tex)
{
	m_Texture = tex;
}

void GLObject::SetNormalTexture(const char* fileName)
{
	m_NormalTexture = m_pParent->GetTextureManager()->GetTexture(fileName);
}

Texture GLObject::GetTexture()
{
	return m_Texture;
}

GLuint GLObject::GetTextureWidth()
{
	return m_Texture.width;
}

GLuint GLObject::GetTextureHeight()
{
	return m_Texture.height;
}

void GLObject::SetVisible(bool visible)
{
	m_bVisible = visible;
}

bool GLObject::IsVisible()
{
	return m_bVisible;
}

void GLObject::SetManager(Manager* man)
{
	m_pParent = man;
}

Manager* GLObject::GetManager()
{
	return m_pParent;
}

SceneManager* GLObject::GetSceneManager()
{
	return m_pSceneMan;
}

void GLObject::SetMesh(Mesh* mesh, bool bChangeShaderProgram, bool bForceNoBones)
{
	m_Mesh = mesh;

	if (m_Mesh && m_Mesh->IsAnimated())
		SetUseBones(!bForceNoBones, bChangeShaderProgram);
	else
		SetUseBones(false, bChangeShaderProgram);

	SetSkeleton(mesh);
}

Mesh* GLObject::GetMesh()
{
	return m_Mesh;
}

void GLObject::SetSkeleton(Mesh* mesh)
{
	if (!mesh)
		return;
	
	m_Bones.clear();
	mesh->CopyBones(m_Bones);
}

GLuint GLObject::GetBoneCount()
{
	return m_Bones.size();
}

Bone& GLObject::GetBone(GLuint index)
{
	static Bone ret;
	
	if (index >= m_Bones.size())
		return ret;

	return m_Bones[index];
}

void GLObject::Animate(Animator* anim)
{
	AnimFrame lower, upper;
	GLfloat time;
	AnimateResult result = anim->Animate(lower, upper, time);
	if (result == animation_running)
	{
		for (GLuint i = 0; i < GetBoneCount(); ++i) {
			GetBone(i).pos.Set(Lerp(lower.translations[i], upper.translations[i], time));
			GetBone(i).rot.Set(Lerp(lower.rotations[i], upper.rotations[i], time));
		}
	}
	else if (result == animation_resetting)
	{
		for (GLuint i = 0; i < GetBoneCount(); ++i) {
			Interpolate(GetBone(i).pos.x, lower.translations[i].x, 10.f, 0.01f);
			Interpolate(GetBone(i).pos.y, lower.translations[i].y, 10.f, 0.01f);
			Interpolate(GetBone(i).pos.z, lower.translations[i].z, 10.f, 0.01f);
			Interpolate(GetBone(i).rot.x, lower.rotations[i].x, 10.f, 0.01f);
			Interpolate(GetBone(i).rot.y, lower.rotations[i].y, 10.f, 0.01f);
			Interpolate(GetBone(i).rot.z, lower.rotations[i].z, 10.f, 0.01f);
		}
	}
	else if (result == animation_still)
	{
		for (GLuint i = 0; i < GetBoneCount(); ++i) {
			GetBone(i).pos.Set(lower.translations[i]);
			GetBone(i).rot.Set(lower.rotations[i]);
		}
	}
}

void GLObject::SetFrameAtTime(Animator* anim, GLfloat t)
{
	AnimFrame lower, upper;
	GLfloat time;
	anim->SetAnimationPosition(t);
	AnimateResult result = anim->Animate(lower, upper, time, true);
	if (result == animation_running)
	{
		for (GLuint i = 0; i < GetBoneCount(); ++i) {
			GetBone(i).pos.Set(Lerp(lower.translations[i], upper.translations[i], time));
			GetBone(i).rot.Set(Lerp(lower.rotations[i], upper.rotations[i], time));
		}
	}
	else if (result == animation_resetting)
	{
		for (GLuint i = 0; i < GetBoneCount(); ++i) {
			Interpolate(GetBone(i).pos.x, lower.translations[i].x, 10.f, 0.01f);
			Interpolate(GetBone(i).pos.y, lower.translations[i].y, 10.f, 0.01f);
			Interpolate(GetBone(i).pos.z, lower.translations[i].z, 10.f, 0.01f);
			Interpolate(GetBone(i).rot.x, lower.rotations[i].x, 10.f, 0.01f);
			Interpolate(GetBone(i).rot.y, lower.rotations[i].y, 10.f, 0.01f);
			Interpolate(GetBone(i).rot.z, lower.rotations[i].z, 10.f, 0.01f);
		}
	}
	else if (result == animation_still)
	{
		for (GLuint i = 0; i < GetBoneCount(); ++i) {
			GetBone(i).pos.Set(lower.translations[i]);
			GetBone(i).rot.Set(lower.rotations[i]);
		}
	}
}

void GLObject::SetLightAmount(vector3df l)
{
	m_LightAmount = l;
}

vector3df GLObject::GetLightAmount()
{
	return m_LightAmount;
}

void GLObject::SetUseBones(bool use, bool bChange)
{
	m_bUseBones = use;

	if (bChange)
	{
		if (m_bUseBones)
		{
			UseShaderProgram(3);
			UseShadowShaderProgram(4);
		}
		else
		{
			UseShaderProgram(0);
			UseShadowShaderProgram(2);
		}
	}
}

bool GLObject::IsAnimated()
{
	return m_bUseBones;
}

void GLObject::AddShaderProgram(GLShaderProgram* program)
{
	GLShaderDetails d;
	d.Program = program;

	d.Program->Bind();
	d.PositionAttr = d.Program->GetAttribLocation("position");
	d.ColourAttr = d.Program->GetAttribLocation("colour");
	d.TextureAttr = d.Program->GetAttribLocation("texcoord");
	d.NormalAttr = d.Program->GetAttribLocation("normal");
	d.LightAttr = d.Program->GetAttribLocation("light");
	d.TangentAttr = d.Program->GetAttribLocation("tangent");
	d.BonesAttr = d.Program->GetAttribLocation("bones");
	d.WeightsAttr = d.Program->GetAttribLocation("weights");
	d.ModelUni = d.Program->GetUniformLocation("model");
	d.LightUni = d.Program->GetUniformLocation("lightAmount");
	d.LightPosUni = d.Program->GetUniformLocation("lightPos");
	d.SkeletonUni = d.Program->GetUniformLocation("skeleton");
	d.Program->Unbind();

	m_ShaderPrograms.push_back(d);
}

void GLObject::ClearShaderPrograms()
{
	for (unsigned int i = 0; i < m_ShaderPrograms.size(); ++i) {
		delete m_ShaderPrograms.at(i).Program;
	}
	
	m_ShaderPrograms.clear();
}

void GLObject::UpdateShaderPrograms()
{
	for (unsigned int i = 0; i < m_ShaderPrograms.size(); ++i) {
		m_ShaderPrograms.at(i).Program->Bind();
		m_ShaderPrograms.at(i).PositionAttr = m_ShaderPrograms.at(i).Program->GetAttribLocation("position");
		m_ShaderPrograms.at(i).ColourAttr = m_ShaderPrograms.at(i).Program->GetAttribLocation("colour");
		m_ShaderPrograms.at(i).TextureAttr = m_ShaderPrograms.at(i).Program->GetAttribLocation("texcoord");
		m_ShaderPrograms.at(i).NormalAttr = m_ShaderPrograms.at(i).Program->GetAttribLocation("normal");
		m_ShaderPrograms.at(i).LightAttr = m_ShaderPrograms.at(i).Program->GetAttribLocation("light");
		m_ShaderPrograms.at(i).TangentAttr = m_ShaderPrograms.at(i).Program->GetAttribLocation("tangent");
		m_ShaderPrograms.at(i).BonesAttr = m_ShaderPrograms.at(i).Program->GetAttribLocation("bones");
		m_ShaderPrograms.at(i).WeightsAttr = m_ShaderPrograms.at(i).Program->GetAttribLocation("weights");
		m_ShaderPrograms.at(i).ModelUni = m_ShaderPrograms.at(i).Program->GetUniformLocation("model");
		m_ShaderPrograms.at(i).LightUni = m_ShaderPrograms.at(i).Program->GetUniformLocation("lightAmount");
		m_ShaderPrograms.at(i).LightPosUni = m_ShaderPrograms.at(i).Program->GetUniformLocation("lightPos");
		m_ShaderPrograms.at(i).SkeletonUni = m_ShaderPrograms.at(i).Program->GetUniformLocation("skeleton");
		m_ShaderPrograms.at(i).Program->Unbind();
	}
}

GLShaderProgram* GLObject::GetShaderProgram(int iIndex)
{
	return m_ShaderPrograms.at(iIndex).Program;
}

GLShaderProgram** GLObject::GetPointerToShaderProgram(int iIndex)
{
	return &m_ShaderPrograms.at(iIndex).Program;
}
