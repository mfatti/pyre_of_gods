#include "Scrollbar.h"
#include "GUIManager.h"

Scrollbar::Scrollbar(GUIElement* scrollee, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size, GLubyte mode)
	: GUIElement(parent, guiParent, pos, size)
	, m_Scrollee(scrollee)
	, m_ScrollbarPos(0)
	, m_Height(0)
	, m_Width(0)
	, m_ScrollbarSize(0)
	, m_ScrollScale(0)
	, m_ScrollMode(mode)
	, m_bVertical(false)
	, m_bHorizontal(false)
{
	// add the scrollbar's quads
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(0, 0, 0, 100));
	//AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(255, 255, 255, 100));
	AddQuad(GLVertex(vector3df(0, 0, -20), GLColour(100, 100), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(200, 100), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(200, 100), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(100, 100), vector2df(0.5, 0.5), GLNormal()));

	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(0, 0, 0, 100));
	//AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(255, 255, 255, 100));
	AddQuad(GLVertex(vector3df(0, 0, -20), GLColour(100, 100), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(100, 100), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(200, 100), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(200, 100), vector2df(0.5, 0.5), GLNormal()));

	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df(0, (GLfloat)size.y - 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df(0, 1), vector2df(0.5, 0.5), vector2df(1, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df((GLfloat)size.x - 1, 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	CreateBufferObjects();

	SetDepth(scrollee->GetDepth() + 1);
}

void Scrollbar::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible())
		return;

	// scroll
	if (IsPressed(left_mouse))
	{
		vector3dd mouse = GetGUIManager()->GetMousePosition();
		vector3dd diff = mouse - m_PrevMousePos;
		//diff *= (m_Height / (GLfloat)GetSize().y);

		if (m_bVertical && m_Height > GetSize().y)
		{
			if (m_ScrollbarPos.y + (GLfloat)diff.y >= 0 && m_ScrollbarPos.y + m_ScrollbarSize.y + (GLfloat)diff.y <= (GLfloat)GetSize().y)
			{
				m_Scrollee->TranslatePosition(vector2df(0, (GLfloat)(-diff.y * m_ScrollScale.y)));
				m_ScrollbarPos.y += (GLfloat)diff.y;
			}
			else if (m_ScrollbarPos.y + m_ScrollbarSize.y + (GLfloat)diff.y > (GLfloat)GetSize().y)
			{
				GLfloat amount = (GLfloat)GetSize().y - (m_ScrollbarPos.y + m_ScrollbarSize.y);
				m_Scrollee->TranslatePosition(vector2df(0, -amount * m_ScrollScale.y));
				m_ScrollbarPos.y += amount;
			}
			else
			{
				GLfloat amount = -m_ScrollbarPos.y;
				m_Scrollee->TranslatePosition(vector2df(0, -amount * m_ScrollScale.y));
				m_ScrollbarPos.y += amount;
			}
		}
		else if (m_bHorizontal && m_Width > GetSize().x)
		{
			if (m_ScrollbarPos.x + (GLfloat)diff.x >= 0 && m_ScrollbarPos.x + m_ScrollbarSize.x + (GLfloat)diff.x <= (GLfloat)GetSize().x)
			{
				m_Scrollee->TranslatePosition(vector2df((GLfloat)(-diff.x * m_ScrollScale.x), 0));
				m_ScrollbarPos.x += (GLfloat)diff.x;
			}
			else if (m_ScrollbarPos.x + m_ScrollbarSize.x + (GLfloat)diff.x > (GLfloat)GetSize().x)
			{
				GLfloat amount = (GLfloat)GetSize().x - (m_ScrollbarPos.x + m_ScrollbarSize.x);
				m_Scrollee->TranslatePosition(vector2df(-amount * m_ScrollScale.x, 0));
				m_ScrollbarPos.x += amount;
			}
			else
			{
				GLfloat amount = -m_ScrollbarPos.x;
				m_Scrollee->TranslatePosition(vector2df(-amount * m_ScrollScale.x, 0));
				m_ScrollbarPos.x += amount;
			}
		}

		m_PrevMousePos = mouse;
	}

	// call GUIElement::Update()
	UpdateGUI();
	GUIElement::Update();
}

bool Scrollbar::IsHovered()
{
	if (!IsVisible())
	{
		return false;
	}

	if (!GetGUIManager())
	{
		return false;
	}

	if (IsVertical() && m_Height <= GetSize().y)
	{
		return false;
	}

	if (IsHorizontal() && m_Width <= GetSize().x)
	{
		return false;
	}

	vector3df pos = GetAbsolutePosition();
	vector3di size = GetSize();
	vector2dd mouse = GetGUIManager()->GetMousePosition();
	m_bVertical = IsVertical() && IsMouseInRect(mouse, vector2df(pos.x + size.x - 20, pos.y + m_ScrollbarPos.y), vector2df(pos.x + size.x, pos.y + m_ScrollbarPos.y + m_ScrollbarSize.y));
	m_bHorizontal = IsHorizontal() && IsMouseInRect(mouse, vector2df(pos.x + m_ScrollbarPos.x, pos.y + size.y - 20), vector2df(pos.x + size.x + m_ScrollbarPos.x + m_ScrollbarSize.x, pos.y + size.y));

	return (m_bVertical || m_bHorizontal);
}

void Scrollbar::MouseDownFunction(GLuint button)
{
	// start scrolling
	m_PrevMousePos = GetGUIManager()->GetMousePosition();
}

void Scrollbar::AddHeight(GLfloat height)
{
	m_Height += height;
	CalculateScrollbarSize();
}

void Scrollbar::RemoveHeight(GLfloat height)
{
	m_Height -= height;
	CalculateScrollbarSize();
}

void Scrollbar::ResetHeight()
{
	m_Height = 0;
}

GLfloat Scrollbar::GetHeight()
{
	return m_Height;
}

void Scrollbar::AddWidth(GLfloat width)
{
	m_Width += width;
	CalculateScrollbarSize();
}

void Scrollbar::RemoveWidth(GLfloat width)
{
	m_Width -= width;
	CalculateScrollbarSize();
}

void Scrollbar::ResetWidth()
{
	m_Width = 0;
}

GLfloat Scrollbar::GetWidth()
{
	return m_Width;
}

void Scrollbar::ScrollToTop()
{
	m_Scrollee->TranslatePosition(vector2df(m_ScrollbarPos.x * m_ScrollScale.x, m_ScrollbarPos.y * m_ScrollScale.y));
	m_ScrollbarPos.Set(0, 0);
}

void Scrollbar::ScrollToBottom()
{
	vector2df size((GLfloat)GetSize().x, (GLfloat)GetSize().y);
	m_Scrollee->TranslatePosition(vector2df((m_ScrollbarPos.x + m_ScrollbarSize.x - size.x) * m_ScrollScale.x, (m_ScrollbarPos.y + m_ScrollbarSize.y - size.y) * m_ScrollScale.y));
	m_ScrollbarPos.Set(size - m_ScrollbarSize);
}

void Scrollbar::Reset()
{
	ScrollToTop();
	ResetWidth();
	ResetHeight();
}

bool Scrollbar::IsHorizontal()
{
	return ((m_ScrollMode & horizontal_scroll) == horizontal_scroll);
}

bool Scrollbar::IsVertical()
{
	return ((m_ScrollMode & vertical_scroll) == vertical_scroll);
}

vector2df Scrollbar::GetScrollAmount()
{
	return m_ScrollbarPos * m_ScrollScale;
}

void Scrollbar::UpdateGUI()
{
	if (IsVertical())
	{
		if (m_Height > GetSize().y)
		{
			// we need the scrollbar to become active
			vector2di size = GetSize();
			ChangeQuadCoords(0, vector2df((GLfloat)size.x - 17, 0), vector2df((GLfloat)size.x, (GLfloat)size.y), false);
			ChangeQuadCoords(1, vector2df((GLfloat)size.x - 16, m_ScrollbarPos.y + 2), vector2df((GLfloat)size.x - 2, m_ScrollbarPos.y + m_ScrollbarSize.y - 2));
		}
		else
		{
			// we do not need the scrollbar
			ChangeQuadCoords(0, vector2df(), vector2df(), false);
			ChangeQuadCoords(1, vector2df(), vector2df());
		}
	}

	if (IsHorizontal())
	{
		if (m_Width > GetSize().x)
		{
			// we need the scrollbar to become active
			vector2di size = GetSize();
			ChangeQuadCoords(2, vector2df(0, (GLfloat)size.y - 17), vector2df((GLfloat)size.x, (GLfloat)size.y), false);
			ChangeQuadCoords(3, vector2df(m_ScrollbarPos.x + 2, (GLfloat)size.y - 16), vector2df(m_ScrollbarPos.x + m_ScrollbarSize.x - 2, (GLfloat)size.y - 2));
		}
		else
		{
			// we do not need the scrollbar
			ChangeQuadCoords(2, vector2df(), vector2df(), false);
			ChangeQuadCoords(3, vector2df(), vector2df());
		}
	}
}

void Scrollbar::CalculateScrollbarSize()
{
	if (IsVertical())
	{
		m_ScrollbarSize.y = GetSize().y * (GetSize().y / m_Height);
		m_ScrollScale.y = (m_Height / (GLfloat)GetSize().y);
	}

	if (IsHorizontal())
	{
		m_ScrollbarSize.x = GetSize().x * (GetSize().x / m_Width);
		m_ScrollScale.x = (m_Width / (GLfloat)GetSize().x);
	}
}