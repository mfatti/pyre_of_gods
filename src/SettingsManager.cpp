#include "SettingsManager.h"
#include "GLFW/glfw3.h"

SettingsManager::SettingsManager(string filename)
	: m_SettingsFile(filename)
{
	ReadFile();
	LoadKeyNames();
}

SettingsManager::~SettingsManager()
{
	m_Settings.clear();
	m_SettingsFile.Close();
	m_KeyNames.clear();
	m_KeyMap.clear();
}

string SettingsManager::GetValueAsString(string name)
{
	return m_Settings[name];
}

int SettingsManager::GetValueAsInt(string name)
{
	return atoi(m_Settings[name].c_str());
}

bool SettingsManager::GetValueAsBool(string name)
{
	return (atoi(m_Settings[name].c_str()) > 0);
}

void SettingsManager::ReadFile()
{
	// clear the file
	m_Settings.clear();

	// ensure the file has been written
	WriteDefaults();

	// read in display settings
	m_Settings["width"] = m_SettingsFile.ReadSetting("width");
	m_Settings["height"] = m_SettingsFile.ReadSetting("height");
	m_Settings["fullscreen"] = m_SettingsFile.ReadSetting("fullscreen");
	m_Settings["vsync"] = m_SettingsFile.ReadSetting("vsync");
	m_Settings["borderless"] = m_SettingsFile.ReadSetting("borderless");
	m_Settings["perpixel"] = m_SettingsFile.ReadSetting("perpixel");
	m_Settings["normalmaps"] = m_SettingsFile.ReadSetting("normalmaps");
	m_Settings["shadows"] = m_SettingsFile.ReadSetting("shadows");

	// read in the control bindings
	m_Settings["move_up"] = m_SettingsFile.ReadSetting("move_up");
	m_Settings["move_down"] = m_SettingsFile.ReadSetting("move_down");
	m_Settings["move_left"] = m_SettingsFile.ReadSetting("move_left");
	m_Settings["move_right"] = m_SettingsFile.ReadSetting("move_right");
	m_Settings["jump"] = m_SettingsFile.ReadSetting("jump");
	m_Settings["roll"] = m_SettingsFile.ReadSetting("roll");
	m_Settings["sprint"] = m_SettingsFile.ReadSetting("sprint");
	m_Settings["inventory"] = m_SettingsFile.ReadSetting("inventory");
	m_Settings["menu"] = m_SettingsFile.ReadSetting("menu");
}

void SettingsManager::WriteFile()
{
	// clear the INI file
	m_SettingsFile.Clear();

	// write the display settings
	m_SettingsFile.WriteSection("Display");
	m_SettingsFile.WriteSetting("width", m_Settings["width"]);
	m_SettingsFile.WriteSetting("height", m_Settings["height"]);
	m_SettingsFile.WriteSetting("fullscreen", m_Settings["fullscreen"]);
	m_SettingsFile.WriteSetting("vsync", m_Settings["vsync"]);
	m_SettingsFile.WriteSetting("borderless", m_Settings["borderless"]);
	m_SettingsFile.WriteSetting("perpixel", m_Settings["perpixel"]);
	m_SettingsFile.WriteSetting("normalmaps", m_Settings["normalmaps"]);
	m_SettingsFile.WriteSetting("shadows", m_Settings["shadows"]);
	m_SettingsFile.WriteSpace();

	m_SettingsFile.WriteSection("Controls");
	m_SettingsFile.WriteSetting("move_up", m_Settings["move_up"]);
	m_SettingsFile.WriteSetting("move_down", m_Settings["move_down"]);
	m_SettingsFile.WriteSetting("move_left", m_Settings["move_left"]);
	m_SettingsFile.WriteSetting("move_right", m_Settings["move_right"]);
	m_SettingsFile.WriteSetting("jump", m_Settings["jump"]);
	m_SettingsFile.WriteSetting("roll", m_Settings["roll"]);
	m_SettingsFile.WriteSetting("sprint", m_Settings["sprint"]);
	m_SettingsFile.WriteSetting("inventory", m_Settings["inventory"]);
	m_SettingsFile.WriteSetting("menu", m_Settings["menu"]);

	// finally, write the file
	m_SettingsFile.Write();
}

string SettingsManager::GetKeyName(int key)
{
	return m_KeyNames[key];
}

int SettingsManager::GetKeyFromName(string name)
{
	std::map<int, string>::iterator it = m_KeyNames.begin();

	while (it != m_KeyNames.end())
	{
		if (it->second == name)
			return it->first;

		++it;
	}

	return 0;
}

void SettingsManager::WriteDefaults()
{
	// the defaults
	const string defaults = "[Display]\n"
		"width=1280\n"
		"height=720\n"
		"fullscreen=0\n"
		"vsync=1\n"
		"borderless=0\n"
		"perpixel=1\n"
		"normalmaps=1\n"
		"shadows=2\n"
		"\n"
		"[Controls]\n"
		"move_up=87\n"
		"move_down=83\n"
		"move_left=65\n"
		"move_right=68\n"
		"jump=32\n"
		"roll=341\n"
		"sprint=342\n"
		"inventory=258\n"
		"menu=256\n";

	// if the file is empty, write the defaults
	if (m_SettingsFile.IsEmpty())
		m_SettingsFile.WriteDefaults(defaults);
}

void SettingsManager::LoadKeyNames()
{
	// painful - set up the names of each key
	m_KeyNames[GLFW_KEY_SPACE] = "Space";
	m_KeyNames[GLFW_KEY_LEFT_ALT] = "Left Alt";
	m_KeyNames[GLFW_KEY_LEFT_CONTROL] = "Left Ctrl";
	m_KeyNames[GLFW_KEY_APOSTROPHE] = "'";
	m_KeyNames[GLFW_KEY_COMMA] = ",";
	m_KeyNames[GLFW_KEY_MINUS] = "-";
	m_KeyNames[GLFW_KEY_PERIOD] = ".";
	m_KeyNames[GLFW_KEY_SLASH] = "/";
	m_KeyNames[GLFW_KEY_0] = "Keyboard 0";
	m_KeyNames[GLFW_KEY_1] = "Keyboard 1";
	m_KeyNames[GLFW_KEY_2] = "Keyboard 2";
	m_KeyNames[GLFW_KEY_3] = "Keyboard 3";
	m_KeyNames[GLFW_KEY_4] = "Keyboard 4";
	m_KeyNames[GLFW_KEY_5] = "Keyboard 5";
	m_KeyNames[GLFW_KEY_6] = "Keyboard 6";
	m_KeyNames[GLFW_KEY_7] = "Keyboard 7";
	m_KeyNames[GLFW_KEY_8] = "Keyboard 8";
	m_KeyNames[GLFW_KEY_9] = "Keyboard 9";
	m_KeyNames[GLFW_KEY_SEMICOLON] = ";";
	m_KeyNames[GLFW_KEY_EQUAL] = "=";
	m_KeyNames[GLFW_KEY_A] = "A";
	m_KeyNames[GLFW_KEY_B] = "B";
	m_KeyNames[GLFW_KEY_C] = "C";
	m_KeyNames[GLFW_KEY_D] = "D";
	m_KeyNames[GLFW_KEY_E] = "E";
	m_KeyNames[GLFW_KEY_F] = "F";
	m_KeyNames[GLFW_KEY_G] = "G";
	m_KeyNames[GLFW_KEY_H] = "H";
	m_KeyNames[GLFW_KEY_I] = "I";
	m_KeyNames[GLFW_KEY_J] = "J";
	m_KeyNames[GLFW_KEY_K] = "K";
	m_KeyNames[GLFW_KEY_L] = "L";
	m_KeyNames[GLFW_KEY_M] = "M";
	m_KeyNames[GLFW_KEY_N] = "N";
	m_KeyNames[GLFW_KEY_O] = "O";
	m_KeyNames[GLFW_KEY_P] = "P";
	m_KeyNames[GLFW_KEY_Q] = "Q";
	m_KeyNames[GLFW_KEY_R] = "R";
	m_KeyNames[GLFW_KEY_S] = "S";
	m_KeyNames[GLFW_KEY_T] = "T";
	m_KeyNames[GLFW_KEY_U] = "U";
	m_KeyNames[GLFW_KEY_V] = "V";
	m_KeyNames[GLFW_KEY_W] = "W";
	m_KeyNames[GLFW_KEY_X] = "X";
	m_KeyNames[GLFW_KEY_Y] = "Y";
	m_KeyNames[GLFW_KEY_Z] = "Z";
	m_KeyNames[GLFW_KEY_LEFT_BRACKET] = "[";
	m_KeyNames[GLFW_KEY_BACKSLASH] = "#"; // english keyboard
	m_KeyNames[GLFW_KEY_WORLD_2] = "\\"; // english keyboard
	m_KeyNames[GLFW_KEY_RIGHT_BRACKET] = "]";
	m_KeyNames[GLFW_KEY_GRAVE_ACCENT] = "`";
	m_KeyNames[GLFW_KEY_ESCAPE] = "Esc";
	m_KeyNames[GLFW_KEY_ENTER] = "Enter";
	m_KeyNames[GLFW_KEY_TAB] = "Tab";
	m_KeyNames[GLFW_KEY_BACKSPACE] = "Backspace";
	m_KeyNames[GLFW_KEY_INSERT] = "Insert";
	m_KeyNames[GLFW_KEY_DELETE] = "Delete";
	m_KeyNames[GLFW_KEY_RIGHT] = "Right";
	m_KeyNames[GLFW_KEY_LEFT] = "Left";
	m_KeyNames[GLFW_KEY_DOWN] = "Down";
	m_KeyNames[GLFW_KEY_UP] = "Up";
	m_KeyNames[GLFW_KEY_PAGE_UP] = "Page Up";
	m_KeyNames[GLFW_KEY_PAGE_DOWN] = "Page Down";
	m_KeyNames[GLFW_KEY_HOME] = "Home";
	m_KeyNames[GLFW_KEY_END] = "End";
	m_KeyNames[GLFW_KEY_PAUSE] = "Pause/Break";
	m_KeyNames[GLFW_KEY_F1] = "F1";
	m_KeyNames[GLFW_KEY_F2] = "F2";
	m_KeyNames[GLFW_KEY_F3] = "F3";
	m_KeyNames[GLFW_KEY_F4] = "F4";
	m_KeyNames[GLFW_KEY_F5] = "F5";
	m_KeyNames[GLFW_KEY_F6] = "F6";
	m_KeyNames[GLFW_KEY_F7] = "F7";
	m_KeyNames[GLFW_KEY_F8] = "F8";
	m_KeyNames[GLFW_KEY_F9] = "F9";
	m_KeyNames[GLFW_KEY_F10] = "F10";
	m_KeyNames[GLFW_KEY_F11] = "F11";
	m_KeyNames[GLFW_KEY_F12] = "F12";
	m_KeyNames[GLFW_KEY_KP_0] = "Numpad 0";
	m_KeyNames[GLFW_KEY_KP_1] = "Numpad 1";
	m_KeyNames[GLFW_KEY_KP_2] = "Numpad 2";
	m_KeyNames[GLFW_KEY_KP_3] = "Numpad 3";
	m_KeyNames[GLFW_KEY_KP_4] = "Numpad 4";
	m_KeyNames[GLFW_KEY_KP_5] = "Numpad 5";
	m_KeyNames[GLFW_KEY_KP_6] = "Numpad 6";
	m_KeyNames[GLFW_KEY_KP_7] = "Numpad 7";
	m_KeyNames[GLFW_KEY_KP_8] = "Numpad 8";
	m_KeyNames[GLFW_KEY_KP_9] = "Numpad 9";
	m_KeyNames[GLFW_KEY_KP_DECIMAL] = "Numpad .";
	m_KeyNames[GLFW_KEY_KP_DIVIDE] = "Numpad /";
	m_KeyNames[GLFW_KEY_KP_MULTIPLY] = "Numpad *";
	m_KeyNames[GLFW_KEY_KP_SUBTRACT] = "Numpad -";
	m_KeyNames[GLFW_KEY_KP_ADD] = "Numpad +";
	m_KeyNames[GLFW_KEY_KP_ENTER] = "Numpad Enter";
	m_KeyNames[GLFW_KEY_KP_EQUAL] = "Numpad =";
}