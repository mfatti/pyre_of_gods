#ifndef _BUTTON_H_
#define _BUTTON_H_

#include "Text.h"

class Button : public GUIElement {
	public:
		Button(const char* text, Font* font, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size);
		
		virtual void Update(bool bForce = false);
		
		virtual void MouseDownFunction(GLuint button);

		void SetText(string text);
		string GetText();
	
	protected:
	
	private:
		bool	m_bSwitched;
};

#endif
