#include "Tab.h"
#include "GUIManager.h"

Tab::Tab(string name, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size)
	: GUIElement(parent, guiParent, pos, size)
	, m_strName(name)
{
	// add the tab background
	//AddQuad(vector2df(2, 2), vector2df(0.5, 0.5), vector2df((GLfloat)size.x - 2, (GLfloat)size.y - 2), vector2df(0.5, 0.5), GLColour(0, 0, 0, 100));
	AddQuad(GLVertex(vector3df(2, 2, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x - 2, 2, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x - 2, (GLfloat)size.y - 2, -20), GLColour(0), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(2, (GLfloat)size.y - 2, -20), GLColour(0), vector2df(0.5, 0.5), GLNormal()));
	CreateBufferObjects();
}

void Tab::MouseDownFunction(GLuint button)
{
	// does nothing
}

string Tab::GetName()
{
	return m_strName;
}

TabManager::TabManager(Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size)
	: GUIElement(parent, guiParent, pos, size)
	, m_TabCount(0)
{
	// construct the frame for the tabs
	AddQuad(vector2df(0, (GLfloat)size.y - 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df(0, 24), vector2df(0.5, 0.5), vector2df(1, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df((GLfloat)size.x - 1, 24), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(61));

	//AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(0, 0, 0, 100));
	AddQuad(GLVertex(vector3df(0, 0, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(0, 0, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df(), vector2df(0.5, 0.5), GLColour(61));

	//AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));

	CreateBufferObjects();
}

void TabManager::MouseDownFunction(GLuint button)
{
	// check if we're selecting tabs
	vector2dd mousePos = GetGUIManager()->GetMousePosition();
	vector2df pos = GetPosition();
	if (mousePos.y > pos.y + 24)
		return;
	if (mousePos.x > pos.x + GetWidthUpToTab(m_TabCount))
		return;

	// make the currently selected tab invisible
	Tab* active = GetActiveTab();
	active->SetVisible(false);

	// switch to the selected tab
	for (GLuint i = 0; i < m_TabCount; ++i) {
		if (mousePos.x >= pos.x + GetWidthUpToTab(i) && mousePos.x < pos.x + GetWidthUpToTab(i + 1))
		{
			GetTab(i)->SetVisible(true);
			UpdateGUI(i);
			return;
		}
	}

	// if for whatever reason the above code failed, set the previously active tab to active
	active->SetVisible(true);
}

Tab* TabManager::AddTab(string name)
{
	// create a new tab
	Tab* t = new Tab(name, GetManager(), this, vector2df(0, 24), GetSize() - vector2di(0, 24));
	AddChild(t);
	t->SetTexture(GetTexture());
	if (IsMenu())
		t->SetMenu();
	++m_TabCount;

	if (m_TabCount > 1)
		t->SetVisible(false);
	else
		UpdateGUI(0);

	// add the tab to the VBO
	GLuint width = GetWidthUpToTab(m_TabCount - 1);

	// add a new text object
	Text* txt = GetGUIManager()->CreateText(name.c_str(), vector2df((GLfloat)width + 10, 5), vector2di(), NULL, this);
	txt->SetSize(vector3di(GetWidthUpToTab(m_TabCount) - width, -1, 0));
	txt->SetText(name.c_str(), true);

	// return the new tab
	return t;
}

Tab* TabManager::GetTab(string name)
{
	// get the tab from the given name
	vector<GUIElement*> children = GetChildren();

	vector<GUIElement*>::iterator it = children.begin();
	while (it != children.end())
	{
		Tab* t = dynamic_cast<Tab*>(*it);

		if (t && t->GetName() == name)
			return t;

		++it;
	}

	return NULL;
}

Tab* TabManager::GetTab(GLuint pos)
{
	// get the tab from the given position
	vector<GUIElement*> children = GetChildren();
	GLuint count = 0;

	vector<GUIElement*>::iterator it = children.begin();
	while (it != children.end())
	{
		Tab* t = dynamic_cast<Tab*>(*it);

		if (t && count == pos)
			return t;
		else if (t)
			++count;

		++it;
	}

	return NULL;
}

Tab* TabManager::GetActiveTab()
{
	// get the tab from the given position
	vector<GUIElement*> children = GetChildren();

	vector<GUIElement*>::iterator it = children.begin();
	while (it != children.end())
	{
		Tab* t = dynamic_cast<Tab*>(*it);

		if (t && t->IsVisible())
			return t;

		++it;
	}

	return NULL;
}

GLuint TabManager::GetWidthUpToTab(GLuint tab)
{
	// get the size of tabs up to the given tab
	vector<GUIElement*> children = GetChildren();
	GLuint width = 0;
	GLuint count = 0;
	Font* fnt = const_cast<Font*>(GetGUIManager()->GetDefaultFont());

	vector<GUIElement*>::iterator it = children.begin();
	while (it != children.end() && count < tab)
	{
		Tab* t = dynamic_cast<Tab*>(*it);

		if (t)
		{
			width += fnt->GetStringWidth(t->GetName()) + 22;
			++count;
		}

		++it;
	}

	return width;
}

void TabManager::UpdateGUI(GLuint tab)
{
	// update the quad positions so that the currently selected tab is drawn
	GLfloat before = (GLfloat)GetWidthUpToTab(tab);
	GLfloat after = (GLfloat)GetWidthUpToTab(tab + 1);
	ChangeQuadCoords(3, vector2df(before + 2, 2), vector2df(after - 2, 26), false); // tab background
	ChangeQuadCoords(4, vector2df(before, 0), vector2df(before + 1, 24), false); // left edge of tab
	ChangeQuadCoords(5, vector2df(after - 1, 0), vector2df(after, 24), false); // right edge of tab
	ChangeQuadCoords(6, vector2df(before, 0), vector2df(after, 1), false); // top of tab
	ChangeQuadCoords(7, vector2df(0, 24), vector2df(before + 1, 25), false); // line leading up to tab
	ChangeQuadCoords(8, vector2df(after - 1, 24), vector2df((GLfloat)GetSize().x, 25)); // line leading from after tab
}