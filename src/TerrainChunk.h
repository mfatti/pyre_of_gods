#ifndef _TERRAIN_CHUNK_H_
#define _TERRAIN_CHUNK_H_

#define CHUNK_SIZE 16
#define ONE_OVER_CHUNK_SIZE 1.f / CHUNK_SIZE
#define BLOCK_SIZE 10.0f
#define ONE_OVER_BLOCK_SIZE 1.f / BLOCK_SIZE
#define CHUNK_WATER_BUFFER_SIZE CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE * sizeof(GLubyte)
#define COMPRESSED_CHUNK_BUFFER_SIZE CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE * 3

#include "GLObject.h"
#include <map>
#include <list>
#include <queue>
#include <mutex>
#include "zlib.h"

using std::pair;
using std::queue;

class Terrain;
class Player;
class ParticleEmitter;

// TerrainBlock structure
#pragma pack(1)
struct TerrainBlock {
	GLuint BlockType : 24;
	GLuint RotX : 1;
	GLuint RotY : 1;
	GLuint RotZ : 1;
	GLuint Multi : 1;
	GLuint Ignored : 1;
	GLuint XX : 1; // lower left
	GLuint XY : 1; // lower right
	GLuint YX : 1; // upper left
	GLuint YY : 1; // upper right
	GLuint LightRed : 4;
	GLuint LightGreen : 4;
	GLuint LightBlue : 4;
	GLuint SkyLight : 4;
	GLuint Solid : 1;
	GLuint Water : 1;
	GLuint Leaked : 1;
	GLuint Half : 1;

	GLuint padding : 11;

	bool operator==(const TerrainBlock& b) { return (BlockType == b.BlockType); }
	bool operator!=(const TerrainBlock& b) { return !(*this == b); }

	TerrainBlock() { BlockType = 0; RotX = 0; RotY = 0; RotZ = 0; Multi = 0; Ignored = 1; XX = 0; XY = 0; YX = 0; YY = 0; LightRed = 0; LightGreen = 0; LightBlue = 0; SkyLight = 0; Solid = 0; Water = 0; Leaked = 0; Half = 0; }
	TerrainBlock(GLuint type, GLuint rotx = 0, GLuint roty = 0, GLuint rotz = 0, GLuint multi = 0, GLuint ignored = 0, GLuint xx = 0, GLuint xy = 0, GLuint yx = 0, GLuint yy = 0,
		GLuint red = 0, GLuint green = 0, GLuint blue = 0, GLuint sky = 0, GLuint s = 2, GLuint water = 0, GLuint leaked = 0, GLuint half = 0)
	{
		BlockType = type; RotX = rotx; RotY = roty; RotZ = rotz; Multi = multi; Ignored = ignored; XX = xx; XY = xy; YX = yx; YY = yy; LightRed = red; LightGreen = green; LightBlue = blue; SkyLight = sky; Solid = s < 2 ? s : type > 0 ? 1 : 0; Water = water; Leaked = leaked; Half = half;
	}
};
#pragma pack()

#define CHUNK_BUFFER_SIZE CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE * sizeof(TerrainBlock)

// animated object struct
struct AnimatedBlock {
	GLObject* obj;
	Animator* anim;
	bool opening;
};

// face visibility struct
struct FaceVisibility {
	GLuint NorthSouth : 1;
	GLuint NorthWest : 1;
	GLuint NorthEast : 1;
	GLuint NorthUp : 1;
	GLuint NorthDown : 1;

	GLuint SouthEast : 1;
	GLuint SouthWest : 1;
	GLuint SouthUp : 1;
	GLuint SouthDown : 1;

	GLuint EastWest : 1;
	GLuint EastUp : 1;
	GLuint EastDown : 1;

	GLuint WestUp : 1;
	GLuint WestDown : 1;

	GLuint UpDown : 1;

	GLuint Initialised : 1;

	FaceVisibility() { memset(this, 0, sizeof(FaceVisibility)); Initialised = 1; }
};

class TerrainChunk : public GLObject {
public:
	TerrainChunk(Manager* parent, Terrain* terrain, vector3di chunkPos);
	~TerrainChunk();

	//void GenerateTerrain(vector<GLfloat> noise);
	void UnpackTerrain(Bytef* buf, uLongf len, bool bCreateVisibility = true);
	void UnpackWater(Bytef* buf, uLongf len);
	bool CompressTerrain(Bytef** buf, uLongf& len);
	bool CompressWater(Bytef** buf, uLongf& len);
	void CalculateFaceVisibility();
	bool CanFaceSeeFace(int face1, int face2);
	bool CanSeeFace(int face);

	void CreateMesh();
	void ClearMesh();

	bool DoesRayCollide(line3df& l, vector3df& oo, GLfloat& out);

	bool IsCreated();
	bool IsCreating();
	void SetCreating(bool set = true);
	bool IsInitialised();
	bool IsWaterInitialised();

	vector3di GetChunkPos();

	TerrainBlock* ArrayPointer();
	TerrainBlock GetBlock(GLint x, GLint y, GLint z) const;
	TerrainBlock GetBlock(const vector3di& pos) const;
	TerrainBlock& BlockAt(const vector3di& pos);
	void SetBlock(GLint x, GLint y, GLint z, TerrainBlock type);
	void SetBlock(vector3di pos, TerrainBlock type);
	void DestroyBlock(GLint x, GLint y, GLint z);

	void Interact(vector3di pos, bool open);

	void SortParticleEmitter(GLint x, GLint y, GLint z, TerrainBlock b);

	void SetBlockLight(const vector3di& pos, GLubyte red, GLuint green, GLuint blue);
	void SetSunLight(const vector3di& pos, GLubyte val);

	void AddToMultiBlockList(vector3di block, std::list<vector3di>* list);
	void RemoveFromMultiBlockList(vector3di block);
	std::list<vector3di>* GetMultiBlockList(vector3di block);

	void DoSunlight(vector<GLfloat> noise);
	bool IsSunlightDone();

	void SetWater(const vector3di& pos, GLfloat water);
	void SetNewWater(const vector3di& pos, GLfloat water);
	GLfloat GetWater(const vector3di& pos) const;
	GLfloat GetNewWater(const vector3di& pos) const;
	void AddWater(const vector3di& pos, GLfloat water);

	//virtual void SortIndices(GLubyte dir);

	virtual void Update(bool bForce = false);
	void UpdateWater();
	virtual void Render(bool& bCreate);
	virtual void RenderShadows();

	void RenderWater();

protected:

private:
	struct BlockMesh {
		TerrainBlock type;
		matrix4f transform;
		Mesh mesh;
		Texture texture;
	};

	TerrainBlock	m_Terrain[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
	FaceVisibility	m_Visibility;
	vector3di		m_ChunkPos;
	bool			m_bMeshCreated;
	bool			m_bBuffersCreated;
	bool			m_bCreating;
	bool			m_bInitialised;
	bool			m_bWaterInitialised;
	bool			m_bSunlightCreated;
	bool			m_bBlockMeshesCreated;
	//map<vector3di, BlockMesh>						m_BlockMeshes;
	map<GLuint, Mesh*>								m_BlockMeshes;
	map<vector3di, std::list<vector3di>*>			m_MultiBlockLists;
	map<vector3di, vector<ParticleEmitter*>>		m_BlockParticleEmitters;
	map<vector3di, TerrainBlock>					m_ParticleEmittersToCreate;
	map<vector3di, AnimatedBlock>					m_AnimatedBlocks;

	Terrain*		m_pTerrain;
	std::mutex		m_Mutex;
	GLint			m_CreatingCounter;

	TerrainBlock _GetBlock(GLint x, GLint y, GLint z) const;
	GLfloat _GetWater(GLint x, GLint y, GLint z) const;
	bool _BlockIsEmpty(GLint x, GLint y, GLint z) const;
	void _CorrectUV(const vector4df from[], vector4df to[], TerrainBlock block) const;
	void _MergeLighting(GLint x, GLint y, GLint z, GLColour& col) const;
	void _ApplyLightingToMesh(Mesh* mesh, matrix4f transform, TerrainBlock block) const;

	void _GenerateTree(GLint x, GLint y, GLint z);

	void CreateWater();

	GLuint					m_WaterVBO;
	GLuint					m_WaterEBO;
	GLuint					m_WaterIndexCount;
	std::list<vector3di>	m_WaterList;
	Texture					m_WaterTex;
	bool					m_bWaterSettled;
	bool					m_bHasWater;
	GLfloat					m_fWaterTimer;
	GLubyte					m_CurrentWater[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
	GLubyte					m_NewWater[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
	vector<GLVertex>		m_WaterVertices;
	vector<GLuint>			m_WaterIndices;
};

#endif