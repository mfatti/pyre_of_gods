#include "Button.h"
#include "Text.h"
#include "GUIManager.h"

Button::Button(const char* text, Font* font, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size)
	: GUIElement(parent, guiParent, pos, size)
	, m_bSwitched(false)
{
	// add the quads for this button
	//AddQuad(vector2df(2, 2), vector2df(0.5, 0.5), vector2df((GLfloat)size.x - 2, (GLfloat)size.y - 2), vector2df(0.5, 0.5), GLColour(0, 0, 0, 200));
	AddQuad(GLVertex(vector3df(2, 2, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x - 2, 2, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x - 2, (GLfloat)size.y - 2, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(2, (GLfloat)size.y - 2, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(vector2df(2, 2), vector2df(0.5, 0.5), vector2df((GLfloat)size.x - 2, (GLfloat)size.y - 2), vector2df(0.5, 0.5), GLColour(0, 0));
	
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 1), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df(0, (GLfloat)size.y - 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df(0, 1), vector2df(0.5, 0.5), vector2df(1, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(61));
	AddQuad(vector2df((GLfloat)size.x - 1, 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(61));
	
	//AddQuad(vector3df(1, 1), vector3df(), vector3df(size.x - 1, 2), vector3df(1, 1), vector3df(0.5, 0.5, 0.5));
	//AddQuad(vector3df(1, size.y - 2), vector3df(), vector3df(size.x - 1, size.y - 1), vector3df(1, 1), vector3df(0.5, 0.5, 0.5));
	//AddQuad(vector3df(1, 2), vector3df(), vector3df(2, size.y - 2), vector3df(1, 1), vector3df(0.5, 0.5, 0.5));
	//AddQuad(vector3df(size.x - 2, 2), vector3df(), vector3df(size.x - 1, size.y - 2), vector3df(1, 1), vector3df(0.5, 0.5, 0.5));
	CreateBufferObjects();
	
	// add the text on this button
	Text* t = GetGUIManager()->CreateText(text, vector3df(), vector2di(size.x, -1), font, this);
	t->SetOffset(vector2df(0, (GLfloat)(size.y / 2) - (GLfloat)(t->GetFont()->GetDetails().z / 2) - 1));
	t->CentreText(true);
}

void Button::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible())
		return;
	
	// hovering
	Text* t = dynamic_cast<Text*>(GetChild(0));
	vector2di size = GetSize();
	if (IsEnabled())
	{
		if (IsHovered() && !m_bSwitched)
		{
			if (IsPressed(left_mouse))
			{
				ChangeQuadColour(1, GLColour(0, 0));
				ChangeQuadColour(0, GLColour(0), GLColour(0), GLColour(15), GLColour(15));

				if (t)
				{
					t->SetOffset(t->GetOffset() + vector2df(0, 1));
					t->SetAlpha(0.8f);
				}
			}
			else
			{
				ChangeQuadColour(1, GLColour(64, 0, 0, 100));
				ChangeQuadColour(0, GLColour(30), GLColour(30), GLColour(10), GLColour(10));
				if (t)
				{
					t->SetOffset(vector2df(0, (GLfloat)(size.y / 2) - (GLfloat)(t->GetFont()->GetDetails().z / 2) - 1));
					t->SetAlpha(1);
				}
			}
			m_bSwitched = true;
		}
		else if (!IsHovered() && m_bSwitched)
		{
			ChangeQuadColour(1, GLColour(0, 0));
			ChangeQuadColour(0, GLColour(30), GLColour(30), GLColour(10), GLColour(10));
			m_bSwitched = false;
			if (t)
			{
				t->SetOffset(vector2df(0, (GLfloat)(size.y / 2) - (GLfloat)(t->GetFont()->GetDetails().z / 2) - 1));
				t->SetAlpha(1);
			}
		}
		else if (IsHovered() && !IsPressed(left_mouse) && m_bSwitched)
		{
			ChangeQuadColour(1, GLColour(64, 0, 0, 100));
			ChangeQuadColour(0, GLColour(30), GLColour(30), GLColour(10), GLColour(10));
			if (t)
			{
				t->SetOffset(vector2df(0, (GLfloat)(size.y / 2) - (GLfloat)(t->GetFont()->GetDetails().z / 2) - 1));
				t->SetAlpha(1);
			}
		}
	}
	
	// call GUIElement::Update()
	GUIElement::Update(bForce);
}

void Button::MouseDownFunction(GLuint button)
{
	// "press" down on the button
	m_bSwitched = false;
}

void Button::SetText(string text)
{
	Text* t = dynamic_cast<Text*>(GetChild(0));
	if (t)
		t->SetText(text, true);
}

string Button::GetText()
{
	Text* t = dynamic_cast<Text*>(GetChild(0));
	if (t)
		return t->GetText();

	return "";
}