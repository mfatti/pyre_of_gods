#include "Projectile.h"
#include "SceneManager.h"

Projectile::Projectile(Manager* parent, const ProjectileData& data, bool mob)
	: GLObject(parent, data.position, vector3di(1))
	, m_fGravity(data.speed * sin(data.direction.y))
	, m_bMobCreated(mob)
	, m_fLife(0.f)
{
	// although not technically an item, we will use the data from the given item class to get our mesh, texture and particle effects
	m_ItemClass = Item::GetItemClass(data.index);
	if (!m_ItemClass->Meshes.empty())
		SetMesh(m_ItemClass->Meshes[0]);
	m_TextureToLoad = m_ItemClass->strTexturePath;
	m_Data = data;

	// create any particles associated with this projectile
	if (!m_ItemClass->Particles.empty())
	{
		for (GLuint i = 0; i < m_ItemClass->Particles.size(); ++i) {
			ParticleEmitter* pe = GetSceneManager()->CreateParticleEmitter(m_ItemClass->Particles[i]);
			pe->SetTextureToLoad(m_ItemClass->Particles[i].texture);

			for (GLuint j = 0; j < m_ItemClass->Particles[i].anims.size(); ++j) {
				pe->AddAnimation(m_ItemClass->Particles[i].anims[j].first, m_ItemClass->Particles[i].anims[j].second);
			}

			m_Emitters.push_back(pe);
		}
	}
}

void Projectile::Update(bool bForce)
{
	// load texture if needs be
	if (!m_TextureToLoad.empty())
	{
		SetTexture(m_TextureToLoad.c_str());
		m_TextureToLoad = "";
	}
	
	// move this projectile along its course
	float delta = DeltaTime::GetDelta();

	m_fLife += 200.f * delta;

	if (m_Data.gravity > 0)
		m_fGravity -= 50.f * delta;

	vector3df prev = GetPosition();
	TranslatePosition(vector3df(
		m_Data.speed * cos(m_Data.direction.x) * cos(m_Data.direction.y) * delta,
		m_fGravity * delta,
		m_Data.speed * sin(m_Data.direction.x) * cos(m_Data.direction.y) * delta
		));
	vector3df current = GetPosition();

	// rotate this projectile
	float angle1 = -atan2(current.y - prev.y, m_Data.speed * delta);
	float angle2 = atan2(current.y - prev.y, current.z - prev.z);
	//SetRotation(vector3df(-m_Data.direction.x + PI + angle2 * sin(-m_Data.direction.x), PI_OVER_2, PI_OVER_2 + angle1 * cos(-m_Data.direction.x) + PI_OVER_2 * sin(-m_Data.direction.x)), 1.f);
	m_Rotation.SetRotation(angle1 + PI_OVER_2, vector3df(1, 0, 0));
	quaternion tmp;
	tmp.SetRotation(-m_Data.direction.x + PI_OVER_2, vector3df(0, 1, 0));
	m_Rotation = tmp * m_Rotation;

	// check for collisions
	Terrain* t = GetSceneManager()->GetFirstInstance<Terrain>();
	TerrainBlock block = t->GetBlock(current);
	bool bHit = block.Solid;

	if (bHit && (block.XX || block.XY || block.YX || block.YY))
	{
		// if this block has any lowered corners, check we've actually collided
		if (current.y > t->GetHeightBelow(current))
			bHit = false;
	}

	if (m_fLife > 1000.f)
		bHit = true;

	if (!bHit)
	{
		if (m_bMobCreated)
		{
			// this will get destroyed when hitting a player
		}
		else
		{
			// this will get destroyed when hitting a mob
			MobManager* pMobMan = GetSceneManager()->GetMobManager();
			const vector<Mob*>* mobs = pMobMan->GetMobs();
			for (GLuint i = 0; i < mobs->size(); ++i) {
				Mob* mob = mobs->at(i);
				if (mob && current.QuickDistance(mob->GetPosition() + vector3df(0, 8, 0)) <= 45.f)
				{
					bHit = true;
					break;
				}
			}
		}
	}

	if (bHit)
	{
		// we need to destroy ourself
		GetSceneManager()->DeleteObject(this);

		// destroy any particle emitters too
		if (!m_Emitters.empty())
		{
			for (GLuint i = 0; i < m_Emitters.size(); ++i) {
				GetSceneManager()->DeleteObject(m_Emitters[i]);
			}
			m_Emitters.clear();
		}
	}

	// update particle emitters
	if (!m_Emitters.empty())
	{
		for (GLuint i = 0; i < m_Emitters.size(); ++i) {
			m_Emitters[i]->SetEmitterPosition(current);
		}
	}

	// call the base class update
	//GLObject::Update(bForce);
	matrix4f m;
	m.SetFromQuaternion(m_Rotation);
	UpdateTransformationMatrix(vector3df(), m);
	CalculateInView();
}
