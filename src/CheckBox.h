#ifndef _CHECKBOX_H_
#define _CHECKBOX_H_

#include "GUIElement.h"

class CheckBox : public GUIElement {
	public:
		CheckBox(Manager* parent, GUIElement* guiParent, vector2df pos, vector3di size, string label);

		virtual void MouseDownFunction(GLuint button);

		void SetChecked(bool checked = true);
		bool GetChecked();

		static void CheckBoxClicked(GUIElement* e);

	protected:

	private:
		bool	m_bChecked;
};

#endif