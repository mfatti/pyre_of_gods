#include "YesNoDialog.h"
#include "GUIManager.h"

YesNoDialog::YesNoDialog(Manager* parent, string title, string text, void(*fny)(GUIElement*), void(*fnn)(GUIElement*))
	: GUIElement(parent, NULL, vector2df(), parent->GetRoot()->GetScreenSize())
{
	// create the background fade
	AddFade();
	CreateBufferObjects();

	// create the actual pop-up
	vector2di size = GetSize();
	vector2df sized(460, 240);
	Pane* pane = GetGUIManager()->CreatePane(string("^8").append(title).c_str(), vector2df(size) * 0.5f, sized, GetGUIManager()->GetFont("font20b"), this, 255, false);
	GetGUIManager()->CreateText(text.c_str(), vector2df(30, 40), vector2di((GLint)sized.x - 60, (GLint)sized.y - 64), NULL, pane)->SplitByWord(true);
	GetGUIManager()->CreateButton("Yes", vector2df(sized.x - 240, sized.y - 44), vector2di(100, 24), (fny ? fny : GUIElement::Close), NULL, pane);
	GetGUIManager()->CreateButton("No", vector2df(sized.x - 120, sized.y - 44), vector2di(100, 24), (fnn ? fnn : GUIElement::Close), NULL, pane);

	// set this as menu so it will work in menus
	SetMenu();

	// set up the animation
	SetAlpha(0);
	pane->SetScale(vector3df());
	FadeTo(1);
	ScaleTo(vector2df(1));
	pane->MoveTo((vector2df(size) - sized) * 0.5f);
}

void YesNoDialog::MouseDownFunction(GLuint button)
{

}

void YesNoDialog::ScaleTo(vector2df scale, bool hide, bool kill)
{
	GetChild(0)->ScaleTo(scale, hide, kill);
}