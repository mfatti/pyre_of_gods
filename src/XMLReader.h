#ifndef _XMLREADER_H_
#define _XMLREADER_H_

#include <string>
#include <fstream>

using std::string;
using std::ifstream;

class XMLReader {
	public:
		XMLReader(string file);
		~XMLReader();

		bool IsOpen();

		bool ReadLine();
		
		bool IsNode(string node);
		bool IsEndOfNode(string node);

		string GetNodeName();
		string GetAttributeValue(string attribute);

	protected:

	private:
		string		m_strCurrentLine;
		ifstream	m_XMLFile;
};

#endif