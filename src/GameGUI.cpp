#include "GameGUI.h"
#include "Game.h"
#include <sstream>

void MinimapThread(bool* run, bool* update, bool* force, Player* p, Terrain* t, GLubyte* data)
{
	static vector3df ppos;

	auto set_pixel = [data](int x, int y, GLubyte r, GLubyte g, GLubyte b, GLubyte a) {
		data[y * 4 * 64 - x * 4] = r;
		data[y * 4 * 64 - x * 4 + 1] = g;
		data[y * 4 * 64 - x * 4 + 2] = b;
		data[y * 4 * 64 - x * 4 + 3] = a;
	};

	while (*run)
	{
		if (*update)
		{
			vector3df _ppos = p->GetPosition();
			if (*force || ppos != _ppos)
			{
				ppos = _ppos;
				GLuint grass = Item::GetBlockFromName("Grass");
				GLuint sand = Item::GetBlockFromName("Sand");
				GLuint tree = Item::GetBlockFromName("Tree");
				const GLfloat one_over_16 = 0.0625f;
				const GLfloat one_over_10 = 0.75f;
				GLColour col;
				GLfloat height;

				GLfloat _h = t->GetHeightBelow(ppos);
				vector3df pos = ppos - vector3df(32 * BLOCK_SIZE, -BLOCK_SIZE, 32 * BLOCK_SIZE);
				for (GLuint i = 0; i < 64; ++i) {
					for (GLuint j = 0; j < 64; ++j) {
						vector3df _p = pos + vector3df(i * BLOCK_SIZE, 60, j * BLOCK_SIZE);
						TerrainBlock b = t->GetBlockBelow(_p, height, true);

						if (b.Water > 0 && !b.Solid)
							col.Set(100, 150, 220, 190);
						else
							col.Set(Item::GetBlockMapColour(b));
						col.Intensity((height - _h) * ONE_OVER_BLOCK_SIZE * one_over_16 * one_over_10);

						// we want to make this a circle so fade out any pixels not in the circle
						GLfloat x = 31.5f - i;
						GLfloat y = 31.5f - j;
						if ((x * x + y * y) > 1024)
							col.a = 0;

						set_pixel(i + 1, j + 1, col.r, col.g, col.b, col.a);
					}
				}

				// set the player point
				set_pixel(33, 33, 255, 255, 255, 255);

				// draw an 'N' at the north of the map
				for (GLuint i = 0; i < 5; ++i) {
					set_pixel(35, 61 - i, 255, 255, 255, 255);
					set_pixel(31, 61 - i, 255, 255, 255, 255);
				}
				set_pixel(34, 60, 255, 255, 255, 255);
				set_pixel(33, 59, 255, 255, 255, 255);
				set_pixel(32, 58, 255, 255, 255, 255);

				*update = false;
				*force = false;
			}
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}

bool GameGUI::m_bMinimapThreadRunning(true);

GameGUI::GameGUI(GUIManager* gui, SceneManager* scene, SettingsManager* settings)
	: m_pGUIMan(gui)
	, m_pSceneMan(scene)
	, m_Settings(settings)
	, m_bCanUpdateMinimap(true)
	, m_bForceMinimapUpdate(false)
	, m_pBlockInventory(NULL)
	, m_GUIBlockInventory(NULL)
	, m_invBuf(NULL)
	, m_invRLen(0)
	, m_bReturnToMainMenu(false)
	, m_DisconnectMessage("")
	, m_bStatChange(true)
	, m_bDead(false)
	, m_bForceEnergyFlash(false)
	, m_NewBiome("")
{
	// create the GUI
	m_pPlayer = scene->GetFirstInstance<Player>();

	// set up the menu
	vector2di ScreenSize = gui->GetRoot()->GetScreenSize();
	m_Menu = gui->CreateMenu(vector2di(700, 500));
	m_Menu->SetVisible(false);
	m_Menu->SetAlpha(0);

	Pane* options = m_Menu->AddPage("Options", vector2df(200, 200), vector2di(300, 40), GameGUI::LoadOptions);
	TabManager* optionsTabs = gui->CreateTabManager(vector2df(10, 30), vector2di(680, 430), options);
	Tab* video = optionsTabs->AddTab("Video");
	Tab* gaudio = optionsTabs->AddTab("Game/Audio");
	Tab* controls = optionsTabs->AddTab("Controls");

	gui->CreateButton("Resume Game", vector2df(200, 155), vector2di(300, 40), GameGUI::CloseMenu, gui->GetFont("font14b"), m_Menu->GetPageByName("Menu"));
	gui->CreateButton("Main Menu", vector2df(200, 245), vector2di(300, 40), [](GUIElement* e) {
		e->GetGUIManager()->ShowYesNoDialog("Return to Main Menu", "Are you sure you want to return to the main menu?", [](GUIElement* e) {
			GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
			if (!g)
				return;

			g->GoToMainMenu();
		});
	}, gui->GetFont("font14b"), m_Menu->GetPageByName("Menu"));
	gui->CreateButton("Exit Game", vector2df(200, 290), vector2di(300, 40), [](GUIElement* e) {
		GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
		if (!g)
			return;

		g->m_pGUIMan->ShowYesNoDialog("Exit Game", "Are you sure you want to quit the game?", GameGUI::ExitGame);
	}, gui->GetFont("font14b"), m_Menu->GetPageByName("Menu"));

	// create the video options tab
	GroupBox* display = gui->CreateGroupBox(vector2df(10, 16), vector2di(325, 127), "Display", video);
	gui->CreateText("Resolution", vector2df(10, 14), vector2di(140, -1), NULL, display);
	m_GUIResolutions = gui->CreateDropDownMenu(vector2df(160, 12), vector2di(155, 21), 160, display);
	int count;
	const GLFWvidmode* resolutions = glfwGetVideoModes(glfwGetPrimaryMonitor(), &count);
	for (int i = 0; i < count; ++i) {
		if (resolutions[i].width < 1280 || resolutions[i].height < 720)
			continue;

		std::stringstream r;
		r << resolutions[i].width << "x" << resolutions[i].height;
		if (!m_GUIResolutions->ItemExists(r.str()))
			m_GUIResolutions->AddItem(r.str());
	}
	m_GUIResolutions->SetListBoxUpdateFunction(GameGUI::EnableApply);
	m_GUIFullscreen = gui->CreateCheckBox(vector2df(10, 40), vector2di(305, 21), "Fullscreen", display);
	m_GUIFullscreen->SetUpdateFunction(GameGUI::FullscreenUpdated);
	m_GUIBorderless = gui->CreateCheckBox(vector2df(10, 68), vector2di(305, 21), "Borderless (Windowed)", display);
	m_GUIBorderless->SetUpdateFunction(GameGUI::BorderlessUpdated);
	m_GUIVSync = gui->CreateCheckBox(vector2df(10, 96), vector2di(305, 21), "V-Sync", display);
	m_GUIVSync->SetUpdateFunction(GameGUI::EnableApply);

	GroupBox* advanced = gui->CreateGroupBox(vector2df(345, 16), vector2di(325, 380), "Advanced", video);
	m_GUIPerPixelLighting = gui->CreateCheckBox(vector2df(10, 12), vector2di(305, 21), "Per-pixel Lighting", advanced);
	m_GUIPerPixelLighting->SetUpdateFunction(GameGUI::PerPixelUpdated);
	m_GUINormalMapping = gui->CreateCheckBox(vector2df(25, 40), vector2di(290, 21), "Normal Mapping", advanced);
	m_GUINormalMapping->SetUpdateFunction(GameGUI::EnableApply);
	m_GUINormalMapping->SetEnabled(false);
	m_GUIShadowQuality = gui->CreateText("Shadows", vector2df(25, 70), vector2di(140, -1), NULL, advanced);
	m_GUIShadows = gui->CreateDropDownMenu(vector2df(195, 68), vector2di(120, 21), 102, advanced);
	m_GUIShadows->SetListBoxUpdateFunction(GameGUI::EnableApply);
	m_GUIShadows->AddItem("Off", false);
	m_GUIShadows->AddItem("Low", false);
	m_GUIShadows->AddItem("Medium", false);
	m_GUIShadows->AddItem("High", false);
	m_GUIShadows->AddItem("Ultra");

	gui->CreateButton("OK", vector2df(380, 466), vector2di(100, 24), GameGUI::OKOptions, NULL, options);
	m_GUIApply = gui->CreateButton("Apply", vector2df(485, 466), vector2di(100, 24), GameGUI::ApplyOptions, NULL, options);
	gui->CreateButton("Cancel", vector2df(590, 466), vector2di(100, 24), GameGUI::CancelOptions, NULL, options);

	LoadOptions(display);

	// create the game/audio tab

	// create the controls tab
	m_GUIControls = gui->CreateTable(vector2df(10, 10), vector2di(660, 356), 2, controls);
	m_GUIControls->SetColumnWidth(0, 480);
	m_GUIControls->SetColumnName(0, "Action");
	m_GUIControls->SetColumnWidth(1, 180);
	m_GUIControls->SetColumnName(1, "Binding");

	// add the controls to the table
	m_GUIControls->AddRow({ "Move Up", m_Settings->GetKeyName(m_Settings->GetValueAsInt("move_up")) });
	m_GUIControls->AddRow({ "Move Down", m_Settings->GetKeyName(m_Settings->GetValueAsInt("move_down")) });
	m_GUIControls->AddRow({ "Move Left", m_Settings->GetKeyName(m_Settings->GetValueAsInt("move_left")) });
	m_GUIControls->AddRow({ "Move Right", m_Settings->GetKeyName(m_Settings->GetValueAsInt("move_right")) });
	m_GUIControls->AddRow({ "Jump", m_Settings->GetKeyName(m_Settings->GetValueAsInt("jump")) });
	m_GUIControls->AddRow({ "Roll", m_Settings->GetKeyName(m_Settings->GetValueAsInt("roll")) });
	m_GUIControls->AddRow({ "Sprint", m_Settings->GetKeyName(m_Settings->GetValueAsInt("sprint")) });
	m_GUIControls->AddRow({ "Inventory", m_Settings->GetKeyName(m_Settings->GetValueAsInt("inventory")) });
	m_GUIControls->AddRow({ "Menu", m_Settings->GetKeyName(m_Settings->GetValueAsInt("menu")) });
	ControlsUpdate();

	gui->CreateButton("Change", vector2df(570, 372), vector2di(100, 24), GameGUI::KeyChangePressed, NULL, controls);
	gui->CreateButton("Reset", vector2df(435, 372), vector2di(100, 24), GameGUI::ResetControlPressed, NULL, controls);
	gui->CreateButton("Defaults", vector2df(330, 372), vector2di(100, 24), GameGUI::DefaultsControlPressed, NULL, controls);

	// load the default controls
	DefaultControls();

	// set up the inventory
	m_GUIInventory = gui->CreateInventory(vector2df((GLfloat)ScreenSize.x - 571, 440), vector2di(480, 192), m_pPlayer->GetInventory(), 40);
	m_GUIInventory->SetVisible(false);
	m_GUIInventory->SetAlpha(0);

	m_GUIHotbar = gui->CreateInventory(vector2df((GLfloat)ScreenSize.x - 520, (GLfloat)ScreenSize.y - 60), vector2di(480, 48), m_pPlayer->GetHotbar(), 10);
	m_GUIHotbar->SetSlotLeftClicked(GameGUI::HotbarLeftClicked);
	m_GUIHotbar->SetSlotRightClicked(GameGUI::HotbarRightClicked);

	m_GUISelected = gui->CreateImage("gui.png", vector2df(), vector2di(48, 48), GLColour(255, 255, 255, 255), m_GUIHotbar);
	m_GUISelected->SetUV(vector2df(), vector2df(48.f / 256.f, 48.f / 256.f));

	// create the text to display the name of the currently held item
	m_GUISelectedName = gui->CreateText(m_GUIHotbar->GetItem(0) ? m_GUIHotbar->GetItem(0)->GetName().c_str() : " ", vector2df((GLfloat)ScreenSize.x - 520, (GLfloat)ScreenSize.y - 90), vector2di(480, 0), gui->GetFont("font20b"));
	m_GUISelectedName->CentreText(true);
	m_GUISelectedName->SetAlpha(0.5f);

	// create the text to display block hover
	m_GUIBlockHoverName = gui->CreateText("##################################################################################################", vector2df(ScreenSize.x / 2 - 150, ScreenSize.y / 2 + 10), vector2di(300, 40), gui->GetFont("font14b"));
	m_GUIBlockHoverName->SetShadowed();
	m_GUIBlockHoverName->CentreText();
	//m_GUIBlockHoverName->SetFollowMouse();
	m_GUIBlockHoverName->SetOffset(vector2df(20, 10));
	m_GUIBlockHoverName->SetAlpha(0.85f);
	m_GUIBlockHoverStatus = gui->CreateText("##################################################################################################", vector2df(ScreenSize.x / 2 - 150, ScreenSize.y / 2 + 25), vector2di(300, 40));
	m_GUIBlockHoverStatus->SetShadowed();
	m_GUIBlockHoverStatus->CentreText();
	//m_GUIBlockHoverStatus->SetFollowMouse();
	m_GUIBlockHoverStatus->SetOffset(vector2df(20, 26));
	m_GUIBlockHoverStatus->SetLineSpacing(1.015f);
	m_GUIBlockHoverStatus->SetAlpha(0.8f);

	// create the crafting pane
	m_GUICrafting = gui->CreatePane(m_pPlayer->GetName().c_str(), vector2df((GLfloat)ScreenSize.x - 622, 10), vector2di(602, 640), gui->GetFont("font14b"), NULL, 100, false);
	dynamic_cast<Text*>(m_GUICrafting->GetChild(0))->SetShadowed();
	m_GUICrafting->SetVisible(false);
	m_GUICrafting->SetAlpha(0);

	// create the tab manager
	m_GUITabMan = gui->CreateTabManager(vector2df(10, 24), vector2di(582, 395), m_GUICrafting);
	Tab* equipment = m_GUITabMan->AddTab("Equipment/Stats");
	Tab* crafting = m_GUITabMan->AddTab("Crafting");
	Tab* quests = m_GUITabMan->AddTab("Quests");
	Tab* towns = m_GUITabMan->AddTab("Towns");
	Tab* players = m_GUITabMan->AddTab("Players");

	// sort out the equipment tab
	m_GUIEquipment = gui->CreateInventory(vector2df(20), vector2di(540, 200), m_pPlayer->GetEquipment(), 14, false, equipment);
	m_GUIEquipment->SetSlotLeftClicked(GameGUI::EquipmentLeftClicked);
	m_GUIEquipment->SetSlotRightClicked(GameGUI::EquipmentRightClicked);

	//GroupBox* ammo = gui->CreateGroupBox(vector2df(364, 298), vector2di(210, 66), "Ammo", equipment);
	gui->CreateText("Ammo", vector2df(362, 290), vector2di(210, 0), gui->GetFont("font14b"), equipment)->CentreText(true);
	for (GLuint i = 0; i < 4; ++i) {
		m_GUIEquipment->GetCell(i)->SetItemFilter(ItemFilter(AMMO));
		m_GUIEquipment->MoveCell(i, vector2df(350 + i * 50, 290));
	}

	gui->CreateText("Accessories", vector2df(90, 290), vector2di(286, 0), gui->GetFont("font14b"), equipment)->CentreText(true);
	for (GLuint i = 4; i < 9; ++i) {
		m_GUIEquipment->GetCell(i)->SetItemFilter(ItemFilter(EQUIPPABLE));
		m_GUIEquipment->MoveCell(i, vector2df(90 + (i - 4) * 50, 290));
	}

	gui->CreateText("Helmet", vector2df(380, 32), vector2di(100, 0), gui->GetFont("font14b"), equipment)->AlignRight();
	gui->CreateText("Chest", vector2df(380, 82), vector2di(100, 0), gui->GetFont("font14b"), equipment)->AlignRight();
	gui->CreateText("Gloves", vector2df(380, 132), vector2di(100, 0), gui->GetFont("font14b"), equipment)->AlignRight();
	gui->CreateText("Greaves", vector2df(380, 182), vector2di(100, 0), gui->GetFont("font14b"), equipment)->AlignRight();
	gui->CreateText("Boots", vector2df(380, 232), vector2di(100, 0), gui->GetFont("font14b"), equipment)->AlignRight();
	for (GLuint i = 9; i < 14; ++i) {
		m_GUIEquipment->GetCell(i)->SetItemFilter(ItemFilter(ARMOUR, static_cast<SubType>(HELMET + (i - 9))));
		m_GUIEquipment->MoveCell(i, vector2df(466, 6 + (i - 9) * 50));

		m_GUIDefence[i - 9] = gui->CreateText("^90 def", vector2df(380, 48 + (i - 9) * 50), vector2di(100, 0), NULL, equipment);
		m_GUIDefence[i - 9]->AlignRight();
	}
	UpdateDefence();

	// sort out the crafting tab
	m_GUIRecipes = gui->CreateListBox(vector2df(10, 30), vector2di(200, 310), crafting);
	m_GUIRecipes->SetFunction(left_mouse, GameGUI::RecipeSelected);

	m_GUIRecipePages = gui->CreateDropDownMenu(vector2df(10, 10), vector2di(200, 21), 84, crafting);
	m_GUIRecipePages->AddItem("All", false);
	m_GUIRecipePages->SetDepth(20);
	m_GUIRecipePages->SetListBoxFunction(GameGUI::RecipePageChanged);

	m_RecipeBook = new RecipeBook();
	m_RecipeBook->LoadRecipes("xml/recipes.xml");
	m_RecipeBook->LoadRecipePages("xml/recipebook.xml", m_GUIRecipePages);
	m_RecipeBook->AddRecipesToListBox("All", m_GUIRecipes);

	m_GUICraftingSearch = gui->CreateTextBox("", vector2df(10, 339), vector2di(200, 21), "Search...", crafting);
	m_GUICraftingSearch->SetCharFunction(GameGUI::CraftingSearchBar);

	m_GUIRecipeName = gui->CreateText("Recipe Name", vector2df(220, 12), vector2di(352, 1), gui->GetFont("font14b"), crafting);
	m_GUIRecipeName->CentreText(true);
	m_GUIStationName = gui->CreateText(" ", vector2df(220, 30), vector2di(352, 1), gui->GetFont("font14b"), crafting);
	m_GUIStationName->CentreText(true);
	m_GUIRecipeDescription = gui->CreateText(" ", vector2df(316, 54), vector2di(262, 1), NULL, crafting);
	m_GUIRecipeDescription->SplitByWord(true);
	m_GUIRecipeDescription->SetLineSpacing(1.275f);
	m_GUIRecipeMesh = gui->CreateMeshView(scene->CreateTerrainBlock("Wood Planks"), "terrain_tiles.png", vector2df(220, 48), vector2di(96, 96), crafting);

	m_GUIIngredients = gui->CreateScrollPane(vector2df(220, 172), vector2di(350, 160), horizontal_scroll, crafting);

	m_GUICraftingAmount = gui->CreateTextBox("1", vector2df(440, 336), vector2di(44, 24), "", crafting);
	m_GUICraftingAmount->SetNumeric(true);
	m_GUICraftingAmount->SetCharLimit(3);
	UpdateRecipe();

	gui->CreateText("Amount:", vector2df(368, 338), vector2di(80, 1), gui->GetFont("font14b"), crafting);
	gui->CreateButton("Craft", vector2df(490, 336), vector2di(80, 24), GameGUI::CraftRecipe, NULL, crafting);
	gui->CreateButton("Craft Max", vector2df(220, 336), vector2di(122, 24), GameGUI::CraftMaxRecipe, NULL, crafting);

	// starting items
	if (m_GUIHotbar)
	{
		m_GUISelectedName->SetText(m_GUIHotbar->GetItem(0) ? m_GUIHotbar->GetItem(0)->GetName() : " ", true);
		m_pPlayer->SetSelected(0);
	}

	// fill the minimap texture with crap
	for (GLuint i = 0; i < 64 * 64 * 4; i += 4) {
		m_Minimap[i] = 0;
		m_Minimap[i + 1] = 0;
		m_Minimap[i + 2] = 0;
		m_Minimap[i + 3] = 200;
	}

	// create the minimap pane
	Pane* minimap = m_pGUIMan->CreatePane(" ", vector2df(20, 20), vector2di(196, 196), NULL, NULL, 100, false);
	m_MinimapTex = m_pGUIMan->GetTextureManager()->CreateTexture("minimap", 64, 64, m_Minimap);
	m_GUIMinimap = m_pGUIMan->CreateImage("", vector2df(98, 98), vector2di(192, 192), GLColour(255, 255, 255, 255), minimap);
	m_GUIMinimap->SetTexture(m_MinimapTex);
	m_GUIMinimap->Centre();
	m_GUIPosition = m_pGUIMan->CreateText("##################################################################################", vector2df(2, 198), vector2di(192, 30), NULL, minimap);
	m_GUIPosition->SetDepth(10);
	m_GUIPosition->CentreText(true);
	m_GUIPosition->SetShadowed();
	m_bMinimapThreadRunning = true;

	// biome text
	m_GUIBiomeText = gui->CreateText("Greenlands", vector2df(2, 214), vector2di(192, 30), gui->GetFont("font14b"), minimap);
	m_GUIBiomeText->CentreText(true);
	m_GUIBiomeText->SetShadowed();

	RunInThread(MinimapThread, &m_bMinimapThreadRunning, &m_bCanUpdateMinimap, &m_bForceMinimapUpdate, m_pPlayer, m_pSceneMan->GetFirstInstance<Terrain>(), m_Minimap);

	// create the health and mana bars
	m_GUIHealthBar = m_pGUIMan->CreateProgressBar(vector2df(227, 20), vector2di(220, 30));
	m_GUIHealthBar->SetMaxValue(80);
	m_GUIHealthBar->SetCurrentPosition(80);
	m_GUIHealthBar->SetColours(0, GLColour(30, 200, 100, 100), GLColour(130, 250, 100, 40));
	m_GUIEnergyBar = m_pGUIMan->CreateProgressBar(vector2df(227, 49), vector2di(220, 25));
	m_GUIEnergyBar->SetMaxValue(50);
	m_GUIEnergyBar->SetCurrentPosition(50);
	m_GUIEnergyBar->SetColours(0, GLColour(200, 200, 100, 100), GLColour(200, 200, 50, 70));
	m_GUIEnergyBar->SetColours(2, GLColour(200, 200, 100, 180), GLColour(200, 200, 50, 180));
	m_GUIEnergyDepleted = m_pGUIMan->CreateImage("all_white.png", vector2df(229, 51), vector2di(216, 21), GLColour(200, 200, 100, 255));
	m_GUIEnergyDepleted->SetVisible(false);
	m_GUIEnergyDepleted->SetDepth(10);
	m_GUIEnergyDepleted->SetAnimationSmoothness(16.f);

	// create chat and chat bar
	m_GUIChatBar = gui->CreateTextBox("", vector2df(20, ScreenSize.y - 33), vector2di(500, 23), "Type something...");
	m_GUIChatBar->SetCharLimit(250);
	m_GUIChatBar->SetVisible(false);
	m_GUIChatBar->SetAlpha(0);
	m_GUIChatBox = gui->CreateChatText(20, vector2df(20, ScreenSize.y - 237), vector2di(500, 200), gui->GetFont("font14b"));

	// add a key function to toggle chat
	scene->AddKeyFunction(m_pPlayer, GLFW_KEY_ENTER, GLFW_PRESS, GameGUI::ToggleChatBar);

	// create the dead text
	m_GUIDeadText = gui->CreateText("You are dead.", vector2df(0, ScreenSize.y * 0.5f), ScreenSize, gui->GetFont("font20b"));
	m_GUIDeadText->CentreText(true);
	m_GUIDeadText->SetShadowed();
	m_GUIDeadText->SetAlpha(0);
	m_GUIDeadText->SetVisible(false);

	// create the underwater blue tint
	m_GUIUnderwater = gui->CreateImage("all_white.png", vector2df(), vector2di(5000, 5000), GLColour(100, 150, 220, 255));
	m_GUIUnderwater->SetVisible(false);

	// crosshair
	gui->CreateImage("all_white.png", vector2df(ScreenSize.x / 2 - 5, ScreenSize.y / 2 - 1), vector2di(10, 2));
	gui->CreateImage("all_white.png", vector2df(ScreenSize.x / 2 - 1, ScreenSize.y / 2 - 5), vector2di(2, 10));
}

GameGUI::~GameGUI()
{
	// delete the recipe book
	DeleteBlockInventory();
	delete m_RecipeBook;
	m_bMinimapThreadRunning = false;
	m_pGUIMan->GetTextureManager()->DeleteTexture("minimap");
	//m_pGUIMan->CleanUp();
	//m_pSceneMan->CleanUp();
	Inventory::ClearCursorItem();
}

void GameGUI::UpdateRecipe()
{
	// get the GUI manager
	GUIManager* pGUIMan = dynamic_cast<GUIManager*>(m_pSceneMan->GetRoot()->GetManager(gui_manager));
	if (!pGUIMan)
		return;

	// update the recipe
	Recipe* recipe = m_RecipeBook->GetRecipe(m_GUIRecipes->GetSelectedText());

	// if we for whatever reason don't have a valid recipe, leave before it is too late
	if (!recipe)
		return;

	std::stringstream namestream;
	namestream << recipe->name;
	if (recipe->produceAmount > 1)
		namestream << " (x" << recipe->produceAmount << ")";

	m_GUIRecipeName->SetText(namestream.str(), true);

	if (recipe->craftingStation)
	{
		std::stringstream stationstream;
		stationstream << "^9Requires: " << Item::GetItemClass(TerrainBlock(recipe->craftingStation))->strName;

		m_GUIStationName->SetText(stationstream.str(), true);
	}
	else
		m_GUIStationName->SetText("^9Can Craft Anywhere");

	m_GUIRecipeDescription->SetText(recipe->produce->strDescription, true);
	m_GUICraftingAmount->SetText("1");

	m_GUIRecipeMesh->SetMeshView(recipe->produce->Meshes[0], recipe->produce->strTexturePath);
	if (recipe->produce->Type == BLOCK)
		m_GUIRecipeMesh->SetBlockModelView(recipe->produce->BlockSize);
	else if (recipe->produce->TypeSub == INGOT)
		m_GUIRecipeMesh->SetIngotModelView();
	else
		m_GUIRecipeMesh->SetDefaultModelView();

	// update the ingredients
	m_GUIIngredients->ClearPane();
	map<const ItemClass*, int>::iterator it = recipe->ingredients.begin();
	GLuint n = 0;

	while (it != recipe->ingredients.end())
	{
		GLuint amount = m_GUIInventory->GetItemAmount(it->first) + m_GUIHotbar->GetItemAmount(it->first) + (m_GUIBlockInventory ? m_GUIBlockInventory->GetItemAmount(it->first) : 0);
		pGUIMan->CreateIngredient(vector2df(6 + (GLfloat)(n / 3) * 172, 10 + (GLfloat)(n % 3) * 46), it->first, it->second, amount, m_GUIIngredients);

		++n;
		++it;
	}

	// set the previous stats
	m_PreviousStats = m_pPlayer->GetStats();
}

void GameGUI::CraftRecipe(int n)
{
	// craft the selected recipe n amount of times
	int amount = n;

	Recipe* recipe = m_RecipeBook->GetRecipe(m_GUIRecipes->GetSelectedText());
	bool bCraft = m_pPlayer->NearCraftingStation(recipe->craftingStation);

	// if we aren't close to the required crafting station then exit
	if (!bCraft)
		return;

	// add our inventory and hotbar
	vector<Inventory*> invs;
	invs.push_back(m_GUIInventory);
	invs.push_back(m_GUIHotbar);

	// if we have another inventory open then we will also use that to find ingredients
	if (m_pBlockInventory)
		invs.push_back(m_GUIBlockInventory);

	// calculate how many recipes we can craft
	map<const ItemClass*, int>::iterator it = recipe->ingredients.begin();
	int i = 0, maxAmount = -1;

	while (it != recipe->ingredients.end())
	{
		i = 0;
		for (GLuint j = 0; j < invs.size(); ++j) {
			i += invs[j]->GetItemAmount(it->first);
		}
		i /= it->second;

		if (maxAmount == -1 || i < maxAmount)
			maxAmount = i;

		++it;
	}

	// if n == -1 then determine how many of this recipe we can make
	if (n == -1)
		amount = maxAmount;
	else
		amount = min(amount, maxAmount);

	// figure out if we have the ingredients required to craft the recipe
	it = recipe->ingredients.begin();
	while (bCraft && it != recipe->ingredients.end())
	{
		i = 0;
		for (GLuint j = 0; j < invs.size(); ++j) {
			i += invs[j]->GetItemAmount(it->first);
		}

		if (i < it->second * amount)
			bCraft = false;

		++it;
	}

	// if we can craft the recipe then do so!
	if (bCraft)
	{
		it = recipe->ingredients.begin();
		while (it != recipe->ingredients.end())
		{
			int amountToTake = it->second * amount;

			// loop through until we've taken the required amount of this ingredient
			for (GLuint j = 0; j < invs.size(); ++j) {
				int slot = -1;

				while (amountToTake > 0 && (GLuint)(slot + 1) <= invs[j]->GetSlotCount())
				{
					Item* item = invs[j]->FindNextItem(it->first, slot);

					if (!item)
						break;

					if (item->DecreaseStackSize(amountToTake))
					{
						// we can remove the whole amount from this one stack
						amountToTake = 0;

						invs[j]->GetCell(slot)->UpdateText();

						// if we need to, remove this item from the inventory
						if (item->GetStackSize() == 0)
						{
							invs[j]->RemoveItem(slot);

							if (!m_pSceneMan->DeleteObject(item))
								delete item;
						}
					}
					else
					{
						// we still need more from another item, decrease the amount needed and delete this item
						amountToTake -= item->GetStackSize();

						invs[j]->RemoveItem(slot);

						if (!m_pSceneMan->DeleteObject(item))
							delete item;
					}
				}
			}

			++it;
		}

		// now that we've taken the ingredients away, give the player the item they wanted to craft
		for (int j = 0; j < amount; ++j) {
			Item* item = m_pSceneMan->CreateItem(recipe->produce->strName, m_pPlayer->GetPosition(), recipe->produceAmount);

			bool bAdded = false;
			GLuint k = 0;
			InventoryResult result;
			while (!bAdded)
			{
				result = invs[k]->AddItem(item);
				if (result == INV_SUCCESS)
				{
					m_pSceneMan->DeleteObject(item);
					bAdded = true;
				}
				else if (result == INV_FAIL)
				{
					++k;

					if (k >= invs.size())
						break;
				}
			}
		}

		// update the recipe
		UpdateRecipe();
	}
}

void GameGUI::SetBlockHoverName(string text)
{
	m_GUIBlockHoverName->SetText(text);
}

void GameGUI::SetBlockHoverStatus(string text)
{
	m_GUIBlockHoverStatus->SetText(text);
}

Inventory* GameGUI::GetInventory()
{
	return m_GUIInventory;
}

Inventory* GameGUI::GetHotbar()
{
	return m_GUIHotbar;
}

Inventory* GameGUI::GetEquipment()
{
	return m_GUIEquipment;
}

bool GameGUI::GUIOpen()
{
	return (m_GUIInventory->IsVisible() || m_Menu->IsVisible());
}

void GameGUI::ResolutionUpdate()
{
	// the screen resolution has changed, update the GUI
	vector2di ScreenSize = m_pGUIMan->GetRoot()->GetScreenSize();

	m_Menu->SetSize(vector3di(ScreenSize));
	m_Menu->SetMenuPosition(vector2df((GLfloat)(ScreenSize.x - m_Menu->GetPageSize().x) * 0.5f, (GLfloat)(ScreenSize.y - m_Menu->GetPageSize().y) * 0.5f));

	bool bInv = m_GUIInventory->IsVisible();
	GLfloat ypos = (ScreenSize.y - 700) / 2.f;
	m_GUIInventory->SetPosition(vector2df((GLfloat)ScreenSize.x - 571, bInv ? ypos + 430 : 440));
	m_GUIHotbar->SetPosition(vector2df(bInv ? (GLfloat)ScreenSize.x - 571 : (GLfloat)ScreenSize.x - 520, bInv ? ypos + 650 : (GLfloat)ScreenSize.y - 60));
	m_GUICrafting->SetPosition(vector2df((GLfloat)ScreenSize.x - 622, bInv ? ypos : 10));
	m_GUISelectedName->SetPosition(vector2df(bInv ? (GLfloat)ScreenSize.x - 571 : (GLfloat)ScreenSize.x - 520, bInv ? ypos + 620 : (GLfloat)ScreenSize.y - 90));
	m_GUIChatBar->SetPosition(vector2df(20, (GLfloat)ScreenSize.y - 33));
	m_GUIChatBox->SetPosition(vector2df(20, (GLfloat)ScreenSize.y - 237));

	m_GUIInventory->Update(true);
	m_GUIHotbar->Update(true);
	m_GUICrafting->Update(true);
	m_GUISelectedName->Update(true);
	m_GUIChatBar->Update(true);
}

void GameGUI::ControlsUpdate()
{
	// the controls have changed, update them!
	vector<int>* controls = m_pPlayer->GetControls();

	// remove any functions associated with the previous controls
	if (!controls->empty())
	{
		m_pSceneMan->RemoveKeyFunction(m_pPlayer, controls->at(menu), GLFW_PRESS);
		m_pSceneMan->RemoveKeyFunction(m_pPlayer, controls->at(inventory), GLFW_PRESS);
	}

	// add the new controls
	controls->clear();
	controls->push_back(m_Settings->GetValueAsInt("move_up"));
	controls->push_back(m_Settings->GetValueAsInt("move_down"));
	controls->push_back(m_Settings->GetValueAsInt("move_left"));
	controls->push_back(m_Settings->GetValueAsInt("move_right"));
	controls->push_back(m_Settings->GetValueAsInt("jump"));
	controls->push_back(m_Settings->GetValueAsInt("roll"));
	controls->push_back(m_Settings->GetValueAsInt("sprint"));
	controls->push_back(m_Settings->GetValueAsInt("inventory"));
	controls->push_back(m_Settings->GetValueAsInt("menu"));

	// set up keybinding functions
	m_pSceneMan->AddKeyFunction(m_pPlayer, controls->at(menu), GLFW_PRESS, GameGUI::ToggleMenu);
	m_pSceneMan->AddKeyFunction(m_pPlayer, controls->at(inventory), GLFW_PRESS, GameGUI::ToggleInventory);
}

void GameGUI::ChangeKeyBinding(string control, GLint key)
{
	m_GUIControls->ChangeRow(m_GUIControls->GetSelectedRow(), { control, m_Settings->GetKeyName(key) });
}

void GameGUI::ResetControl(GLuint control)
{
	m_GUIControls->ChangeRow(control, { m_GUIControls->GetCell(0, control), m_Settings->GetKeyName(m_DefaultControls[control]) });
}

void GameGUI::UpdateSelected()
{
	m_GUISelected->SetOffset(vector2df((GLfloat)m_pPlayer->GetSelected() * 50, 0));
	m_GUISelectedName->SetText(m_GUIHotbar->GetItem(m_pPlayer->GetSelected()) ? m_GUIHotbar->GetItem(m_pPlayer->GetSelected())->GetName() : " ", true);
}

void GameGUI::UpdateMinimapTexture()
{
	if (!m_bCanUpdateMinimap)
	{
		glBindTexture(GL_TEXTURE_2D, m_MinimapTex.gltex);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 64, 64, GL_RGBA, GL_UNSIGNED_BYTE, m_Minimap);
		glBindTexture(GL_TEXTURE_2D, 0);
		m_bCanUpdateMinimap = true;

		// update the position text
		vector3df pos = m_pPlayer->GetPosition();
		if (pos.x < 0.f) pos.x -= 10.f;
		if (pos.z < 0.f) pos.z -= 10.f;
		std::stringstream ss;
		ss << (GLint)(pos.x * 0.1f) << ", " << (GLint)(pos.y * 0.1f) << ", " << (GLint)(pos.z * 0.1f);
		m_GUIPosition->SetText(ss.str());
	}
}

void GameGUI::ForceUpdateMinimap()
{
	m_bForceMinimapUpdate = true;
}

void GameGUI::SetMinimapRotation(GLfloat rot)
{
	m_GUIMinimap->SetRotation(vector3df(0, 0, 1), -rot, vector3df(32, 32, 32));
}

void GameGUI::AddChatMessage(string text)
{
	m_GUIChatBox->AddChatMessage(text);
}

string GameGUI::GetKeyName(int key)
{
	return m_Settings->GetKeyName(key);
}

void GameGUI::ProcessBlockInventory(unsigned char* buf, int rlen)
{
	// copy the data to our internal variables to process later (in the main thread)
	m_invBuf = new unsigned char[rlen];
	memcpy(m_invBuf, buf, rlen);
	m_invRLen = rlen;
}

void GameGUI::WaitForUpdate()
{
	if (m_invRLen > 0)
	{
		// ensure we start from a clean slate
		//DeleteBlockInventory();
		
		// handle the receiving of a block inventory, ie for a chest
		// get the size of the inventory and create a new one
		GLuint invsize = m_invBuf[1];
		m_pBlockInventory = new vector<Item*>(invsize);

		for (GLuint i = 0; i < invsize; ++i) {
			m_pBlockInventory->at(i) = NULL;
		}

		// if there was any data sent with this then read that in
		if (m_invRLen > 2)
		{
			// uncompress the data
			uLongf size = invsize * 300 + 300;
			unsigned char* ubuf = new unsigned char[size];
			uncompress(ubuf, &size, &m_invBuf[2], m_invRLen - 2);

			// now read in the items
			GLuint pos = 0;
			const size_t ilen = sizeof(ItemData);
			while (pos < size)
			{
				// read the cell
				int cell = ubuf[pos];

				// create the item and add it to the inventory
				Item* item = m_pSceneMan->CreateItem(ItemDataFromBuffer(&ubuf[pos + 1]));
				item->SetVisible(false);
				m_pBlockInventory->at(cell) = item;

				// increase position
				pos += ilen + 1;
			}

			// clean up
			delete[] ubuf;
		}

		// we can now clean up
		delete[] m_invBuf;
		m_invRLen = 0;

		// now that we have an inventory of items, open the player's inventory
		if (!m_GUIInventory->IsVisible())
			ToggleInventory(m_pPlayer);

		// get the name of the block we're opening
		string name = "Inventory";
		Terrain* t = m_pSceneMan->GetFirstInstance<Terrain>();
		if (t)
		{
			const ItemClass* ic = Item::GetItemClass(t->GetBlock(m_pPlayer->GetBlockInteractionPosition()));
			if (ic)
				name = ic->strName;
		}

		// create a new inventory GUI widget and assign it to our new inventory
		vector2di ScreenSize = m_pGUIMan->GetRoot()->GetScreenSize();
		GLfloat ypos = (ScreenSize.y - 700) / 2.f;
		Pane* invpane = m_pGUIMan->CreatePane(name.c_str(), vector2df((GLfloat)ScreenSize.x - 1180, 410), vector2di(540, 240), m_pGUIMan->GetFont("font14b"), NULL, 100, false);
		dynamic_cast<Text*>(invpane->GetChild(0))->SetShadowed();
		m_GUIBlockInventory = m_pGUIMan->CreateInventory(vector2df(20, 30), vector2di(480, 192), m_pBlockInventory, invsize, true, invpane);
		m_GUIBlockInventory->UpdateFromInventory();
		invpane->SetAlpha(0);
		invpane->FadeTo(1);
		invpane->MoveTo(vector2df((GLfloat)ScreenSize.x - 1180, ypos + 400));

		// update the recipe
		UpdateRecipe();
	}

	// are we returning to the main menu?
	if (m_bReturnToMainMenu)
	{
		Game* g = m_pGUIMan->GetRoot()->CastTo<Game>();
		
		if (m_DisconnectMessage.empty())
		{
			unsigned char buf[1];
			buf[0] = g->GetNetworkManager()->GetClientID();
			g->GetNetworkManager()->SendPacket(disconnect, buf, 1);
		}

		g->StartMenu(m_DisconnectMessage);
	}

	// have the player's stats changed?
	if (m_bStatChange && m_pPlayer)
	{
		// update the relevant GUI widgets
		const Stats& stats = m_pPlayer->GetStats();
		m_GUIHealthBar->SetMaxValue(stats.maxhealth);
		m_GUIHealthBar->SetCurrentPosition(stats.health);
		m_GUIEnergyBar->SetMaxValue(stats.maxenergy);
		m_GUIEnergyBar->SetCurrentPosition(stats.energy);

		// create damage text if needs be
		if (m_PreviousStats.health > stats.health)
		{
			std::stringstream ss;
			ss << "^1";
			ss << m_PreviousStats.health - stats.health;

			vector3df pos3(m_pPlayer->GetPosition() + vector3df(-3 + rand() % 6, 7 + rand() % 8, -4 + rand() % 6));
			vector2df pos = m_pSceneMan->ConvertToScreenCoordinates(pos3);
			Text* damage = m_pGUIMan->CreateText(ss.str().c_str(), pos, vector2di(100, 25), m_pGUIMan->GetFont("font20b"));
			damage->SetAlpha(0.9f);
			damage->AnchorToWorld(pos3);
			damage->SetAnimationSmoothness(48.f);
			damage->FadeTo(0, false, true);

			// create a little animation on the health bar to show part of the health being taken off
			vector2df startPos(229, 22);
			vector2di size(0, 26);
			GLfloat ratio = 1.f / (GLfloat)stats.maxhealth;

			startPos.x += (ratio * stats.health) * 216;
			size.x = ((ratio * m_PreviousStats.health) - (ratio * stats.health)) * 216;

			cout << startPos.x << ", " << size.x << endl;

			Image* img = m_pGUIMan->CreateImage("all_white.png", startPos, size, GLColour(130, 250, 100, 255));
			img->SetAnimationSmoothness(24.f);
			img->SetDepth(10);
			img->MoveTo(vector2df(startPos.x, 4));
			img->FadeTo(0.f, false, true);
		}
		m_PreviousStats = stats;

		// are we dead?
		if (!m_bDead)
		{
			if (stats.health == 0)
			{
				m_bDead = true;
				m_GUIDeadText->SetVisible(true);
				m_GUIDeadText->FadeTo(1);
			}
		}
		else if (stats.health > 0 && m_GUIDeadText->IsVisible())
		{
			m_bDead = false;
			m_GUIDeadText->FadeTo(0, true);
		}

		// reset the flag
		m_bStatChange = false;
	}

	// keep flashing energy bar if it's at 0
	if (m_pPlayer)
	{
		const Stats& stats = m_pPlayer->GetStats();

		if ((stats.energy == 0 || m_bForceEnergyFlash) && !m_GUIEnergyDepleted->IsVisible())
		{
			m_GUIEnergyDepleted->SetVisible(true);
			m_GUIEnergyDepleted->SetAlpha(1.f);
			m_GUIEnergyDepleted->FadeTo(0.f, true);
			m_bForceEnergyFlash = false;
		}
	}

	// if we have moved into a new biome, display it on screen
	if (m_GUIBiomeText->GetText() != m_NewBiome)
	{
		m_GUIBiomeText->SetText(m_NewBiome, true);
	}
}

void GameGUI::DeleteBlockInventory()
{
	// if we have a block inventory, delete it
	if (m_pBlockInventory)
	{
		// get the network manager
		NetworkManager* n = m_pSceneMan->GetRoot()->CastTo<Game>()->GetNetworkManager();
		if (!n)
			return;
		
		// firstly we need to send a close interaction message
		Terrain* t = m_pSceneMan->GetFirstInstance<Terrain>();
		if (!t)
			return;

		t->SendInteraction(m_pPlayer->GetBlockInteractionPosition(), false);

		// next we need to create a block inventory message to send to the server to update this inventory
		vector<unsigned char> items;

		// union for conversion
		union {
			short size;
			unsigned char byte[2];
		} conv;

		for (GLuint i = 0; i < m_pBlockInventory->size(); ++i) {
			if (m_pBlockInventory->at(i))
			{
				// there is an item here, write the position
				items.push_back(i);

				// create some itemdata and push it back
				ItemData data;
				data.index = m_pBlockInventory->at(i)->GetIndex();
				data.amount = m_pBlockInventory->at(i)->GetStackSize();
				data.pos.Set(0, 0, 0);
				unsigned char* itemdata = ItemDataToBuffer(data);

				for (GLuint j = 0; j < sizeof(ItemData); ++j) {
					items.push_back(itemdata[j]);
				}

				// delete the object
				m_pSceneMan->DeleteObject(m_pBlockInventory->at(i));
			}
		}

		// get position of this block to send
		vector3di blockPos, chunkPos;
		t->ConvertToChunkBlockPosition(m_pPlayer->GetBlockInteractionPosition(), blockPos, chunkPos);

		// function to place position in buffer
		auto position_to_buffer = [](const vector3di& blockPos, vector3di& chunkPos, unsigned char* buf) {
			// correct the data in the buffer
			unsigned char _x = (unsigned char)blockPos.x;
			unsigned char _y = (unsigned char)blockPos.y;
			unsigned char _z = (unsigned char)blockPos.z;
			buf[1] = _x;
			buf[2] = _y;
			buf[3] = _z;

			// place the chunk position in the buffer
			IntToByte(chunkPos.x, &buf[4]);
			IntToByte(chunkPos.y, &buf[8]);
			IntToByte(chunkPos.z, &buf[12]);
		};

		if (!items.empty())
		{
			// now we can compress the data
			uLongf size = items.size() + 100;
			unsigned char* cbuf = new unsigned char[size];
			compress(cbuf, &size, items.data(), items.size());

			// we now need to create another buffer to add the position detail
			unsigned char* buf = new unsigned char[size + 16];
			buf[0] = n->GetClientID();
			position_to_buffer(blockPos, chunkPos, buf);
			memcpy(&buf[16], cbuf, size);

			// send the packet and clean up
			n->SendPacket(block_inventory, buf, size + 16, true);
			delete[] cbuf;
			delete[] buf;
		}
		else
		{
			// we now need to create another buffer to add the position detail
			unsigned char* buf = new unsigned char[16];
			buf[0] = n->GetClientID();
			position_to_buffer(blockPos, chunkPos, buf);

			// send the packet and clean up
			n->SendPacket(block_inventory, buf, 16, true);
			delete[] buf;
		}

		// clean up
		delete m_pBlockInventory;
		m_pBlockInventory = NULL;
	}

	// now clean up the GUI if it exists
	if (m_GUIBlockInventory)
	{
		m_GUIBlockInventory->GetRoot()->FadeTo(0);
		m_GUIBlockInventory->GetRoot()->MoveTo(vector2df((GLfloat)m_GUIBlockInventory->GetRoot()->GetPosition().x, 410), false, true);
		m_GUIBlockInventory = NULL;
	}
}

void GameGUI::GoToMainMenu(string message)
{
	Terrain::StopThreads();
	
	m_bReturnToMainMenu = true;
	m_DisconnectMessage = message;
}

void GameGUI::StatsUpdated()
{
	m_bStatChange = true;
}

void GameGUI::Underwater(bool bUnder)
{
	if (m_GUIUnderwater->IsVisible() != bUnder)
		m_GUIUnderwater->SetVisible(bUnder);
}

void GameGUI::FlashEnergyBar()
{
	m_bForceEnergyFlash = true;
}

void GameGUI::NewBiome(const string& biome)
{
	m_NewBiome = biome;
}

void GameGUI::CloseMenu(GUIElement* e)
{
	// close the menu
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();

	if (g)
		g->ToggleMenu(g->m_pPlayer);
}

void GameGUI::HotbarLeftClicked(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	SceneManager* sceneMan = g->m_pSceneMan;

	if (g)
	{
		Player* p = g->m_pPlayer;
		if (p)
		{
			if (g->m_GUIInventory->IsVisible())
			{
				Inventory::CellLeftClicked(e);
				e->GetManager()->GetRoot()->CastTo<Game>()->GetNetworkManager()->SendPlayerInventory(player_hotbar_data, p->GetHotbar());
				p->SetSelected(p->GetSelected());
			}
			else
			{
				InventoryCell* ic = dynamic_cast<InventoryCell*>(e);
				if (ic)
				{
					Inventory* inv = dynamic_cast<Inventory*>(e->GetParent());
					if (inv)
					{
						p->SetSelected(inv->GetSlot(ic));
						g->UpdateSelected();
					}
				}
			}
		}
	}
}

void GameGUI::HotbarRightClicked(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();

	if (g)
	{
		if (g->m_GUIInventory->IsVisible())
		{
			Player* p = g->m_pPlayer;
			if (p)
			{
				p->SetSelected(p->GetSelected());

				Inventory::CellRightClicked(e);
				e->GetManager()->GetRoot()->CastTo<Game>()->GetNetworkManager()->SendPlayerInventory(player_hotbar_data, p->GetHotbar());
			}
		}
	}
}

void GameGUI::EquipmentLeftClicked(GUIElement* e)
{
	Inventory::CellLeftClicked(e);

	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();

	if (g)
	{
		g->m_pPlayer->UpdateAmmo();
		g->UpdateDefence();

		e->GetManager()->GetRoot()->CastTo<Game>()->GetNetworkManager()->SendPlayerInventory(player_equipment_data, g->m_pPlayer->GetEquipment());
	}
}

void GameGUI::EquipmentRightClicked(GUIElement* e)
{
	Inventory::CellRightClicked(e);

	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();

	if (g)
	{
		g->m_pPlayer->UpdateAmmo();
		g->UpdateDefence();

		e->GetManager()->GetRoot()->CastTo<Game>()->GetNetworkManager()->SendPlayerInventory(player_equipment_data, g->m_pPlayer->GetEquipment());
	}
}

void GameGUI::CraftingSearchBar(GUIElement* e)
{
	// search through the recipe list
	TextBox* tb = dynamic_cast<TextBox*>(e);
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();

	if (tb && g)
	{
		string t = tb->GetText();
		if (t != "")
		{
			g->m_GUIRecipes->Search(t);
		}
		else
		{
			g->m_GUIRecipes->ClearSearch();
		}
	}
}

void GameGUI::RecipePageChanged(GUIElement* e)
{
	// call the default function to get the selection
	DropDownMenu::ListBoxFunction(e);

	// change the recipe tab!
	DropDownMenu* menu = dynamic_cast<DropDownMenu*>(e->GetParent());
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();

	if (menu && g)
	{
		g->m_RecipeBook->AddRecipesToListBox(menu->GetSelected(), g->m_GUIRecipes);
		g->m_GUICraftingSearch->SetText("");
	}
}

void GameGUI::RecipeSelected(GUIElement* e)
{
	// call the default function to get the selection
	ListBox::DefaultFunction(e);

	// update the selected recipe!
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (g)
		g->UpdateRecipe();
}

void GameGUI::CraftRecipe(GUIElement* e)
{
	// craft the selected recipe
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (g)
		g->CraftRecipe(atoi(g->m_GUICraftingAmount->GetText().c_str()));
}

void GameGUI::CraftMaxRecipe(GUIElement* e)
{
	// craft the selected recipe
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (g)
		g->CraftRecipe(-1);
}

void GameGUI::LoadOptions(GUIElement* e)
{
	// read the display options
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (!g)
		return;

	vector2di ScreenSize = g->m_pGUIMan->GetRoot()->GetScreenSize();
	SettingsManager* sm = g->m_Settings;
	std::stringstream r;
	r << ScreenSize.x << "x" << ScreenSize.y;
	g->m_GUIResolutions->Select(g->m_GUIResolutions->GetFirstItem(r.str()));

	g->m_GUIFullscreen->SetChecked(sm->GetValueAsBool("fullscreen"));
	g->m_GUIBorderless->SetChecked(sm->GetValueAsBool("borderless"));
	g->m_GUIVSync->SetChecked(sm->GetValueAsBool("vsync"));

	g->m_GUIPerPixelLighting->SetChecked(sm->GetValueAsBool("perpixel"));
	g->m_GUINormalMapping->SetChecked(sm->GetValueAsBool("normalmaps"));
	g->m_GUIShadows->Select(sm->GetValueAsInt("shadows"));

	FullscreenUpdated(g->m_GUIFullscreen);
	BorderlessUpdated(g->m_GUIBorderless);
	PerPixelUpdated(g->m_GUIPerPixelLighting);

	g->m_GUIApply->SetEnabled(false);

	// ensure the controls are displayed correctly
	vector<int>* controls = g->m_pPlayer->GetControls();
	g->m_GUIControls->ChangeRow(0, { "Move Up", g->m_Settings->GetKeyName(controls->at(move_up)) });
	g->m_GUIControls->ChangeRow(1, { "Move Down", g->m_Settings->GetKeyName(controls->at(move_down)) });
	g->m_GUIControls->ChangeRow(2, { "Move Left", g->m_Settings->GetKeyName(controls->at(move_left)) });
	g->m_GUIControls->ChangeRow(3, { "Move Right", g->m_Settings->GetKeyName(controls->at(move_right)) });
	g->m_GUIControls->ChangeRow(4, { "Jump", g->m_Settings->GetKeyName(controls->at(jump)) });
	g->m_GUIControls->ChangeRow(5, { "Roll", g->m_Settings->GetKeyName(controls->at(roll)) });
	g->m_GUIControls->ChangeRow(6, { "Sprint", g->m_Settings->GetKeyName(controls->at(sprint)) });
	g->m_GUIControls->ChangeRow(7, { "Inventory", g->m_Settings->GetKeyName(controls->at(inventory)) });
	g->m_GUIControls->ChangeRow(8, { "Menu", g->m_Settings->GetKeyName(controls->at(menu)) });
}

void GameGUI::ApplyOptions(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (!g)
		return;

	// disable the button
	g->m_GUIApply->SetEnabled(false);

	// write the display options
	string res = g->m_GUIResolutions->GetSelected();
	int width = atoi(res.substr(0, res.find('x')).c_str());
	int height = atoi(res.substr(res.find('x') + 1, res.length()).c_str());
	g->m_Settings->ChangeSetting("width", width);
	g->m_Settings->ChangeSetting("height", height);
	g->m_Settings->ChangeSetting("fullscreen", g->m_GUIFullscreen->GetChecked());
	g->m_Settings->ChangeSetting("borderless", g->m_GUIBorderless->GetChecked());
	g->m_Settings->ChangeSetting("vsync", g->m_GUIVSync->GetChecked());
	g->m_Settings->ChangeSetting("perpixel", g->m_GUIPerPixelLighting->GetChecked());
	g->m_Settings->ChangeSetting("normalmaps", g->m_GUINormalMapping->GetChecked());
	g->m_Settings->ChangeSetting("shadows", g->m_GUIShadows->GetSelectedNumber());

	// write the controls
	g->m_Settings->ChangeSetting("move_up", g->m_Settings->GetKeyFromName(g->m_GUIControls->GetCell(1, 0)));
	g->m_Settings->ChangeSetting("move_down", g->m_Settings->GetKeyFromName(g->m_GUIControls->GetCell(1, 1)));
	g->m_Settings->ChangeSetting("move_left", g->m_Settings->GetKeyFromName(g->m_GUIControls->GetCell(1, 2)));
	g->m_Settings->ChangeSetting("move_right", g->m_Settings->GetKeyFromName(g->m_GUIControls->GetCell(1, 3)));
	g->m_Settings->ChangeSetting("jump", g->m_Settings->GetKeyFromName(g->m_GUIControls->GetCell(1, 4)));
	g->m_Settings->ChangeSetting("roll", g->m_Settings->GetKeyFromName(g->m_GUIControls->GetCell(1, 5)));
	g->m_Settings->ChangeSetting("sprint", g->m_Settings->GetKeyFromName(g->m_GUIControls->GetCell(1, 6)));
	g->m_Settings->ChangeSetting("inventory", g->m_Settings->GetKeyFromName(g->m_GUIControls->GetCell(1, 7)));
	g->m_Settings->ChangeSetting("menu", g->m_Settings->GetKeyFromName(g->m_GUIControls->GetCell(1, 8)));

	// apply these controls to the player
	g->ControlsUpdate();

	// write the settings
	g->m_Settings->WriteFile();

	// now we've written the settings, let us apply them
	Game::ApplySettings(e);
}

void GameGUI::OKOptions(GUIElement* e)
{
	ApplyOptions(e);
	Menu::ReturnToMainScreen(e);
}

void GameGUI::CancelOptions(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (!g)
		return;

	if (g->m_GUIApply->IsEnabled())
		g->m_pGUIMan->ShowYesNoDialog("Unsaved Settings", "You have unsaved settings. Are you sure you want to continue?", Menu::ReturnToMainScreen);
	else
		Menu::ReturnToMainScreen(e);
}

void GameGUI::EnableApply(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (!g)
		return;

	g->m_GUIApply->SetEnabled(true);
}

void GameGUI::FullscreenUpdated(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (!g)
		return;

	CheckBox* c = dynamic_cast<CheckBox*>(e);
	g->m_GUIBorderless->SetEnabled(!c->GetChecked());

	EnableApply(e);
}

void GameGUI::BorderlessUpdated(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (!g)
		return;

	CheckBox* c = dynamic_cast<CheckBox*>(e);
	g->m_GUIFullscreen->SetEnabled(!c->GetChecked());

	EnableApply(e);
}

void GameGUI::PerPixelUpdated(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (!g)
		return;

	CheckBox* c = dynamic_cast<CheckBox*>(e);
	g->m_GUINormalMapping->SetEnabled(c->GetChecked());

	if (!c->GetChecked())
		g->m_GUINormalMapping->SetChecked(false);

	EnableApply(e);
}

void GameGUI::KeyChangePressed(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (g)
		g->m_pGUIMan->ShowKeyChange(g->m_GUIControls->GetCell(0, g->m_GUIControls->GetSelectedRow()));
}

void GameGUI::KeyBindingChange(GUIElement* e)
{
	KeyChange* k = dynamic_cast<KeyChange*>(e->GetRoot());
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (k && g)
	{
		g->ChangeKeyBinding(k->GetControl(), k->GetKey());
		g->EnableApply(e);
	}

	GUIElement::Close(e);
}

void GameGUI::ResetControlPressed(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (g)
		g->ResetControl(g->m_GUIControls->GetSelectedRow());
}

void GameGUI::DefaultsControlPressed(GUIElement* e)
{
	GameGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (g)
	{
		for (GLuint i = 0; i < g->m_DefaultControls.size(); ++i) {
			g->ResetControl(i);
		}
	}
}

void GameGUI::ExitGame(GUIElement* e)
{
	Terrain::StopThreads();
	
	unsigned char buf[1];
	buf[0] = e->GetManager()->GetRoot()->CastTo<Game>()->GetNetworkManager()->GetClientID();
	e->GetManager()->GetRoot()->CastTo<Game>()->GetNetworkManager()->SendPacket(disconnect, buf, 1);

	GUIElement::CloseGame(e);
}

bool GameGUI::ToggleMenu(GLObject* o)
{
	GameGUI* g = o->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (g)
	{
		// if we are chatting, close the chat and clear it
		if (g->m_GUIChatBar->IsVisible())
		{
			g->m_GUIChatBar->SetText("");
			g->m_GUIChatBar->SetVisible(false);
			g->m_GUIChatBox->ToggleChat(false);
			g->m_pGUIMan->SetCharCallback(NULL);
			return true;
		}
		
		if (!g->m_Menu->IsVisible())
		{
			g->m_Menu->SetVisible(true);
			g->m_Menu->FadeTo(1);
			g->m_pPlayer->SaveToFile();
			o->GetSceneManager()->ToggleMouse(false);
		}
		else
		{
			g->m_Menu->SwitchPage("Menu");
			g->m_Menu->FadeTo(0, true);
			o->GetSceneManager()->ToggleMouse(!g->m_GUIInventory->IsVisible());
		}
	}

	return true;
}

bool GameGUI::ToggleInventory(GLObject* o)
{
	// if we are in a menu then don't do anything
	GameGUI* g = o->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (g->m_Menu->IsVisible())
		return false;

	// if we are dead, do nothing
	if (g->m_pPlayer->GetStats().health == 0)
		return false;

	// if the keyboard is not free
	if (!g->m_pGUIMan->IsKeyboardFree())
		return false;

	// toggle the inventory's visibility
	if (g && !g->m_Menu->IsVisible())
	{
		g->m_GUIInventory->SetVisible(!g->m_GUIInventory->IsVisible());

		vector2di ScreenSize = o->GetManager()->GetRoot()->GetScreenSize();
		GLfloat ypos = (ScreenSize.y - 700) / 2.f;

		if (g->m_GUIInventory->IsVisible())
		{
			g->m_GUICrafting->SetVisible(true);
			g->m_GUICrafting->MoveTo(vector2df(g->m_GUICrafting->GetPosition().x, ypos));
			g->m_GUICrafting->FadeTo(1);
			g->m_GUIInventory->SetVisible(true);
			g->m_GUIHotbar->MoveTo(vector2df((GLfloat)ScreenSize.x - 571, ypos + 650));
			g->m_GUIInventory->MoveTo(vector2df((GLfloat)ScreenSize.x - 571, ypos + 430));
			g->m_GUIInventory->FadeTo(1);
			g->m_GUISelectedName->MoveTo(vector2df((GLfloat)ScreenSize.x - 571, ypos + 620));
			g->m_GUISelectedName->FadeTo(0);

			g->UpdateRecipe();
		}
		else
		{
			g->m_GUICrafting->MoveTo(vector2df(g->m_GUICrafting->GetPosition().x, 10));
			g->m_GUICrafting->FadeTo(0, true);
			g->m_GUIHotbar->MoveTo(vector2df((GLfloat)ScreenSize.x - 520, (GLfloat)ScreenSize.y - 60));
			g->m_GUIInventory->MoveTo(vector2df((GLfloat)ScreenSize.x - 571, 440));
			g->m_GUIInventory->FadeTo(0, true);
			g->m_GUISelectedName->MoveTo(vector2df((GLfloat)ScreenSize.x - 520, (GLfloat)ScreenSize.y - 90));
			g->m_GUISelectedName->FadeTo(0.5f);

			GUIManager* pGUIMan = g->m_pGUIMan;
			TextBox* tb = pGUIMan->GetCharCallback();
			if (tb)
				tb->UnFocus();

			if (g->m_GUIInventory->GetHeldItem())
				g->m_GUIInventory->DropHeldItem();

			g->DeleteBlockInventory();
			g->m_pPlayer->SetBlockInventoryOpen(false);
		}

		// toggle the mouse cursor
		o->GetSceneManager()->ToggleMouse(!g->m_Menu->IsVisible() && !g->m_GUIInventory->IsVisible());
	}

	// return true as this will always happen
	return true;
}

bool GameGUI::ToggleChatBar(GLObject* o)
{
	GameGUI* g = o->GetManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	NetworkManager* n = o->GetManager()->GetRoot()->CastTo<Game>()->GetNetworkManager();
	if (g && n)
	{
		// toggle chat bar / send message
		if (g->m_GUIChatBar->IsVisible())
		{
			string text = g->m_GUIChatBar->GetText();
			g->m_GUIChatBar->SetText("");
			//g->m_GUIChatBar->SetVisible(false);
			g->m_GUIChatBar->FadeTo(0, true);
			g->m_pGUIMan->SetCharCallback(NULL);
			g->m_GUIChatBox->ToggleChat(false);

			if (!text.empty())
			{
				// create the chat message
				unsigned char* buf = new unsigned char[BUFFER_SIZE];
				memset(buf, 0, BUFFER_SIZE);
				int len = text.length();
				buf[0] = n->GetClientID();

				for (GLuint i = 0; i < len; ++i) {
					buf[i + 1] = (unsigned char)text[i];
				}
				buf[len + 1] = '\0';
				++len;

				// send the message and clean up
				n->SendPacket(chat_message, buf, len, true);
				delete[] buf;
			}
		}
		else
		{
			g->m_GUIChatBar->SetVisible(true);
			g->m_GUIChatBar->FadeTo(1);
			TextBox::DefaultFunction(g->m_GUIChatBar);
			g->m_GUIChatBox->ToggleChat(true);
		}
	}

	return true;
}

void GameGUI::DefaultControls()
{
	// load the default controls
	m_DefaultControls.push_back(GLFW_KEY_W);
	m_DefaultControls.push_back(GLFW_KEY_S);
	m_DefaultControls.push_back(GLFW_KEY_A);
	m_DefaultControls.push_back(GLFW_KEY_D);
	m_DefaultControls.push_back(GLFW_KEY_SPACE);
	m_DefaultControls.push_back(GLFW_KEY_R);
	m_DefaultControls.push_back(GLFW_KEY_TAB);
	m_DefaultControls.push_back(GLFW_KEY_E);
	m_DefaultControls.push_back(GLFW_KEY_Q);
	m_DefaultControls.push_back(GLFW_KEY_ESCAPE);
}

void GameGUI::UpdateDefence()
{
	// update the defence text
	for (GLuint i = 0; i < 5; ++i) {
		Item* armour = m_GUIEquipment->GetItem(i + 9);

		if (armour)
		{
			std::stringstream ss;
			ss << "^9";
			ss << armour->GetDamage();
			ss << " def";
			
			m_GUIDefence[i]->SetText(ss.str(), true);
		}
		else
			m_GUIDefence[i]->SetText("^90 def");
	}
}
