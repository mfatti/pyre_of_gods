#include "ChatText.h"
#include "GUIManager.h"
#include <sstream>

ChatText::ChatText(GLuint maxlines, Font* font, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size)
	: Text(" ", font, parent, guiParent, pos, size)
	, m_MaxLineCount(maxlines)
	, m_LineCount(0)
	, m_bNeedsCreation(false)
	, m_bChatVisible(false)
{
	// add a scrollbar
	SetRenderBoundary(vector4df(0, 0, (GLfloat)size.x, (GLfloat)size.y));
	m_Scrollbar = GetGUIManager()->CreateScrollbar(this, pos, size, vertical_scroll, guiParent);
	m_Scrollbar->SetVisible(false);

	// set up chat box
	SetLineSpacing(1.05f);
	SetShadowed();
	SetSize(vector3di(GetSize().x - 10, GetSize().y, 1));
	//SplitByWord(true);
}

void ChatText::AddChatMessage(const string& text)
{
	// if we have hit our limit, delete the first line
	if (++m_LineCount > m_MaxLineCount)
	{
		m_LineCount = m_MaxLineCount;
		m_Lines.erase(m_Lines.begin());
	}

	// add a new chatline
	ChatLine line;
	line.text = text;
	line.timer = 5;
	m_Lines.push_back(line);

	// sort out scrollbar / positioning
	GLint thisheight = GetSize().y;
	GLint height = GetFont()->GetDetails().z * 1.1f;//(GLint)GetHeight();
	GLfloat y = m_Scrollbar->GetPosition().y + thisheight - height;
	m_Scrollbar->ScrollToTop();
	m_Scrollbar->ResetHeight();
	SetPosition(vector2df(m_Scrollbar->GetPosition()));

	// loop through all the chat lines and sort out final transformations & string
	m_TextStream.str("");
	for (GLuint i = 0; i < m_LineCount; ++i) {
		// push back our text line
		m_TextStream << "^7" << m_Lines[i].text << endl;

		// add to the scrollbar height
		//m_Scrollbar->AddHeight(height);
	}
	//m_Scrollbar->ScrollToBottom();
	TranslatePosition(vector2df(5, 0));

	// set the needs creation flag
	m_bNeedsCreation = true;
}

void ChatText::ToggleChat(bool visible)
{
	m_bChatVisible = visible;
	m_Scrollbar->SetVisible(m_bChatVisible);
}

void ChatText::SetPosition(const vector2df& pos)
{
	GUIElement::SetPosition(pos);
	m_Scrollbar->SetPosition(pos);
	SetRenderBoundary(vector4df(0, 0, (GLfloat)GetSize().x, (GLfloat)GetSize().y));
}

void ChatText::Update(bool bForce)
{
	// if we have a line to add, add it
	if (m_bNeedsCreation)
	{
		// set the text
		SetText(m_TextStream.str(), true);
		m_bNeedsCreation = false;

		// sort the height / scrolling
		m_Scrollbar->AddHeight(GetHeight() - GetFont()->GetDetails().z * 0.95f);
		m_Scrollbar->ScrollToBottom();
	}
	
	// force updates of this widget and its scrollbar
	GUIElement::Update(true);
	m_Scrollbar->Update(true);
}

bool ChatText::IsHovered()
{
	if (m_bChatVisible)
		return GUIElement::IsHovered();
	else
		return false;// GUIElement::IsHovered();
}
