#ifndef _ANIMATOR_H_
#define _ANIMATOR_H_

#include "Mesh.h"

enum AnimateResult {
	animation_failed = 0,
	animation_still,
	animation_running,
	animation_ended,
	animation_resetting
};

class Animator {
	public:
		Animator(Mesh* mesh);

		void SetMesh(Mesh* mesh);

		AnimateResult Animate(AnimFrame& lower, AnimFrame& upper, GLfloat& t, bool force = false);
		void SetAnimation(string anim, GLfloat pos = 0.f);
		void SetAnimationSpeed(GLfloat speed);
		void SetAnimationPosition(GLfloat pos);
		void MoveToStart();
		void SetAnimationLoop(bool loop);
		bool IsLooping();
		void Start();
		AnimateResult GetAnimationStatus();
		GLfloat GetCurrentPosition();
		GLfloat GetAnimationLength();

	protected:

	private:
		Mesh*				m_Mesh;
		Animation*			m_CurrentAnim;
		AnimFrame			m_LowerFrame;
		AnimFrame			m_UpperFrame;
		GLfloat				m_CurrentAnimPos;
		GLfloat				m_AnimSpeed;
		bool				m_bMoveToStart;
		bool				m_bAnimRunningOver;
		bool				m_bNotLooping;
		bool				m_bRunning;
		string				m_CurrentAnimName;
		AnimateResult		m_Status;

		void CalculateFrames();
};

#endif