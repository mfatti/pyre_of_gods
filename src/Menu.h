#ifndef _MENU_H_
#define _MENU_H_

#include "GUIElement.h"

class Pane;

class Menu : public GUIElement {
	public:
		Menu(Manager* parent, vector2di page = vector2di(600, 400));

		virtual void MouseDownFunction(GLuint button);

		Pane* AddPage(string name, vector2df buttonPos, vector2di buttonSize, void(*fn)(GUIElement* e));
		void SwitchPage(string name);

		Pane* GetPageByName(string name);
		Pane* GetActivePage();

		vector2di GetPageSize();

		void SetMenuPosition(const vector2df &pos);

		static void ButtonClicked(GUIElement* e);
		static void ReturnToMainScreen(GUIElement* e);

	protected:

	private:
		vector2di								m_PageSize;
		std::map<string, void(*)(GUIElement*)>	m_PageLoadFunctions;
};

#endif
