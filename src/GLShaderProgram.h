#ifndef _GLSHADERPROGRAM_H_
#define _GLSHADERPROGRAM_H_

#include "Misc.h"

class GLShaderProgram {
	public:
		GLShaderProgram() : m_VertexShader(0), m_FragmentShader(0), m_ShaderProgram(0) {}
		GLShaderProgram(const GLchar* vertex, const GLchar* fragment);
		~GLShaderProgram();

		void Clear();
		void CompileShaders();
		void RecompileShaders();

		void SetShaders(const GLchar* vertex, const GLchar* fragment);

		void Bind();
		void Unbind();

		GLint GetAttribLocation(const GLchar* attrib);
		GLint GetUniformLocation(const GLchar* attrib);

		void SetOrthoProjection(int width, int height);
		void SetPerspectiveProjection(float angle, float ratio, float near, float far, bool bDefaults = true);
		void SmoothFOV(float dest);

		matrix4f& GetProjectionMatrix();

	protected:

	private:
		GLuint		m_VertexShader;
		GLuint		m_FragmentShader;
		GLuint		m_ShaderProgram;
		matrix4f	m_ProjMatrix;

		GLfloat		m_fAngle;
		GLfloat		m_fRatio;
		GLfloat		m_fNear;
		GLfloat		m_fFar;
		GLfloat		m_fCurrentAngle;

		const GLchar* m_Vertex;
		const GLchar* m_Fragment;
};

#endif
