#include "Inventory.h"
#include "GUIManager.h"
#include "Game.h"
#include "SceneManager.h"
#include <sstream>

Item* Inventory::m_CursorItem = NULL;
MeshView* Inventory::m_CursorMesh = NULL;

InventoryCell::InventoryCell(Manager* parent, Inventory* guiParent, vector3df pos, vector3di size, GLuint quad)
	: GUIElement(parent, guiParent, pos, size)
	, m_Inventory(guiParent)
	, m_Quad(quad)
	, m_bSwitched(false)
	, m_Item(NULL)
{
	// create a mesh view
	m_MeshView = GetGUIManager()->CreateMeshView(NULL, "", vector3df(), size, this);

	m_Text = GetGUIManager()->CreateText("", vector3df(), vector2di(size.x, -1), GetGUIManager()->GetFont("font14b"), this);
	m_Text->SetOffset(vector2df(2, (GLfloat)(size.y - m_Text->GetFont()->GetDetails().z)));
	m_Text->SetDepth(50);
	m_Text->SetShadowed();
}

bool InventoryCell::SetItem(Item* item)
{
	// if we are animating, don't do anything
	if (IsAnimating())
		return false;

	// set the item
	LockMutex();

	if (item)
	{
		const ItemClass* c = Item::GetItemClass(item->GetIndex());

		// check that this item passes filters
		if (!FilterCheck(c))
		{
			UnlockMutex();
			return false;
		}
	}

	m_Item = item;

	if (m_Item != NULL)
	{
		// the item has passed filters, set it here
		const ItemClass* c = Item::GetItemClass(item->GetIndex());
		m_MeshView->SetMeshView(c->Meshes[0], c->strTexturePath);
		if (m_Item->GetStackSize() > 1)
		{
			std::stringstream ss;
			ss << m_Item->GetStackSize();
			m_Text->SetText(ss.str(), true);
		}
		else
			m_Text->SetText("", true);

		if (c->Type == BLOCK)
			m_MeshView->SetBlockModelView(c->BlockSize);
		else if (c->TypeSub == INGOT)
			m_MeshView->SetIngotModelView();
		else
			m_MeshView->SetDefaultModelView();
	}
	else
	{
		m_MeshView->SetMeshView(NULL, "");
		m_Text->SetText("", true);
	}
	UnlockMutex();

	return true;
}

bool InventoryCell::IsHovered()
{
	// if we don't have the game gui open, then return false
	GameGUI* g = GetGUIManager()->GetRoot()->CastTo<Game>()->GetGameGUI();
	if (g && !g->GUIOpen())
		return false;

	return GUIElement::IsHovered();
}

void InventoryCell::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible())
		return;

	// ensure we don't create any labels whilst animating
	bool bAnimating = IsAnimating();

	// hovering
	if (!bAnimating && IsHovered() && !m_bSwitched)
	{
		m_Inventory->ChangeQuadColour(m_Quad, GLColour(80, 80, 80, 150), GLColour(80, 80, 80, 150), GLColour(120, 120, 120, 150), GLColour(120, 120, 120, 150));
		m_bSwitched = true;
		ItemLabel();
		m_MeshView->ScaleTo(vector2df(1.5f, 1.5f));
		m_MeshView->SetDepth(10);
	}
	else if (!bAnimating && !IsHovered() && m_bSwitched)
	{
		m_Inventory->ChangeQuadColour(m_Quad, GLColour(0, 0, 0, 150), GLColour(0, 0, 0, 150), GLColour(20, 20, 20, 150), GLColour(20, 20, 20, 150));
		m_bSwitched = false;
		m_MeshView->ScaleTo(vector2df(1.f, 1.f));
		m_MeshView->SetDepth(0);
	}

	// call GUIElement::Update()
	GUIElement::Update(bForce);
}

void InventoryCell::ItemLabel()
{
	LockMutex();
	if (m_Item && !GetLabel() && !m_Item->GetName().empty())
	{
		// set the description text for the label
		std::stringstream ss;
		ss << m_Item->GetDescription();
		const ItemClass* ic = Item::GetItemClass(m_Item->GetIndex());
		if (ic)
		{
			if (ic->Type == WEAPON)
			{
				ss << " \n\nWeapon Class: ^8%150 " << m_Item->GetWeaponType();
				ss << " \n^7Damage: ^8%150 " << m_Item->GetDamage();
				ss << " \n^7Weapon Speed: ^8%150 " << m_Item->GetWeaponSpeed();
				ss << " \n^7Knockback: ^8%150 " << m_Item->GetKnockbackStrength();
			}
			else if (ic->Type == TOOL)
			{
				if (ic->TypeSub == PICKAXE)
				{
					ss << " \n\nMining Power: ^8%150 " << m_Item->GetDamage() << "%";
				}
				else if (ic->TypeSub == AXE)
				{
					ss << " \n\nChopping Power: ^8%150 " << m_Item->GetDamage() << "%";
				}
			}
		}

		// show the label
		GetGUIManager()->ShowLabel(m_Item->GetName(), ss.str(), this);
	}
	UnlockMutex();
}

void InventoryCell::MouseDownFunction(GLuint button)
{
	// do nothing for now
}

Item* InventoryCell::GetItem()
{
	return m_Item;
}

void InventoryCell::UpdateText()
{
	if (m_Item != NULL)
	{
		if (m_Item->GetStackSize() > 1)
		{
			std::stringstream ss;
			ss << m_Item->GetStackSize();
			m_Text->SetText(ss.str(), true);
		}
		else
			m_Text->SetText("", true);
	}
}

void InventoryCell::SetText(int amount)
{
	std::stringstream ss("");

	if (amount > 0)
		ss << amount;

	m_Text->SetText(ss.str(), true);
}

void InventoryCell::SetItemFilter(const ItemFilter& filter)
{
	m_Filter = filter;
}

bool InventoryCell::CompareItemFilter(const ItemFilter& filter)
{
	return m_Filter == filter;
}

bool InventoryCell::CompareItemToItemFilter(const ItemFilter& filter)
{
	if (!m_Item)
		return false;

	const ItemClass* ic = Item::GetItemClass(m_Item->GetIndex());

	return FilterCheck(ic, filter);
}

bool InventoryCell::PassesFilters(Item* item)
{
	// get item class
	const ItemClass* ic = Item::GetItemClass(item->GetIndex());
	if (ic)
		return FilterCheck(ic);

	return false;
}

bool InventoryCell::FilterCheck(const ItemClass* ic, const ItemFilter& filter)
{
	// choose the filter
	const ItemFilter* _filter = &m_Filter;
	if (ItemFilter() != filter)
		_filter = &filter;
	
	// check if the given item class passes our filter
	if (_filter->type_filter != NO_TYPE && ic->Type != _filter->type_filter)
		return false;

	if (_filter->subtype_filter != NO_SUBTYPE && ic->TypeSub != _filter->subtype_filter)
		return false;

	return true;
}

Inventory::Inventory(Manager* parent, GUIElement* guiParent, vector<Item*>* inventory, GLuint slots, vector3df pos, vector3di size, bool bBackground)
	: GUIElement(parent, guiParent, pos, size)
	, m_Slots(slots)
	, m_Inventory(inventory)
	, m_bCreated(false)
{
	// sort out the inventory
	//for (GLuint i = 0; i < slots; i++) {
	//	m_Inventory[i] = &inventory[i];
	//}
	
	// create a row of inventory cells
	vector2di cellSize(48, 48);

	// work out how many cells we need
	GLuint xn = size.x / cellSize.x;
	GLuint yn = slots / xn;

	if (bBackground)
	{
		AddQuad(vector2df(-2, -2), vector2df(0.5, 0.5), vector2df((GLfloat)size.x + xn * 2, (GLfloat)size.y + yn * 2), vector2df(0.5, 0.5), GLColour(0, 0, 0, 50));

		AddQuad(vector2df(-2, -2), vector2df(0.5, 0.5), vector2df((GLfloat)size.x + xn * 2, -1), vector2df(0.5, 0.5), GLColour(61));
		AddQuad(vector2df(-2, (GLfloat)size.y + yn * 2 - 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x + xn * 2, (GLfloat)size.y + yn * 2), vector2df(0.5, 0.5), GLColour(61));
		AddQuad(vector2df(-2, -1), vector2df(0.5, 0.5), vector2df(-1, (GLfloat)size.y + yn * 2 - 1), vector2df(0.5, 0.5), GLColour(61));
		AddQuad(vector2df((GLfloat)size.x + xn * 2 - 1, -1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x + xn * 2, (GLfloat)size.y + yn * 2 - 1), vector2df(0.5, 0.5), GLColour(61));
	}

	for (GLuint i = 0; i < slots; ++i) {
		vector2df pos((GLfloat)((i % xn) * cellSize.x + 2 * (i % xn)), (GLfloat)((GLuint)(i / xn) * cellSize.y + 2 * (GLuint)(i / xn)));
		InventoryCell* ic = new InventoryCell(parent, this, pos, cellSize, /*AddQuad(pos, vector2df(0.5, 0.5), pos + vector2df((GLfloat)cellSize.x, (GLfloat)cellSize.y), vector2df(0.5, 0.5), GLColour(0, 0, 0, 150))*/
			AddQuad(GLVertex(vector3df(pos.x, pos.y, -20), GLColour(0, 0, 0, 150), vector2df(0.5, 0.5), GLNormal()),
				GLVertex(vector3df(pos.x + (GLfloat)cellSize.x, pos.y, -20), GLColour(0, 0, 0, 150), vector2df(0.5, 0.5), GLNormal()),
				GLVertex(vector3df(pos.x + (GLfloat)cellSize.x, pos.y + (GLfloat)cellSize.y, -20), GLColour(20, 20, 20, 150), vector2df(0.5, 0.5), GLNormal()),
				GLVertex(vector3df(pos.x, pos.y + (GLfloat)cellSize.y, -20), GLColour(20, 20, 20, 150), vector2df(0.5, 0.5), GLNormal())));
		ic->SetFunction(left_mouse, Inventory::CellLeftClicked);
		ic->SetFunction(right_mouse, Inventory::CellRightClicked);
		AddChild(ic);

		//AddQuad(pos, vector2df(0.5, 0.5), pos + vector2df((GLfloat)cellSize.x, (GLfloat)cellSize.y), vector2df(0.5, 0.5), GLColour(0, 0, 0, 255));

		AddQuad(pos, vector2df(0.5, 0.5), pos + vector2df((GLfloat)cellSize.x, 1), vector2df(0.5, 0.5), GLColour(61));
		AddQuad(pos + vector2df(0, (GLfloat)cellSize.y - 1), vector2df(0.5, 0.5), pos + vector2df((GLfloat)cellSize.x, (GLfloat)cellSize.y), vector2df(0.5, 0.5), GLColour(61));
		AddQuad(pos + vector2df(0, 1), vector2df(0.5, 0.5), pos + vector2df(1, (GLfloat)cellSize.y - 1), vector2df(0.5, 0.5), GLColour(61));
		AddQuad(pos + vector2df((GLfloat)cellSize.x - 1, 1), vector2df(0.5, 0.5), pos + vector2df((GLfloat)cellSize.x, (GLfloat)cellSize.y - 1), vector2df(0.5, 0.5), GLColour(61));
	}
	//CreateBufferObjects();

	// create the cursor mesh view
	if (!m_CursorMesh)
	{
		m_CursorMesh = GetGUIManager()->CreateMeshView(NULL, "", vector3df(), cellSize);
		m_CursorMesh->SetDepth(50);
	}
}

void Inventory::MouseDownFunction(GLuint button)
{
	// do nothing for now
}

void Inventory::SetTexture(Texture tex)
{
	// set the texture
	GLObject::SetTexture(tex);
	
	// set children textures
	std::vector<GUIElement*> children = GetChildren();

	for (GLuint i = 0; i < children.size(); ++i) {
		children[i]->SetTexture(GetTexture());
	}
}

void Inventory::Update(bool bForce)
{
	// create the mesh if needs be
	if (!m_bCreated)
	{
		CreateBufferObjects();
		m_bCreated = true;
	}

	// update the cursor mesh view
	vector2dd CursorPos = GetGUIManager()->GetMousePosition();
	m_CursorMesh->SetPosition(vector2df((GLfloat)CursorPos.x + 10, (GLfloat)CursorPos.y + 10));

	// call GUIElement::Update
	GUIElement::Update(bForce);
}

void Inventory::ChangeQuadColour(int pos, GLColour col1, GLColour col2, GLColour col3, GLColour col4)
{
	GLObject::ChangeQuadColour(pos, col1, col2, col3, col4, true);
}

InventoryResult	Inventory::AddItem(Item* item)
{
	// firstly, if this item is stackable (stack size > 1) then try and see if we have any of this item already!
	GLint slot = -1;
	bool someTaken = false;
	SceneManager* pSceneMan = dynamic_cast<SceneManager*>(GetManager()->GetRoot()->GetManager(scene_manager));
	if (item->GetMaxStackSize() > 1)
	{
		bool loop = true;
		while (loop)
		{
			Item* other = FindNextItem(item, slot);
			if (other)
			{
				// we have found another item of this class, let's try and stack the item
				if (other->IncreaseStackSize(item->GetStackSize()))
				{
					// update the current recipe list
					//UpdateCurrentRecipeList();
					GetCell(slot)->UpdateText();

					if (pSceneMan)
						pSceneMan->DeleteObject(item);

					// we were able to add to this item's current stack size, so we are finished here
					return INV_SUCCESS;
				}
				else
				{
					// we have more than the max size of the stack!
					// set the current inventory item's stack size to max and try and add a new item
					int diff = other->GetMaxStackSize() - other->GetStackSize();
					item->DecreaseStackSize(diff);
					other->SetStackSize();
					someTaken = true;
				}
			}
			else
			{
				// we can't find another item this can stack onto, exit this loop
				loop = false;
			}
		}
	}

	// if the item is not stackable / we didn't find any other instance of the item, just try and add it to a free slot
	slot = GetFreeSlot(item);
	if (slot < (GLint)m_Slots)
	{
		// update the current recipe list
		//UpdateCurrentRecipeList();

		if (pSceneMan)
			pSceneMan->DeleteObject(item);

		SetItem(item, slot);
		return INV_SUCCESS;
	}

	// update the current recipe list
	//UpdateCurrentRecipeList();

	// if we reach this part of the code, there's no space for the item!
	return (someTaken ? INV_PARTIAL : INV_FAIL);
}

Item* Inventory::GetItem(GLuint slot)
{
	if (slot >= m_Slots)
		return nullptr;

	return (*m_Inventory)[slot];
}

Item* Inventory::FindNextItem(Item* item, GLint& slot)
{
	for (GLuint i = 0; i < m_Slots; ++i) {
		if ((*m_Inventory)[i] != NULL && *item == *(*m_Inventory)[i] && (GLint)i > slot)
		{
			slot = i;
			return (*m_Inventory)[i];
		}
	}

	return nullptr;
}

Item* Inventory::FindNextItem(const ItemClass *item, GLint& slot)
{
	for (GLuint i = slot + 1; i < m_Slots; ++i) {
		if ((*m_Inventory)[i] != NULL && *(*m_Inventory)[i] == *item)
		{
			slot = i;
			return (*m_Inventory)[i];
		}
	}

	slot = GetSlotCount();
	return nullptr;
}

Item* Inventory::FindItem(const ItemFilter& filter)
{
	// find the first item which passes the given filter
	for (GLuint i = 0; i < m_Slots; ++i) {
		if ((*m_Inventory)[i] != NULL)
		{
			InventoryCell* cell = GetCell(i);

			if (cell->CompareItemToItemFilter(filter))
				return (*m_Inventory)[i];
		}
	}

	return NULL;
}

GLuint Inventory::FindItemSlot(Item* item)
{
	if (item == NULL)
		return m_Slots;
	
	for (GLuint i = 0; i < m_Slots; ++i) {
		if ((*m_Inventory)[i] != NULL && *item == *(*m_Inventory)[i])
			return i;
	}

	return m_Slots;
}

int Inventory::GetItemAmount(const ItemClass* item)
{
	int amount = 0;

	for (GLuint i = 0; i < m_Slots; ++i) {
		if ((*m_Inventory)[i] && *(*m_Inventory)[i] == *item)
		{
			amount += (*m_Inventory)[i]->GetStackSize();
		}
	}

	return amount;
}

int Inventory::GetItemAmount(const ItemFilter& filter)
{
	int amount = 0;

	for (GLuint i = 0; i < m_Slots; ++i) {
		if ((*m_Inventory)[i])
		{
			if (ItemFilter() != filter)
			{
				InventoryCell* cell = GetCell(i);

				if (!cell->CompareItemToItemFilter(filter))
					continue;
			}

			amount += (*m_Inventory)[i]->GetStackSize();
		}
	}

	return amount;
}

bool Inventory::SetItem(Item* item, GLuint slot)
{
	// return if passed an invalid slot
	if (slot >= m_Slots)
		return false;

	// create a copy of the item passed in and add it to the inventory
	Item* newitem = new Item(item, false);

	if (GetCell(slot)->SetItem(newitem))
	{
		UpdateInventory(newitem, slot);
		return true;
	}
	else
	{
		delete newitem;
		return false;
	}
	//m_Inventory[slot].setID(NONE);
}

void Inventory::RemoveItem(GLuint slot)
{
	// return if passed an invalid slot
	if (slot >= m_Slots)
		return;

	// update the current recipe list
	//UpdateCurrentRecipeList();

	// delete the item in the given slot
	(*m_Inventory)[slot] = NULL;
	GetCell(slot)->SetItem(NULL);
}

void Inventory::DecreaseStackSize(GLuint slot, GLshort amount)
{
	if ((*m_Inventory)[slot]->DecreaseStackSize(amount))
		GetCell(slot)->UpdateText();
	else
		RemoveItem(slot);
}

GLuint Inventory::GetSlotCount()
{
	return m_Slots;
}

GLuint Inventory::GetSlot(InventoryCell* ic)
{
	std::vector<GUIElement*> children = GetChildren();

	for (GLuint i = 0; i < children.size(); ++i) {
		if (children[i] == ic)
			return i;
	}

	return m_Slots;
}

int Inventory::GetFreeSlot(Item* item)
{
	for (GLuint i = 0; i < m_Slots; ++i) {
		if ((*m_Inventory)[i] == NULL && GetCell(i)->PassesFilters(item))
			return i;
	}

	return -1;
}

InventoryCell* Inventory::GetCell(GLuint slot)
{
	return dynamic_cast<InventoryCell*>(GetChild(slot));
}

void Inventory::MoveCell(GLuint slot, const vector2df& pos)
{
	InventoryCell* cell = GetCell(slot);
	cell->SetPosition(pos);

	GLuint n = slot * 5;
	vector2di cellSize = cell->GetSize();

	ChangeQuadCoords(n, pos, pos + vector2df(cellSize), false);
	ChangeQuadCoords(n + 1, pos, pos + vector2df((GLfloat)cellSize.x, 1), false);
	ChangeQuadCoords(n + 2, pos + vector2df(0, (GLfloat)cellSize.y - 1), pos + vector2df((GLfloat)cellSize.x, (GLfloat)cellSize.y), false);
	ChangeQuadCoords(n + 3, pos + vector2df(0, 1), pos + vector2df(1, (GLfloat)cellSize.y - 1), false);
	ChangeQuadCoords(n + 4, pos + vector2df((GLfloat)cellSize.x - 1, 1), pos + vector2df((GLfloat)cellSize.x, (GLfloat)cellSize.y - 1));
}

void Inventory::UpdateInventory(Item* item, GLuint slot)
{
	if (slot >= m_Inventory->size())
		return;

	(*m_Inventory)[slot] = item;
}

void Inventory::SetSlotLeftClicked(void(*fn)(GUIElement*))
{
	for (GLuint i = 0; i < m_Slots; ++i) {
		GetCell(i)->SetFunction(left_mouse, fn);
	}
}

void Inventory::SetSlotRightClicked(void(*fn)(GUIElement*))
{
	for (GLuint i = 0; i < m_Slots; ++i) {
		GetCell(i)->SetFunction(right_mouse, fn);
	}
}

void Inventory::CellLeftClicked(GUIElement* e)
{
	// ensure we can't do anything during animations
	if (e->IsAnimating())
		return;
	
	// e should always be an InventoryCell so let's get the inventory it belongs to
	InventoryCell* ic = dynamic_cast<InventoryCell*>(e);
	Inventory* inv = dynamic_cast<Inventory*>(e->GetParent());
	SceneManager* pSceneMan = dynamic_cast<SceneManager*>(e->GetManager()->GetRoot()->GetManager(scene_manager));
	if (ic && inv)
	{
		Item* item = ic->GetItem();
		GLuint slot = inv->GetSlot(ic);

		if (slot == inv->GetSlotCount())
			return;

		if (m_CursorItem == NULL && item != NULL)
		{
			const ItemClass* c = Item::GetItemClass(item->GetName());
			
			m_CursorItem = item;
			m_CursorMesh->SetMeshView(c->Meshes[0], c->strTexturePath);

			ic->SetItem(NULL);
			inv->UpdateInventory(NULL, slot);

			if (ic->GetLabel())
			{
				ic->GetGUIManager()->KillElement(ic->GetLabel());
				ic->SetLabel(NULL);
			}
		}
		else if (m_CursorItem != NULL)
		{
			if (item == NULL)
			{
				if (ic->SetItem(m_CursorItem))
				{
					inv->UpdateInventory(m_CursorItem, slot);

					m_CursorItem = NULL;
					m_CursorMesh->SetMeshView(NULL, "");
				}
			}
			else
			{
				if (*item == *m_CursorItem)
				{
					if (item->IncreaseStackSize(m_CursorItem->GetStackSize()))
					{
						// update the current recipe list
						//UpdateCurrentRecipeList();
						ic->UpdateText();

						bool bDeleted = false;
						if (pSceneMan)
							bDeleted = pSceneMan->DeleteObject(m_CursorItem);

						if (!bDeleted)
							delete m_CursorItem;

						m_CursorItem = NULL;
						m_CursorMesh->SetMeshView(NULL, "");
					}
					else
					{
						// we have more than the max size of the stack!
						// set the current inventory item's stack size to max and try and add a new item
						int diff = item->GetMaxStackSize() - item->GetStackSize();
						m_CursorItem->DecreaseStackSize(diff);
						item->SetStackSize();
					}
				}
				else
				{
					Item* temp = item;
					const ItemClass* c = Item::GetItemClass(item->GetName());

					if (ic->SetItem(m_CursorItem))
					{
						inv->UpdateInventory(m_CursorItem, slot);

						m_CursorItem = temp;
						m_CursorMesh->SetMeshView(c->Meshes[0], c->strTexturePath);
					}
				}
			}

			if (ic->GetItem())
			{
				if (ic->GetLabel())
				{
					ic->GetGUIManager()->KillElement(ic->GetLabel());
					ic->SetLabel(NULL);
				}
				
				ic->ItemLabel();
			}
		}
	}

	if (m_CursorItem)
	{
		const ItemClass* c = Item::GetItemClass(m_CursorItem->GetName());
		if (c->Type == BLOCK)
		{
			m_CursorMesh->SetBlockModelView(c->BlockSize);
		}
		else if (c->TypeSub == INGOT)
			m_CursorMesh->SetIngotModelView();
		else
		{
			m_CursorMesh->SetDefaultModelView();
			vector2di size = m_CursorMesh->GetSize();
			m_CursorMesh->SetModelOffset(vector2df((GLfloat)(-size.x / 2) + 12, (GLfloat)(-size.y / 2) + 20));
		}
	}
}

void Inventory::CellRightClicked(GUIElement* e)
{
	// ensure we can't do anything during animations
	if (e->IsAnimating())
		return;
	
	// e should always be an InventoryCell so let's get the inventory it belongs to
	InventoryCell* ic = dynamic_cast<InventoryCell*>(e);
	Inventory* inv = dynamic_cast<Inventory*>(e->GetParent());
	SceneManager* pSceneMan = dynamic_cast<SceneManager*>(e->GetManager()->GetRoot()->GetManager(scene_manager));
	if (ic && inv)
	{
		Item* item = ic->GetItem();
		GLuint slot = inv->GetSlot(ic);

		if (slot == inv->GetSlotCount())
			return;

		if (m_CursorItem == NULL && item != NULL)
		{
			const ItemClass* c = Item::GetItemClass(item->GetName());
			short stackSize = item->GetStackSize();

			if (stackSize > 1)
			{
				m_CursorItem = new Item(item, false);
				m_CursorMesh->SetMeshView(c->Meshes[0], c->strTexturePath);

				item->SetStackSize(stackSize / 2);
				m_CursorItem->SetStackSize(stackSize - (stackSize / 2));

				ic->UpdateText();
			}
			else
			{
				m_CursorItem = item;
				m_CursorMesh->SetMeshView(c->Meshes[0], c->strTexturePath);

				ic->SetItem(NULL);
				inv->UpdateInventory(NULL, slot);
			}

			if (ic->GetLabel())
			{
				ic->GetGUIManager()->KillElement(ic->GetLabel());
				ic->SetLabel(NULL);
			}
		}
		else if (m_CursorItem != NULL)
		{
			if (item == NULL)
			{
				Item* item = new Item(m_CursorItem, false);
				if (ic->SetItem(item))
				{
					inv->UpdateInventory(item, slot);
					ic->GetItem()->SetStackSize(1);
					ic->UpdateText();

					if (!m_CursorItem->DecreaseStackSize(1))
					{
						bool bDeleted = false;
						if (pSceneMan)
							bDeleted = pSceneMan->DeleteObject(m_CursorItem);

						if (!bDeleted)
							delete m_CursorItem;

						m_CursorItem = NULL;
						m_CursorMesh->SetMeshView(NULL, "");
					}
				}
			}
			else
			{
				if (*item == *m_CursorItem)
				{
					if (item->IncreaseStackSize(1))
					{
						// update the current recipe list
						//UpdateCurrentRecipeList();
						ic->UpdateText();

						if (!m_CursorItem->DecreaseStackSize(1))
						{
							bool bDeleted = false;
							if (pSceneMan)
								bDeleted = pSceneMan->DeleteObject(m_CursorItem);

							if (!bDeleted)
								delete m_CursorItem;

							m_CursorItem = NULL;
							m_CursorMesh->SetMeshView(NULL, "");
						}
					}
				}
				else
				{
					Item* temp = item;
					const ItemClass* c = Item::GetItemClass(item->GetName());

					if (ic->SetItem(m_CursorItem))
					{
						inv->UpdateInventory(m_CursorItem, slot);

						m_CursorItem = temp;
						m_CursorMesh->SetMeshView(c->Meshes[0], c->strTexturePath);
					}
				}
			}

			if (ic->GetItem())
			{
				if (ic->GetLabel())
				{
					ic->GetGUIManager()->KillElement(ic->GetLabel());
					ic->SetLabel(NULL);
				}

				ic->ItemLabel();
			}
		}
	}

	if (m_CursorItem)
	{
		const ItemClass* c = Item::GetItemClass(m_CursorItem->GetName());
		if (c->Type == BLOCK)
		{
			m_CursorMesh->SetBlockModelView(c->BlockSize);
		}
		else if (c->TypeSub == INGOT)
			m_CursorMesh->SetIngotModelView();
		else
		{
			m_CursorMesh->SetDefaultModelView();
			vector2di size = m_CursorMesh->GetSize();
			m_CursorMesh->SetModelOffset(vector2df((GLfloat)(-size.x / 2) + 12, (GLfloat)(-size.y / 2) + 20));
		}
	}
}

Item* Inventory::GetHeldItem()
{
	return m_CursorItem;
}

void Inventory::RemoveHeldItem()
{
	SceneManager* pSceneMan = dynamic_cast<SceneManager*>(GetManager()->GetRoot()->GetManager(scene_manager));

	if (!pSceneMan->DeleteObject(m_CursorItem))
		delete m_CursorItem;
	
	m_CursorItem = NULL;
	m_CursorMesh->SetMeshView(NULL, "");
}

void Inventory::DropHeldItem()
{
	if (!m_CursorItem)
		return;
	
	SceneManager* pSceneMan = dynamic_cast<SceneManager*>(GetManager()->GetRoot()->GetManager(scene_manager));
	NetworkManager* n = GetManager()->GetRoot()->CastTo<Game>()->GetNetworkManager();
	Player* player = pSceneMan->GetFirstInstance<Player>();

	Item* item = pSceneMan->CreateItem(m_CursorItem->GetName(), player->GetMousePlacement(), m_CursorItem->GetStackSize());
	item->SetVisible(false);
	n->SendItem(create_item, item);
	pSceneMan->DeleteObject(item);

	//RemoveHeldItem();
}

void Inventory::DecreaseHeldItemStackSize(GLshort amount)
{
	if (!m_CursorItem->DecreaseStackSize(amount))
		RemoveHeldItem();
}

void Inventory::UpdateFromInventory()
{
	for (GLuint i = 0; i < m_Inventory->size(); ++i) {
		GetCell(i)->SetItem((*m_Inventory)[i]);
	}
}

void Inventory::ClearCursorItem()
{
	m_CursorItem = NULL;
	m_CursorMesh = NULL;
}
