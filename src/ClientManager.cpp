#include "ClientManager.h"
#include "NetworkManager.h"
#include "GUIManager.h"

ClientManager::ClientManager(SceneManager* sceneman, NetworkManager* netman)
	: m_pSceneMan(sceneman)
	, m_pNetworkMan(netman)
	, m_NewClientID(-1)
	, m_NewClientName("")
	, m_DisconnectID(-1)
{

}

void ClientManager::Update()
{
	// if there are any new clients to add, create them
	if (m_NewClientID != -1)
	{
		//GLObject* c = m_pSceneMan->CreateGLObject(vector3df());//new GLObject(m_pSceneMan, vector3df(), vector3di(1));
		Client* c = new Client(m_pSceneMan, vector3df());
		c->SetName(m_NewClientName);
		m_pSceneMan->AddGLObject(c);

		m_Clients.emplace(m_NewClientID, c);

		// create the nameplate
		GUIManager* pGUIMan = dynamic_cast<GUIManager*>(m_pSceneMan->GetRoot()->GetManager(gui_manager));
		Text* text = pGUIMan->CreateText(m_NewClientName.c_str(), vector2df(), vector2di(400, 10), pGUIMan->GetFont("font14b"));
		text->SetShadowed();
		text->SetAlpha(0.8f);
		CharacterGUI gui;
		gui.nameplate = text;
		gui.width = text->GetFont()->GetStringWidth(m_NewClientName);
		m_GUIClientNames.emplace(m_NewClientID, gui);

		// send a packet to ask for inventory data
		unsigned char buf[1];
		buf[0] = m_NewClientID;
		m_pNetworkMan->SendPacket(player_hotbar_data, buf, 1, false, true);
		m_pNetworkMan->SendPacket(player_equipment_data, buf, 1, false, true);

		m_NewClientID = -1;
		m_NewClientName = "";
	}

	// if there are any clients leaving, delete them
	if (m_DisconnectID != -1)
	{
		if (DoesClientExist(m_DisconnectID))
		{
			m_pSceneMan->DeleteObject(m_Clients[m_DisconnectID]);
			m_Clients.erase(m_DisconnectID);
			GUIElement::Close(m_GUIClientNames.at(m_DisconnectID).nameplate);
			m_GUIClientNames.erase(m_DisconnectID);
		}

		m_DisconnectID = -1;
	}

	// update name tags
	map<int, CharacterGUI>::iterator it = m_GUIClientNames.begin();
	while (it != m_GUIClientNames.end())
	{
		Client* client = m_Clients.at(it->first);

		vector2df pos = m_pSceneMan->ConvertToScreenCoordinates(client->GetPosition() + vector3df(0, 22, 0));
		pos.x -= it->second.width * 0.5f;
		it->second.nameplate->SetPosition(pos);

		if (client->GetStats().health == 0 && it->second.nameplate->IsVisible())
			it->second.nameplate->SetVisible(false);
		else if (client->GetStats().health > 0 && !it->second.nameplate->IsVisible())
			it->second.nameplate->SetVisible(true);

		++it;
	}
}

void ClientManager::NewClient(int id, string name)
{
	// add a new client
	m_NewClientID = id;
	m_NewClientName = name;
}

void ClientManager::DeleteClient(int id)
{
	// delete a client
	m_DisconnectID = id;
}

string ClientManager::GetClientName(int id)
{
	if (id == m_pNetworkMan->GetClientID())
		return m_pNetworkMan->GetPlayerName();
	
	if (id == 255)
		return "[SERVER]";

	if (DoesClientExist(id))
		return m_Clients[id]->GetName();
	else
		return "<unknown>";
}

bool ClientManager::DoesClientExist(int id)
{
	// check if the client exists with the given id
	return m_Clients.find(id) != m_Clients.end();
}

void ClientManager::UpdateClientPosition(int id, PlayerData& data)
{
	// update this client's position/rotation information
	m_Clients[id]->SetDestPosition(data.pos);
	m_Clients[id]->SetLookRotation(data.rot);
	m_Clients[id]->SetSwinging(data.swing);
	m_Clients[id]->SetSelectedItem(data.selected);
}

void ClientManager::UpdateClientInventory(int id, int type, unsigned char* data, int len)
{
	// update this client's hotbar, used for displaying held items/tools/weapons
	if (len > 0)
		m_Clients[id]->LoadInventory(type, data, len);
}

void ClientManager::UpdateClientStats(int id, const Stats& stats)
{
	// update this client's stats
	m_Clients[id]->SetStats(stats);
}