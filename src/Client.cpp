#include "Client.h"
#include "SceneManager.h"

Client::Client(Manager* parent, vector3df pos)
	: GLObject(parent, pos, vector3di(1))
	, m_Animation(NULL)
	, m_ToolAnimation(NULL)
	, m_SelectedItem(0)
	, m_Name("")
{
	SetTexture("bronze_anvil.png");
	SetMesh(GetSceneManager()->LoadMeshFromAOBJ("human2.aobj"));
	m_Animation.SetMesh(GetMesh());
	m_Animation.SetAnimation("Walk");
	m_Animation.SetAnimationSpeed(3.5f);
	m_ToolAnimation.SetMesh(GetMesh());
	m_ToolAnimation.SetAnimationSpeed(2.5f);
	m_ToolAnimation.SetAnimation("Pickaxe Swing");
	m_ToolAnimation.SetAnimationLoop(false);

	m_Stats.health = 10;

	for (GLuint i = 0; i < 14; i++) {
		m_Equipment.push_back(NULL);
		m_EquipmentItemClass.push_back(NULL);

		if (i < 10)
		{
			m_Hotbar.push_back(NULL);
			m_HotbarItemClass.push_back(NULL);
		}
	}
}

Client::~Client()
{
	ClearHotbar();
}

void Client::SetDestPosition(vector3df& pos)
{
	m_DestPos.Set(pos);
}

void Client::SetLookRotation(const vector2df& rot)
{
	m_LookRot = rot;
}

void Client::SetSwinging(bool swing)
{
	m_bSwinging = swing;
}

void Client::SetSelectedItem(int item)
{
	m_SelectedItem = item;
}

void Client::LoadInventory(int type, unsigned char* buf, int len)
{
	// load the hotbar from the given data
	// determine which inventory this is going into
	int num = 10;
	vector<Item*>* inv = &m_Hotbar;
	vector<const ItemClass*>* invic = &m_HotbarItemClass;
	if (type == 5/*player_equipment_data*/)
	{
		num = 14;
		inv = &m_Equipment;
		invic = &m_EquipmentItemClass;
	}

	// we need to uncompress the data
	uLongf size = (num) * 300 + 300;
	unsigned char* data = new unsigned char[size];
	uncompress(data, &size, buf, len);

	// now we have the uncompressed data, extract item names
	SceneManager* pSceneMan = GetSceneManager();
	int n = 0;
	const size_t ilen = sizeof(ItemData);
	for (GLuint i = 0; i < num; ++i) {
		// firstly make sure there's no existing item here
		if ((*inv)[i])
			if (!pSceneMan->DeleteObject((*inv)[i]))
				delete (*inv)[i];
		(*inv)[i] = NULL;
		(*invic)[i] = NULL;

		// see if there's an item here
		bool itemHere = data[n] > 0;
		++n;

		if (itemHere)
		{
			// create the item!
			Item* item = pSceneMan->CreateItem(ItemDataFromBuffer(&data[n]));
			item->SetVisible(false);
			(*inv)[i] = item;

			// get the item class
			(*invic)[i] = Item::GetItemClass(item->GetIndex());

			// reset for the next item
			n += ilen;
		}
	}

	// clean up
	delete[] data;
}

void Client::ClearHotbar()
{
	// delete items in the hotbar
	for (GLuint i = 0; i < 10; ++i) {
		// firstly make sure there's no existing item here
		if (m_Hotbar[i])
			if (!GetSceneManager()->DeleteObject(m_Hotbar[i]))
				delete m_Hotbar[i];
		m_Hotbar[i] = NULL;
		m_HotbarItemClass[i] = NULL;
	}
}

string Client::GetName()
{
	return m_Name;
}

void Client::SetName(string name)
{
	m_Name = name;
}

Stats Client::GetStats() const
{
	return m_Stats;
}

void Client::SetStats(const Stats& stats)
{
	m_Stats = stats;
}

void Client::Update(bool bForce)
{
	// interpolate this object to the destination position
	vector3df pos = GetPosition();
	float fMoveX = pos.x - m_DestPos.x;
	float fMoveY = pos.z - m_DestPos.z;
	//float fFallSpeed = pos.y - m_DestPos.y;
	Interpolate(pos.x, m_DestPos.x, 4.5f, 0.01f);
	Interpolate(pos.y, m_DestPos.y, 4.5f, 0.01f);
	Interpolate(pos.z, m_DestPos.z, 4.5f, 0.01f);
	SetPosition(pos);

	// interpolate the rotation
	float rot = m_LookRot.y;
	if (m_LookRot.y - rot > PI)
		rot += TWO_PI;
	else if (rot - m_LookRot.y > PI)
		rot -= TWO_PI;
	Interpolate(rot, m_LookRot.y, 4.5f, 0.001f);
	SetRotation(vector3df(0, rot, 0), 1);

	// animations
	if (fMoveX == 0 && fMoveY == 0/* && abs(fFallSpeed) < 0.2f*/)
		m_Animation.MoveToStart();

	/*if (abs(fFallSpeed) > 0.2f)
		m_Animation.SetAnimation("Jump");
	else
		m_Animation.SetAnimation("Walk");*/

	Animate(&m_Animation);

	// look rotation and swing animation
	//GetBone(1).rot.Set(0, m_LookRot.y, 0);
	//Interpolate(GetBone(1).rot.y, m_LookRot, 4.5f, 0.01f);

	if (m_bSwinging)
		m_ToolAnimation.Start();

	AnimFrame lower, upper;
	GLfloat time;
	if (m_ToolAnimation.Animate(lower, upper, time) == animation_running)
	{
		GetBone(3).rot.Set(Lerp(lower.rotations[3], upper.rotations[3], time));
		GetBone(4).rot.Set(Lerp(lower.rotations[4], upper.rotations[4], time));
		GetBone(13).rot.Set(Lerp(lower.rotations[13], upper.rotations[13], time));
	}

	// call GLObject::Update
	GLObject::Update(bForce);

	// set the selected item transformation matrix
	matrix4f m;
	m_SelectedItemMatrix.Identity();
	m_SelectedItemMatrix.Translate(vector3df(1.4f, 11.5f, 9.f));
	m_SelectedItemMatrix = GetBone(13).transform.Multiply(m_SelectedItemMatrix);
	m.SetRotation(GetRotation());
	m_SelectedItemMatrix = m.Multiply(m_SelectedItemMatrix);
	m.Identity();
	m.SetRotation(vector3df(0, 0, -PI_OVER_2));
	m_SelectedItemMatrix = m_SelectedItemMatrix.Multiply(m);
	m_SelectedItemMatrix.Translate(GetPosition());

	// lighting
	/*SceneManager* pSceneMan = GetSceneManager();
	TerrainBlock b = m_pTerrain->GetBlock(pos + vector3df(0, 5, 0));
	GLfloat sun = pSceneMan->GetSun();
	SetLightAmount(vector3df(max((GLfloat)b.LightRed, b.SkyLight * sun) * 0.066667f, max((GLfloat)b.LightGreen, b.SkyLight * sun) * 0.066667f, max((GLfloat)b.LightBlue, b.SkyLight * sun) * 0.066667f));*/
}

void Client::Render()
{
	// if we are dead, don't do anything
	if (m_Stats.health == 0)
		return;

	// draw this client
	GLObject::Render();

	// draw any equipment
	UseShaderProgram(0);
	GetShaderProgram(0)->Bind();
	if (m_Hotbar[m_SelectedItem])
	{
		RenderMesh(m_HotbarItemClass[m_SelectedItem]->Meshes[0], GetSceneManager()->GetTextureManager()->GetTexture(m_HotbarItemClass[m_SelectedItem]->strTexturePath).gltex, m_SelectedItemMatrix, vector3df(0), true);
	}
	GetShaderProgram(3)->Bind();
	UseShaderProgram(3);
}

void Client::RenderShadows()
{
	// if we are dead, don't do anything
	if (m_Stats.health == 0)
		return;

	// draw this client
	GLObject::RenderShadows();

	// draw any equipment
	UseShadowShaderProgram(2);
	GetShaderProgram(2)->Bind();
	if (m_Hotbar[m_SelectedItem])
	{
		RenderShadowMesh(m_HotbarItemClass[m_SelectedItem]->Meshes[0], GetSceneManager()->GetTextureManager()->GetTexture(m_HotbarItemClass[m_SelectedItem]->strTexturePath).gltex, m_SelectedItemMatrix, true);
	}
	GetShaderProgram(4)->Bind();
	UseShadowShaderProgram(4);
}