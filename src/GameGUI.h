#ifndef _GAME_GUI_H_
#define _GAME_GUI_H_

#include "SceneManager.h"
#include "GUIManager.h"
#include "RecipeBook.h"
#include "SettingsManager.h"

class GameGUI {
	public:
		GameGUI(GUIManager* gui, SceneManager* scene, SettingsManager* settings);
		~GameGUI();

		void UpdateRecipe();
		void CraftRecipe(int n);
		void SetBlockHoverName(string text);
		void SetBlockHoverStatus(string text);

		Inventory* GetInventory();
		Inventory* GetHotbar();
		Inventory* GetEquipment();

		bool GUIOpen();

		void ResolutionUpdate();
		void ControlsUpdate();

		void ChangeKeyBinding(string control, GLint key);
		void ResetControl(GLuint control);

		void UpdateSelected();
		void UpdateMinimapTexture();
		void ForceUpdateMinimap();
		void SetMinimapRotation(GLfloat rot);
		void AddChatMessage(string text);

		string GetKeyName(int key);

		void ProcessBlockInventory(unsigned char* buf, int rlen);
		void WaitForUpdate();
		void DeleteBlockInventory();

		void GoToMainMenu(string message = "");

		void StatsUpdated();
		void Underwater(bool bUnder);

		void FlashEnergyBar();

		void NewBiome(const string& biome);

		// gui mouse functions
		static void CloseMenu(GUIElement* e);
		static void HotbarLeftClicked(GUIElement* e);
		static void HotbarRightClicked(GUIElement* e);
		static void EquipmentLeftClicked(GUIElement* e);
		static void EquipmentRightClicked(GUIElement* e);
		static void CraftingSearchBar(GUIElement* e);
		static void RecipePageChanged(GUIElement* e);
		static void RecipeSelected(GUIElement* e);
		static void CraftRecipe(GUIElement* e);
		static void CraftMaxRecipe(GUIElement* e);

		// menu functions
		static void LoadOptions(GUIElement* e);
		static void ApplyOptions(GUIElement* e);
		static void OKOptions(GUIElement* e);
		static void CancelOptions(GUIElement* e);
		static void EnableApply(GUIElement* e);
		static void FullscreenUpdated(GUIElement* e);
		static void BorderlessUpdated(GUIElement* e);
		static void PerPixelUpdated(GUIElement* e);
		static void KeyChangePressed(GUIElement* e);
		static void KeyBindingChange(GUIElement* e);
		static void ResetControlPressed(GUIElement* e);
		static void DefaultsControlPressed(GUIElement* e);
		static void ExitGame(GUIElement* e);

		// key functions
		static bool ToggleMenu(GLObject* o);
		static bool ToggleInventory(GLObject* o);
		static bool ToggleChatBar(GLObject* o);

	protected:

	private:
		GUIManager*			m_pGUIMan;
		SceneManager*		m_pSceneMan;
		Player*				m_pPlayer;
		SettingsManager*	m_Settings;
		vector<GLint>		m_DefaultControls;
		bool				m_bReturnToMainMenu;
		string				m_DisconnectMessage;
		bool				m_bStatChange;
		Stats				m_PreviousStats;
		string				m_NewBiome;

		// GUI elements
		Menu*				m_Menu;
		RecipeBook*			m_RecipeBook;
		Inventory*			m_GUIInventory;
		Inventory*			m_GUIHotbar;
		Inventory*			m_GUIEquipment;
		Pane*				m_GUICrafting;
		TabManager*			m_GUITabMan;
		Image*				m_GUISelected;
		Text*				m_GUISelectedName;
		Text*				m_GUIBlockHoverName;
		Text*				m_GUIBlockHoverStatus;
		ProgressBar*		m_GUIHealthBar;
		ProgressBar*		m_GUIEnergyBar;
		Image*				m_GUIEnergyDepleted;
		TextBox*			m_GUIChatBar;
		ChatText*			m_GUIChatBox;
		Text*				m_GUIDeadText;
		Image*				m_GUIUnderwater;
		bool				m_bDead;
		bool				m_bForceEnergyFlash;
		Text*				m_GUIBiomeText;

		// equipment tab
		Text*				m_GUIDefence[5];

		// crafting tab
		ListBox*			m_GUIRecipes;
		DropDownMenu*		m_GUIRecipePages;
		TextBox*			m_GUICraftingSearch;
		TextBox*			m_GUICraftingAmount;
		Text*				m_GUIRecipeName;
		Text*				m_GUIStationName;
		Text*				m_GUIRecipeDescription;
		MeshView*			m_GUIRecipeMesh;
		ScrollPane*			m_GUIIngredients;

		// settings
		Button*				m_GUIApply;

		// display settings
		DropDownMenu*		m_GUIResolutions;
		CheckBox*			m_GUIFullscreen;
		CheckBox*			m_GUIBorderless;
		CheckBox*			m_GUIVSync;

		// advanced
		CheckBox*			m_GUIPerPixelLighting;
		CheckBox*			m_GUINormalMapping;
		DropDownMenu*		m_GUIShadows;
		Text*				m_GUIShadowQuality;

		// controls
		Table*				m_GUIControls;

		// minimap
		Image*				m_GUIMinimap;
		Text*				m_GUIPosition;
		Texture				m_MinimapTex;
		GLubyte				m_Minimap[64 * 64 * 4];
		bool				m_bCanUpdateMinimap;
		bool				m_bForceMinimapUpdate;
		static bool			m_bMinimapThreadRunning;

		// chest / block inventory
		unsigned char*		m_invBuf;
		int					m_invRLen;
		vector<Item*>*		m_pBlockInventory;
		Inventory*			m_GUIBlockInventory;

		// private functions
		void DefaultControls();
		void UpdateDefence();
};

#endif