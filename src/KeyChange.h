#ifndef _KEY_CHANGE_H_
#define _KEY_CHANGE_H_

#include "YesNoDialog.h"

class GameGUI;
class Text;

class KeyChange : public YesNoDialog {
	public:
		KeyChange(Manager* parent, string control);

		virtual void Update(bool bForce = false);

		string GetControl();
		GLint GetKey();

	protected:

	private:
		string		m_ControlName;
		GLint		m_Key;
		Text*		m_KeyText;
		GameGUI*	m_pGameGUI;
};

#endif