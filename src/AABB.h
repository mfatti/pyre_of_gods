#ifndef _AABB_H_
#define _AABB_H_

#include "Vector.h"

class ViewFrustum;

class AABB {
	public:
		AABB() : bottomLeft(), topRight() {}
		AABB(const AABB& aabb) : bottomLeft(aabb.bottomLeft), topRight(aabb.topRight) {}
		AABB(const vector3df& bl, const vector3df& tr) : bottomLeft(bl), topRight(tr) {}

		void SetAABB(const vector3df& bl, const vector3df& tr);

		bool InViewFrustum(const ViewFrustum& frustum) const;
		bool QuickInViewFrustum(const ViewFrustum& frustum, const vector3df& pos) const;

		vector3df bottomLeft;
		vector3df topRight;
		float size;

	protected:

	private:
		float halfsize;
		vector3df midPoint;
};

// AABB2D Class
class AABB2D {
public:
	AABB2D(const vector2df& _where, const float _size);
	AABB2D(const AABB2D& other);

	bool PointInside(const vector2df& _where) const;
	bool Intersects(const AABB2D& other) const;

	float GetHalfSize() const;
	vector2df GetPosition() const;

protected:

private:
	vector2df m_Position;
	float m_HalfSize;
};

#endif