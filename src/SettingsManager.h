#ifndef _SETTINGS_MANAGER_H_
#define _SETTINGS_MANAGER_H_

#include "INIFile.h"
#include <map>

class SettingsManager {
	public:
		SettingsManager(string filename);
		~SettingsManager();

		string GetValueAsString(string name);
		int GetValueAsInt(string name);
		bool GetValueAsBool(string name);

		template<class T> void ChangeSetting(string setting, T value)
		{
			m_Settings[setting] = string(std::to_string(value));
		}

		void ReadFile();
		void WriteFile();

		string GetKeyName(int key);
		int GetKeyFromName(string name);

	protected:

	private:
		INIFile						m_SettingsFile;
		std::map<string, string>	m_Settings;
		std::map<int, string>		m_KeyNames;
		std::map<string, int>		m_KeyMap;

		void WriteDefaults();
		void LoadKeyNames();
};

#endif