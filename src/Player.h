#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "AnimatedObject.h"
#include "Item.h"
#include "Terrain.h"

class Camera;
class Item;
class GameGUI;
class GUIElement;
class Terrain;
struct ItemClass;

enum {
	move_up,
	move_down,
	move_left,
	move_right,
	jump,
	roll,
	sprint,
	inventory,
	menu
};

class Player : public GLObject {
	public:
		Player(Manager* parent, string filename, string name);
		~Player();

		void LoadFromFile();
		void SaveToFile();
		static void SaveDefaultCharacter(string filename, string name);

		void SetName(string name);
		string GetName();

		virtual void Update(bool bForce = false);
		virtual void Render();
		virtual void RenderShadows();

		vector<Item*>* GetInventory();
		vector<Item*>* GetHotbar();
		vector<Item*>* GetEquipment();

		bool NearCraftingStation(GLulong block);

		void AttachGameGUI(GameGUI* gui);

		void SetSelected(GLint i);
		GLint GetSelected();

		vector<int>* GetControls();

		InventoryResult PickUpItem(Item* item);

		vector3df GetMousePlacement() const;
		vector3df GetBlockInteractionPosition() const;

		vector2df GetLookDirection() const;

		bool IsSwinging() const;
		bool IsSprinting() const;

		const Stats& GetStats() const;
		void SetStats(const Stats& stats);

		void AttachTerrain(Terrain* t);

		void KnockBack(GLfloat dir, GLfloat speed);

		void SetBlockInventoryOpen(bool open);

		void UseAmmo(int type);
		void UpdateAmmo();

		// transparency render functions
		static void RenderMouseOver(GLObject* o);
		static void RenderBlockBreak(GLObject* o);

		// mouse functions
		void LeftClick();
		static bool RightClick(GLObject* o);
		static bool Scroll(GLObject* o, double x, double y);
	
	protected:
	
	private:
		Camera*			m_pCamera;
		GameGUI*		m_pGUI;
		vector2df		m_CameraRotation;
		GLfloat			m_fLookFactor;
		GLfloat			m_fFallSpeed;
		GLfloat			m_fMoveSpeed;
		vector<Item*>	m_Inventory;
		vector<Item*>	m_Hotbar;
		vector<Item*>	m_Equipment;
		vector3df		m_MouseDestroy;
		vector3df		m_MousePlace;
		vector3df		m_TargetPlace;
		vector3df		m_BlockInteractionPos;
		vector3di		m_BlockInventoryPos;
		bool			m_bBlockInventoryOpen;
		bool			m_bMouseHit;
		GLint			m_SelectedItem;
		matrix4f		m_SelectedItemMatrix;
		Mesh*			m_BlockMesh;
		Mesh*			m_BreakMesh;
		bool			m_bCanPlace;
		vector3df		m_PlacementOffset;
		vector<int>		m_Controls;
		vector4df		m_FadeBox;
		GLfloat			m_FadeHeight;
		TerrainBlock	m_TargetBlock;
		ItemClass*		m_TargetBlockClass;
		ItemClass*		m_SelectedItemClass;
		bool			m_bJump;
		bool			m_bFalling;
		bool			m_bRolling;
		bool			m_bSprinting;
		string			m_Name;
		string			m_FileName;
		bool			m_bSwinging;
		Stats			m_Stats;
		Terrain*		m_pTerrain;
		GLfloat			m_fKnockbackDirection;
		GLfloat			m_fKnockbackSpeed;
		Texture			m_BlockBreakTexture;
		int				m_CurrentAmmo[10];
		Item*			m_AmmoDecrease;
		bool			m_bAmmoUpdated;
		GLfloat			m_fViewRot;
		GLfloat			m_fViewRotFactor;

		Animator		m_Animation;
		Animator		m_ToolAnimation;

		struct BlockBreak {
			GLint full;
			GLint current;
			vector3di pos;
		};

		BlockBreak		m_BlockBreak;

		bool MoveCheck(const vector3df& pos, GLfloat& fMoveX, GLfloat& fMoveY);
		void Roll(GLfloat dir);
};

#endif
