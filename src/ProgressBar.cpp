#include "ProgressBar.h"
#include "GUIManager.h"
#include <sstream>

ProgressBar::ProgressBar(Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size)
	: GUIElement(parent, guiParent, pos, size)
	, m_CurrentPosition(0)
	, m_Ratio(0.01f)
	, m_MaxValue(100)
{
	// create this progress bar
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(0, 50));
	AddQuad(GLVertex(vector3df(2, 2, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x - 2, 2, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x - 2, (GLfloat)size.y - 2, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(2, (GLfloat)size.y - 2, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(GLVertex(vector3df(4, 4, -20), GLColour(30, 200, 100, 180), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(4, 4, -20), GLColour(30, 200, 100, 180), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(4, (GLfloat)size.y - 4, -20), GLColour(130, 250, 100, 180), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(4, (GLfloat)size.y - 4, -20), GLColour(130, 250, 100, 180), vector2df(0.5, 0.5), GLNormal()));

	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 1), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df(0, (GLfloat)size.y - 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df(0, 1), vector2df(0.5, 0.5), vector2df(1, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 255));
	AddQuad(vector2df((GLfloat)size.x - 1, 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 255));
	CreateBufferObjects();

	// create the label for this progressbar
	m_Text = GetGUIManager()->CreateText("###############################################", vector2df(), size, NULL, this);
	m_Text->SetShadowed();
	m_Text->CentreText(true);

	GLfloat off = (size.y - m_Text->GetFont()->GetDetails().z) * 0.5f - 1;
	if (size.y % 2 == 0)
		off--;

	m_Text->SetOffset(vector2df(0, off));
}

void ProgressBar::MouseDownFunction(GLuint button)
{

}

bool ProgressBar::IsHovered()
{
	return false;
}

void ProgressBar::SetCurrentPosition(GLfloat amount)
{
	GLfloat pos = amount * m_Ratio;
	if (m_CurrentPosition == pos)
		return;

	m_CurrentPosition = pos;
	ChangeQuadCoords(2, vector2df(4), vector2df(max((GetSize().x - 4) * m_CurrentPosition, 4.f), GetSize().y - 4));

	std::stringstream ss;
	ss << amount << " / " << m_MaxValue;
	m_Text->SetText(ss.str());
}

void ProgressBar::SetMaxValue(GLfloat value)
{
	m_Ratio = 1.f / value;
	m_MaxValue = value;
}

void ProgressBar::SetColours(GLuint pos, const GLColour& one, const GLColour& two)
{
	ChangeQuadColour(pos, one, one, two, two);
}
