#include "MenuGUI.h"
#include "Game.h"
#include <fstream>
#include <sys/stat.h>
#include <filesystem>
#ifndef __linux__
#include <Windows.h>
#endif

void ReadPlayerList(vector<string>& out)
{
	// check that the save folder exists first!
	string saveDir = string("save/");
	if (!DirectoryExists(saveDir))
	{
#ifdef _WIN32
		_wmkdir(ToWideString(saveDir).c_str());
		_wmkdir(ToWideString(saveDir.append("player/").c_str());
#else
		mkdir(saveDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		mkdir(saveDir.append("player/").c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#endif
	}
	
	// read the players from the player save folder
#ifndef __linux__
	// windows version
	WIN32_FIND_DATA data;
	HANDLE file;

	file = FindFirstFile("save/player/*.sav", &data);
	while (file != INVALID_HANDLE_VALUE)
	{
		out.push_back(string("save/player/").append(data.cFileName));

		if (FindNextFile(file, &data) == 0)
			file = INVALID_HANDLE_VALUE;
	}
	FindClose(file);
#else
	// linux version
	std::string save_directory = "save/player/";
	for (const auto & entry : std::filesystem::directory_iterator(save_directory)) {
		out.push_back(entry.path());
	}
#endif
}

string GetCharacterName(string filename)
{
	// read the player from the file
	char* fbuf;
	std::streampos length;

	std::ifstream file(filename, std::fstream::binary | std::fstream::ate);
	if (file.is_open())
	{
		// read the data
		length = file.tellg();
		fbuf = new char[length];
		file.seekg(0, std::fstream::beg);
		file.read(fbuf, length);
		file.close();

		// uncompress the data
		uLongf size = (50) * 300 + 300;
		unsigned char* buf = new unsigned char[size];
		uncompress(buf, &size, (unsigned char*)fbuf, length);
		delete[] fbuf;

		// create the player
		union {
			short size;
			unsigned char byte[2];
		} conv;
		conv.byte[0] = buf[0];
		conv.byte[1] = buf[1];

		// player name
		short namelen = conv.size;
		char* name = new char[namelen + 1];
		for (short i = 0; i < namelen; ++i) {
			name[i] = (char)buf[2 + i];
		}
		name[namelen] = '\0';
		string pname(name);
		delete[] name;

		return pname;
	}

	return "";
}

MenuGUI::MenuGUI(GUIManager* gui, SceneManager* scene, SettingsManager* settings)
	: m_pGUIMan(gui)
	, m_pSceneMan(scene)
	, m_Settings(settings)
	, m_PaneSize(640, 400)
	, m_CurrentPage(0)
	, m_PreviousPage(0)
	, m_Status(initialisation)
{
	// create the main menu
	vector2di screenSize = m_pGUIMan->GetRoot()->CastTo<Game>()->GetScreenSize();
	Pane* menupane = m_pGUIMan->CreatePane(" ", vector2df(40, -2), vector2di(400, screenSize.y + 4), NULL, NULL, 180, false);
	m_pGUIMan->CreateButton("Exit Game", vector2df(40, screenSize.y - 100), vector2di(320, 40), [](GUIElement* e) {
		MenuGUI* g = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
		if (!g)
			return;

		g->m_pGUIMan->ShowYesNoDialog("Exit Game", "Are you sure you want to quit the game?", GUIElement::CloseGame);
	}, m_pGUIMan->GetFont("font20b"), menupane);
	m_pGUIMan->CreateButton("Play Game", vector2df(40, screenSize.y - 200), vector2di(320, 40), [](GUIElement* e) {
		MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
		if (!m)
			return;

		m->ShowPlayMenu();
	}, m_pGUIMan->GetFont("font20b"), menupane);
	m_pGUIMan->CreateButton("Options", vector2df(40, screenSize.y - 150), vector2di(320, 40), NULL, m_pGUIMan->GetFont("font20b"), menupane);

	// character pane
	vector2df centre(screenSize.x * 0.5f + 200, screenSize.y * 0.5f);
	Pane* characterpane = m_pGUIMan->CreatePane("Character Selection", centre - vector2df(m_PaneSize.x * 0.5f, 200), m_PaneSize, m_pGUIMan->GetFont("font20b"), NULL, 180, false);
	characterpane->SetMenu();

	m_GUICharacters = m_pGUIMan->CreateListBox(vector2df(20, 40), vector2di(200, 315), characterpane);
	m_pGUIMan->CreateButton("New", vector2df(20, 360), vector2di(80, 25), [](GUIElement* e) {
		MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
		if (!m)
			return;

		m->ForwardPage();
	}, NULL, characterpane);
	m_pGUIMan->CreateButton("Delete", vector2df(140, 360), vector2di(80, 25), [](GUIElement* e) {
		e->GetGUIManager()->ShowYesNoDialog("Delete Character?", "Are you sure you want to delete this character?", [](GUIElement* e) {
			GUIElement::Close(e);
			MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
			if (!m)
				return;

			m->DeletePlayer();
		});
	}, NULL, characterpane);
	m_pGUIMan->CreateButton("Play", vector2df(m_PaneSize.x - 100, 360), vector2di(80, 25), [](GUIElement* e) {
		MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
		if (!m)
			return;

		m->GotoPage(2);
	}, NULL, characterpane);

	// get a list of the available saved players
	ReadPlayerList(m_PlayerFiles);
	for (GLuint i = 0; i < m_PlayerFiles.size(); i++) {
		m_GUICharacters->AddItem(GetCharacterName(m_PlayerFiles[i]), i + 1 == m_PlayerFiles.size());
	}

	// new character pane
	Pane* newcharacterpane = m_pGUIMan->CreatePane("New Character", centre - vector2df(m_PaneSize.x * 0.5f, -220), m_PaneSize, m_pGUIMan->GetFont("font20b"), NULL, 180, false);
	newcharacterpane->SetMenu();
	newcharacterpane->SetEnabled(false);
	newcharacterpane->SetAlpha(0.2f);

	m_GUICharacterName = m_pGUIMan->CreateTextBox("", vector2df(20, 40), vector2di(200, 21), "Character name...", newcharacterpane);
	m_pGUIMan->CreateButton("Create Character", vector2df(170, 360), vector2df(160, 25), [](GUIElement* e) {
		MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
		if (!m)
			return;

		m->NewPlayer();
	}, m_pGUIMan->GetFont("font14b"), newcharacterpane);
	m_pGUIMan->CreateButton("Back", vector2df(20, 10), vector2di(80, 25), [](GUIElement* e) {
		MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
		if (!m)
			return;

		m->ReversePage();
	}, NULL, newcharacterpane);

	// play pane
	Pane* playpane = m_pGUIMan->CreatePane("Play", centre - vector2df(m_PaneSize.x * 0.5f, -640), m_PaneSize, m_pGUIMan->GetFont("font20b"), NULL, 180, false);
	playpane->SetMenu();
	playpane->SetEnabled(false);
	playpane->SetAlpha(0.2f);

	m_pGUIMan->CreateButton("Join Game", vector2df(m_PaneSize.x * 0.5f - 100, 120), vector2df(200, 40), [](GUIElement* e) {
		MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
		if (!m)
			return;

		m->ForwardPage();
	}, m_pGUIMan->GetFont("font20b"), playpane);
	m_pGUIMan->CreateButton("Back", vector2df(20, 10), vector2di(80, 25), [](GUIElement* e) {
		MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
		if (!m)
			return;

		m->GotoPage(0);
	}, NULL, playpane);

	// join pane
	Pane* joinpane = m_pGUIMan->CreatePane("Join Game", centre - vector2df(m_PaneSize.x * 0.5f, -1060), m_PaneSize, m_pGUIMan->GetFont("font20b"), NULL, 180, false);
	joinpane->SetMenu();
	joinpane->SetEnabled(false);
	joinpane->SetAlpha(0.2f);

	m_GUIServers = gui->CreateTable(vector2df(10, 40), vector2di(620, 270), 2, joinpane);
	m_GUIServers->SetColumnWidth(0, 360);
	m_GUIServers->SetColumnName(0, "Server");
	m_GUIServers->SetColumnWidth(1, 280);
	m_GUIServers->SetColumnName(1, "Address");

	LoadServers();

	GroupBox* details = m_pGUIMan->CreateGroupBox(vector2df(10, 315), vector2di(360, 75), "Manual Entry", joinpane);
	m_pGUIMan->CreateText("IP Address:\nPort:", vector2df(10, 18), vector2di(100, 0), NULL, details)->SetLineSpacing(1.25f);
	m_GUIIPAddress = m_pGUIMan->CreateTextBox("", vector2df(94, 16), vector2di(170, 21), "Enter IP...", details);
	m_GUIPortNumber = m_pGUIMan->CreateTextBox("", vector2df(94, 44), vector2di(170, 21), "8076", details);
	m_pGUIMan->CreateButton("Connect", vector2df(270, 44), vector2di(80, 21), MenuGUI::StartNetworkManager, NULL, details);
	m_pGUIMan->CreateButton("Save", vector2df(270, 16), vector2di(80, 21), MenuGUI::AddNewServer, NULL, details);

	m_pGUIMan->CreateButton("Join", vector2df(530, 331), vector2di(100, 49), MenuGUI::StartNetworkManager, m_pGUIMan->GetFont("font14b"), joinpane);
	m_pGUIMan->CreateButton("Edit Server", vector2df(380, 331), vector2di(140, 21), MenuGUI::StartEditServer, NULL, joinpane);
	m_pGUIMan->CreateButton("Remove Server", vector2df(380, 359), vector2di(140, 21), [](GUIElement* e) {
		e->GetGUIManager()->ShowYesNoDialog("Remove Server?", "Are you sure you want to remove this server from the list?", [](GUIElement* e) {
			MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
			if (!m)
				return;

			m->RemoveServer();
			GUIElement::Close(e);
		});
	}, NULL, joinpane);

	m_pGUIMan->CreateButton("Back", vector2df(20, 10), vector2di(80, 25), [](GUIElement* e) {
		MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
		if (!m)
			return;

		m->ReversePage();
	}, NULL, joinpane);

	// local play pane
	Pane* localpane = m_pGUIMan->CreatePane("Local Play", centre - vector2df(m_PaneSize.x * 0.5f, -1480), m_PaneSize, m_pGUIMan->GetFont("font20b"), NULL, 180, false);
	localpane->SetMenu();
	localpane->SetEnabled(false);
	localpane->SetAlpha(0.2f);

	// connection pane
	Pane* connectionpane = m_pGUIMan->CreatePane("Joining Server", centre - vector2df(m_PaneSize.x * 0.5f, -1900), m_PaneSize, m_pGUIMan->GetFont("font20b"), NULL, 180, false);
	connectionpane->SetMenu();
	connectionpane->SetEnabled(false);
	connectionpane->SetAlpha(0.2f);
	m_GUIConnectionStatus = m_pGUIMan->CreateText("Attempting connection...", vector2df(0, 180), vector2di(m_PaneSize.x, 0), m_pGUIMan->GetFont("font14b"), connectionpane);
	m_GUIConnectionStatus->CentreText(true);

	// add pages to the page vector
	m_Pages.push_back(characterpane);
	m_Pages.push_back(newcharacterpane);
	m_Pages.push_back(playpane);
	m_Pages.push_back(joinpane);
	m_Pages.push_back(localpane);
	m_Pages.push_back(connectionpane);

	// hide the play menu
	HidePlayMenu();
}

MenuGUI::~MenuGUI()
{
	// clean up
	//m_pGUIMan->CleanUp();
	//m_pSceneMan->CleanUp();
}

void MenuGUI::GotoPage(GLuint page)
{
	if (page < m_Pages.size())
	{
		m_PreviousPage = m_CurrentPage;
		m_CurrentPage = page;

		int diff = m_CurrentPage - m_PreviousPage;
		
		for (GLuint i = 0; i < m_Pages.size(); ++i) {
			m_Pages[i]->MoveTo(m_Pages[i]->GetPosition() - vector2df(0, (m_PaneSize.y + 20) * diff));
		}

		m_Pages[m_PreviousPage]->SetEnabled(false);
		m_Pages[m_PreviousPage]->FadeTo(0.2f, false, false, true);
		m_Pages[m_CurrentPage]->SetEnabled(true);
		m_Pages[m_CurrentPage]->FadeTo(1, false, false, true);
	}
}

void MenuGUI::ReturnToPreviousPage()
{
	GotoPage(m_PreviousPage);
}

void MenuGUI::ForwardPage()
{
	GotoPage(m_CurrentPage + 1);
}

void MenuGUI::ReversePage()
{
	GotoPage(m_CurrentPage - 1);
}

void MenuGUI::ShowPlayMenu()
{
	for (GLuint i = 0; i < m_Pages.size(); ++i) {
		m_Pages[i]->SetVisible(true);
	}
}

void MenuGUI::HidePlayMenu()
{
	for (GLuint i = 0; i < m_Pages.size(); ++i) {
		m_Pages[i]->SetVisible(false);
	}
}

void MenuGUI::ConnectionFailed()
{
	ReturnToPreviousPage();
	m_Status = connection_failed;
}

void MenuGUI::ConnectionSuccessful()
{
	m_Status = connection_successful;
}

int MenuGUI::GetStatus()
{
	return m_Status;
}

void MenuGUI::SetStatus(int status)
{
	m_Status = status;
}

void MenuGUI::WaitForChange()
{
	if (m_Status == connection_failed)
	{
		m_pGUIMan->ShowOKDialog("Connection Failed", "Connection to the server failed with no response.");
		m_Status = initialisation;
	}
	else if (m_Status == connection_successful)
	{
		m_GUIConnectionStatus->SetText("Connection successful. Downloading data...", true);
		m_Status = terrain_download;
	}
	else if (m_Status == starting_game)
	{
		Game* g = m_pGUIMan->GetRoot()->CastTo<Game>();
		g->StartGame(m_PlayerFiles[m_GUICharacters->GetSelected()], m_GUICharacters->GetSelectedText());
	}
}

void MenuGUI::NewPlayer()
{
	// create a new player with the given name
	string name = m_GUICharacterName->GetText();
	if (name == "")
		return;

	// if the player name isn't blank, get the filename
	string filename = string("save/player/").append(ToLower(name)).append(".sav");

	struct stat buffer;
	if (stat(filename.c_str(), &buffer) != 0)
	{
		// the file doesn't exist, create the player etc
		Player::SaveDefaultCharacter(filename, name);

		// add the information to the list
		m_GUICharacters->AddItem(name);
		m_PlayerFiles.push_back(filename);

		// return to the character page
		GotoPage(0);
	}
}

void MenuGUI::DeletePlayer()
{
	// delete the currently selected player
	string name = m_GUICharacters->GetSelectedText();
	GLuint selected = m_GUICharacters->GetSelected();
	string filename = m_PlayerFiles[selected];

	std::remove(filename.c_str());
	if (!std::ifstream(filename))
	{
		// the file was deleted successfully
		m_GUICharacters->RemoveItem(selected);
		m_GUICharacters->Select(0);

		int i = 0;
		vector<string>::iterator it = m_PlayerFiles.begin();
		while (it != m_PlayerFiles.end())
		{
			if (i == selected)
			{
				m_PlayerFiles.erase(it);
				break;
			}

			++it;
			++i;
		}

		// alert the player
		m_pGUIMan->ShowOKDialog("Character Deleted", "The character '" + name + "' has been deleted.");
	}
}

string MenuGUI::GetPlayerName()
{
	return m_GUICharacters->GetSelectedText();
}

void MenuGUI::LoadServers()
{
	// read the servers from a file and place them in the server list
	std::ifstream file("serverlist");

	if (file.is_open())
	{
		// the file exists, so we have some saved servers (or previously saved servers)
		string line, name, address, port;
		int i = 0;
		while (std::getline(file, line, '\n'))
		{
			// store the details
			if (i == 0)
				name = line;
			else if (i == 1)
				address = line;
			else
				port = line;

			if (i == 2)
			{
				// add to the server list
				ServerDetails server;
				server.name = name;
				server.address = address;
				server.port = port;

				m_GUIServers->AddRow({ name, string("\\") + address + string(":") + port });
				m_Servers.push_back(server);
				i = -1;
			}

			++i;
		}

		// close the file
		file.close();
	}
}

void MenuGUI::AddServer(string name)
{
	// create a new server
	ServerDetails server;
	server.name = name;
	server.address = m_GUIIPAddress->GetText();
	server.port = m_GUIPortNumber->GetText();
	if (server.port.empty())
		server.port = "8076";

	// add this to the list
	m_GUIServers->AddRow({ name, string("\\") + server.address + string(":") + server.port });
	m_Servers.push_back(server);

	// save the server list
	SaveServerList();
}

void MenuGUI::RemoveServer()
{
	// remove the selected server from the list
	GLuint selected = m_GUIServers->GetSelectedRow();
	m_GUIServers->RemoveRow(selected);

	// find this detail in the server details and remove it
	vector<ServerDetails>::iterator it = m_Servers.begin();
	GLuint i = 0;
	while (it != m_Servers.end())
	{
		if (i == selected)
		{
			m_Servers.erase(it);
			break;
		}

		++i;
		++it;
	}

	// save the server list
	SaveServerList();
}

void MenuGUI::EditServer(string name, string address, string port)
{
	// edit the selected server with the given details
	GLuint selected = m_GUIServers->GetSelectedRow();
	m_GUIServers->ChangeRow(selected, { name, string("\\") + address + string(":") + port });

	// change the server details object
	m_Servers[selected].name = name;
	m_Servers[selected].address = address;
	m_Servers[selected].port = port;

	// save the server list
	SaveServerList();
}

void MenuGUI::SaveServerList()
{
	// now we are going to save to the serverlist file
	std::ofstream file("serverlist", std::ofstream::out);
	for (GLuint i = 0; i < m_Servers.size(); ++i) {
		file << m_Servers[i].name << "\n";
		file << m_Servers[i].address << "\n";
		file << m_Servers[i].port << "\n";
	}
	file.close();
}

void MenuGUI::StartNetworkManager(GUIElement* e)
{
	MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
	Game* g = e->GetManager()->GetRoot()->CastTo<Game>();
	if (!m || !g)
		return;

	Button* b = dynamic_cast<Button*>(e);
	if (b->GetText() == "Connect")
	{
		// check that we've got an ip address
		string ip = m->m_GUIIPAddress->GetText();
		if (ip.empty())
			return;
		
		// we are directly joining the game
		string port = m->m_GUIPortNumber->GetText();
		if (port.empty())
			port = "8076"; // default port
		g->CreateNetworkManager(ip, atoi(port.c_str()));
		//g->DeleteNetworkManager();
	}
	else
	{
		// check that there are actually any servers in the list
		if (m->m_Servers.empty())
			return;
		
		// we are joining a game saved in the server list
		GLuint selected = m->m_GUIServers->GetSelectedRow();
		g->CreateNetworkManager(m->m_Servers[selected].address, atoi(m->m_Servers[selected].port.c_str()));
	}

	m->GotoPage(5);
}

void MenuGUI::AddNewServer(GUIElement* e)
{
	MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
	if (!m || m->m_GUIIPAddress->GetText().empty())
		return;

	e->GetGUIManager()->ShowTextBoxDialog("Enter Server Name", "Server", [](GUIElement* e) {
		TextBoxDialog* tb = dynamic_cast<TextBoxDialog*>(e->GetRoot());
		if (tb)
		{
			MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
			TextBox* text = dynamic_cast<TextBox*>(tb->GetChild(0)->GetChild(1));
			if (text)
			{
				m->AddServer(text->GetText());
			}

			GUIElement::Close(tb);
		}
	});
}

void MenuGUI::StartEditServer(GUIElement* e)
{
	MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
	if (!m || m->m_Servers.empty())
		return;

	GLuint selected = m->m_GUIServers->GetSelectedRow();
	string name = m->m_Servers[selected].name;
	string address = m->m_Servers[selected].address;
	string port = m->m_Servers[selected].port;
	
	e->GetGUIManager()->ShowServerDetailsDialog("Edit Server Details", name, address, port, [](GUIElement* e) {
		ServerDetailsDialog* sd = dynamic_cast<ServerDetailsDialog*>(e->GetRoot());
		if (sd)
		{
			MenuGUI* m = e->GetManager()->GetRoot()->CastTo<Game>()->GetMenuGUI();
			TextBox* text1 = dynamic_cast<TextBox*>(sd->GetChild(0)->GetChild(1));
			TextBox* text2 = dynamic_cast<TextBox*>(sd->GetChild(0)->GetChild(2));
			TextBox* text3 = dynamic_cast<TextBox*>(sd->GetChild(0)->GetChild(3));
			if (text1 && text2 && text3)
			{
				m->EditServer(text1->GetText(), text2->GetText(), text3->GetText());
			}

			GUIElement::Close(sd);
		}
	});
}
