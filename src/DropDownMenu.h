#ifndef _DROP_DOWN_MENU_H_
#define _DROP_DOWN_MENU_H_

#include "GUIElement.h"
#include "ListBox.h"

class DropDownMenu : public GUIElement {
	public:
		DropDownMenu(Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size, GLfloat height = 80);

		virtual void Update(bool bForce = false);
		virtual bool IsHovered();
		virtual void MouseDownFunction(GLuint button);
		virtual void Release(GLuint button);

		void AddItem(string text, bool remake = true);
		string GetSelected();
		GLuint GetSelectedNumber();

		void ClearList();
		void SetListBoxFunction(void (*fn)(GUIElement*));
		void SetListBoxUpdateFunction(void(*fn)(GUIElement*));
		void SetDepth(GLfloat depth);

		bool ItemExists(string item);
		GLuint GetFirstItem(string item);

		void Select(GLuint i);

		static void DefaultFunction(GUIElement* e);
		static void ListBoxFunction(GUIElement* e);

	protected:

	private:
		ListBox*	m_ListBox;
		Text*		m_Selected;
		bool		m_bSwitched;
};

#endif