#ifndef _MOB_H_
#define _MOB_H_

#include "GLObject.h"
#include "Misc.h"

//----------------------------------------------------------------------------\\
// Mobs can be customised by the player through the editing of the XML file.  \\
// Each mob entry will have a number of things listed, varying depending on   \\
// what type of mob it is. This should allow for easy mob modding into the    \\
// game - simply copy an existing mob, tweak the values and the player will   \\
// be able to start finding the mob in game (based on spawn conditions).      \\
//----------------------------------------------------------------------------\\

enum MobAggression {
	friendly,
	neutral,
	hostile
};

enum MobCombatClass {
	melee,
	ranged
};

struct MobClass {
	string								strName;
	string								strTexturePath;
	Mesh*								pMesh;
	int									iHealth;
	short								sDamage;
	MobAggression						Aggression;
	vector3df							Scale;
	int									index;
	MobCombatClass						style;
	map<string, vector<AnimDetails>>	anims;

	MobClass()
	{
		strName = "Mob";
		strTexturePath = "";
		pMesh = NULL;
		iHealth = 0;
		sDamage = 0;
		Aggression = neutral;
		Scale.Set(1, 1, 1);
		index = -1;
		style = melee;
	}

	MobClass(MobClass* other)
	{
		strName = other->strName;
		strTexturePath = other->strTexturePath;
		pMesh = other->pMesh;
		iHealth = other->iHealth;
		sDamage = other->sDamage;
		Aggression = other->Aggression;
		Scale = other->Scale;
		index = other->index;
		style = other->style;

		map<string, vector<AnimDetails>>::iterator it = other->anims.begin();
		while (it != other->anims.end())
		{
			vector<AnimDetails> evtanims(it->second);
			anims.emplace(it->first, evtanims);

			++it;
		}
	}
};

class ProgressBar;
class Terrain;

class Mob : public GLObject {
	public:
		Mob(Manager* parent, string type, vector3df pos = vector3df());
		Mob(Manager* parent, int type, vector3df pos = vector3df());

		static bool	LoadMobs(string file, string type, SceneManager* pSceneMan);
		static void	UnloadMobs();
		static bool	ReloadMobs(string file, string type, SceneManager* pSceneMan);
		static const MobClass* GetMobClass(string name);
		static const MobClass* GetMobClass(int id);
		static bool MobsLoaded();

		MobData& GetMobData();
		void AttachHealthBar(ProgressBar* hpbar);

		string GetName() const;

		void MobSeen();

		void Update(bool bForce = false);

	protected:
		static std::map<string, MobClass*>	ListOfMobs;
		static std::vector<MobClass*>		IntListOfMobs;

	private:
		MobClass		m_MobClass;
		MobData			m_MobData;
		bool			m_bCreated;
		ProgressBar*	m_GUIHealthBar;
		Terrain*		m_pTerrain;

		Animator		m_Animation;
		string			m_CurrentEvtAnim;

		GLfloat			m_fTimeout;
		GLfloat			m_fServerTimeout;

		void ChooseAnimation(string evt, bool loop = true);
};

#endif
