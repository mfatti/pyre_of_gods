#ifndef _OK_DIALOG_H_
#define _OK_DIALOG_H_

#include "GUIElement.h"

class OKDialog : public GUIElement {
	public:
		OKDialog(Manager* parent, string title, string text, void(*fn)(GUIElement*) = NULL);

		virtual void MouseDownFunction(GLuint button);

		virtual void ScaleTo(vector2df scale, bool hide = false, bool kill = false);

	protected:

	private:
		
};

#endif