#ifndef _PROGRESS_BAR_H_
#define _PROGRESS_BAR_H_

#include "GUIElement.h"
#include "Text.h"

class ProgressBar : public GUIElement {
	public:
		ProgressBar(Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size);

		virtual void MouseDownFunction(GLuint button);
		virtual bool IsHovered();

		void SetCurrentPosition(GLfloat amount);
		void SetMaxValue(GLfloat value);
		void SetColours(GLuint pos, const GLColour& one, const GLColour& two);

	protected:

	private:
		GLfloat		m_CurrentPosition;
		GLfloat		m_Ratio;
		GLfloat		m_MaxValue;
		Text*		m_Text;
};

#endif
