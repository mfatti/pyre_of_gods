#include "ViewFrustum.h"

ViewFrustum::ViewFrustum()
{

}

void ViewFrustum::Set(matrix4f& matrix)
{
	// get the data
	const GLfloat *m = matrix.GetDataPointer();
	
	// left
	m_Planes[left_plane].normal.x = m[12] + m[0];
	m_Planes[left_plane].normal.y = m[13] + m[1];
	m_Planes[left_plane].normal.z = m[14] + m[2];
	m_Planes[left_plane].d = m[15] + m[3];

	// right
	m_Planes[right_plane].normal.x = m[12] - m[0];
	m_Planes[right_plane].normal.y = m[13] - m[1];
	m_Planes[right_plane].normal.z = m[14] - m[2];
	m_Planes[right_plane].d = m[15] - m[3];

	// top
	m_Planes[top_plane].normal.x = m[12] - m[4];
	m_Planes[top_plane].normal.y = m[13] - m[5];
	m_Planes[top_plane].normal.z = m[14] - m[6];
	m_Planes[top_plane].d = m[15] - m[7];

	// bottom
	m_Planes[bottom_plane].normal.x = m[12] + m[4];
	m_Planes[bottom_plane].normal.y = m[13] + m[5];
	m_Planes[bottom_plane].normal.z = m[14] + m[6];
	m_Planes[bottom_plane].d = m[15] + m[7];

	// far
	m_Planes[far_plane].normal.x = m[12] - m[8];
	m_Planes[far_plane].normal.y = m[13] - m[9];
	m_Planes[far_plane].normal.z = m[14] - m[10];
	m_Planes[far_plane].d = m[15] - m[11];

	// near
	m_Planes[near_plane].normal.x = m[8];
	m_Planes[near_plane].normal.y = m[9];
	m_Planes[near_plane].normal.z = m[10];
	m_Planes[near_plane].d = m[11];

	// normalise
	for (GLuint i = 0; i < plane_count; ++i) {
		const GLfloat l = -1.0f / m_Planes[i].normal.Magnitude();
		//m_Planes[i].normal.Normal();
		//m_Planes[i].normal.MinusOne();
		m_Planes[i].normal *= l;
		m_Planes[i].d *= l;
	}
}

vector3df ViewFrustum::FarLeftUp()
{
	vector3df p;

	m_Planes[far_plane].IntersectWithPlanes(m_Planes[top_plane], m_Planes[left_plane], p);

	return p;
}

vector3df ViewFrustum::FarLeftDown()
{
	vector3df p;

	m_Planes[far_plane].IntersectWithPlanes(m_Planes[bottom_plane], m_Planes[left_plane], p);

	return p;
}

vector3df ViewFrustum::FarRightUp()
{
	vector3df p;

	m_Planes[far_plane].IntersectWithPlanes(m_Planes[top_plane], m_Planes[right_plane], p);

	return p;
}

vector3df ViewFrustum::FarRightDown()
{
	vector3df p;

	m_Planes[far_plane].IntersectWithPlanes(m_Planes[bottom_plane], m_Planes[right_plane], p);

	return p;
}

bool ViewFrustum::PointInFrustum(const vector3df& p) const
{
	// returns true if the point is inside the frustum
	for (GLuint i = 0; i < plane_count; ++i) {
		if (m_Planes[i].PointInFront(p))
			return false;
	}

	// we are not outside so return true
	return true;
}

bool ViewFrustum::QuickPointInFrustum(const vector3df& p, GLfloat radius) const
{
	// this is called to check the centre of an AABB rather than all 8 corners (should be faster)
	// returns true if the point is inside the frustum
	for (GLuint i = 0; i < plane_count; ++i) {
		if (m_Planes[i].PointInFront(vector3df(m_Planes[i].normal) * -radius + p))
			return false;
	}

	// we are not outside so return true
	return true;
}