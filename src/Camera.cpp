#include "Camera.h"
#include "SceneManager.h"

Camera::Camera(SceneManager* parent)
	: m_pSceneMan(parent)
	, m_fDirection(0)
	, m_fDirCos(0)
	, m_fDirSin(0)
{
	
}

Camera::~Camera()
{

}

void Camera::LookAt(const vector3df& pos)
{
	m_Target = pos;
}

void Camera::Translate(const vector3df& pos)
{
	m_Position += pos;
}

void Camera::SetPosition(const vector3df& pos)
{
	m_Position = pos;
}

void Camera::SetRotation(const vector3df& rot)
{
	m_Rotation = rot;
}

void Camera::Update()
{
	// update the camera
	m_Matrix.Identity();

	vector3df zaxis = (m_Position - m_Target);
	zaxis.Normal();

	vector3df xaxis(0, 1, 0);
	xaxis.Cross(zaxis);
	xaxis.Normal();

	vector3df yaxis = zaxis;
	yaxis.Cross(xaxis);

	m_Matrix.Set(0, xaxis.x);
	m_Matrix.Set(1, xaxis.y);
	m_Matrix.Set(2, xaxis.z);
	m_Matrix.Set(3, -xaxis.Dot(m_Position));
	m_Matrix.Set(4, yaxis.x);
	m_Matrix.Set(5, yaxis.y);
	m_Matrix.Set(6, yaxis.z);
	m_Matrix.Set(7, -yaxis.Dot(m_Position));
	m_Matrix.Set(8, zaxis.x);
	m_Matrix.Set(9, zaxis.y);
	m_Matrix.Set(10, zaxis.z);
	m_Matrix.Set(11, -zaxis.Dot(m_Position));

	// rotate the camera by our given rotation vector
	matrix4f mat;
	mat.SetRotation(m_Rotation);

	m_Matrix = mat.Multiply(m_Matrix);

	m_RetMatrix.Set(m_Matrix);

	matrix4f m;
	m.MultiplicationByProduct(GLObject::GetShaderProgram(0)->GetProjectionMatrix(), m_Matrix);
	m_Frustum.Set(m);

	// update the sin and cos of the camera rotation
	m_fDirCos = cos(-m_fDirection * DEG_TO_RAD);
	m_fDirSin = sin(-m_fDirection * DEG_TO_RAD);
}

const GLfloat* Camera::GetCameraMatrix()
{
	return m_Matrix.GetDataPointer();
}

matrix4f& Camera::GetMatrix()
{
	return m_RetMatrix;
}

const vector3df& Camera::GetPosition()
{
	return m_Position;
}

const ViewFrustum& Camera::GetViewFrustum()
{
	return m_Frustum;
}

const GLfloat& Camera::GetDirection()
{
	return m_fDirection;
}

void Camera::SetDirection(GLfloat dir)
{
	m_fDirection = dir;
}

const GLfloat& Camera::GetDirectionCos()
{
	return m_fDirCos;
}

const GLfloat& Camera::GetDirectionSin()
{
	return m_fDirSin;
}

vector3df Camera::GetTarget() const
{
	return m_Target;
}