#include "InputManager.h"
#include "Game.h"

InputManager& InputManager::GetInstance()
{
	static InputManager instance;
	return instance;
}

void InputManager::MouseCallback(GLFWwindow* window, int button, int action, int mods)
{
	// if we don't have a Game object attached then just leave this function
	if (!GetInstance().m_pGame)
		return;
	
	// handle mouse input events
	if (button <= GLFW_MOUSE_BUTTON_RIGHT)
	{
		switch (action)
		{
			case GLFW_RELEASE:
				GetInstance().m_pGame->MouseRelease(button);
				break;
			case GLFW_PRESS:
				GetInstance().m_pGame->MouseDown(button);
				break;
		}
	}
}

void InputManager::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	// if we don't have a Game object attached then just leave this function
	if (!GetInstance().m_pGame)
		return;

	// handle key input events
	switch (action)
	{
		case GLFW_RELEASE:
			GetInstance().m_pGame->KeyRelease(key);
			break;
		case GLFW_PRESS:
			GetInstance().m_pGame->KeyDown(key);
			break;
	}
}

void InputManager::CharCallback(GLFWwindow* window, unsigned int codepoint)
{
	// if we don't have a Game object attached then just leave this function
	if (!GetInstance().m_pGame)
		return;

	// handle char input events
	GetInstance().m_pGame->CharCallback(codepoint);
}

void InputManager::ScrollCallback(GLFWwindow* window, double x, double y)
{
	// if we don't have a Game object attached then just leave this function
	if (!GetInstance().m_pGame)
		return;

	// handle scroll input events
	GetInstance().m_pGame->ScrollCallback(x, y);
}

void InputManager::AssignToGame(Game* game)
{
	m_pGame = game;
}
