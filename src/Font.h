#ifndef _FONT_H_
#define _FONT_H_

#include "Misc.h"
#include <map>

struct CharacterDetail {
	short charWidth;
	short charX;
	short charY;
};

class Font {
	public:
		Font(string fileName, TextureManager* texMan);
		~Font();
		
		Texture GetTexture();
		const CharacterDetail& GetChar(const char c);
		GLuint GetStringWidth(string text);
		const vector3ds& GetDetails();
	
	protected:
	
	private:
		map<const char, CharacterDetail>	chars;
		Texture								texture;
		vector3ds							details;
};

#endif
