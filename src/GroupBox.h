#ifndef _GROUP_BOX_H_
#define _GROUP_BOX_H_

#include "GUIElement.h"

class GroupBox : public GUIElement {
	public:
		GroupBox(Manager* parent, GUIElement* guiParent, vector2df pos, vector3di size, string label);

		virtual void MouseDownFunction(GLuint button);

	protected:

	private:

};

#endif