#include "ParticleEmitter.h"
#include <random>

ParticleEmitter::ParticleEmitter(Manager* parent, vector3df pos, vector3df dir, GLfloat speed, GLuint amount, vector2df psize, GLfloat spread, GLfloat alpha, vector2df size)
	: Billboard(parent, size)
	, m_Direction(dir)
	, m_fSpeed(speed)
	, m_fTimer(0)
	, m_StartPos(pos)
	, m_fSpread(spread)
	, m_bNeedsCreation(true)
	, m_TextureToLoad("")
{
	m_Direction.Normal();

	for (GLuint i = 0; i < amount; ++i) {
		Board b(pos, psize, 0, 6 + (rand() % 120) * 0.1f, m_fSpeed);
		GLint s = (GLint)(m_fSpread * 4);
		b.pos.Set(m_StartPos + vector3df(m_fSpread - (rand() % s) * 0.5f, m_fSpread - (rand() % s) * 0.5f, m_fSpread - (rand() % s) * 0.5f));

		AddBoard(b, false);
		ChangeQuadColour(i, GLColour(255, 255 * alpha), false);
	}
	m_Direction = dir;
	//CreateBufferObjects();
}

ParticleEmitter::ParticleEmitter(Manager* parent, ParticleDefinition p)
	: ParticleEmitter(parent, p.pos, p.dir, p.speed, p.amount, p.psize, p.spread, p.alpha, p.size)
{
	//ParticleEmitter(parent, p.pos, p.dir, p.speed, p.amount, p.psize, p.spread, p.size);
}

void ParticleEmitter::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible())
		return;

	// create us if needs be
	if (m_bNeedsCreation)
	{
		CreateBufferObjects();
		SetTexture(m_TextureToLoad.c_str());
		m_bNeedsCreation = false;
	}

	// get delta time
	double delta = DeltaTime::GetDelta();

	if (m_Direction != vector3df())
	{
		for (GLuint i = 0; i < m_Boards.size(); ++i) {
			// update the billboard position
			Board *b = &m_Boards[i];
			if (b->dead)
				continue;

			b->pos += m_Direction * b->speed * delta;
		}
	}

	Billboard::Update(bForce);
}

void ParticleEmitter::OnAnimationFinish(GLuint pos)
{
	//ChangeQuadUV(pos, vector2df(), vector2df());
	//m_Boards[pos].dead = true;
	Board *b = &m_Boards[pos];
	GLint s = (GLint)(m_fSpread * 4);
	b->speed = m_fSpeed + (rand() % 200) * 0.001f;
	b->pos.Set(m_StartPos + vector3df(m_fSpread - (rand() % s) * 0.5f, m_fSpread - (rand() % s) * 0.5f, m_fSpread - (rand() % s) * 0.5f));
}

void ParticleEmitter::SetEmitterPosition(vector3df pos)
{
	m_StartPos = pos;
}

void ParticleEmitter::TranslateEmitterPosition(vector3df pos)
{
	m_StartPos += pos;
}

void ParticleEmitter::SetSpread(GLfloat spread)
{
	m_fSpread = spread;
}

void ParticleEmitter::SetTextureToLoad(string tex)
{
	m_TextureToLoad = tex;
}

/*Board* ParticleEmitter::GetFreeParticle()
{
	for (GLuint i = 0; i < m_Boards.size(); i++) {
		if (m_Boards[i].dead)
			return &m_Boards[i];
	}

	return NULL;
}*/