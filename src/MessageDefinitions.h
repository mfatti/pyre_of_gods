#ifndef _MESSAGE_DEFINITIONS_H_
#define _MESSAGE_DEFINITIONS_H_

// this header file contains structs used for messages sent over the network
// this was done as it is easier to use and cleaner than setting fields individually
// it also makes it easy to add / remove fields from messages
#include "Vector.h"

template<typename T> static unsigned char* ToBuffer(T& data) { return reinterpret_cast<unsigned char*>(&data); }
template<typename T> static T& FromBuffer(unsigned char* buffer) { return *reinterpret_cast<T*>(buffer); }

struct PlayerData {
	vector3df pos;
	vector2df rot;
	char swing;
	char selected;
	int sprinting : 1;

	int padding : 7;
};

struct MobData {
	int id;
	int index;
	vector3df pos;
	int health;
	int maxhealth;
	float rot;
	unsigned char flags;
};

struct ProjectileData {
	unsigned int index;
	vector3df position;
	vector2df direction;
	float speed;
	char gravity;
};

struct ItemData {
	unsigned int index;
	short amount;
	vector3df pos;

	bool operator==(const ItemData& other) { return (index == other.index && amount == other.amount && pos == other.pos); }
};

// typedefs for buffer functions
// to buffer
auto static const MobDataToBuffer = ToBuffer<MobData>;
auto static const ProjectileDataToBuffer = ToBuffer<ProjectileData>;
auto static const PlayerDataToBuffer = ToBuffer<PlayerData>;
auto static const ItemDataToBuffer = ToBuffer<ItemData>;

// from buffer
auto static const MobDataFromBuffer = FromBuffer<MobData>;
auto static const ProjectileDataFromBuffer = FromBuffer<ProjectileData>;
auto static const PlayerDataFromBuffer = FromBuffer<PlayerData>;
auto static const ItemDataFromBuffer = FromBuffer<ItemData>;

#endif