#include "ScrollPane.h"
#include "GUIManager.h"

ScrollPane::ScrollPane(Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size, GLubyte mode)
	: GUIElement(parent, guiParent, pos, size)
{
	// create the quads
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(0, 100));
	CreateBufferObjects();

	// set the render boundary
	SetRenderBoundary(vector4df(0, 0, (GLfloat)size.x, (GLfloat)size.y));

	// create the scrollbar
	m_Scrollbar = GetGUIManager()->CreateScrollbar(this, pos, size, mode, guiParent);
	m_Scrollbar->AddWidth((GLfloat)size.x);
	m_Scrollbar->AddHeight((GLfloat)size.y);
}

bool ScrollPane::IsHovered()
{
	if (!IsVisible())
	{
		return false;
	}

	if (!GetGUIManager())
	{
		return false;
	}

	vector4df b = m_RenderBoundary;
	return IsMouseInRect(GetGUIManager()->GetMousePosition(), vector2df(b.x, b.y), vector2df(b.z, b.w));
}

void ScrollPane::MouseDownFunction(GLuint button)
{
	// do nothing
}

void ScrollPane::AddChild(GUIElement* child)
{
	// call guielement::addchild
	GUIElement::AddChild(child);

	// increase size of area
	vector4df b = m_RenderBoundary;
	vector3df pos = GetAbsolutePosition();
	b.x -= pos.x; b.y -= pos.y;
	b.z -= pos.x; b.w -= pos.y;
	vector2di childSize = child->GetSize();
	vector2df childPos = child->GetPosition();

	if (childPos.x + childSize.x > b.z)
	{
		m_Scrollbar->AddWidth(childPos.x + childSize.x - b.z);
		ChangeQuadCoords(0, vector2df(), vector2df(childPos.x + childSize.x, 0));
	}
	if (childPos.y + childSize.y > b.w)
	{
		m_Scrollbar->AddHeight(childPos.y + childSize.y - b.w);
		ChangeQuadCoords(0, vector2df(), vector2df(0, childPos.y + childSize.y));
	}
}

void ScrollPane::ClearPane()
{
	// kill all the children
	KillChildren();

	// reset scrollbar
	m_Scrollbar->Reset();
}