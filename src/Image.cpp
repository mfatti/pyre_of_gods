#include "Image.h"
#include "GUIManager.h"

Image::Image(string texture, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size, GLColour col)
	: GUIElement(parent, guiParent, pos, size)
	, m_bCentred(false)
	, m_bHoverable(false)
{
	// set the texture
	SetTexture(texture.c_str());

	// add a quad
	AddQuad(vector2df(), vector2df(), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(1, 1), col);
	CreateBufferObjects();
}

void Image::MouseDownFunction(GLuint button)
{
	// do nothing
}

bool Image::IsHovered()
{
	if (!m_bHoverable)
		return false;
	
	if (!m_bCentred)
		return GUIElement::IsHovered();
	
	if (!IsVisible())
		return false;

	if (!GetGUIManager())
		return false;

	if (!IsEnabled())
		return false;

	vector3df pos = GetAbsolutePosition();
	vector3di size = GetSize();
	pos -= vector3df(size.x * 0.5f, size.y * 0.5f, 0);
	return IsMouseInRect(GetGUIManager()->GetMousePosition(), pos, vector2df(pos.x + size.x, pos.y + size.y));
}

void Image::SetUV(vector2df one, vector2df two)
{
	ChangeQuadUV(0, one, two);
}

void Image::Centre()
{
	vector2di size = GetSize();
	ChangeQuadCoords(0, vector2df(-size.x * 0.5f, -size.y * 0.5f), vector2df(size.x * 0.5f, size.y * 0.5f));
	m_bCentred = true;
}

void Image::SetHoverable(bool bHoverable)
{
	m_bHoverable = bHoverable;
}