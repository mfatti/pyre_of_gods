#ifndef _PARTICLE_EMITTER_H_
#define _PARTICLE_EMITTER_H_

#include "Billboard.h"

class ParticleEmitter : public Billboard {
	public:
		ParticleEmitter(Manager* parent, vector3df pos, vector3df dir, GLfloat speed, GLuint amount = 50, vector2df psize = vector2df(5), GLfloat spread = 3, GLfloat alpha = 0.6f, vector2df size = vector2df(64));
		ParticleEmitter(Manager* parent, ParticleDefinition p);

		virtual void Update(bool bForce = false);
		virtual void OnAnimationFinish(GLuint pos);

		void SetEmitterPosition(vector3df pos);
		void TranslateEmitterPosition(vector3df pos);

		void SetSpread(GLfloat spread);

		void SetTextureToLoad(string tex);

	protected:

	private:
		vector3df	m_Direction;
		vector3df	m_StartPos;
		GLfloat		m_fSpeed;
		GLfloat		m_fTimer;
		bool		m_bNeedsCreation;
		string		m_TextureToLoad;

		GLfloat		m_fSpread;

		//Board* GetFreeParticle();
};

#endif