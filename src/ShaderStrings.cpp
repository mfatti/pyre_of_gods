#include "ShaderStrings.h"

ShaderStrings::ShaderStrings()
{
	// plain shader - no effects added, per-vertex lighting
	m_VertPlainShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying float diffuse;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"\n"
		"    diffuse = max(min(dot(n, -lightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"}";

	m_FragPlainShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying float diffuse;\n"
		"uniform sampler2D texImage;\n"
		"uniform vec3 lightAmount;\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    vec3 _lighting = (ambient + vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// per-pixel lighting shader
	m_VertPerPixelShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform float sun;\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"\n"
		"}";

	m_FragPerPixelShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"uniform sampler2D texImage;\n"
		"uniform vec3 lightDir;\n"
		"uniform vec3 lightAmount;\n"
		"uniform float sun;\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    float diffuse = max(min(dot(n, -lightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"    vec3 _lighting = (ambient + vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// normal mapped shader
	m_VertNormalMappedShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"attribute vec4 tangent;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 tLightDir;\n"
		"varying vec4 l;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    vec3 n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"\n"
		"    vec3 t = normalize(tangent.xyz);\n"
		"    vec3 T = normalize(normalMatrix * t);\n"
		"    vec3 N = normalize(normalMatrix * n);\n"
		"    vec3 B = cross(T, N);\n"
		"    mat3 TBN = transpose(mat3(T, B, N));\n"
		"    tLightDir = TBN * lightDir;\n"
		"}";

	m_FragNormalMappedShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 tLightDir;\n"
		"varying vec4 l;\n"
		"uniform sampler2D texImage;\n"
		"uniform sampler2D normalMap;\n"
		"uniform vec3 lightAmount;\n"
		"uniform float sun;\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec3 normal = texture(normalMap, textureCoord).rgb;\n"
		"    normal = normalize(normal * 2.0 - 1.0);\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    float diffuse = max(min(dot(normal, -tLightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"    vec3 _lighting = (ambient + vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// normal mapped with shadows shader
	m_VertNormalMappedShadowsShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"attribute vec4 tangent;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 tLightDir;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform mat4 lightSpace;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    vec3 n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"    lightSpacePos = lightSpace * (model * vec4(_pos.xyz + (n * 0.4), 1.0));\n"
		"\n"
		"    vec3 t = normalize(tangent.xyz);\n"
		"    vec3 T = normalize(normalMatrix * t);\n"
		"    vec3 N = normalize(normalMatrix * n);\n"
		"    vec3 B = cross(T, N);\n"
		"    mat3 TBN = transpose(mat3(T, B, N));\n"
		"    tLightDir = TBN * lightDir;\n"
		"}";

	m_FragNormalMappedShadowsShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 tLightDir;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"uniform sampler2D texImage;\n"
		"uniform sampler2D shadowMap;\n"
		"uniform sampler2D normalMap;\n"
		"uniform vec3 lightAmount;\n"
		"uniform float sun;\n"
		"float ShadowCalc(vec4 pos) {\n"
		"    vec3 projCoords = pos.xyz;\n"
		"    projCoords = projCoords * 0.5 + 0.5;\n"
		"    float current = projCoords.z;\n"
		"    float shadow = 0;\n"
		"    if (current <= 1.0) {\n"
		"        vec2 texelSize = (1.0 / textureSize(shadowMap, 0)) * 0.5;\n"
		"        float bias = 0.001;\n"
		"        for (int i = -1; i <= 1; i++) {\n"
		"            for (int j = -1; j <= 1; j++) {\n"
		"                float depth = texture2D(shadowMap, projCoords.xy + vec2(i, j) * texelSize).r;\n"
		"                shadow += current - bias > depth ? 1.0 : 0.0;\n"
		"            }"
		"        }"
		"        shadow /= 9.0;\n"
		"    }"
		"    return shadow;\n"
		"}\n"
		"\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec3 normal = texture(normalMap, textureCoord).rgb;\n"
		"    normal = normalize(normal * 2.0 - 1.0);\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    float diffuse = max(min(dot(normal, -tLightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"    float shadow = ShadowCalc(lightSpacePos);\n"
		"    vec3 _lighting = (ambient + (1.0 - shadow) * vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// per-pixel lighting with shadows shader
	m_VertPerPixelShadowsShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"attribute vec4 tangent;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform mat4 lightSpace;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"    lightSpacePos = lightSpace * (model * vec4(_pos.xyz + (n * 0.4), 1.0));\n"
		"}";

	m_FragPerPixelShadowsShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"uniform sampler2D texImage;\n"
		"uniform sampler2D shadowMap;\n"
		"uniform vec3 lightDir;\n"
		"uniform vec3 lightAmount;\n"
		"uniform float sun;\n"
		"float ShadowCalc(vec4 pos) {\n"
		"    vec3 projCoords = pos.xyz;\n"
		"    projCoords = projCoords * 0.5 + 0.5;\n"
		"    float current = projCoords.z;\n"
		"    float shadow = 0;\n"
		"    if (current <= 1.0) {\n"
		"        vec2 texelSize = 1.0 / textureSize(shadowMap, 0);\n"
		"        float bias = 0.001;\n"
		"        for (int i = -1; i <= 1; i++) {\n"
		"            for (int j = -1; j <= 1; j++) {\n"
		"                float depth = texture2D(shadowMap, projCoords.xy + vec2(i, j) * texelSize).r;\n"
		"                shadow += current - bias > depth ? 1.0 : 0.0;\n"
		"            }"
		"        }"
		"        shadow /= 9.0;\n"
		"    }"
		"    return shadow;\n"
		"}\n"
		"\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    float diffuse = max(min(dot(n, -lightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"    float shadow = ShadowCalc(lightSpacePos);\n"
		"    vec3 _lighting = (ambient + (1.0 - shadow) * vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// shadows shader
	m_VertShadowShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"attribute vec4 tangent;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"varying float diffuse;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform mat4 lightSpace;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"    lightSpacePos = lightSpace * (model * vec4(_pos.xyz + (n * 0.4), 1.0));\n"
		"\n"
		"    diffuse = max(min(dot(n, -lightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"}";

	m_FragShadowShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"varying float diffuse;\n"
		"uniform sampler2D texImage;\n"
		"uniform sampler2D shadowMap;\n"
		"uniform vec3 lightDir;\n"
		"uniform vec3 lightAmount;\n"
		"uniform float sun;\n"
		"float ShadowCalc(vec4 pos) {\n"
		"    vec3 projCoords = pos.xyz;\n"
		"    projCoords = projCoords * 0.5 + 0.5;\n"
		"    float current = projCoords.z;\n"
		"    float shadow = 0;\n"
		"    if (current <= 1.0) {\n"
		"        vec2 texelSize = 1.0 / textureSize(shadowMap, 0);\n"
		"        float bias = 0.001;\n"
		"        for (int i = -1; i <= 1; i++) {\n"
		"            for (int j = -1; j <= 1; j++) {\n"
		"                float depth = texture2D(shadowMap, projCoords.xy + vec2(i, j) * texelSize).r;\n"
		"                shadow += current - bias > depth ? 1.0 : 0.0;\n"
		"            }"
		"        }"
		"        shadow /= 9.0;\n"
		"    }"
		"    return shadow;\n"
		"}\n"
		"\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    float shadow = ShadowCalc(lightSpacePos);\n"
		"    vec3 _lighting = (ambient + (1.0 - shadow) * vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// now the same but for animated shaders, so with the addition of bones/skeleton and vertex information
	// plain shader - no effects added, per-vertex lighting
	m_AnimVertPlainShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"attribute vec4 bones;\n"
		"attribute vec4 weights;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying float diffuse;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"uniform mat4 skeleton[64];\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    vec4 tempNormal = vec4(n, 1.0);\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    if (bones.x > -1) { _pos = (skeleton[int(bones.x)] * vec4(position.xyz, 1.0)) * weights.x; n = (transpose(inverse(skeleton[int(bones.x)])) * tempNormal).xyz * weights.x; }\n"
		"    if (bones.y > -1) { _pos += (skeleton[int(bones.y)] * vec4(position.xyz, 1.0)) * weights.y; n += (transpose(inverse(skeleton[int(bones.y)])) * tempNormal).xyz * weights.y; }\n"
		"    if (bones.z > -1) { _pos += (skeleton[int(bones.z)] * vec4(position.xyz, 1.0)) * weights.z; n += (transpose(inverse(skeleton[int(bones.z)])) * tempNormal).xyz * weights.z; }\n"
		"    if (bones.w > -1) { _pos += (skeleton[int(bones.w)] * vec4(position.xyz, 1.0)) * weights.w; n += (transpose(inverse(skeleton[int(bones.w)])) * tempNormal).xyz * weights.w; }\n"
		"    n = normalize(n);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"\n"
		"    diffuse = max(min(dot(n, -lightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"}";

	m_AnimFragPlainShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying float diffuse;\n"
		"uniform sampler2D texImage;\n"
		"uniform vec3 lightAmount;\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    vec3 _lighting = (ambient + vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// per-pixel lighting shader
	m_AnimVertPerPixelShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"attribute vec4 bones;\n"
		"attribute vec4 weights;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform float sun;\n"
		"uniform mat4 skeleton[64];\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    vec4 tempNormal = vec4(n, 1.0);\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    if (bones.x > -1) { _pos = (skeleton[int(bones.x)] * vec4(position.xyz, 1.0)) * weights.x; n = (transpose(inverse(skeleton[int(bones.x)])) * tempNormal).xyz * weights.x; }\n"
		"    if (bones.y > -1) { _pos += (skeleton[int(bones.y)] * vec4(position.xyz, 1.0)) * weights.y; n += (transpose(inverse(skeleton[int(bones.y)])) * tempNormal).xyz * weights.y; }\n"
		"    if (bones.z > -1) { _pos += (skeleton[int(bones.z)] * vec4(position.xyz, 1.0)) * weights.z; n += (transpose(inverse(skeleton[int(bones.z)])) * tempNormal).xyz * weights.z; }\n"
		"    if (bones.w > -1) { _pos += (skeleton[int(bones.w)] * vec4(position.xyz, 1.0)) * weights.w; n += (transpose(inverse(skeleton[int(bones.w)])) * tempNormal).xyz * weights.w; }\n"
		"    n = normalize(n);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"\n"
		"}";

	m_AnimFragPerPixelShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"uniform sampler2D texImage;\n"
		"uniform vec3 lightDir;\n"
		"uniform vec3 lightAmount;\n"
		"uniform float sun;\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    float diffuse = max(min(dot(n, -lightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"    vec3 _lighting = (ambient + vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// normal mapped shader
	m_AnimVertNormalMappedShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"attribute vec4 tangent;\n"
		"attribute vec4 bones;\n"
		"attribute vec4 weights;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 tLightDir;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"uniform mat4 skeleton[64];\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    vec4 tempNormal = vec4(n, 1.0);\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    if (bones.x > -1) { _pos = (skeleton[int(bones.x)] * vec4(position.xyz, 1.0)) * weights.x; n = (transpose(inverse(skeleton[int(bones.x)])) * tempNormal).xyz * weights.x; }\n"
		"    if (bones.y > -1) { _pos += (skeleton[int(bones.y)] * vec4(position.xyz, 1.0)) * weights.y; n += (transpose(inverse(skeleton[int(bones.y)])) * tempNormal).xyz * weights.y; }\n"
		"    if (bones.z > -1) { _pos += (skeleton[int(bones.z)] * vec4(position.xyz, 1.0)) * weights.z; n += (transpose(inverse(skeleton[int(bones.z)])) * tempNormal).xyz * weights.z; }\n"
		"    if (bones.w > -1) { _pos += (skeleton[int(bones.w)] * vec4(position.xyz, 1.0)) * weights.w; n += (transpose(inverse(skeleton[int(bones.w)])) * tempNormal).xyz * weights.w; }\n"
		"    n = normalize(n);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"\n"
		"    vec3 t = normalize(tangent.xyz);\n"
		"    vec3 T = normalize(normalMatrix * t);\n"
		"    vec3 N = normalize(normalMatrix * n);\n"
		"    vec3 B = cross(T, N);\n"
		"    mat3 TBN = transpose(mat3(T, B, N));\n"
		"    tLightDir = TBN * lightDir;\n"
		"}";

	m_AnimFragNormalMappedShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 tLightDir;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"uniform sampler2D texImage;\n"
		"uniform sampler2D normalMap;\n"
		"uniform vec3 lightAmount;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec3 normal = texture(normalMap, textureCoord).rgb;\n"
		"    normal = normalize(normal * 2.0 - 1.0);\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    float diffuse = max(min(dot(n, -lightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"    vec3 _lighting = (ambient + vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// normal mapped with shadows shader
	m_AnimVertNormalMappedShadowsShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"attribute vec4 tangent;\n"
		"attribute vec4 bones;\n"
		"attribute vec4 weights;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 tLightDir;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform mat4 lightSpace;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"uniform mat4 skeleton[64];\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    vec4 tempNormal = vec4(n, 1.0);\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    if (bones.x > -1) { _pos = (skeleton[int(bones.x)] * vec4(position.xyz, 1.0)) * weights.x; n = (transpose(inverse(skeleton[int(bones.x)])) * tempNormal).xyz * weights.x; }\n"
		"    if (bones.y > -1) { _pos += (skeleton[int(bones.y)] * vec4(position.xyz, 1.0)) * weights.y; n += (transpose(inverse(skeleton[int(bones.y)])) * tempNormal).xyz * weights.y; }\n"
		"    if (bones.z > -1) { _pos += (skeleton[int(bones.z)] * vec4(position.xyz, 1.0)) * weights.z; n += (transpose(inverse(skeleton[int(bones.z)])) * tempNormal).xyz * weights.z; }\n"
		"    if (bones.w > -1) { _pos += (skeleton[int(bones.w)] * vec4(position.xyz, 1.0)) * weights.w; n += (transpose(inverse(skeleton[int(bones.w)])) * tempNormal).xyz * weights.w; }\n"
		"    n = normalize(n);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"    lightSpacePos = lightSpace * (model * vec4(_pos.xyz + (n * 0.4), 1.0));\n"
		"\n"
		"    vec3 t = normalize(tangent.xyz);\n"
		"    vec3 T = normalize(normalMatrix * t);\n"
		"    vec3 N = normalize(normalMatrix * n);\n"
		"    vec3 B = cross(T, N);\n"
		"    mat3 TBN = transpose(mat3(T, B, N));\n"
		"    tLightDir = TBN * lightDir;\n"
		"}";

	m_AnimFragNormalMappedShadowsShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 tLightDir;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"uniform sampler2D texImage;\n"
		"uniform sampler2D shadowMap;\n"
		"uniform sampler2D normalMap;\n"
		"uniform vec3 lightAmount;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"float ShadowCalc(vec4 pos) {\n"
		"    vec3 projCoords = pos.xyz;\n"
		"    projCoords = projCoords * 0.5 + 0.5;\n"
		"    float current = projCoords.z;\n"
		"    float shadow = 0;\n"
		"    if (current <= 1.0) {\n"
		"        vec2 texelSize = 1.0 / textureSize(shadowMap, 0);\n"
		"        float bias = 0.001;\n"
		"        for (int i = -1; i <= 1; i++) {\n"
		"            for (int j = -1; j <= 1; j++) {\n"
		"                float depth = texture2D(shadowMap, projCoords.xy + vec2(i, j) * texelSize).r;\n"
		"                shadow += current - bias > depth ? 1.0 : 0.0;\n"
		"            }"
		"        }"
		"        shadow /= 9.0;\n"
		"    }"
		"    return shadow;\n"
		"}\n"
		"\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec3 normal = texture(normalMap, textureCoord).rgb;\n"
		"    normal = normalize(normal * 2.0 - 1.0);\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    float diffuse = max(min(dot(n, -lightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"    float shadow = ShadowCalc(lightSpacePos);\n"
		"    vec3 _lighting = (ambient + (1.0 - shadow) * vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// per-pixel lighting with shadows shader
	m_AnimVertPerPixelShadowsShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"attribute vec4 tangent;\n"
		"attribute vec4 bones;\n"
		"attribute vec4 weights;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform mat4 lightSpace;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"uniform mat4 skeleton[64];\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    vec4 tempNormal = vec4(n, 1.0);\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    if (bones.x > -1) { _pos = (skeleton[int(bones.x)] * vec4(position.xyz, 1.0)) * weights.x; n = (transpose(inverse(skeleton[int(bones.x)])) * tempNormal).xyz * weights.x; }\n"
		"    if (bones.y > -1) { _pos += (skeleton[int(bones.y)] * vec4(position.xyz, 1.0)) * weights.y; n += (transpose(inverse(skeleton[int(bones.y)])) * tempNormal).xyz * weights.y; }\n"
		"    if (bones.z > -1) { _pos += (skeleton[int(bones.z)] * vec4(position.xyz, 1.0)) * weights.z; n += (transpose(inverse(skeleton[int(bones.z)])) * tempNormal).xyz * weights.z; }\n"
		"    if (bones.w > -1) { _pos += (skeleton[int(bones.w)] * vec4(position.xyz, 1.0)) * weights.w; n += (transpose(inverse(skeleton[int(bones.w)])) * tempNormal).xyz * weights.w; }\n"
		"    n = normalize(n);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"    lightSpacePos = lightSpace * (model * vec4(_pos.xyz + (n * 0.4), 1.0));\n"
		"}";

	m_AnimFragPerPixelShadowsShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"uniform sampler2D texImage;\n"
		"uniform sampler2D shadowMap;\n"
		"uniform vec3 lightDir;\n"
		"uniform vec3 lightAmount;\n"
		"uniform float sun;\n"
		"float ShadowCalc(vec4 pos) {\n"
		"    vec3 projCoords = pos.xyz;\n"
		"    projCoords = projCoords * 0.5 + 0.5;\n"
		"    float current = projCoords.z;\n"
		"    float shadow = 0;\n"
		"    if (current <= 1.0) {\n"
		"        vec2 texelSize = 1.0 / textureSize(shadowMap, 0);\n"
		"        float bias = 0.001;\n"
		"        for (int i = -1; i <= 1; i++) {\n"
		"            for (int j = -1; j <= 1; j++) {\n"
		"                float depth = texture2D(shadowMap, projCoords.xy + vec2(i, j) * texelSize).r;\n"
		"                shadow += current - bias > depth ? 1.0 : 0.0;\n"
		"            }"
		"        }"
		"        shadow /= 9.0;\n"
		"    }"
		"    return shadow;\n"
		"}\n"
		"\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    float diffuse = max(min(dot(n, -lightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"    float shadow = ShadowCalc(lightSpacePos);\n"
		"    vec3 _lighting = (ambient + (1.0 - shadow) * vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";

	// shadows shader
	m_AnimVertShadowShader =
		"#version 140\n"
		"attribute vec3 position;\n"
		"attribute vec4 colour;\n"
		"attribute vec2 texcoord;\n"
		"attribute vec4 normal;\n"
		"attribute vec4 light;\n"
		"attribute vec4 tangent;\n"
		"attribute vec4 bones;\n"
		"attribute vec4 weights;\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"varying float diffuse;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"uniform mat4 lightSpace;\n"
		"uniform float sun;\n"
		"uniform vec3 lightDir;\n"
		"uniform mat4 skeleton[64];\n"
		"void main() {\n"
		"    vertexColour = colour;\n"
		"    textureCoord = vec2(texcoord.x / 8192, texcoord.y / 8192);\n"
		"    n = normalize(normal.xyz);\n"
		"    mat3 normalMatrix = transpose(inverse(mat3(model)));\n"
		"    n = normalMatrix * n;\n"
		"    vec4 tempNormal = vec4(n, 1.0);\n"
		"    l = light;\n"
		"    l.a *= sun;\n"
		"\n"
		"    vec4 _pos = vec4(position.xyz, 1.0);\n"
		"    if (bones.x > -1) { _pos = (skeleton[int(bones.x)] * vec4(position.xyz, 1.0)) * weights.x; n = (transpose(inverse(skeleton[int(bones.x)])) * tempNormal).xyz * weights.x; }\n"
		"    if (bones.y > -1) { _pos += (skeleton[int(bones.y)] * vec4(position.xyz, 1.0)) * weights.y; n += (transpose(inverse(skeleton[int(bones.y)])) * tempNormal).xyz * weights.y; }\n"
		"    if (bones.z > -1) { _pos += (skeleton[int(bones.z)] * vec4(position.xyz, 1.0)) * weights.z; n += (transpose(inverse(skeleton[int(bones.z)])) * tempNormal).xyz * weights.z; }\n"
		"    if (bones.w > -1) { _pos += (skeleton[int(bones.w)] * vec4(position.xyz, 1.0)) * weights.w; n += (transpose(inverse(skeleton[int(bones.w)])) * tempNormal).xyz * weights.w; }\n"
		"    n = normalize(n);\n"
		"    vec4 pos = model * vec4(_pos.xyz, 1.0);\n"
		"    gl_Position = projection * view * pos;\n"
		"    lightSpacePos = lightSpace * (model * vec4(_pos.xyz + (n * 0.4), 1.0));\n"
		"\n"
		"    diffuse = max(min(dot(n, -lightDir), 1.0), 0.0);\n"
		"    diffuse = diffuse * max(min(sun, 0.6), 0.05) * l.a;\n"
		"}";

	m_AnimFragShadowShader =
		"#version 140\n"
		"varying vec4 vertexColour;\n"
		"varying vec2 textureCoord;\n"
		"varying vec3 n;\n"
		"varying vec4 l;\n"
		"varying vec4 lightSpacePos;\n"
		"varying float diffuse;\n"
		"uniform sampler2D texImage;\n"
		"uniform sampler2D shadowMap;\n"
		"uniform vec3 lightDir;\n"
		"uniform vec3 lightAmount;\n"
		"uniform float sun;\n"
		"float ShadowCalc(vec4 pos) {\n"
		"    vec3 projCoords = pos.xyz;\n"
		"    projCoords = projCoords * 0.5 + 0.5;\n"
		"    float current = projCoords.z;\n"
		"    float shadow = 0;\n"
		"    if (current <= 1.0) {\n"
		"        vec2 texelSize = 1.0 / textureSize(shadowMap, 0);\n"
		"        float bias = 0.001;\n"
		"        for (int i = -1; i <= 1; i++) {\n"
		"            for (int j = -1; j <= 1; j++) {\n"
		"                float depth = texture2D(shadowMap, projCoords.xy + vec2(i, j) * texelSize).r;\n"
		"                shadow += current - bias > depth ? 1.0 : 0.0;\n"
		"            }"
		"        }"
		"        shadow /= 9.0;\n"
		"    }"
		"    return shadow;\n"
		"}\n"
		"\n"
		"void main() {\n"
		"    if (vertexColour.a < 0.05) discard;\n"
		"    vec4 textureColour = texture2D(texImage, textureCoord);\n"
		"    if (textureColour.a < 0.1) discard;\n"
		"    vec4 lighting = vec4(vertexColour.x * max(l.x * 1.5, l.a) * lightAmount.x, vertexColour.y * max(l.y * 1.5, l.a) * lightAmount.y, vertexColour.z * max(l.z * 1.5, l.a) * lightAmount.z, vertexColour.a);\n"
		"    vec3 ambient = lighting.xyz * 0.4;\n"
		"    float shadow = ShadowCalc(lightSpacePos);\n"
		"    vec3 _lighting = (ambient + (1.0 - shadow) * vec3(diffuse)) * textureColour.xyz * vertexColour.xyz;\n"
		"    gl_FragColor = vec4(_lighting, vertexColour.a * textureColour.a);\n"
		"}";
}

ShaderStrings& ShaderStrings::Instance()
{
	static ShaderStrings instance;

	return instance;
}

const GLchar* ShaderStrings::GetVertexShader(bool bAnimated, bool bPerPixel, bool bNormalMapping, bool bShadows)
{
	// get the shader based off of the given parameters
	if (bAnimated)
	{
		if (bPerPixel)
		{
			if (bNormalMapping)
			{
				if (bShadows)
				{
					return Instance().m_AnimVertNormalMappedShadowsShader;
				}
				else
				{
					return Instance().m_AnimVertNormalMappedShader;
				}
			}
			else if (bShadows)
			{
				return Instance().m_AnimVertPerPixelShadowsShader;
			}
			else
			{
				return Instance().m_AnimVertPerPixelShader;
			}
		}
		else
		{
			if (bShadows)
			{
				return Instance().m_AnimVertShadowShader;
			}
			else
			{
				return Instance().m_AnimVertPlainShader;
			}
		}
	}
	else
	{
		if (bPerPixel)
		{
			if (bNormalMapping)
			{
				if (bShadows)
				{
					return Instance().m_VertNormalMappedShadowsShader;
				}
				else
				{
					return Instance().m_VertNormalMappedShader;
				}
			}
			else if (bShadows)
			{
				return Instance().m_VertPerPixelShadowsShader;
			}
			else
			{
				return Instance().m_VertPerPixelShader;
			}
		}
		else
		{
			if (bShadows)
			{
				return Instance().m_VertShadowShader;
			}
			else
			{
				return Instance().m_VertPlainShader;
			}
		}
	}
}

const GLchar* ShaderStrings::GetFragmentShader(bool bAnimated, bool bPerPixel, bool bNormalMapping, bool bShadows)
{
	// get the shader based off of the given parameters
	if (bAnimated)
	{
		if (bPerPixel)
		{
			if (bNormalMapping)
			{
				if (bShadows)
				{
					return Instance().m_AnimFragNormalMappedShadowsShader;
				}
				else
				{
					return Instance().m_AnimFragNormalMappedShader;
				}
			}
			else if (bShadows)
			{
				return Instance().m_AnimFragPerPixelShadowsShader;
			}
			else
			{
				return Instance().m_AnimFragPerPixelShader;
			}
		}
		else
		{
			if (bShadows)
			{
				return Instance().m_AnimFragShadowShader;
			}
			else
			{
				return Instance().m_AnimFragPlainShader;
			}
		}
	}
	else
	{
		if (bPerPixel)
		{
			if (bNormalMapping)
			{
				if (bShadows)
				{
					return Instance().m_FragNormalMappedShadowsShader;
				}
				else
				{
					return Instance().m_FragNormalMappedShader;
				}
			}
			else if (bShadows)
			{
				return Instance().m_FragPerPixelShadowsShader;
			}
			else
			{
				return Instance().m_FragPerPixelShader;
			}
		}
		else
		{
			if (bShadows)
			{
				return Instance().m_FragShadowShader;
			}
			else
			{
				return Instance().m_FragPlainShader;
			}
		}
	}
}
