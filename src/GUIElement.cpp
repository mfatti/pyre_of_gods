#include "GUIElement.h"
#include "GUIManager.h"
#include "SceneManager.h"
#include <algorithm>

GLuint GUIElement::m_SizeUni = 0;
GLuint GUIElement::m_AlphaUni = 0;

GUIElement::GUIElement(Manager* parent, GUIElement* guiParent, vector2df pos, vector3di size)
	: GLObject(parent, pos, size)
	, m_pGUIParent(guiParent)
	, m_bClickable(true)
	, m_Depth(0)
	, m_RenderBoundary(vector4df())
	, m_OriginalRenderBoundary(vector4df())
	, m_AnimationPosition(pos)
	, m_AnimationScale(vector3df(1))
	, m_bHideOnAnimationEnd(false)
	, m_bKillOnAnimationEnd(false)
	, m_bAnimating(false)
	, m_fAlpha(1)
	, m_fDestAlpha(1)
	, m_fSetAlpha(1)
	, m_bMenu(false)
	, m_bEnabled(true)
	, m_UpdateFunction(NULL)
	, m_Label(NULL)
	, m_bFollowMouse(false)
	, m_fAnimationSmooth(8.f)
	, m_bAnchored(false)
{
	if (m_pGUIParent)
	{
		m_Offset = pos;
	}

	for (GLuint i = 0; i < mouse_fns; i++) {
		m_bHovered[i] = false;
		m_bPressed[i] = false;
		m_Function[i] = NULL;
	}

	m_GUIChildren.clear();

	UseShaderProgram(1);
	m_SizeUni = GLObject::GetShaderProgram(1)->GetUniformLocation("size");
	m_AlphaUni = GLObject::GetShaderProgram(1)->GetUniformLocation("alpha");

	SetClearData(false);
}

GUIElement::~GUIElement()
{
	// clean up
	if (m_Label)
		m_Label->SetElement(NULL);

	KillChildren();
	Clear();
}

void GUIElement::Update(bool bForce)
{
	// if we aren't visible/animating then don't update anything
	bool bAnimated = IsAnimating();
	if (!bForce && !IsVisible() && !bAnimated)
		return;

	// if we are anchored to the world, move us there
	if (m_bAnchored)
	{
		vector2df pos = dynamic_cast<SceneManager*>(GetManager()->GetRoot()->GetManager(scene_manager))->ConvertToScreenCoordinates(m_WorldAnchor);
		SetPosition(pos);
	}

	// animations
	GUIElement* parent = GetParent();
	vector3df pos;
	if (parent)
		pos = m_Offset;
	else
		pos = GetAbsolutePosition();

	vector2df scale = GetScale();

	if (bAnimated)
	{		
		Interpolate(pos.x, m_AnimationPosition.x, m_fAnimationSmooth, 1);
		Interpolate(pos.y, m_AnimationPosition.y, m_fAnimationSmooth, 1);
		Interpolate(m_fAlpha, m_fDestAlpha, m_fAnimationSmooth);
		Interpolate(scale.x, m_AnimationScale.x, m_fAnimationSmooth, 0.001f);
		Interpolate(scale.y, m_AnimationScale.y, m_fAnimationSmooth, 0.001f);

		if (pos.x == m_AnimationPosition.x && pos.y == m_AnimationPosition.y && m_fAlpha == m_fDestAlpha && scale.x == m_AnimationScale.x && scale.y == m_AnimationScale.y)
		{
			m_bAnimating = false;

			if (m_bHideOnAnimationEnd)
			{
				SetVisible(false);
				m_bHideOnAnimationEnd = false;
			}

			if (m_bKillOnAnimationEnd)
			{
				GetGUIManager()->KillElement(GetRoot());
			}
		}
	}

	SetScale(vector3df(scale.x, scale.y, 1));
		
	// offset
	if (parent)
	{
		GLObject::SetPosition(parent->GetPosition() + (pos * parent->GetScale()) + vector3df(0, 0, m_Depth));
		m_Offset = pos;
		SetScale(GetScale() * parent->GetScale());
	}
	else
	{
		pos.z += m_Depth;
		GLObject::SetPosition(pos);
	}

	if (bAnimated && m_RenderBoundary != vector4df())
		SetRenderBoundary(m_OriginalRenderBoundary);

	if (!IsEnabled())
	{
		if (m_fAlpha > 0.55f) m_fAlpha = 0.55f;
		if (m_fDestAlpha > 0.55f) m_fDestAlpha = 0.55f;
	}

	// if we are following the mouse, set the position
	if (m_bFollowMouse)
	{
		vector2dd mouse = GetGUIManager()->GetMousePosition();
		SetPosition(vector2df(mouse.x, mouse.y) + GetOffset());
	}
	
	// update this yo
	GLObject::UpdateTransformationMatrix();
	
	// update any children
	std::vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	
	while (it != m_GUIChildren.end())
	{
		(*it)->Update(bForce);
		++it;
	}

	if (!parent)
		GLObject::TranslatePosition(vector3df(0, 0, -m_Depth));
	else
		SetScale(vector3df(scale.x, scale.y, 1));
}

void GUIElement::Render()
{
	// if we aren't visible then don't draw anything
	if (!IsVisible())
		return;
	
	// render this GUIElement
	RenderBoundaryToShader();
	glUniform1f(m_AlphaUni, m_fAlpha);
	GLObject::Render();
	
	// render any children
	ReverseChildren();
	vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	GLfloat alpha;
	
	while (it != m_GUIChildren.end())
	{
		RenderBoundaryToShader();

		if (!*it)
		{
			++it;
			continue;
		}

		alpha = (*it)->m_fAlpha;
		(*it)->m_fAlpha *= m_fAlpha;

		(*it)->Render();

		(*it)->m_fAlpha = alpha;
		++it;
	}
	ReverseChildren();
}

bool GUIElement::InViewFrustum() const
{
	return true;
}

void GUIElement::KillChildren()
{
	// go through all the children of this GUIElement and kill them
	vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	
	while (it != m_GUIChildren.end())
	{
		delete (*it);
		++it;
	}
	m_GUIChildren.clear();
}

void GUIElement::Kill()
{
	delete this;
}

GUIElement* GUIElement::GetParent()
{
	return m_pGUIParent;
}

GUIElement* GUIElement::GetRoot()
{
	// find the root parent of this element so it can be passed to GUIManager::KillElement()
	GUIElement* e = this;
	
	while (e->GetParent())
	{
		e = e->GetParent();
	}
	
	return e;
}

const vector<GUIElement*>& GUIElement::GetChildren()
{
	return m_GUIChildren;
}

bool GUIElementCompare(GUIElement* one, GUIElement* two)
{
	return (*one < *two);
}

void GUIElement::AddChild(GUIElement* child)
{
	m_GUIChildren.push_back(child);
	if (m_GUIChildren.size())
		std::sort(m_GUIChildren.begin(), m_GUIChildren.end(), GUIElementCompare);
}

void GUIElement::RemoveChild(GUIElement* child)
{
	// find this element in the list and remove it
	vector<GUIElement*>::iterator it = find(m_GUIChildren.begin(), m_GUIChildren.end(), child);
	
	if (it != m_GUIChildren.end())
	{
		m_GUIChildren.erase(it);
	}
	else
	{
		cout << "ERROR: Specified GUIElement not a child of the given GUIElement!" << endl;
	}
}

GUIElement* GUIElement::GetChild(GLuint index)
{
	return m_GUIChildren[index];
}

vector2df GUIElement::GetAbsolutePosition()
{
	if (GetParent())
	{
		return GetParent()->GetAbsolutePosition() + m_Offset;
	}
	else
		return GetPosition();
}

void GUIElement::TranslatePosition(const vector2df& translate)
{
	if (!GetParent())
		GLObject::TranslatePosition(vector3df(translate));
	else
		m_Offset += translate;
	m_AnimationPosition += translate;
}

void GUIElement::SetTexture(const char* fileName)
{
	GLObject::SetTexture(fileName);
}

void GUIElement::SetTexture(Texture tex)
{
	GLObject::SetTexture(tex);
}

void GUIElement::SetFunction(GLuint button, void (*fn)(GUIElement*))
{
	m_Function[button] = fn;
}

void GUIElement::SetUpdateFunction(void(*fn)(GUIElement*))
{
	m_UpdateFunction = fn;
}

void GUIElement::ExecuteFunction(GLuint button)
{
	if (m_Function[button] != NULL)
	{
		m_Function[button](this);
	}
}

bool GUIElement::MouseRelease(GLuint button)
{
	if (!IsEnabled())
		return false;
	
	vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	while (it != m_GUIChildren.end())
	{
		GUIElement* e = (*it);
		if (e->MouseRelease(button))
			return true;
		++it;
	}
	
	if (IsHovered() && IsClickable() && IsPressed(button))
	{
		ExecuteFunction(button);

		if (m_UpdateFunction)
			m_UpdateFunction(this);

		return true;
	}

	return false;
}

bool GUIElement::MouseDown(GLuint button)
{
	if (!IsEnabled())
		return false;
	
	vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	while (it != m_GUIChildren.end())
	{
		GUIElement* e = (*it);
		if (e->MouseDown(button))
			return true;
		++it;
	}
	
	if (IsHovered() && IsClickable())
	{
		MouseDownFunction(button);
		Press(button);
		return true;
	}

	return false;
}

bool GUIElement::KeyRelease(int key)
{
	if (!IsEnabled())
		return false;
	
	vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	while (it != m_GUIChildren.end())
	{
		GUIElement* e = (*it);
		if (e->IsHovered())
		{
			return e->KeyRelease(key);
		}
		++it;
	}

	return false;
}

bool GUIElement::KeyPress(int key)
{
	if (!IsEnabled())
		return false;
	
	vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	while (it != m_GUIChildren.end())
	{
		GUIElement* e = (*it);
		if (e->IsHovered())
		{
			return e->KeyPress(key);
		}
		++it;
	}

	return false;
}

const bool GUIElement::operator<(const GUIElement& o) const
{
	return (o.m_Depth < m_Depth);
}

bool GUIElement::IsHovered()
{
	if (!IsVisible())
		return false;
	
	if (!GetGUIManager())
		return false;

	if (!IsEnabled())
		return false;
	
	vector3df pos = GetAbsolutePosition();
	vector3di size = GetSize();
	return IsMouseInRect(GetGUIManager()->GetMousePosition(), pos, vector2df(pos.x + size.x, pos.y + size.y));
}

bool GUIElement::IsPressed(GLuint button)
{
	return m_bPressed[button];
}

bool GUIElement::IsAnyChildPressed(GLuint button)
{
	vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	while (it != m_GUIChildren.end())
	{
		if ((*it)->IsPressed(button) || (*it)->IsAnyChildPressed(button))
		{
			return true;
		}
		++it;
	}
	
	return false;
}

void GUIElement::Press(GLuint button)
{
	m_bPressed[button] = true;
}

void GUIElement::Release(GLuint button)
{
	vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	while (it != m_GUIChildren.end())
	{
		GUIElement* e = (*it);
		e->Release(button);
		it++;
	}
	
	m_bPressed[button] = false;
}

void GUIElement::SetClickable(bool clickable)
{
	m_bClickable = clickable;
}

bool GUIElement::IsClickable()
{
	return m_bClickable;
}

GUIManager* GUIElement::GetGUIManager()
{
	if (GetManager() != NULL)
	{
		return dynamic_cast<GUIManager*>(GetManager());
	}
	else
	{
		return NULL;
	}
}

void GUIElement::UpdateTransformationMatrix()
{
	// if we have a parent then offset from our parent's position
	GLObject::UpdateTransformationMatrix();
}

void GUIElement::RenderBoundaryToShader()
{
	if (m_RenderBoundary == vector4df())
	{
		if (!GetParent() || GetParent()->m_RenderBoundary == vector4df())
		{
			vector2di ScreenSize = GetManager()->GetRoot()->GetScreenSize();
			GLfloat screen[4] = { 0, 0, (GLfloat)ScreenSize.x, (GLfloat)ScreenSize.y };
			glUniform4fv(GetSizeUniPosition(), 1, screen);
		}
		return;
	}
	
	GLfloat b[4];
	m_RenderBoundary.ToBuffer(b);
	GLfloat t = b[1];
	b[1] = GetManager()->GetRoot()->GetScreenSize().y - b[3];
	b[3] = GetManager()->GetRoot()->GetScreenSize().y - t;
	glUniform4fv(GetSizeUniPosition(), 1, b);
}

void GUIElement::SetRenderBoundary(vector4df boundary)
{
	vector2df pos = GetAbsolutePosition();
	m_RenderBoundary.Set(pos.x + boundary.x, pos.y + boundary.y, pos.x + boundary.z, pos.y + boundary.w);
	m_OriginalRenderBoundary = boundary;
}

void GUIElement::SetPosition(const vector2df& pos)
{
	m_AnimationPosition = pos;
	GLObject::SetPosition(vector3df(pos));
}

void GUIElement::SetOffset(const vector2df& offset)
{
	m_Offset = offset;
	m_AnimationPosition = offset;
}

vector2df GUIElement::GetOffset()
{
	return m_Offset;
}

void GUIElement::SetDepth(GLfloat depth)
{
	m_Depth = depth;

	if (GetParent())
		std::sort(GetParent()->m_GUIChildren.begin(), GetParent()->m_GUIChildren.end(), GUIElementCompare);
	else
		GetGUIManager()->Sort();
}

GLfloat GUIElement::GetDepth()
{
	return m_Depth;
}

bool GUIElement::IsVisible()
{
	// take into account parent visibility
	if (GetParent() && !GetParent()->IsVisible())
		return false;

	return GLObject::IsVisible();
}

void GUIElement::MoveTo(vector2df pos, bool hide, bool kill)
{
	m_AnimationPosition = pos;
	m_bAnimating = true;
	m_bHideOnAnimationEnd = hide;
	m_bKillOnAnimationEnd = kill;
}

void GUIElement::FadeTo(GLfloat alpha, bool hide, bool kill, bool children)
{
	m_fDestAlpha = alpha;
	m_bAnimating = true;
	m_bHideOnAnimationEnd = hide;
	m_bKillOnAnimationEnd = kill;

	if (children)
	{
		vector<GUIElement*>::iterator it = m_GUIChildren.begin();
		while (it != m_GUIChildren.end())
		{
			GUIElement* e = (*it);
			e->FadeTo(alpha, hide, kill, children);
			++it;
		}
	}
}

void GUIElement::ScaleTo(vector2df scale, bool hide, bool kill)
{
	m_AnimationScale = scale;
	m_bAnimating = true;
	m_bHideOnAnimationEnd = hide;
	m_bKillOnAnimationEnd = kill;
}

void GUIElement::SetAlpha(GLfloat alpha, bool changeset)
{
	m_fAlpha = alpha;
	m_fDestAlpha = alpha;
	if (changeset)
		m_fSetAlpha = alpha;
}

GLfloat GUIElement::GetAlpha()
{
	return m_fAlpha;
}

void GUIElement::SetMenu()
{
	m_bMenu = true;

	vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	while (it != m_GUIChildren.end())
	{
		(*it)->SetMenu();
		++it;
	}
}

bool GUIElement::IsMenu()
{
	return m_bMenu;
}

void GUIElement::SetEnabled(bool enabled)
{
	m_bEnabled = enabled;

	if (!m_bEnabled)
	{
		m_fAlpha = 0.55f;
		m_fDestAlpha = 0.55f;
	}
	else
	{
		m_fAlpha = m_fSetAlpha;
		m_fDestAlpha = m_fSetAlpha;
	}

	vector<GUIElement*>::iterator it = m_GUIChildren.begin();
	while (it != m_GUIChildren.end())
	{
		if ((*it)->IsEnabled())
		{
			(*it)->m_fAlpha = min(m_fSetAlpha, (*it)->m_fSetAlpha);
			(*it)->m_fDestAlpha = min(m_fSetAlpha, (*it)->m_fSetAlpha);
		}
		++it;
	}
}

bool GUIElement::IsEnabled()
{
	if (GetParent())
		return m_bEnabled && GetParent()->IsEnabled();
	
	return m_bEnabled;
}

void GUIElement::SetFollowMouse(bool follow)
{
	m_bFollowMouse = follow;
}

void GUIElement::SetLabel(Label* l)
{
	m_Label = l;
}

Label* GUIElement::GetLabel()
{
	return m_Label;
}

bool GUIElement::IsAnimating()
{
	if (GetParent())
		return m_bAnimating || GetParent()->IsAnimating();

	return m_bAnimating;
}

void GUIElement::SetAnimationSmoothness(GLfloat smooth)
{
	m_fAnimationSmooth = smooth;
}

void GUIElement::AnchorToWorld(const vector3df& pos)
{
	m_WorldAnchor = pos;
	m_bAnchored = true;
}

void GUIElement::AddFade()
{
	//vector2di ScreenSize = GetGUIManager()->GetRoot()->GetScreenSize();
	AddQuad(vector2df(), vector2df(0.5), vector2df(4000, 4000), vector2df(0.5), GLColour(0, 200));
}

// some basic button functionality
void GUIElement::Close(GUIElement* e)
{
	// kill everything
	e->GetGUIManager()->KillElement(e->GetRoot());
}

void GUIElement::Hide(GUIElement* e)
{
	// hide everything
	e->GetRoot()->SetVisible(false);
}

void GUIElement::CloseGame(GUIElement* e)
{
	// close the game
	glfwSetWindowShouldClose(e->GetGUIManager()->GetWindow(), GLFW_TRUE);
}

GLuint GUIElement::GetSizeUniPosition()
{
	return m_SizeUni;
}
