#include "SceneManager.h"
#include "Game.h"

SceneManager::SceneManager(Root* root, GLFWwindow* window, TextureManager* texMan)
	: Manager(root, texMan)
	, m_MobManager(this)
	, m_pWindow(window)
	, m_ItemToPickUp(NULL)
	, m_bCanPickUp(false)
	, m_fShadowDistance(256)
{
	UpdateUniforms();

	// create the light projection matrix
	m_LightProj.Orthographic(-384, 384, -384, 384, 0.1f, 1000.f);

	// create a camera object
	m_pCamera = new Camera(this);

	// set the sun
	m_Sun = 1;
	m_SunChange = 0.35f;
	m_DestSunChange = 0.f;

	// get a pointer to the game object
	m_pGame = dynamic_cast<Game*>(root);
}

SceneManager::~SceneManager()
{
	CleanUp();
	delete m_pCamera;
}

void SceneManager::SetWindow(GLFWwindow* window)
{
	m_pWindow = window;
}

void SceneManager::SetShadowRenderSize(GLuint i)
{
	// change the light projection matrix based on the input
	switch (i)
	{
		case 1:
			m_fShadowDistance = 256.f;
			break;
		case 2:
		case 3:
			m_fShadowDistance = 384.f;
			break;
		case 4:
			m_fShadowDistance = 512.f;
			break;
	}

	m_LightProj.Orthographic(-m_fShadowDistance, m_fShadowDistance, -m_fShadowDistance, m_fShadowDistance, 0.1f, 1000.f);
}

void SceneManager::UpdateUniforms()
{
	// get the camera uniform position
	GLObject::GetShaderProgram(0)->Bind();
	m_CameraUni[0] = GLObject::GetShaderProgram(0)->GetUniformLocation("view");
	m_SunUni[0] = GLObject::GetShaderProgram(0)->GetUniformLocation("sun");
	m_LightSpaceUni[0] = GLObject::GetShaderProgram(0)->GetUniformLocation("lightSpace");
	m_LightDirUni[0] = GLObject::GetShaderProgram(0)->GetUniformLocation("lightDir");
	m_LightPosUni = GLObject::GetShaderProgram(0)->GetUniformLocation("lightPos");
	GLObject::GetShaderProgram(0)->Unbind();

	// get the shadow shaders uniform location
	GLObject::GetShaderProgram(2)->Bind();
	m_ShadowLightSpaceUni[0] = GLObject::GetShaderProgram(2)->GetUniformLocation("lightSpace");
	GLObject::GetShaderProgram(2)->Unbind();

	// get the camera uniform position
	GLObject::GetShaderProgram(3)->Bind();
	m_CameraUni[1] = GLObject::GetShaderProgram(3)->GetUniformLocation("view");
	m_SunUni[1] = GLObject::GetShaderProgram(3)->GetUniformLocation("sun");
	m_LightSpaceUni[1] = GLObject::GetShaderProgram(3)->GetUniformLocation("lightSpace");
	m_LightDirUni[1] = GLObject::GetShaderProgram(3)->GetUniformLocation("lightDir");
	GLObject::GetShaderProgram(3)->Unbind();

	// get the shadow shaders uniform location
	GLObject::GetShaderProgram(4)->Bind();
	m_ShadowLightSpaceUni[1] = GLObject::GetShaderProgram(4)->GetUniformLocation("lightSpace");
	GLObject::GetShaderProgram(4)->Unbind();
}

void SceneManager::CleanUp()
{
	m_DeadObjects.clear();
	list<GLObject*>::iterator it = m_Objects.begin();
	while (it != m_Objects.end())
	{
		if (*it)
			delete (*it);

		++it;
	}
	m_Objects.clear();
	m_RenderQueue.clear();
	m_AnimatedRenderQueue.clear();

	std::map<string, Mesh*>::iterator mit = m_Meshes.begin();
	while (mit != m_Meshes.end())
	{
		if (mit->second)
			delete mit->second;
		++mit;
	}
	m_Meshes.clear();

	m_TransparencyRenderFunctions.clear();
	m_KeyFunctions.clear();
	m_MouseFunctions.clear();
	m_ScrollFunctions.clear();
}

void SceneManager::Update()
{
	// update the current cursor position
	glfwGetCursorPos(m_pWindow, &m_CursorPos.x, &m_CursorPos.y);

	// item pick up
	if (m_bCanPickUp && m_ItemToPickUp)
	{
		GetFirstInstance<Player>()->PickUpItem(m_ItemToPickUp);
		m_ItemToPickUp = NULL;
		m_bCanPickUp = false;
	}

	// iterate through everything in the dead GLObject list and kill it
	list<GLObject*>::iterator it = m_DeadObjects.begin();
	while (it != m_DeadObjects.end())
	{
		// ensure m_Objects isn't empty
		if (m_Objects.empty())
		{
			m_DeadObjects.clear();
			break;
		}
		
		// find all functions bound to this object and delete them
		vector<std::pair<GLObject*, void(*)(GLObject*)>>::iterator renderit = m_TransparencyRenderFunctions.begin();
		while (renderit != m_TransparencyRenderFunctions.end())
		{
			if ((*renderit).first == (*it))
			{
				m_TransparencyRenderFunctions.erase(renderit);
				renderit = m_TransparencyRenderFunctions.begin();
			}
			else
				++renderit;
		}

		list<GLObject*>::iterator _it = find(m_Objects.begin(), m_Objects.end(), *it);
		if (_it != m_Objects.end())
			m_Objects.erase(_it);

		_it = find(m_RenderQueue.begin(), m_RenderQueue.end(), *it);
		if (_it != m_RenderQueue.end())
			m_RenderQueue.erase(_it);
		else
		{
			// must be animated
			_it = find(m_AnimatedRenderQueue.begin(), m_AnimatedRenderQueue.end(), *it);
			if (_it != m_AnimatedRenderQueue.end())
				m_AnimatedRenderQueue.erase(_it);
		}
		
		delete *it;
		m_DeadObjects.erase(it);

		it = m_DeadObjects.begin();
	}
	
	// iterate through everything in the scene and update it
	it = m_Objects.begin();
	while (it != m_Objects.end())
	{
		(*it)->Update();
		++it;
	}

	// sort out the light view matrix
	GLfloat _sun = m_SunChange * PI;
	vector3df sunPos(m_pCamera->GetPosition());
	sunPos.y = 100.f;//m_pCamera->GetPosition().y + 300.f;
	m_LightPos.Set(sunPos + vector3df(cos(_sun) * -600.f, max(sin(_sun) * 360.f, 120.f), 20.f));
	m_LightDir = vector3df(cos(_sun) * 600.f, min(sin(_sun) * -360.f, -120.f), -20.f).Normal();
	m_LightView.LookAt(m_LightPos, sunPos);
	m_LightSpace = m_LightProj * m_LightView;

	// get delta time
	double delta = DeltaTime::GetDelta();

	// night / day cycle
	m_SunChange += 0.01f * delta;

	if (m_SunChange > m_DestSunChange || m_SunChange < m_DestSunChange - 0.01f)
		m_SunChange = m_DestSunChange;
	//m_SunChange = m_DestSunChange;
	//m_SunChange += (m_Sun > 0.35f ? 0.001f : 0.002f) * delta;
	m_Sun = (sin(_sun) + 1) * 0.5f;
	//if (m_SunChange > 2)
	//	m_SunChange = 0;

	// mob management
	m_MobManager.Update();

	// deal with network-created items
	if (m_ItemsToCreate.size() > 0)
	{
		m_ItemMutex.lock();
		list<ItemData>::iterator it = m_ItemsToCreate.begin();

		while (it != m_ItemsToCreate.end())
		{
			CreateItem(*it);

			++it;
		}

		m_ItemsToCreate.clear();
		m_ItemMutex.unlock();
	}

	// deal with given items
	if (m_ItemsToGive.size() > 0)
	{
		m_ItemMutex.lock();
		list<ItemData>::iterator it = m_ItemsToGive.begin();
		Player* p = GetFirstInstance<Player>();

		while (it != m_ItemsToGive.end())
		{
			p->PickUpItem(CreateItem(*it));

			++it;
		}

		m_ItemsToGive.clear();
		m_ItemMutex.unlock();
	}

	// timeout for picking up items
	if (m_PickUpTimeout > 0)
		m_PickUpTimeout -= 1 * delta;
	else
	{
		m_bCanPickUp = true;
		m_ItemToPickUp = NULL;
	}

	// temp: adjust sun time
	/*static int dir = 1;
	m_Sun += 0.1f * dir * delta;
	if (m_Sun <= 0)
	{
		m_Sun = 0;
		dir = 1;
	}
	else if (m_Sun >= 1)
	{
		m_Sun = 1;
		dir = -1;
	}*/
}

void SceneManager::Render()
{
	// update the camera
	m_pCamera->Update();

	// now render animated objects
	GLObject::GetShaderProgram(3)->Bind();

	glUniformMatrix4fv(m_CameraUni[1], 1, GL_TRUE, m_pCamera->GetCameraMatrix());
	glUniformMatrix4fv(m_LightSpaceUni[1], 1, GL_TRUE, m_LightSpace.GetDataPointer());
	glUniform3f(m_LightDirUni[1], m_LightDir.x, m_LightDir.y, m_LightDir.z);
	glUniform1f(m_SunUni[1], m_Sun);

	list<GLObject*>::iterator it = m_AnimatedRenderQueue.begin();
	while (it != m_AnimatedRenderQueue.end())
	{
		(*it)->Render();
		++it;
	}

	// use the correct shader program
	GLObject::GetShaderProgram(0)->Bind();

	// update the camera
	glUniformMatrix4fv(m_CameraUni[0], 1, GL_TRUE, m_pCamera->GetCameraMatrix());
	glUniformMatrix4fv(m_LightSpaceUni[0], 1, GL_TRUE, m_LightSpace.GetDataPointer());
	glUniform3f(m_LightDirUni[0], m_LightDir.x, m_LightDir.y, m_LightDir.z);
	//glUniform3f(m_LightPosUni, m_LightPos.x, m_LightPos.y, m_LightPos.z);
	glUniform1f(m_SunUni[0], m_Sun);

	// iterate through everything in the scene and draw it
	it = m_RenderQueue.begin();
	while (it != m_RenderQueue.end())
	{
		(*it)->Render();
		++it;
	}

	// now after we have drawn everything, execute any transparency render functions
	vector<std::pair<GLObject*, void(*)(GLObject*)>>::iterator tit = m_TransparencyRenderFunctions.begin();
	while (tit != m_TransparencyRenderFunctions.end())
	{
		//if (!tit->first->IsAnimated())
		tit->second(tit->first);

		++tit;
	}

	// now after we have drawn everything, execute any transparency render functions
	/*tit = m_TransparencyRenderFunctions.begin();
	while (tit != m_TransparencyRenderFunctions.end())
	{
		if (tit->first->IsAnimated())
			tit->second(tit->first);

		++tit;
	}*/
}

void SceneManager::RenderShadows()
{
	// use the correct shader program
	GLObject::GetShaderProgram(2)->Bind();

	// update the camera
	glUniformMatrix4fv(m_ShadowLightSpaceUni[0], 1, GL_TRUE, m_LightSpace.GetDataPointer());

	// iterate through everything in the scene and draw it
	list<GLObject*>::iterator it = m_RenderQueue.begin();
	//vector3df pos;
	while (it != m_RenderQueue.end())
	{
		//pos = (*it)->GetPosition();
		
		//if (pos.Distance(GetActiveCamera()->GetPosition()) <= m_fShadowDistance + 50.f)
		(*it)->RenderShadows();

		++it;
	}

	// now draw animated objects
	GLObject::GetShaderProgram(4)->Bind();

	// update the camera
	glUniformMatrix4fv(m_ShadowLightSpaceUni[1], 1, GL_TRUE, m_LightSpace.GetDataPointer());

	// iterate through everything in the scene and draw it
	it = m_AnimatedRenderQueue.begin();
	while (it != m_AnimatedRenderQueue.end())
	{
		//pos = (*it)->GetPosition();

		//if (pos.Distance(GetActiveCamera()->GetPosition()) <= m_fShadowDistance + 50.f)
		(*it)->RenderShadows();

		++it;
	}
}

vector2dd& SceneManager::GetMousePosition()
{
	return m_CursorPos;
}

int SceneManager::GetMouseButton(int button)
{
	return glfwGetMouseButton(m_pWindow, button);
}

void SceneManager::CenterCursor()
{
	vector2di ScreenSize = GetRoot()->GetScreenSize();
	glfwSetCursorPos(m_pWindow, ScreenSize.x / 2, ScreenSize.y / 2);
	m_CursorPos.Set(ScreenSize.x / 2, ScreenSize.y / 2);
}

bool SceneManager::RestrictCursorToWindow()
{
	vector2di ScreenSize = GetRoot()->GetScreenSize();
	vector2dd pos(m_CursorPos);
	bool bReturn = false;

	if (pos.x <= 0)
	{
		pos.x = ScreenSize.x;
		bReturn = true;
	}
	else if (pos.x >= ScreenSize.x)
	{
		pos.x = 0;
		bReturn = true;
	}

	if (pos.y <= 0)
	{
		pos.y = ScreenSize.y;
		bReturn = true;
	}
	else if (pos.y >= ScreenSize.y - 1)
	{
		pos.y = 0;
		bReturn = true;
	}

	glfwSetCursorPos(m_pWindow, pos.x, pos.y);
	m_CursorPos.Set(pos);

	return bReturn;
}

void SceneManager::ToggleMouse(bool hidden)
{
	if (hidden)
		glfwSetInputMode(m_pWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	else
		glfwSetInputMode(m_pWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

int SceneManager::GetKeyState(int key)
{
	return glfwGetKey(m_pWindow, key);
}

bool SceneManager::KeyDoublePressed(GLuint key)
{
	return m_pGame->KeyDoublePressed(key);
}

bool SceneManager::Input(int type, int key, int evt)
{
	map<int, map<GLObject*, bool(*[2])(GLObject*)>>* in = (type ? &m_KeyFunctions : &m_MouseFunctions);
	
	map<GLObject*, bool(*[2])(GLObject*)>::iterator it = (*in)[key].begin();
	while (it != (*in)[key].end())
	{
		if (it->second[evt] && it->second[evt](it->first))
			return true;

		++it;
	}

	return false;
}

bool SceneManager::ScrollInput(double x, double y)
{
	map<GLObject*, bool(*)(GLObject*, double, double)>::iterator it = m_ScrollFunctions.begin();
	while (it != m_ScrollFunctions.end())
	{
		if (it->second && it->second(it->first, x, y))
			return true;

		++it;
	}

	return false;
}

void SceneManager::AddKeyFunction(GLObject* o, int key, int evt, bool(*fn)(GLObject*))
{
	m_KeyFunctions[key][o][evt] = fn;
}

void SceneManager::RemoveKeyFunction(GLObject* o, int key, int evt)
{
	// loop through functions associated with this key
	map<GLObject*, bool(*[2])(GLObject*)>::iterator it = m_KeyFunctions[key].begin();
	while (it != m_KeyFunctions[key].end())
	{
		// see if there are any functions associated with this GLObject
		if (it->first == o)
		{
			// set the function to NULL
			it->second[evt] = NULL;

			// if both functions are NULL, delete this entry
			if (it->second[1 - evt] == NULL)
				m_KeyFunctions[key].erase(it->first);

			// we can safely leave this function now
			return;
		}

		++it;
	}
}

void SceneManager::AddMouseFunction(GLObject* o, int key, int evt, bool(*fn)(GLObject*))
{
	m_MouseFunctions[key][o][evt] = fn;
}

void SceneManager::AddScrollFunction(GLObject* o, bool(*fn)(GLObject*, double, double))
{
	m_ScrollFunctions[o] = fn;
}

void SceneManager::AddTransparencyRenderFunction(GLObject* o, void(*fn)(GLObject*))
{
	m_TransparencyRenderFunctions.push_back(std::pair<GLObject*, void(*)(GLObject*)>(o, fn));
}

void SceneManager::RemoveTransparencyRenderFunction(GLObject* o, void(*fn)(GLObject*))
{
	vector<std::pair<GLObject*, void(*)(GLObject*)>>::iterator it = std::find(m_TransparencyRenderFunctions.begin(), m_TransparencyRenderFunctions.end(), std::pair<GLObject*, void(*)(GLObject*)>(o, fn));

	if (it != m_TransparencyRenderFunctions.end())
		m_TransparencyRenderFunctions.erase(it);
}

Mesh* SceneManager::LoadMeshFromOBJ(string filename)
{
	if (m_Meshes[filename])
		return m_Meshes[filename];

	Mesh* mesh = new Mesh();
	if (!mesh->LoadFromOBJ(filename))
	{
		cout << "ERROR: Cannot open mesh " << filename << "! Check it exists!" << endl;
		delete mesh;
		return NULL;
	}

	cout << "Loading mesh " << filename << endl;
	m_Meshes[filename] = mesh;
	return mesh;
}

Mesh* SceneManager::LoadMeshFromAOBJ(string filename)
{
	if (m_Meshes[filename])
		return m_Meshes[filename];

	Mesh* mesh = new Mesh();
	if (!mesh->LoadFromAOBJ(filename))
	{
		cout << "ERROR: Cannot open animated mesh " << filename << "! Check it exists!" << endl;
		delete mesh;
		return NULL;
	}

	cout << "Loading animated mesh " << filename << endl;
	m_Meshes[filename] = mesh;
	return mesh;
}

Mesh* SceneManager::CreateTerrainBlock(string type)
{
	if (m_Meshes[type])
		return m_Meshes[type];
	
	const ItemClass* ic = Item::GetItemClass(type);
	if (ic == NULL)
		return NULL;

	Mesh* mesh = new Mesh();
	mesh->CreateTerrainBlockMesh(ic->TerrainUV);

	m_Meshes[type] = mesh;
	return mesh;
}

Camera* SceneManager::GetActiveCamera() const
{
	return m_pCamera;
}

GLfloat SceneManager::GetSun() const
{
	return m_Sun;
}

void SceneManager::SetSun(GLfloat sun)
{
	m_Sun = sun;
}

GLfloat SceneManager::GetShadowDistance() const
{
	return m_fShadowDistance;
}

line3df SceneManager::GetRayFromScreenCoords(vector2di& pos)
{
	line3df out(0, 0, 0, 0, 0, 0);
	Camera* pCamera = GetActiveCamera();

	ViewFrustum vf = pCamera->GetViewFrustum();
	vector3df farLeftUp = vf.FarLeftUp();
	vector3df leftToRight = vf.FarRightUp() - farLeftUp;
	vector3df upToDown = vf.FarLeftDown() - farLeftUp;

	vector2di ScreenSize = GetRoot()->GetScreenSize();

	GLfloat dx = pos.x / (GLfloat)ScreenSize.x;
	GLfloat dy = pos.y / (GLfloat)ScreenSize.y;

	out.start = pCamera->GetPosition();
	out.end = farLeftUp + (leftToRight * dx) + (upToDown * dy);

	return out;
}

bool SceneManager::DoesRayCollide(GLObject* obj, line3df& ray)
{
	// check if a ray collides with the given GLObject
	GLfloat step = 2.f;
	vector3df stepv = ray.end - ray.start;
	stepv.Normal();
	GLfloat distDone = step;
	vector3df temp = ray.start;

	while (distDone < ray.Magnitude())
	{
		temp += stepv * step;

		if (obj->DoesPointCollide(temp))
		{
			return true;
		}

		distDone += step;
	}

	return false;
}

bool SceneManager::GetRayCollisionPoint(GLObject* obj, line3df& ray, vector3df& out, vector3df& outBefore, bool(*check)(SceneManager*, vector3df))
{
	// check if a ray collides with the given GLObject and if so, where
	GLfloat step = 2.f;
	vector3df stepv = ray.end - ray.start;
	stepv.Normal();
	GLfloat distDone = step;
	vector3df temp = ray.start;

	while (distDone < ray.Magnitude())
	{
		temp += stepv * step;

		if (obj->DoesPointCollide(temp) && (check == NULL || check(this, temp - (stepv * step))))
		{
			outBefore = temp - (stepv * step);
			out = temp;

			return true;
		}

		distDone += step;
	}

	return false;

	// new code, should hopefully be a lot faster
	/*GLfloat dist = 0;
	if (obj->DoesRayCollide(ray, (ray.end - ray.start).OneOverNormal(), dist))
	{
		//out = ray.start + (ray.end - ray.start) * (ray.Magnitude() / dist);
		vector3df n = (ray.end - ray.start);
		n.Normal();
		out = ray.start + (n * dist);

		return true;
	}

	return false;*/
}

bool SceneManager::GetRayCollisionWithTerrain(line3df& ray, vector3df& out, vector3df& outBefore, bool(*check)(SceneManager*, vector3df, vector3df))
{
	// check if a ray collides with the terrain and if so, where
	Terrain* t = GetFirstInstance<Terrain>();

	if (!t)
		return false;

	GLfloat length = ray.Magnitude();

	if (length == 0)
		return false;

	auto intbound = [](GLfloat s, GLfloat ds) -> GLfloat {
		bool sIsInteger = round(s) == s;
		if (ds <= 0 && sIsInteger)
			return 0;

		return (ds > 0 ? ceil(s) - s : s - floor(s)) / abs(ds);
	};

	/*vector3df tmp(
		(GLfloat)((int)ray.start.x - (int)ray.start.x % 10) + 5,
		(GLfloat)((int)ray.start.y - (int)ray.start.y % 10) + 5,
		(GLfloat)((int)ray.start.z - (int)ray.start.z % 10) + 5
		);
	vector3df end(
		(GLfloat)((int)ray.end.x - (int)ray.end.x % 10) + 5,
		(GLfloat)((int)ray.end.y - (int)ray.end.y % 10) + 5,
		(GLfloat)((int)ray.end.z - (int)ray.end.z % 10) + 5
		);*/
	vector3df tmp(ray.start);
	vector3df end(ray.end);
	vector3df prev = tmp;

	vector3df n = (ray.end - ray.start).Normal();

	GLfloat _x = end.x > tmp.x ? 10 : end.x < tmp.x ? -10 : 0;
	GLfloat _y = end.y > tmp.y ? 10 : end.y < tmp.y ? -10 : 0;
	GLfloat _z = end.z > tmp.z ? 10 : end.z < tmp.z ? -10 : 0;

	GLfloat _dX = (_x * 0.1f) / (n.x != 0 ? n.x : 1.f);
	GLfloat _dY = (_y * 0.1f) / (n.y != 0 ? n.y : 1.f);
	GLfloat _dZ = (_z * 0.1f) / (n.z != 0 ? n.z : 1.f);

	GLfloat _mX = intbound(tmp.x * 0.1f, n.x);
	GLfloat _mY = intbound(tmp.y * 0.1f, n.y);
	GLfloat _mZ = intbound(tmp.z * 0.1f, n.z);

	while (tmp != end && (end - tmp).Magnitude() <= length)
	{
		if (t->GetBlock(tmp).BlockType && (!check || check(this, tmp, prev)))
		{
			out.Set(tmp);
			outBefore.Set(prev);

			return true;
		}
		
		prev = tmp;

		if (_mX <= _mY)
		{
			if (_mX <= _mZ)
			{
				tmp.x += _x;
				_mX += _dX;

				if (_x == 0 || _dX == 0)
					break;
			}
			else
			{
				tmp.z += _z;
				_mZ += _dZ;

				if (_z == 0 || _dZ == 0)
					break;
			}
		}
		else
		{
			if (_mY <= _mZ)
			{
				tmp.y += _y;
				_mY += _dY;

				if (_y == 0 || _dY == 0)
					break;
			}
			else
			{
				tmp.z += _z;
				_mZ += _dZ;

				if (_z == 0 || _dZ == 0)
					break;
			}
		}
	}

	return false;
}

bool SceneManager::GetRayCollisionWithMobs(line3df& ray, vector3df& out, Mob** mob)
{
	// check if a ray collides with the given GLObject and if so, where
	GLfloat step = 4.f;
	vector3df stepv = ray.end - ray.start;
	stepv.Normal();
	GLfloat distDone = step;
	vector3df temp = ray.start;
	const vector<Mob*>& mobs = *m_MobManager.GetMobs();
	const size_t mobcount = mobs.size();

	while (distDone < ray.Magnitude())
	{
		temp += stepv * step;

		for (GLuint i = 0; i < mobcount; ++i) {
			Mob* _mob = mobs[i];
			if (_mob && _mob->GetPosition().QuickDistance(temp) <= 80.f)
			{
				out = temp;
				if (mob)
					*mob = const_cast<Mob*>(_mob);

				return true;
			}
		}

		distDone += step;
	}

	return false;
}

vector2df SceneManager::ConvertToScreenCoordinates(const vector3df& pos)
{
	// convert the given 3d coordinates to a 2d screen coordinate
	// get information on the window
	vector2di ScreenSize = GetRoot()->GetScreenSize();

	// create a temporary vector4df
	vector4df temp(pos.x, pos.y, pos.z, 1);

	// transform by view matrix and projection matrix
	temp = m_pCamera->GetMatrix().Multiply(temp);
	temp = GLObject::GetShaderProgram(0)->GetProjectionMatrix().Multiply(temp);

	if (temp.z < 0)
		return vector2df(-1000, -1000);

	// correct the coordinates
	vector2df ret(temp.x, temp.y);

	ret.x = ret.x / temp.z;
	ret.y = ret.y / temp.z;

	ret.x = ScreenSize.x * (ret.x + 1.f) * 0.5f;
	ret.y = ScreenSize.y * (1.f - (ret.y + 1.f) * 0.5f);

	// return the vector
	return ret;
}

bool SceneManager::DeleteObject(GLObject* o)
{
	// find this object in the list and delete it
	list<GLObject*>::iterator it = find(m_Objects.begin(), m_Objects.end(), o);

	if (it != m_Objects.end())
	{
		m_DeadObjects.push_back(o);

		// ensure there is only one of this object in the list
		m_DeadObjects.sort();
		m_DeadObjects.unique();
		//m_Objects.erase(it);
		return true;
	}
	else
	{
		return false;
	}
}

void SceneManager::AddGLObject(GLObject* o)
{
	ProcessObject(o);
}

void SceneManager::DeleteNetworkItem(const ItemData& item)
{
	// find this item and delete it
	list<GLObject*>::iterator it = m_Objects.begin();

	while (it != m_Objects.end())
	{
		Item* i = dynamic_cast<Item*>(*it);
		if (i && i->IsServerItem(item))
		{
			m_DeadObjects.push_back(i);
			m_Objects.erase(it);
			break;
		}

		++it;
	}
}

void SceneManager::CreateNetworkItem(ItemData item)
{
	// push this back in our objects to create list
	m_ItemsToCreate.push_back(item);
}

void SceneManager::PickUpItem(const ItemData& item)
{
	// the server has said "OK you've picked that up" so do so
	if (m_ItemToPickUp && m_ItemToPickUp->IsServerItem(item))
	{
		m_bCanPickUp = true;
	}
}

void SceneManager::GiveItem(const ItemData& item)
{
	// the server has issued a giveitem command, add a new item to the list
	m_ItemsToGive.push_back(item);
}

bool SceneManager::SetItemToPickUp(Item* item)
{
	// if we're not trying to pick up an item, pick up the given item
	if (m_ItemToPickUp == NULL)
	{
		m_ItemToPickUp = item;
		m_PickUpTimeout = 2;
		m_bCanPickUp = false;

		// send the request to the server
		NetworkManager* n = m_pGame->GetNetworkManager();
		if (n)
			n->SendItem(destroy_item, item);

		return true;
	}

	return false;
}

void SceneManager::SetTimeOfDay(GLfloat time)
{
	m_DestSunChange = time;
}

MobManager* SceneManager::GetMobManager()
{
	return &m_MobManager;
}

GLObject* SceneManager::CreateGLObject(vector3df pos)
{
	GLObject* obj = new GLObject(this, pos, vector3di());

	ProcessObject(obj);
	return obj;
}

Player* SceneManager::CreatePlayer(vector3df pos, string filename, string name)
{
	Player *player = new Player(this, filename, name);
	player->SetPosition(pos);
	
	ProcessObject(player);
	return player;
}

Terrain* SceneManager::CreateTerrain()
{
	// there should only ever be one instance of a Terrain at any given time
	Terrain* tmp = GetFirstInstance<Terrain>();
	if (tmp)
	{
		return tmp;
	}

	Terrain* terrain = new Terrain(this);

	ProcessObject(terrain);
	return terrain;
}

Item* SceneManager::CreateItem(string type, vector3df pos, short amount)
{
	Item* item = new Item(this, type, amount, pos);

	ProcessObject(item);
	return item;
}

Item* SceneManager::CreateItem(TerrainBlock blocktype, vector3df pos, short amount)
{
	Item* item = new Item(this, Item::GetItemClass(blocktype)->strName, amount, pos);

	ProcessObject(item);
	return item;
}

Item* SceneManager::CreateItem(const ItemData& data)
{
	Item* item = new Item(this, data.index, data.amount, data.pos);

	ProcessObject(item);
	return item;
}

Item* SceneManager::CreateItem(vector<unsigned char>& in)
{
	// read an item and create one
	union {
		short size;
		unsigned char byte[2];
	} conv;

	// read in length of the name
	conv.byte[0] = in[0];
	conv.byte[1] = in[1];
	short strlen = conv.size;

	// read in the name
	char* name = new char[strlen + 1];
	for (short i = 0; i < strlen; ++i) {
		name[i] = in[2 + i];
	}
	name[strlen] = '\0';

	// read in the stack size
	conv.byte[0] = in[strlen + 2];
	conv.byte[1] = in[strlen + 3];
	short size = conv.size;

	// create the item
	Item* item = new Item(this, string(name), size);

	// clean up memory
	delete[] name;

	// process object and return the new item
	ProcessObject(item);
	return item;
}

Mob* SceneManager::CreateMob(string type, vector3df pos)
{
	Mob* mob = new Mob(this, type, pos);

	ProcessObject(mob);
	return mob;
}

Mob* SceneManager::CreateMob(int type, vector3df pos)
{
	Mob* mob = new Mob(this, type, pos);

	ProcessObject(mob);
	return mob;
}

Billboard* SceneManager::CreateBillboard(vector2df size)
{
	Billboard* b = new Billboard(this, size);

	ProcessObject(b);
	return b;
}

ParticleEmitter* SceneManager::CreateParticleEmitter(vector3df pos, vector3df dir, GLfloat speed, GLuint amount, vector2di psize, GLfloat spread, GLfloat alpha, vector2df size)
{
	ParticleEmitter* pe = new ParticleEmitter(this, pos, dir, speed, amount, psize, spread, alpha, size);

	ProcessObject(pe);
	return pe;
}

ParticleEmitter* SceneManager::CreateParticleEmitter(ParticleDefinition p)
{
	ParticleEmitter* pe = new ParticleEmitter(this, p);

	ProcessObject(pe);
	return pe;
}

Projectile* SceneManager::CreateProjectile(const ProjectileData& data, bool mob)
{
	Projectile* p = new Projectile(this, data, mob);

	ProcessObject(p);
	return p;
}
