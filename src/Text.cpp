#include "Text.h"
#include <sstream>

Text::Text(string text, Font* font, Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size)
	: GUIElement(parent, guiParent, pos, size)
	, m_Font(font)
	, m_bCentered(false)
	, m_bSplitByWord(false)
	, m_LineSpacing(1.5f)
	, m_LineHeight(1)
	, m_bShadowed(false)
	, m_bRightAlign(false)
{
	SetText(text, true);
	
	if (font)
	{
		SetTexture(font->GetTexture());
	}
}

void Text::SetText(string text, bool remake)
{
	m_Text = text;
	
	// if we have a font set then create the buffers
	if (m_Font)
	{
		CreateCharacterBuffers(remake);
	}
}

void Text::AppendText(string text, bool remake)
{
	m_Text.append(text);

	// if we have a font set then create the buffers
	if (m_Font && remake)
	{
		CreateCharacterBuffers(true);
	}
}

void Text::ClearText(bool clearVBO)
{
	m_Text = "";

	if (clearVBO)
	{
		Clear();
		ClearVectors();
	}
}

string Text::GetText()
{
	return m_Text;
}

string Text::GetReadableText()
{
	// get a readable version of the text
	int iLength = GetTextLength();
	char c = m_Text[0];
	GLColour col;
	std::stringstream str;
	for (int i = 0; i < iLength; c = m_Text[++i]) {
		// sort out the colour
		if (c == '^' && m_Text[i + 1] != '\0')
		{
			if (GetColour(col, m_Text[i + 1]))
			{
				++i;
				continue;
			}
		}
		if (c == '%' && m_Text[i + 1] != '\0')
		{
			int j = i + 1;
			while (m_Text[j] != '\0')
			{
				if (m_Text[j] == '\\')
				{
					++j;
					break;
				}

				if (!isdigit(m_Text[j]))
					break;

				++j;
			}
			
			i += (j - i - 1);
			continue;
		}
		if (c == '\n')
		{
			continue;
		}

		str << c;
	}

	// return the string
	return str.str();
}

void Text::GetLineLengths(std::vector<int>& out)
{
	// get the length of each line in this text object
	// used to centre text that has multiple lines
	int iLength = GetTextLength();
	char c = m_Text[0];
	GLColour col;
	std::stringstream str;
	for (int i = 0; i < iLength; c = m_Text[++i]) {
		// sort out the colour
		if (c == '^' && m_Text[i + 1] != '\0')
		{
			if (GetColour(col, m_Text[i + 1]))
			{
				++i;
				continue;
			}
		}
		if (c == '%' && m_Text[i + 1] != '\0')
		{
			int j = i + 1;
			while (m_Text[j] != '\0')
			{
				if (m_Text[j] == '\\')
				{
					++j;
					break;
				}

				if (!isdigit(m_Text[j]))
					break;

				++j;
			}

			i += (j - i - 1);
			continue;
		}
		if (c == '\n')
		{
			out.push_back(m_Font->GetStringWidth(str.str()));
			str.str("");

			continue;
		}

		str << c;
	}

	// add the final line length
	out.push_back(m_Font->GetStringWidth(str.str()));
}

int Text::GetTextLength()
{
	return m_Text.length();
}

void Text::SetLineSpacing(GLfloat spacing)
{
	m_LineSpacing = spacing;
}

GLfloat Text::GetLineSpacing()
{
	return m_LineSpacing;
}

GLuint Text::GetHeight()
{
	return (GLuint)(m_LineHeight * m_Font->GetDetails().z * m_LineSpacing);
}

void Text::SetShadowed(bool shadow)
{
	m_bShadowed = shadow;

	if (m_Font)
	{
		CreateCharacterBuffers(true);
	}
}

bool Text::IsHovered()
{
	return false;
}

void Text::SetFont(Font* font)
{
	m_Font = font;
	
	if (m_Font)
	{
		SetTexture(m_Font->GetTexture());
		CreateCharacterBuffers(true);
	}
}

Font* Text::GetFont()
{
	return m_Font;
}

void Text::CentreText(bool centre)
{
	m_bCentered = centre;

	if (centre)
		m_bRightAlign = false;
	
	if (m_Font)
	{
		CreateCharacterBuffers(false);
	}
}

void Text::AlignRight(bool align)
{
	m_bRightAlign = align;

	if (align)
		m_bCentered = false;

	if (m_Font)
	{
		CreateCharacterBuffers(false);
	}
}

void Text::SplitByWord(bool split)
{
	m_bSplitByWord = split;

	if (m_Font)
	{
		CreateCharacterBuffers(true);
	}
}

void Text::MouseDownFunction(GLuint button)
{
	// do nothing
}

void Text::CreateCharacterBuffers(bool remake)
{
	// sort out the buffers
	if (remake)
	{
		Clear();
	}
	ClearVectors();
	
	// variables for building the buffers
	char c = m_Text[0];
	CharacterDetail cd;
	vector3ds d = m_Font->GetDetails();
	GLint n = 0, t = 0;
	GLfloat y = 0;
	GLColour col = GLColour(255, 255, 255, 255);
	GLint width = GetSize().x;
	std::stringstream word;
	m_LineHeight = 1;
	std::vector<int> lineLengths;
	GetLineLengths(lineLengths);
	
	// centre the text
	if (m_bCentered)
	{
		n = (width / 2) - (lineLengths[0] / 2);
	}
	else if (m_bRightAlign)
	{
		string text = GetReadableText();
		n = width - m_Font->GetStringWidth(text);
	}
	
	for (GLuint i = 0; c != '\0' && i < m_Text.length(); c = m_Text[++i]) {
		// get the character details
		cd = m_Font->GetChar(c);
		
		// sort out the colour
		if (c == '^' && m_Text[i + 1] != '\0')
		{
			if (GetColour(col, m_Text[i + 1]))
			{
				++i;
				continue;
			}
		}
		if (c == '%' && m_Text[i + 1] != '\0')
		{
			char a = m_Text[i + 1];
			if (isdigit(a))
			{
				std::stringstream s;
				int j = i + 1;

				while (m_Text[j] != '\0')
				{
					if (m_Text[j] == '\\')
					{
						++j;
						break;
					}
					
					if (!isdigit(m_Text[j]))
						break;

					s << m_Text[j];
					++j;
				}

				n = stoi(s.str());
				i += (j - i - 1);
				continue;
			}
		}

		if (c != '\n')
			word << c;

		if ((c == ' ' || m_Text[i + 1] == '\0') && m_bSplitByWord)
		{
			for (GLuint j = 0; j < word.str().length(); ++j) {
				CharacterDetail _cd = m_Font->GetChar(word.str()[j]);
				if (m_bShadowed)
					AddQuad(vector2df((GLfloat)n + 2, (GLfloat)(y * d.z) + 2), vector2df((float)(_cd.charX) / d.x, 1 - (float)(_cd.charY + d.z) / d.y),
						vector2df((GLfloat)(n + _cd.charWidth) + 2, (GLfloat)((y + 1) * d.z) + 2), vector2df((float)(_cd.charX + _cd.charWidth) / d.x, 1 - (float)(_cd.charY) / d.y), GLColour(0, 190));
				AddQuad(vector2df((GLfloat)n, (GLfloat)(y * d.z)), vector2df((float)(_cd.charX) / d.x, 1 - (float)(_cd.charY + d.z) / d.y),
					vector2df((GLfloat)(n + _cd.charWidth), (GLfloat)((y + 1) * d.z)), vector2df((float)(_cd.charX + _cd.charWidth) / d.x, 1 - (float)(_cd.charY) / d.y), col);

				n += _cd.charWidth;
			}

			t = n;
			word.str(std::string());
			word.clear();
		}
		if (c == '\n' || t + cd.charWidth > width)
		{
			y += m_LineSpacing;
			++m_LineHeight;
			n = 0;
			t = 0;

			if (m_bCentered)
			{
				int _where = min(lineLengths.size() - 1, static_cast<size_t>(m_LineHeight - 1));
				n = (width / 2) - (lineLengths[_where] / 2);
			}

			if (c == '\n' || (c == ' ' && !m_bSplitByWord))
			{
				if (i + 1 >= m_Text.length())
					break;

				continue;
			}
		}
		
		// add the quad
		if (!m_bSplitByWord)
		{
			if (m_bShadowed)
				AddQuad(vector2df((GLfloat)n + 2, (GLfloat)(y * d.z) + 2), vector2df((float)(cd.charX) / d.x, 1 - (float)(cd.charY + d.z) / d.y),
					vector2df((GLfloat)(n + cd.charWidth) + 2, (GLfloat)((y + 1) * d.z) + 2), vector2df((float)(cd.charX + cd.charWidth) / d.x, 1 - (float)(cd.charY) / d.y), GLColour(0, 190));
			AddQuad(vector2df((GLfloat)n, (GLfloat)(y * d.z)), vector2df((float)(cd.charX) / d.x, 1 - (float)(cd.charY + d.z) / d.y),
				vector2df((GLfloat)(n + cd.charWidth), (GLfloat)((y + 1) * d.z)), vector2df((float)(cd.charX + cd.charWidth) / d.x, 1 - (float)(cd.charY) / d.y), col);

			// go to the next character
			n += cd.charWidth;
		}

		t += cd.charWidth;
	}

	if (remake)
	{
		CreateBufferObjects();
	}
	else
	{
		UpdateVBO();
	}
}

bool Text::GetColour(GLColour& col, char c)
{
	switch (c)
	{
		case '0':
			col.r = 0; col.g = 0; col.b = 0;
			break;
		case '1':
			col.r = 255; col.g = 0; col.b = 0;
			break;
		case '2':
			col.r = 0; col.g = 255; col.b = 0;
			break;
		case '3':
			col.r = 0; col.g = 0; col.b = 255;
			break;
		case '4':
			col.r = 255; col.g = 0; col.b = 255;
			break;
		case '5':
			col.r = 0; col.g = 255; col.b = 255;
			break;
		case '6':
			col.r = 255; col.g = 255; col.b = 0;
			break;
		case '7':
			col.r = 255; col.g = 255; col.b = 255;
			break;
		case '8':
			col.r = 125; col.g = 125; col.b = 125;
			break;
		case '9':
			col.r = 75; col.g = 75; col.b = 75;
			break;
		default:
			return false;
			break;
	}
	
	return true;
}
