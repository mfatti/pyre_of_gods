#include "DropDownMenu.h"
#include "GUIManager.h"

DropDownMenu::DropDownMenu(Manager* parent, GUIElement* guiParent, vector3df pos, vector3di size, GLfloat height)
	: GUIElement(parent, guiParent, pos, size)
	, m_bSwitched(false)
{
	// create the quads
	//AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(0, 0, 0, 200));
	AddQuad(GLVertex(vector3df(2, 2, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x - 2, 2, -20), GLColour(30), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df((GLfloat)size.x - 2, (GLfloat)size.y - 2, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()),
		GLVertex(vector3df(2, (GLfloat)size.y - 2, -20), GLColour(10), vector2df(0.5, 0.5), GLNormal()));
	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(0, 0, 0, 0));

	AddQuad(vector2df(), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df(0, (GLfloat)size.y - 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df(0, 1), vector2df(0.5, 0.5), vector2df(1, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));
	AddQuad(vector2df((GLfloat)size.x - 1, 1), vector2df(0.5, 0.5), vector2df((GLfloat)size.x, (GLfloat)size.y - 1), vector2df(0.5, 0.5), GLColour(51, 51, 51, 255));

	Add2DTriangle(vector2df((GLfloat)size.x - 13, 7), vector2df(0.5, 0.5), vector2df((GLfloat)size.x - 6, 7), vector2df(0.5, 0.5), vector2df((GLfloat)size.x - 9.5f, 14), vector2df(0.5, 0.5), GLColour(255, 255, 255, 100));
	CreateBufferObjects();

	// create the text
	m_Selected = GetGUIManager()->CreateText("", vector2df(5, 2), vector2di(size.x, -1), NULL, this);

	// create the listbox item
	m_ListBox = GetGUIManager()->CreateListBox(vector2df(0, (GLfloat)size.y - 1), vector2di(size.x, (GLint)height), this);
	m_ListBox->SetVisible(false);
	m_ListBox->SetFunction(left_mouse, DropDownMenu::ListBoxFunction);
	m_ListBox->SetDepth(m_Depth + 1);
	m_ListBox->SetBackgroundAlpha(255);

	// set the default click action
	SetFunction(left_mouse, DropDownMenu::DefaultFunction);
}

void DropDownMenu::Update(bool bForce)
{
	// if we aren't visible then don't update anything
	if (!bForce && !IsVisible() && !IsAnimating())
		return;

	// hovering
	if (IsHovered() && !m_bSwitched)
	{
		ChangeQuadColour(1, GLColour(64, 0, 0, 100));
		m_bSwitched = true;
	}
	else if (!IsHovered() && m_bSwitched)
	{
		ChangeQuadColour(1, GLColour(0, 0, 0, 0));
		m_bSwitched = false;
	}

	// call GUIElement::Update()
	GUIElement::Update(bForce);
}

bool DropDownMenu::IsHovered()
{
	if (!GUIElement::IsHovered())
		return m_ListBox->IsHovered();

	return true;
}

void DropDownMenu::MouseDownFunction(GLuint button)
{
	// do nothing
}

void DropDownMenu::Release(GLuint button)
{
	// call base class release
	GUIElement::Release(button);

	// if we click outside of the dropdown menu close it
	if (!IsHovered() && m_ListBox->IsVisible() && !m_ListBox->IsHovered())
		m_ListBox->SetVisible(false);
}

void DropDownMenu::AddItem(string text, bool remake)
{
	m_ListBox->AddItem(text, remake);

	if (m_Selected->GetText() == "")
		m_Selected->SetText(text, true);
}

string DropDownMenu::GetSelected()
{
	return m_Selected->GetText();
}

GLuint DropDownMenu::GetSelectedNumber()
{
	return m_ListBox->GetSelected();
}

void DropDownMenu::ClearList()
{
	m_ListBox->ClearList();
	m_Selected->ClearText();
}

void DropDownMenu::SetListBoxFunction(void(*fn)(GUIElement*))
{
	m_ListBox->SetFunction(left_mouse, fn);
}

void DropDownMenu::SetListBoxUpdateFunction(void(*fn)(GUIElement*))
{
	m_ListBox->SetUpdateFunction(fn);
}

void DropDownMenu::SetDepth(GLfloat depth)
{
	GUIElement::SetDepth(depth);
	m_ListBox->SetDepth(depth + 1);
}

bool DropDownMenu::ItemExists(string item)
{
	return m_ListBox->ItemExists(item);
}

GLuint DropDownMenu::GetFirstItem(string item)
{
	return DropDownMenu::m_ListBox->GetFirstItem(item);
}

void DropDownMenu::Select(GLuint i)
{
	m_ListBox->Select(i);
	m_ListBox->SetVisible(false);
	m_Selected->SetText(m_ListBox->GetSelectedText(), true);
}

void DropDownMenu::DefaultFunction(GUIElement* e)
{
	// what happens when we click on the drop down menu
	DropDownMenu* menu = dynamic_cast<DropDownMenu*>(e);

	if (menu)
	{
		menu->m_ListBox->SetVisible(!menu->m_ListBox->IsVisible());
	}
}

void DropDownMenu::ListBoxFunction(GUIElement* e)
{
	// call listbox::defaultfunction to ensure we select an option
	ListBox::DefaultFunction(e);

	DropDownMenu* menu = dynamic_cast<DropDownMenu*>(e->GetParent());

	if (menu)
	{
		menu->m_ListBox->SetVisible(false);
		menu->m_Selected->SetText(menu->m_ListBox->GetSelectedText(), true);
	}
}