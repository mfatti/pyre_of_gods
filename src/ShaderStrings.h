#ifndef _SHADER_STRINGS_H_
#define _SHADER_STRINGS_H_

#include "Misc.h"

class ShaderStrings {
	public:
		ShaderStrings(const ShaderStrings&) = delete;
		void operator=(const ShaderStrings&) = delete;

		static const GLchar* GetVertexShader(bool bAnimated, bool bPerPixel, bool bNormalMapping, bool bShadows);
		static const GLchar* GetFragmentShader(bool bAnimated, bool bPerPixel, bool bNormalMapping, bool bShadows);

	protected:

	private:
		ShaderStrings();

		static ShaderStrings& Instance();

		// vertex shaders
		const GLchar* m_VertPlainShader;
		const GLchar* m_VertShadowShader;
		const GLchar* m_VertPerPixelShader;
		const GLchar* m_VertNormalMappedShader;
		const GLchar* m_VertNormalMappedShadowsShader;
		const GLchar* m_VertPerPixelShadowsShader;
		const GLchar* m_AnimVertPlainShader;
		const GLchar* m_AnimVertShadowShader;
		const GLchar* m_AnimVertPerPixelShader;
		const GLchar* m_AnimVertNormalMappedShader;
		const GLchar* m_AnimVertNormalMappedShadowsShader;
		const GLchar* m_AnimVertPerPixelShadowsShader;

		// fragment shaders
		const GLchar* m_FragPlainShader;
		const GLchar* m_FragShadowShader;
		const GLchar* m_FragPerPixelShader;
		const GLchar* m_FragNormalMappedShader;
		const GLchar* m_FragNormalMappedShadowsShader;
		const GLchar* m_FragPerPixelShadowsShader;
		const GLchar* m_AnimFragPlainShader;
		const GLchar* m_AnimFragShadowShader;
		const GLchar* m_AnimFragPerPixelShader;
		const GLchar* m_AnimFragNormalMappedShader;
		const GLchar* m_AnimFragNormalMappedShadowsShader;
		const GLchar* m_AnimFragPerPixelShadowsShader;
};

#endif