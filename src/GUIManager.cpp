#include "GUIManager.h"

GUIManager::GUIManager(Root* root, GLFWwindow* window, TextureManager* texMan)
	: Manager(root, texMan)
	, m_DefaultFont(NULL)
	, m_pWindow(window)
	, m_CharCallback(NULL)
	, m_Menu(NULL)
	, m_LastKey(0)
{
	// get the default texture
	m_Texture = texMan->GetTexture("gui.png");
}

GUIManager::~GUIManager()
{
	CleanUp();
}

void GUIManager::SetWindow(GLFWwindow* window)
{
	m_pWindow = window;
}

void GUIManager::CleanUp()
{
	list<GUIElement*>::iterator it = m_Elements.begin();
	while (it != m_Elements.end())
	{
		delete (*it);
		++it;
	}
	m_Elements.clear();
	
	map<string, Font*>::iterator fit = m_Fonts.begin();
	while (fit != m_Fonts.end())
	{
		delete fit->second;
		++fit;
	}
	m_Fonts.clear();
	m_DefaultFont = NULL;
	m_Menu = NULL;
}

void GUIManager::Update()
{
	// update the current cursor position
	glfwGetCursorPos(m_pWindow, &m_CursorPos.x, &m_CursorPos.y);

	// iterate through everything in the dead GUI element list and kill it
	list<GUIElement*>::iterator it = m_DeadElements.begin();
	while (it != m_DeadElements.end())
	{
		list<GUIElement*>::iterator _it = find(m_Elements.begin(), m_Elements.end(), *it);
		if (_it != m_Elements.end())
			m_Elements.erase(_it);
		
		(*it)->Kill();
		m_DeadElements.erase(it);

		it = m_DeadElements.begin();
	}
	
	// iterate through everything in the GUI and update it
	it = m_Elements.begin();
	while (it != m_Elements.end())
	{		
		(*it)->Update();

		++it;
	}
}

void GUIManager::Render()
{
	// use the correct shader program
	GLObject::GetShaderProgram(1)->Bind();
	
	// iterate through everything in the GUI and draw it
	vector2di ScreenSize = GetRoot()->GetScreenSize();
	GLfloat screen[4] = { 0, 0, (GLfloat)ScreenSize.x, (GLfloat)ScreenSize.y };
	ReverseElements();
	list<GUIElement*>::iterator it = m_Elements.begin();
	while (it != m_Elements.end())
	{
		glUniform4fv(GUIElement::GetSizeUniPosition(), 1, screen);
		(*it)->Render();
		++it;
	}
	ReverseElements();
}

bool GUIManager::MouseRelease(GLuint button)
{
	// have we registered an event?
	bool bEvent(false);

	// failsafe - make sure the mouse is actually inside the window
	vector2di ScreenSize = GetRoot()->GetScreenSize();
	if (m_CursorPos.x < 0 || m_CursorPos.x > ScreenSize.x || m_CursorPos.y < 0 || m_CursorPos.y > ScreenSize.y)
	{
		// we return true just so no more handling can be done elsewhere
		return true;
	}
	
	// iterate through everything in the GUI and if it is hovered, execute its function
	list<GUIElement*>::iterator it = m_Elements.begin();
	while (it != m_Elements.end())
	{
		GUIElement* e = (*it);
		if (!bEvent && e->IsPressed(button) || e->IsAnyChildPressed(button))
		{
			if (e->MouseRelease(button))
			{
				e->Release(button);
				bEvent = true;
				//return true;
			}
		}
		e->Release(button);
		++it;
	}
	
	// no GUI element has been clicked so return false
	return bEvent;
}

bool GUIManager::MouseDown(GLuint button)
{
	// iterate through everything in the GUI
	list<GUIElement*>::iterator it = m_Elements.begin();
	while (it != m_Elements.end())
	{
		GUIElement* e = (*it);
		if (e->MouseDown(button))
			return true;
		++it;
	}
	
	// no GUI element has been clicked so return false
	return false;
}

bool GUIManager::KeyRelease(int key)
{
	// set the last key
	m_LastKey = key;
	
	// iterate through everything in the GUI
	list<GUIElement*>::iterator it = m_Elements.begin();
	while (it != m_Elements.end())
	{
		GUIElement* e = (*it);
		if (e->IsHovered())
		{
			return e->KeyRelease(key);
		}
		++it;
	}

	// no GUI element has been key released so return false
	return false;
}

bool GUIManager::KeyPress(int key)
{
	// iterate through everything in the GUI
	list<GUIElement*>::iterator it = m_Elements.begin();
	while (it != m_Elements.end())
	{
		GUIElement* e = (*it);
		if (e->IsHovered())
		{
			return e->KeyPress(key);
		}
		++it;
	}

	// no GUI element has been pressed so return false
	return false;
}

bool GUIManager::IsHovered()
{
	list<GUIElement*>::iterator it = m_Elements.begin();
	while (it != m_Elements.end())
	{
		GUIElement* e = (*it);
		if (e->IsHovered())
		{
			return true;
		}
		++it;
	}

	return false;
}

int GUIManager::GetKeyState(int key)
{
	return glfwGetKey(m_pWindow, key);
}

int GUIManager::GetMouseState(int button)
{
	return glfwGetMouseButton(m_pWindow, button);
}

bool GUIManager::KillElement(GUIElement* e)
{
	// find this element in the list and kill it
	list<GUIElement*>::iterator it = find(m_Elements.begin(), m_Elements.end(), e);
	
	if (it != m_Elements.end())
	{
		m_DeadElements.push_back(e);
		//m_Elements.erase(it);
		return true;
	}
	else
	{
		return false;
	}
}

Font* GUIManager::GetFont(const char* fileName)
{
	// if the font has already been loaded return it
	if (m_Fonts[fileName])
	{
		return m_Fonts[fileName];
	}
	
	// otherwise we load the font
	Font* font = new Font(fileName, GetTextureManager());
	m_Fonts[fileName] = font;
	return m_Fonts[fileName];
}

void GUIManager::SetDefaultFont(Font* font)
{
	m_DefaultFont = font;
}

const Font* GUIManager::GetDefaultFont()
{
	return m_DefaultFont;
}

Font* GUIManager::SortFont(Font* font)
{
	Font* fnt = NULL;
	if (!font)
	{
		fnt = const_cast<Font*>(GetDefaultFont());
		
		if (!fnt)
		{
			cout << "ERROR: No default font and no font provided! Cannot create text!" << endl;
			return NULL;
		}
	}
	else
	{
		fnt = font;
	}
	
	return fnt;
}

vector2dd& GUIManager::GetMousePosition()
{
	return m_CursorPos;
}

bool ElementCompare(GUIElement* one, GUIElement* two)
{
	return (*one < *two);//(one->GetDepth() < two->GetDepth());
}

void GUIManager::Sort()
{
	// sort elements by depth
	m_Elements.sort(ElementCompare);
}

void GUIManager::SetCharCallback(TextBox* tb)
{
	m_CharCallback = tb;
}

TextBox* GUIManager::GetCharCallback()
{
	return m_CharCallback;
}

bool GUIManager::IsKeyboardFree()
{
	return (m_CharCallback == NULL || !m_CharCallback->IsVisible());
}

void GUIManager::CharCallback(GLuint codepoint)
{
	if (m_CharCallback && m_CharCallback->IsVisible())
		m_CharCallback->TextEntered(codepoint);
}

GLint GUIManager::GetLastKey()
{
	return m_LastKey;
}

Menu* GUIManager::GetMenu()
{
	return m_Menu;
}

GLFWwindow* GUIManager::GetWindow()
{
	return m_pWindow;
}

// creation methods
Text* GUIManager::CreateText(const char* text, vector2df pos, vector2di size, Font* font, GUIElement* parent)
{
	// get the font
	Font* fnt = SortFont(font);
	if (!fnt)
	{
		return NULL;
	}
	
	// create a new text object
	Text* t = new Text(text, fnt, this, parent, pos, size);
	
	// process this object
	ProcessObject(t, parent, false);
	
	// finally return the new object
	return t;
}

Button* GUIManager::CreateButton(const char* text, vector2df pos, vector2di size, void (*fn)(GUIElement*), Font* font, GUIElement* parent)
{
	// get the font
	Font* fnt = SortFont(font);
	if (!fnt)
	{
		return NULL;
	}
	
	// create a new button object
	Button* b = new Button(text, fnt, this, parent, pos, size);
	b->SetFunction(left_mouse, fn);
	
	// process this object
	ProcessObject(b, parent);
	
	// finally return the new object
	return b;
}

Pane* GUIManager::CreatePane(const char* text, vector2df pos, vector2di size, Font* font, GUIElement* parent, GLubyte alpha, bool movable)
{
	// get the font
	Font* fnt = SortFont(font);
	if (!fnt)
	{
		return NULL;
	}
	
	// create a new pane object
	Pane* p = new Pane(text, fnt, this, parent, pos, size, alpha, movable);
	
	// process this object
	ProcessObject(p, parent);
	
	// finally return the new object
	return p;
}

MeshView* GUIManager::CreateMeshView(Mesh* mesh, string texture, vector2df pos, vector2di size, GUIElement* parent)
{
	// create a new mesh view object
	MeshView *mv = new MeshView(mesh, texture, this, parent, pos, size);

	// process this object
	ProcessObject(mv, parent, false);

	// finally return the new object
	return mv;
}

Inventory* GUIManager::CreateInventory(vector2df pos, vector2di size, vector<Item*>* inventory, GLuint slots, bool bBackground, GUIElement* parent)
{
	// create an inventory
	Inventory* inv = new Inventory(this, parent, inventory, slots, pos, size, bBackground);

	// process this object
	ProcessObject(inv, parent);

	// finally return the new object
	return inv;
}

Image* GUIManager::CreateImage(string texture, vector2df pos, vector2di size, GLColour col, GUIElement* parent)
{
	// create an image
	Image* im = new Image(texture, this, parent, pos, size, col);

	// process this object
	ProcessObject(im, parent, false);

	// finally return the new object
	return im;
}

TabManager* GUIManager::CreateTabManager(vector2df pos, vector2di size, GUIElement* parent)
{
	// create a tab manager
	TabManager* tm = new TabManager(this, parent, pos, size);

	// process this object
	ProcessObject(tm, parent);

	// finally return the new object
	return tm;
}

ListBox* GUIManager::CreateListBox(vector2df pos, vector2di size, GUIElement* parent)
{
	// create a text list
	ListBox* lb = new ListBox(this, parent, pos, size);

	// process this object
	ProcessObject(lb, parent);

	// finally return the new object
	return lb;
}

Scrollbar* GUIManager::CreateScrollbar(GUIElement* scrollee, vector2df pos, vector2di size, GLubyte mode, GUIElement* parent)
{
	// create a scrollbar
	Scrollbar* sb = new Scrollbar(scrollee, this, parent, pos, size, mode);

	// process this object
	ProcessObject(sb, parent);

	// finally return the new object
	return sb;
}

DropDownMenu* GUIManager::CreateDropDownMenu(vector2df pos, vector2di size, GLfloat height, GUIElement* parent)
{
	// create a drop down menu
	DropDownMenu* ddm = new DropDownMenu(this, parent, pos, size, height);

	// process this object
	ProcessObject(ddm, parent);

	// finally return the new object
	return ddm;
}

TextBox* GUIManager::CreateTextBox(string text, vector2df pos, vector2di size, string hint, GUIElement* parent)
{
	// create a drop down menu
	TextBox* tb = new TextBox(text, this, parent, pos, size);
	if (hint != "")
		tb->SetHint(hint);

	// process this object
	ProcessObject(tb, parent);

	// finally return the new object
	return tb;
}

ScrollPane* GUIManager::CreateScrollPane(vector2df pos, vector2di size, GLubyte mode, GUIElement* parent)
{
	// create a scrollable pane
	ScrollPane* sp = new ScrollPane(this, parent, pos, size, mode);

	// process this object
	ProcessObject(sp, parent);

	// finally return the new object
	return sp;
}

Ingredient* GUIManager::CreateIngredient(vector2df pos, const ItemClass* ic, GLuint amount, GLuint amountOwned, GUIElement* parent)
{
	// create an ingredient
	Ingredient* i = new Ingredient(this, parent, pos, ic, amount, amountOwned);

	// process this object
	ProcessObject(i, parent);

	// finally return the new object
	return i;
}

Menu* GUIManager::CreateMenu(vector2di size)
{
	// create a menu
	Menu* m = new Menu(this, size);
	m->SetDepth(90);

	// process this object
	ProcessObject(m, NULL);

	// set this as the menu object
	m_Menu = m;

	// finally return the new object
	return m;
}

GroupBox* GUIManager::CreateGroupBox(vector2df pos, vector2di size, string label, GUIElement* parent)
{
	// create a groupbox
	GroupBox* gb = new GroupBox(this, parent, pos, size, label);

	// process this object
	ProcessObject(gb, parent);

	// finally return the new object
	return gb;
}

CheckBox* GUIManager::CreateCheckBox(vector2df pos, vector2di size, string label, GUIElement* parent)
{
	// create a checkbox
	CheckBox* cb = new CheckBox(this, parent, pos, size, label);

	// process this object
	ProcessObject(cb, parent);

	// finally return the new object
	return cb;
}

Table* GUIManager::CreateTable(vector2df pos, vector2di size, GLuint columncount, GUIElement* parent)
{
	// create a table
	Table* t = new Table(this, parent, pos, size, columncount);

	// process this object
	ProcessObject(t, parent);

	// finally return the new object
	return t;
}

ProgressBar* GUIManager::CreateProgressBar(vector2df pos, vector2di size, GUIElement* parent)
{
	// create a progressbar
	ProgressBar* p = new ProgressBar(this, parent, pos, size);

	// process this object
	ProcessObject(p, parent);

	// finally return the new object
	return p;
}

ChatText* GUIManager::CreateChatText(GLuint maxlines, vector2df pos, vector2di size, Font* font, GUIElement* parent)
{
	// get the font
	Font* fnt = SortFont(font);
	if (!fnt)
	{
		return NULL;
	}

	// create a new chattext object
	ChatText* ct = new ChatText(maxlines, fnt, this, parent, pos, size);

	// process this object
	ProcessObject(ct, parent, false);

	// finally return the new object
	return ct;
}

OKDialog* GUIManager::ShowOKDialog(string title, string text, void(*fn)(GUIElement*))
{
	// create an OK dialog
	OKDialog* ok = new OKDialog(this, title, text, fn);
	ok->SetDepth(100);

	// process this object
	ProcessObject(ok, NULL);

	// finally return the new object
	return ok;
}

YesNoDialog* GUIManager::ShowYesNoDialog(string title, string text, void(*fny)(GUIElement*), void(*fnn)(GUIElement*))
{
	// create a Yes/No dialog
	YesNoDialog* yn = new YesNoDialog(this, title, text, fny, fnn);
	yn->SetDepth(100);

	// process this object
	ProcessObject(yn, NULL);

	// finally return the new object
	return yn;
}

TextBoxDialog* GUIManager::ShowTextBoxDialog(string title, string text, void(*fny)(GUIElement*), void(*fnn)(GUIElement*))
{
	// create a textbox dialog
	TextBoxDialog* tb = new TextBoxDialog(this, title, text, fny, fnn);
	tb->SetDepth(100);

	// process this object
	ProcessObject(tb, NULL);

	// finally return the new object
	return tb;
}

ServerDetailsDialog* GUIManager::ShowServerDetailsDialog(string title, string text1, string text2, string text3, void(*fny)(GUIElement*), void(*fnn)(GUIElement*))
{
	// create a server details dialog
	ServerDetailsDialog* sd = new ServerDetailsDialog(this, title, text1, text2, text3, fny, fnn);
	sd->SetDepth(100);

	// process this object
	ProcessObject(sd, NULL);

	// finally return the new object
	return sd;
}

KeyChange* GUIManager::ShowKeyChange(string control)
{
	// create a key change dialog
	KeyChange* k = new KeyChange(this, control);
	k->SetDepth(100);

	// process this object
	ProcessObject(k, NULL);

	// finally return the new object
	return k;
}

Label* GUIManager::ShowLabel(string title, string text, GUIElement* e)
{
	// create a label
	Label* l = new Label(this, title, text, e);
	l->SetDepth(50);
	l->SetAlpha(0);
	l->FadeTo(1);

	// process this object
	ProcessObject(l, NULL);

	// finally return the new object
	return l;
}