#ifndef _NETWORK_MANAGER_H_
#define _NETWORK_MANAGER_H_

#include "Misc.h"

#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#else
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

#include <string>

#define BUFFER_SIZE 512
#define HEADER_SIZE 6
#define ACK_MSG_COUNT 20

// in an attempt to keep the code as similar between platforms as possible, define stuff here so I don't have to change the code much
#ifdef __linux
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

typedef int SOCKET;

#endif

class Game;
class Item;

// message header struct
#pragma pack(1)
struct MessageHeader {
	unsigned char info;
	unsigned char message;
	unsigned int counter;

	MessageHeader() : info(0), message(0), counter(0) {}
	MessageHeader(const MessageHeader& other) : info(other.info), message(other.message), counter(other.counter) {}
	bool operator==(const MessageHeader& other) { return (message == other.message && counter == other.counter); }
	MessageHeader& operator=(const MessageHeader& other) { info = other.info; message = other.message; counter = other.counter; return *this; }

	static unsigned char* ToBuffer(MessageHeader& header) { return reinterpret_cast<unsigned char*>(&header); }
	static MessageHeader& FromBuffer(unsigned char* buffer) { return *reinterpret_cast<MessageHeader*>(buffer); }
};
#pragma pack()

// ack message struct
struct AckMsg {
	bool isfree;
	unsigned char* buf;
	int len;
	int count;
	MessageHeader header;

	bool Compare(const MessageHeader& other)
	{
		return (header == other);
	}
};

// chunk data struct
struct ChunkData {
	std::vector<unsigned char> data;
	bool created;
	int count;

	ChunkData() : created(false), count(0) {}
};

typedef std::map<vector3di, ChunkData> TerrainData;

enum {
	server_packet,
	new_connection,
	disconnect,
	player_data,
	player_hotbar_data,
	player_equipment_data,
	player_stat_data,
	player_action,
	terrain_block_data,
	terrain_water_data,
	change_block,
	create_item,
	destroy_item,
	chat_message,
	block_interaction,
	block_inventory,
	use_item,
	mob_updates,
	new_projectile,

	max_message_number
};

enum {
	sp_server_update,
	sp_reset_counter,
	sp_kick_player,
	sp_give_item,
	sp_move_player,
	sp_knockback,
	sp_new_biome
};

enum {
	player_rolled
};

class NetworkManager {
	public:
		NetworkManager(Game* game, std::string ip, short port);
		~NetworkManager();

		void Finish();

		bool SendPacket(unsigned char type, unsigned char* buf, int size, bool ack = false, bool req = false);
		bool SendAck(unsigned char* buf, int size = 0);

		bool GetTerrainData(const vector3di& pos, std::vector<unsigned char>& out, bool request = true);
		bool GetWaterData(const vector3di& pos, std::vector<unsigned char>& out, bool request = true);
		void SaveTerrainData(const vector3di& pos, unsigned char** buf, int len);
		void SaveWaterData(const vector3di& pos, unsigned char** buf, int len);
		void StartPlayerUpdateThread();

		int GetClientID();

		void SendPlayerInventory(int type, vector<Item*>* inv);
		void SendPlayerStats();
		void SendPlayerAction(int type);
		void SendItem(unsigned char type, Item* item);

		string GetPlayerName();

	protected:

	private:
		Game*				m_pGame;

		SOCKET				m_Socket;
		struct sockaddr_in	m_Server;
		std::string			m_IPAddress;
		short				m_PortNumber;
		static bool			m_Initialised;
		int					m_ClientID;

		std::thread*		m_pConnectThread;
		std::thread*		m_pListenThread;
		std::thread*		m_pPlayerUpdateThread;
		bool				m_ReplyReceived;
		bool				m_CanRequestChunkData;

		vector<GLuint>		m_RxCounters;
		vector<GLuint>		m_TxCounters;

		TerrainData			m_ChunkBuffers;
		TerrainData			m_WaterBuffers;

		AckMsg				m_AckMessages[ACK_MSG_COUNT];

		AckMsg* FindFreeAckMsg();
		void AddAckMsg(const MessageHeader& header, unsigned char* buffer, int len);
		void AcknowledgeMessage(const MessageHeader& other);

		GLuint NextTxCounter(int message);
		GLuint GetTxCounter(int message) const;
		bool SetTxCounter(int message, GLuint counter);
		void ResetTxCounter(int message);
		GLuint GetRxCounter(int message) const;
		bool SetRxCounter(int message, GLuint counter);
		void ResetRxCounter(int message);

		static void Listen(NetworkManager* n);

		static void WaitForConnectionReply(NetworkManager* n);
		static void ConnectionTimeout(NetworkManager* n);
		static void SendPlayerData(NetworkManager* n);
};

#endif
