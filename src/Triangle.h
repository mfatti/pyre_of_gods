#ifndef _TRIANGLE_H_
#define _TRIANGLE_H_

#include "Line.h"

template<typename T>
class Triangle3 {
	public:
		Triangle3() : p1(), p2(), p3() {}
		Triangle3(const Vector3<T>& one, const Vector3<T>& two, const Vector3<T>& three) : p1(one), p2(two), p3(three) {}

		bool operator==(const Triangle3<T>& other) const { return (p1 == other.p1 && p2 == other.p2 && p3 == other.p3); }
		bool operator!=(const Triangle3<T>& other) const { return !(*this == other); }

		Vector3<T> Normal() { return (p2 - p1).CrossProduct(p3 - p1); }

		bool IsPointInside(Vector3<T>& p)
		{
			return (SameSide(p, p1, p2, p3) && SameSide(p, p2, p1, p3) && SameSide(p, p3, p1, p2));
		}
		bool IntersectOfPlaneWithLine(Vector3<T>& p, Vector3<T>& l, Vector3<T>& out)
		{
			Vector3<T> normal = Normal().Normal();
			T intersect = normal.Dot(l);

			if (intersect == 0)
				return false;

			T d = p1.Dot(normal);
			T t = -(normal.Dot(p) - d) / intersect;
			out = p + (l * t);
			return true;
		}
		bool IntersectWithLine(Vector3<T>& p, Vector3<T>& l, Vector3<T>& out)
		{
			if (IntersectOfPlaneWithLine(p, l, out))
				return IsPointInside(out);

			return false;
		}

		Vector3<T> p1, p2, p3;
	protected:

	private:
		bool SameSide(Vector3<T>& p1, Vector3<T>& p2, Vector3<T>& a, Vector3<T>& b) const
		{
			Vector3<T> bminusa = b - a;
			Vector3<T> cp1 = bminusa.CrossProduct(p1 - a);
			Vector3<T> cp2 = bminusa.CrossProduct(p2 - a);
			T res = cp1.Dot(cp2);
			if (res < 0)
			{
				Vector3<T> cp1 = bminusa.Normal().CrossProduct((p1 - a).Normal());
				if (abs(cp1.x) < 0.00000001 && abs(cp1.y) < 0.00000001 && abs(cp1.z) < 0.00000001)
				{
					res = 0;
				}
			}
			return (res >= 0.0f);
		}
};

typedef Triangle3<GLint>	triangle3di;
typedef Triangle3<GLshort>	triangle3ds;
typedef Triangle3<GLfloat>	triangle3df;
typedef Triangle3<GLdouble>	triangle3dd;

#endif