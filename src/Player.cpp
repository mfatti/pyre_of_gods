#include "Player.h"
#include "Camera.h"
#include "SceneManager.h"
#include "Game.h"
#include "GameGUI.h"
#include "zlib.h"
#include <sstream>
#include <fstream>

Player::Player(Manager* parent, string filename, string name)
	: GLObject(parent, vector3df(), vector3di(32, 48, 0))
	, m_fMoveSpeed(32.f)
	, m_SelectedItem(0)
	, m_PlacementOffset()
	, m_pGUI(NULL)
	, m_FadeHeight(0)
	, m_Animation(NULL)
	, m_ToolAnimation(NULL)
	, m_TargetBlockClass(NULL)
	, m_SelectedItemClass(NULL)
	, m_bJump(false)
	, m_bFalling(false)
	, m_bRolling(false)
	, m_bSprinting(false)
	, m_Name(name)
	, m_FileName(filename)
	, m_bSwinging(false)
	, m_fKnockbackSpeed(0)
	, m_bBlockInventoryOpen(false)
	, m_AmmoDecrease(NULL)
	, m_bAmmoUpdated(true)
	, m_fLookFactor(1.f)
	, m_fViewRot(0.f)
	, m_fViewRotFactor(0.f)
{
	// load the player's texture file
	SetTexture("bronze_anvil.png");
	m_BlockBreakTexture = GetSceneManager()->GetTextureManager()->GetTexture("block_break.png");

	//SetMesh(GetSceneManager()->LoadMeshFromAOBJ("media/bendy_cube.aobj"));
	//SetMesh(GetSceneManager()->LoadMeshFromOBJ("media/human.obj"));
	SetMesh(GetSceneManager()->LoadMeshFromAOBJ("human2.aobj"));
	m_Animation.SetMesh(GetMesh());
	m_Animation.SetAnimation("Walk");
	m_Animation.SetAnimationSpeed(3.5f);
	m_ToolAnimation.SetMesh(GetMesh());
	m_ToolAnimation.SetAnimationSpeed(3.5f);
	m_ToolAnimation.SetAnimationLoop(false);

	for (int i = 0; i < 40; ++i) {
		//m_Inventory[i] = NULL;
		m_Inventory.push_back(NULL);

		if (i < 10)
		{
			//m_Hotbar[i] = NULL;
			m_Hotbar.push_back(NULL);
			m_CurrentAmmo[i] = -1;
		}

		if (i < 14)
			m_Equipment.push_back(NULL);
	}

	// set the active camera
	m_pCamera = GetSceneManager()->GetActiveCamera();
	m_pCamera->SetDirection(m_CameraRotation.y);

	// add mouse commands for left and right click
	//AddMouseFunction(GLFW_MOUSE_BUTTON_LEFT, GLFW_PRESS, Player::LeftClick);
	AddMouseFunction(GLFW_MOUSE_BUTTON_RIGHT, GLFW_PRESS, Player::RightClick);
	AddScrollFunction(Player::Scroll);

	// sort out the mouse over rendering
	m_BlockMesh = GetSceneManager()->CreateTerrainBlock("Grass");
	m_BreakMesh = new Mesh();
	m_BreakMesh->CopyFrom(m_BlockMesh);
	GetSceneManager()->AddTransparencyRenderFunction(this, Player::RenderMouseOver);
	GetSceneManager()->AddTransparencyRenderFunction(this, Player::RenderBlockBreak);

	// call the break mesh get VBO to create the buffer object (this avoids error 1282 later)
	m_BreakMesh->GetVBO();

	// set our AABB
	SetAABB(vector3df(-5.f, 0.f, -5.f), vector3df(5.f, 16.f, 5.f));

	GetSceneManager()->ToggleMouse(true);

	// load from the file
	LoadFromFile();
}

Player::~Player()
{
	Clear();
	delete m_BreakMesh;
}

void Player::LoadFromFile()
{
	// read the player from the file
	char* fbuf;
	std::streampos length;

	std::ifstream file(m_FileName, std::fstream::binary | std::fstream::ate);
	if (file.is_open())
	{
		// read the data
		length = file.tellg();
		fbuf = new char[length];
		file.seekg(0, std::fstream::beg);
		file.read(fbuf, length);
		file.close();

		// uncompress the data
		uLongf size = (50) * 300 + 300;
		unsigned char* buf = new unsigned char[size];
		uncompress(buf, &size, (unsigned char*)fbuf, length);
		delete[] fbuf;

		// create the player
		union {
			short size;
			unsigned char byte[2];
		} conv;
		conv.byte[0] = buf[0];
		conv.byte[1] = buf[1];

		// player name
		short namelen = conv.size;
		char* name = new char[namelen + 1];
		for (short i = 0; i < namelen; ++i) {
			name[i] = (char)buf[2 + i];
		}
		name[namelen] = '\0';
		string pname(name);
		delete[] name;
		m_Name = pname;

		// load the inventory
		SceneManager* pSceneMan = GetSceneManager();
		vector<unsigned char> data;
		GLuint pos = 0;
		for (GLuint i = 0; i < 64; ++i) {
			// get the length of the name
			conv.byte[0] = buf[namelen + 2 + pos];
			conv.byte[1] = buf[namelen + 3 + pos];
			pos += 2;
			short len = conv.size;

			if (len > 0)
			{
				// put the name
				data.push_back(conv.byte[0]);
				data.push_back(conv.byte[1]);
				for (short j = 0; j < len; ++j, ++pos) {
					data.push_back(buf[namelen + 2 + pos]);
				}

				// stack size
				data.push_back(buf[namelen + 2 + pos]);
				data.push_back(buf[namelen + 3 + pos]);
				pos += 2;

				// create the item
				Item* item = pSceneMan->CreateItem(data);
				item->SetVisible(false);

				// put it in the inventory
				if (i < 40)
					m_Inventory[i] = item;
				else if (i < 50)
					m_Hotbar[i - 40] = item;
				else
					m_Equipment[i - 50] = item;

				// clean up
				data.clear();
			}
		}

		// load up player stats
		m_Stats = Stats::FromBuffer(&buf[namelen + 2 + pos]);

		// clean up
		delete[] buf;
	}
	else
	{
		// this is a new player, give them default items
		SceneManager* pSceneMan = GetSceneManager();
		m_Hotbar[0] = pSceneMan->CreateItem("Bronze Pickaxe", vector3df());
		m_Hotbar[1] = pSceneMan->CreateItem("Bronze Sword", vector3df());
		m_Hotbar[2] = pSceneMan->CreateItem("Bronze Axe", vector3df());
		m_Hotbar[0]->SetVisible(false);
		m_Hotbar[1]->SetVisible(false);
		m_Hotbar[2]->SetVisible(false);
	}
}

void Player::SaveToFile()
{
	// save this player to a file
	vector<unsigned char> buf;

	union {
		short size;
		unsigned char byte[2];
	} conv;

	// output the name
	conv.size = (short)m_Name.length();
	buf.push_back(conv.byte[0]);
	buf.push_back(conv.byte[1]);

	for (unsigned int i = 0; i < m_Name.length(); ++i) {
		buf.push_back((unsigned char)m_Name[i]);
	}

	// output the inventory
	for (GLuint i = 0; i < 40; ++i) {
		if (m_Inventory[i] != NULL)
			m_Inventory[i]->WriteTo(buf);
		else
		{
			buf.push_back(0);
			buf.push_back(0);
		}
	}

	// output the hotbar
	for (GLuint i = 0; i < 10; ++i) {
		if (m_Hotbar[i] != NULL)
			m_Hotbar[i]->WriteTo(buf);
		else
		{
			buf.push_back(0);
			buf.push_back(0);
		}
	}

	// output the equipment
	for (GLuint i = 0; i < 14; ++i) {
		if (m_Equipment[i] != NULL)
			m_Equipment[i]->WriteTo(buf);
		else
		{
			buf.push_back(0);
			buf.push_back(0);
		}
	}

	// output player stats
	unsigned char* stats = Stats::ToBuffer(m_Stats);
	for (GLuint i = 0; i < sizeof(m_Stats); ++i) {
		buf.push_back(stats[i]);
	}

	// compress the data to write to the disk
	uLongf size = buf.size();
	unsigned char* cbuf = new unsigned char[size];
	compress(cbuf, &size, &buf[0], buf.size());

	// write to the file
	std::ofstream file(m_FileName, std::fstream::binary | std::fstream::out);

	for (uLongf i = 0; i < size; ++i) {
		file << cbuf[i];
	}

	file.close();
}

void Player::SaveDefaultCharacter(string filename, string name)
{
	// write a new default character to the given file with the given name
	vector<unsigned char> buf;

	union {
		short size;
		unsigned char byte[2];
	} conv;

	// output the name
	conv.size = (short)name.length();
	buf.push_back(conv.byte[0]);
	buf.push_back(conv.byte[1]);

	for (unsigned int i = 0; i < name.length(); ++i) {
		buf.push_back((unsigned char)name[i]);
	}

	// output the blank inventory
	for (GLuint i = 0; i < 40; ++i) {
		buf.push_back(0);
		buf.push_back(0);
	}

	// write the three default items
	auto create_item = [&buf](string name) {
		int length = name.length();

		union {
			short size;
			unsigned char byte[2];
		} conv;
		conv.size = length;

		// output size of this item
		buf.push_back(conv.byte[0]);
		buf.push_back(conv.byte[1]);

		// output name of the item
		for (short i = 0; i < length; ++i) {
			buf.push_back((unsigned char)name[i]);
		}

		// output the stack size
		conv.size = 1;
		buf.push_back(conv.byte[0]);
		buf.push_back(conv.byte[1]);
	};
	create_item("Bronze Pickaxe");
	create_item("Bronze Sword");
	create_item("Bronze Axe");

	// output the rest of the hotbar + equipment
	for (GLuint i = 0; i < 21; ++i) {
			buf.push_back(0);
			buf.push_back(0);
	}

	// output player stats
	Stats stats;
	stats.health = 80;
	stats.maxhealth = 80;
	stats.energy = 50;
	stats.maxenergy = 50;

	unsigned char* statbuf = Stats::ToBuffer(stats);
	for (GLuint i = 0; i < sizeof(stats); ++i) {
		buf.push_back(statbuf[i]);
	}

	// compress the data to write to the disk
	uLongf size = buf.size();
	unsigned char* cbuf = new unsigned char[size];
	compress(cbuf, &size, &buf[0], buf.size());

	// write to the file
	std::ofstream file(filename, std::fstream::binary | std::fstream::out);

	for (uLongf i = 0; i < size; ++i) {
		file << cbuf[i];
	}

	file.close();
}

void Player::SetName(string name)
{
	m_Name = name;
}

string Player::GetName()
{
	return m_Name;
}

bool Player::MoveCheck(const vector3df& pos, GLfloat& fMoveX, GLfloat& fMoveY)
{
	bool bReturn = false;
	
	float fCheckX = fMoveX;
	float fCheckY = fMoveY;

	if (fCheckX < 0)
		fCheckX = min(fCheckX, -2.0f);
	else
		fCheckX = max(fCheckX, 2.0f);

	if (fCheckY < 0)
		fCheckY = min(fCheckY, -2.0f);
	else
		fCheckY = max(fCheckY, 2.0f);

	vector3di chunkPos, blockPos, oChunkPos, oBlockPos;
	m_pTerrain->ConvertToChunkBlockPosition(vector3df(pos) + vector3df(0, 12.f, 0), blockPos, chunkPos);
	bool bStuck = m_pTerrain->GetBlock(vector3df(pos) + vector3df(0, 16.f, 0)).Solid > 0;
	
	vector3df check = pos;
	GLfloat aboveDist = 0.f;
	GLfloat moveHeight = check.y;
	check.y += 12.0f;

	// check the x movement
	check.x += fCheckX;
	moveHeight = m_pTerrain->GetHeightBelow(check);
	aboveDist = m_pTerrain->GetDistanceAbove(check);
	m_pTerrain->ConvertToChunkBlockPosition(check, oBlockPos, oChunkPos);
	if ((moveHeight - (pos.y - 6.0f) > 10.0f || aboveDist < 4.0f) && (!bStuck || blockPos != oBlockPos))
	{
		fMoveX = 0;
		bReturn = true;
	}

	// check the z movement
	check.z = pos.z;
	check.y = pos.y + 12.0f;
	check.x = pos.x;
	check.z += fCheckY;
	moveHeight = m_pTerrain->GetHeightBelow(check);
	aboveDist = m_pTerrain->GetDistanceAbove(check);
	m_pTerrain->ConvertToChunkBlockPosition(check, oBlockPos, oChunkPos);
	if ((moveHeight - (pos.y - 6.0f) > 10.0f || aboveDist < 4.0f) && (!bStuck || blockPos != oBlockPos))
	{
		fMoveY = 0;
		bReturn = true;
	}

	if (fMoveY != 0.f)
	{
		check.z = pos.z;
		check.y = pos.y + 12.0f;
		check.x = pos.x + fCheckX;
		check.z += fCheckY;
		moveHeight = m_pTerrain->GetHeightBelow(check);
		aboveDist = m_pTerrain->GetDistanceAbove(check);
		m_pTerrain->ConvertToChunkBlockPosition(check, oBlockPos, oChunkPos);
		if ((moveHeight - (pos.y - 6.0f) > 14.0f || abs(aboveDist) < 4.0f) && (!bStuck || blockPos != oBlockPos))
		{
			fMoveX = 0;
			bReturn = true;
		}
	}

	return bReturn;
}

void Player::Roll(GLfloat dir)
{
	// check we have enough energy
	if (m_Stats.energy >= 7)
	{
		// knock us back in the given direction
		KnockBack(dir, 1.f);

		// set the rolling flag to true
		m_bRolling = true;

		// pre-emptively decrease our energy (the server will correct us if we're wrong anyway)
		m_Stats.energy -= 7;

		// send a packet to the server to let it know we have rolled
		GetSceneManager()->GetRoot()->CastTo<Game>()->GetNetworkManager()->SendPlayerAction(player_rolled);
	}
	else
	{
		// flash the energy bar to let the player know they need to wait
		m_pGUI->FlashEnergyBar();
	}
}

void Player::Update(bool bForce)
{
	// if we are dead, do nothing
	if (m_Stats.health == 0)
		return;
	
	// make sure the keyboard is free
	GUIManager* pGUIMan = dynamic_cast<GUIManager*>(GetManager()->GetRoot()->GetManager(gui_manager));
	bool bKeyboardFree = pGUIMan->IsKeyboardFree();

	// get delta time
	double delta = DeltaTime::GetDelta();

	// are we being knocked back?
	GLfloat cameraOffset = 0.f;
	if (m_fKnockbackSpeed > 0.f)
	{
		vector3df pos = GetPosition();
		vector3df diff(m_fKnockbackSpeed * cos(m_fKnockbackDirection), 0, m_fKnockbackSpeed * sin(m_fKnockbackDirection));

		// check for terrain collision
		if (MoveCheck(pos, diff.x, diff.z))
			m_bSprinting = false;

		// set our rolling offset
		if (m_bRolling)
		{
			cameraOffset = -sin(m_fKnockbackSpeed * PI) * 3.f;
		}

		pos += diff;
		m_fKnockbackSpeed -= (m_bRolling ? 3.f : 6.f) * delta;
		SetPosition(pos);
	}
	else
	{
		m_bRolling = false;
	}

	// view rotation
	m_fViewRot += (m_bSprinting ? 12.f : 10.f) * delta;
	if (m_fViewRot > TWO_PI)
		m_fViewRot = 0.f;
	
	// we want the camera to look at us
	vector3df cameraPosition = GetPosition();
	vector3df oldPosition = cameraPosition;
	//m_pCamera->LookAt(vector3df(cameraPosition.x + 0.5f, cameraPosition.y, cameraPosition.z + 0.5f));
	
	// call GLObject's Update() method
	GLObject::Update();

	// set the selected item transformation matrix
	SetRotation(vector3df(0, 1, 0), (-m_CameraRotation.y + 180) * DEG_TO_RAD);
	matrix4f m;
	m_SelectedItemMatrix.Identity();

	if (m_SelectedItem && m_SelectedItemClass->Type == BLOCK)
		m_SelectedItemMatrix.Scale(vector3df(0.5f));

	m_SelectedItemMatrix.Translate(vector3df(1.4f, 11.5f, 9.f));
	m_SelectedItemMatrix = GetBone(13).transform.Multiply(m_SelectedItemMatrix);
	m.SetRotation(GetRotation());
	m_SelectedItemMatrix = m.Multiply(m_SelectedItemMatrix);
	m.Identity();
	m.SetRotation(vector3df(0, 0, -PI_OVER_2));
	m_SelectedItemMatrix = m_SelectedItemMatrix.Multiply(m);
	m_SelectedItemMatrix.Translate(GetPosition());

	// the following ugly code is movement code
	SceneManager* pSceneMan = GetSceneManager();
	GLfloat fRotation = 0;
	GLuint uiSize = 0;

	// if we have run out of energy, stop sprinting
	if (m_Stats.energy == 0)
		m_bSprinting = false;

	if (bKeyboardFree && pSceneMan->GetKeyState(m_Controls[move_up]) == GLFW_PRESS)
	{
		fRotation += 180;
		++uiSize;
	}
	if (bKeyboardFree && pSceneMan->GetKeyState(m_Controls[move_down]) == GLFW_PRESS)
	{
		fRotation += 0;
		++uiSize;
		m_bSprinting = false;
	}
	if (bKeyboardFree && pSceneMan->GetKeyState(m_Controls[move_left]) == GLFW_PRESS)
	{
		fRotation += -90;

		if (pSceneMan->GetKeyState(m_Controls[move_up]) == GLFW_PRESS)
		{
			fRotation += 360;

			if (pSceneMan->GetKeyState(m_Controls[move_down]) == GLFW_PRESS)
				fRotation -= 270;
		}
		else
			m_bSprinting = false;

		++uiSize;
	}
	if (bKeyboardFree && pSceneMan->GetKeyState(m_Controls[move_right]) == GLFW_PRESS)
	{
		fRotation += 90;
		++uiSize;

		if (!pSceneMan->GetKeyState(m_Controls[move_up]) == GLFW_PRESS)
			m_bSprinting = false;
	}

	if (uiSize == 0 || !m_bSprinting)
	{
		m_bSprinting = false;
		GetShaderProgram(0)->SmoothFOV(0);
		GetShaderProgram(3)->SmoothFOV(0);
	}
	else if (m_bSprinting)
	{
		GetShaderProgram(0)->SmoothFOV(100);
		GetShaderProgram(3)->SmoothFOV(100);
	}

	static bool bRollPressed = false;
	if (!bRollPressed && !m_bFalling && uiSize > 0 && pSceneMan->GetKeyState(m_Controls[roll]) == GLFW_PRESS && m_fKnockbackSpeed < 0.02f)
	{
		Roll((m_CameraRotation.y - fRotation / uiSize) * DEG_TO_RAD);
		bRollPressed = true;
	}

	if (pSceneMan->GetKeyState(m_Controls[roll]) == GLFW_RELEASE)
		bRollPressed = false;

	float aboveDist;
	float fMoveX = 0, fMoveY = 0;

	// are we in water?
	bool bInWater = m_pTerrain->Underwater(GetPosition());
	bool bUnderwater = m_pTerrain->Underwater(GetPosition() + vector3df(0, 16, 0));

	m_pGUI->Underwater(bUnderwater);

	if (uiSize > 0)
	{
		fRotation /= uiSize;
		GLfloat angle = -m_CameraRotation.y + fRotation;

		//SetRotation(vector3df(0, 1, 0), angle * DEG_TO_RAD);

		// collision with the terrain
		fMoveX = (m_fMoveSpeed * (bInWater ? 0.75f : 1.f) * (m_bSprinting ? 1.5f : 1.f)) * (float)cos(-angle * DEG_TO_RAD) * delta;
		fMoveY = (m_fMoveSpeed * (bInWater ? 0.75f : 1.f) * (m_bSprinting ? 1.5f : 1.f)) * (float)sin(-angle * DEG_TO_RAD) * delta;

		if (MoveCheck(cameraPosition, fMoveX, fMoveY))
			m_bSprinting = false;

		// finally, move the player
		TranslatePosition(vector3df(fMoveX, 0, fMoveY));

		// view rotation
		if (!m_bFalling && !m_bRolling)
		{
			Interpolate(m_fViewRotFactor, 1.f, 8.f, 0.001f);
			Interpolate(cameraOffset, sin(m_fViewRot * 2.f) * (m_bSprinting ? 3.f : 1.f), 8.f, 0.001f);
		}
	}
	
	if (uiSize == 0 || m_bFalling || m_bRolling)
	{
		Interpolate(m_fViewRotFactor, 0.f, 8.f, 0.001f);

		if (!m_bRolling)
		{
			Interpolate(cameraOffset, 0.f, 8.f, 0.001f);
		}
	}

	if (fMoveX == 0 && fMoveY == 0 && abs(m_fFallSpeed) < 0.2f)
		m_Animation.MoveToStart();

	if (abs(m_fFallSpeed) > 0.2f)
		m_Animation.SetAnimation("Jump");
	else
		m_Animation.SetAnimation("Walk");

	// collision with the terrain
	vector3df cPos = GetPosition();
	cPos.y += 16.0f;
	float fBelow = m_pTerrain->GetHeightBelow(cPos);
	aboveDist = m_pTerrain->GetDistanceAbove(cPos);
	cPos.y -= 16.0f;
	cPos.y -= m_fFallSpeed * delta;
	if (m_bJump || (cPos.y - 0.5f > fBelow))
	{
		m_fFallSpeed += 240 * delta;

		if (bInWater && m_fFallSpeed > 60)
			m_fFallSpeed = 60;

		if (m_fFallSpeed >= 0.f)
			m_bJump = false;
		m_bFalling = true;
	}
	else if (!m_bJump && cPos.y - 0.5f <= fBelow && aboveDist > 3.0f)
	{
		if (!m_bFalling)
			Interpolate(cPos.y, fBelow, 1);

		if (m_fFallSpeed > 0.f)
		{
			m_fFallSpeed = 0;
			m_bFalling = false;
		}
	}

	// jumping
	if (bKeyboardFree && pSceneMan->GetKeyState(m_Controls[jump]) == GLFW_PRESS)
	{
		if (bInWater)
		{
			if (m_fFallSpeed > -20)
				m_fFallSpeed -= 360 * delta;
			m_bJump = true;
		}
		else if (!m_bFalling && aboveDist > 3.0f)
		{
			m_fFallSpeed = -90;
			m_bJump = true;
		}
	}

	if (aboveDist < 3.0f)
	{
		if (!m_bJump && cPos.y - 0.5f <= fBelow)
		{
			m_fFallSpeed = 0;
			SetPosition(oldPosition);
		}
		else
		{
			// bounce back down from the block above
			if (m_fFallSpeed <= 0.0f)
				m_fFallSpeed = -m_fFallSpeed * 0.2f;

			cPos.y -= m_fFallSpeed * delta;
		}
	}

	// hotbar item switching
	if (bKeyboardFree)
	{
		for (GLuint i = 0; i < 9; ++i) {
			if (pSceneMan->GetKeyState(GLFW_KEY_1 + i) == GLFW_PRESS)
			{
				m_SelectedItem = i;
				SetSelected(m_SelectedItem);
				m_pGUI->UpdateSelected();
				break;
			}
		}

		// we do 0 separately as it is 48
		if (pSceneMan->GetKeyState(GLFW_KEY_0) == GLFW_PRESS)
		{
			m_SelectedItem = 9;
			SetSelected(m_SelectedItem);
			m_pGUI->UpdateSelected();
		}
	}

	// write translation
	SetPosition(cPos);

	// sort out the camera
	m_pGUI->SetMinimapRotation((m_CameraRotation.y + 90) * DEG_TO_RAD);

	// we also want the camera to float above us (with the correct rotation)
	GLfloat _dir = m_CameraRotation.y * DEG_TO_RAD;
	GLfloat _dir2 = m_CameraRotation.x * DEG_TO_RAD;
	m_pCamera->SetDirection(m_CameraRotation.y);

	// mouse look
	static vector2dd LastMousePos = pSceneMan->GetMousePosition();
	vector2dd CurrentMousePos = pSceneMan->GetMousePosition();

	if (bKeyboardFree && !m_pGUI->GUIOpen())
	{
		pSceneMan->CenterCursor();
		
		m_CameraRotation.y += (CurrentMousePos.x - LastMousePos.x) * 0.25f * m_fLookFactor;
		m_CameraRotation.x += (CurrentMousePos.y - LastMousePos.y) * 0.24f * m_fLookFactor;
		Limit(m_CameraRotation.x, -89.f, 89.f);

		//pSceneMan->RestrictCursorToWindow();
	}

	if (m_CameraRotation.y < 0.f)
		m_CameraRotation.y += 360.f;
	else if (m_CameraRotation.y > 360.f)
		m_CameraRotation.y -= 360.f;

	LastMousePos = pSceneMan->GetMousePosition();

	vector3df lookPos(cameraPosition + vector3df(0, 16 + cameraOffset, 0));
	lookPos.y -= 75.f * sin(_dir2);
	lookPos.x -= 75.f * cos(_dir) * cos(_dir2);
	lookPos.z -= 75.f * sin(_dir) * cos(_dir2);
	m_pCamera->LookAt(lookPos);

	m_pCamera->SetPosition(cameraPosition + vector3df(0, 16 + cameraOffset, 0));
	m_pCamera->SetRotation(vector3df(0, 0, sin(m_fViewRot) * m_fViewRotFactor * (m_bSprinting ? 0.017f : 0.003f)));

	// lighting
	int rot = (GLint)(m_CameraRotation.y + 45) / 90;
	TerrainBlock b = m_pTerrain->GetBlock(cPos + vector3df(0, 5, 0));
	GLfloat sun = pSceneMan->GetSun();
	SetLightAmount(vector3df(max((GLfloat)b.LightRed, b.SkyLight * sun) * 0.066667f, max((GLfloat)b.LightGreen, b.SkyLight * sun) * 0.066667f, max((GLfloat)b.LightBlue, b.SkyLight * sun) * 0.066667f));

	// terrain collision
	vector2di _where(pSceneMan->GetRoot()->GetScreenSize().x / 2, pSceneMan->GetRoot()->GetScreenSize().y / 2);

	if (m_pGUI->GUIOpen())
		_where = pSceneMan->GetMousePosition();

	line3df line = pSceneMan->GetRayFromScreenCoords(_where);

	m_bMouseHit = pSceneMan->GetRayCollisionWithTerrain(line, m_MouseDestroy, m_MousePlace);

	// check whether the placement position is within range
	if (m_bMouseHit)
	{
		m_bMouseHit = m_MouseDestroy.Distance(m_pCamera->GetPosition()) <= 60.f;
	}

	// determine whether we can place the currently held block
	if (m_bMouseHit)
	{
		m_bCanPlace = true;
		Item* item = m_Hotbar[m_SelectedItem];
		Inventory* inv = m_pGUI->GetInventory();

		TerrainBlock block = m_pTerrain->GetBlock(m_MouseDestroy);
		if (m_TargetBlock != block)
		{
			m_TargetBlock = block;
			m_TargetBlockClass = const_cast<ItemClass*>(Item::GetItemClass(block));
		}

		if (inv->GetHeldItem())
			item = inv->GetHeldItem();

		if (item)
		{
			const ItemClass* ic = Item::GetItemClass(item->GetName());
			m_PlacementOffset.Set(0, 0, 0);

			if (ic->BlockSize.x > 1 || ic->BlockSize.z > 1)
			{
				vector3di check = ic->BlockSize;
				switch (rot)
				{
				case 0:
				case 4:
					check.z = ic->BlockSize.x;
					check.x = ic->BlockSize.z;

					if (ic->BlockSize.z > 1)
						m_PlacementOffset.x -= (1 + ((ceil(check.x / 2.0f) - 1))) * BLOCK_SIZE;
					if (ic->BlockSize.x > 1)
						m_PlacementOffset.z -= ((ic->BlockSize.x + 1) % 2 + ceil(check.z / 2.0f) - 1) * BLOCK_SIZE;
					break;
				case 1:
					if (ic->BlockSize.x > 1)
						m_PlacementOffset.x -= ((ic->BlockSize.x + 1) % 2 + ceil(check.x / 2.0f) - 1) * BLOCK_SIZE;
					if (ic->BlockSize.z > 1)
						m_PlacementOffset.z -= (1 + ((ceil(check.z / 2.0f) - 1))) * BLOCK_SIZE;
					break;
				case 2:
					check.z = ic->BlockSize.x;
					check.x = ic->BlockSize.z;

					if (ic->BlockSize.z > 1)
						m_PlacementOffset.x -= (ceil(check.x / 2.0f) - 1) * BLOCK_SIZE;
					if (ic->BlockSize.x > 1)
						m_PlacementOffset.z -= (ceil(check.z / 2.0f) - 1) * BLOCK_SIZE;
					break;
				case 3:
					if (ic->BlockSize.x > 1)
						m_PlacementOffset.x -= (ceil(check.x / 2.0f) - 1) * BLOCK_SIZE;
					if (ic->BlockSize.z > 1)
						m_PlacementOffset.z -= (ceil(check.z / 2.0f) - 1) * BLOCK_SIZE;
					break;
				}

				for (int i = 0; i < check.x; ++i) {
					for (int j = 0; j < check.z; ++j) {
						for (int k = 0; k < check.y; ++k) {
							if (m_pTerrain->GetBlock(m_MousePlace + m_PlacementOffset + vector3df(i * BLOCK_SIZE, k * BLOCK_SIZE, j * BLOCK_SIZE)).BlockType)
							{
								m_bCanPlace = false;
								break;
							}
						}
					}
				}
			}
		}
	}

	// check for collisions with mobs
	Mob* mob = NULL;
	bool bMobHit = pSceneMan->GetRayCollisionWithMobs(line, m_TargetPlace, &mob);
	GLfloat mobDist = cPos.QuickDistance(m_TargetPlace);

	if (bMobHit && mobDist > 15000.f)
	{
		bMobHit = false;
	}

	if (bMobHit)
	{
		bMobHit = (mobDist < cPos.QuickDistance(m_MouseDestroy));
	}

	/*if (!bMobHit)
	{
		vector3df tmp;
		pSceneMan->GetRayCollisionPoint(m_pTerrain, line, m_TargetPlace, tmp);
	}*/

	// update hover text
	if (bMobHit && mob)
	{
		m_pGUI->SetBlockHoverName(string("^1").append(mob->GetName()));
		m_pGUI->SetBlockHoverStatus("Hostile mob");
	}
	else if (m_bMouseHit && m_Hotbar[m_SelectedItem] && m_TargetBlockClass && m_SelectedItemClass && (m_SelectedItemClass->TypeSub == PICKAXE || m_SelectedItemClass->TypeSub == AXE) && !pGUIMan->IsHovered())
	{
		m_pGUI->SetBlockHoverName(m_TargetBlockClass->strName);
		string pre("");
		if (m_TargetBlockClass->sValues[5] == 1)
			pre = "^5Right-Click ^7to interact\n";
		
		if (m_TargetBlockClass->TypeSub == WOOD || m_TargetBlockClass->TypeSub == TREE)
		{
			if (m_SelectedItemClass->TypeSub == PICKAXE)
				m_pGUI->SetBlockHoverStatus(pre + "^8Cannot chop^7: Need ^5Axe");
			else
				m_pGUI->SetBlockHoverStatus(pre + "^5Axe: ^7Can chop");
		}
		else
		{
			if (m_SelectedItemClass->TypeSub == PICKAXE)
			{
				if (m_Hotbar[m_SelectedItem]->GetDamage() >= m_TargetBlockClass->sValues[1])
					m_pGUI->SetBlockHoverStatus(pre + "^5Pickaxe: ^7Can mine" + (m_TargetBlock.Ignored ? " - Ignored" : ""));
				else
					m_pGUI->SetBlockHoverStatus(pre + "^8Cannot mine^7: Need more ^5power");
			}
			else
				m_pGUI->SetBlockHoverStatus(pre + "^8Cannot mine^7: Need ^5Pickaxe");
		}
	}
	else
	{
		m_pGUI->SetBlockHoverName(" ");
		m_pGUI->SetBlockHoverStatus(" ");
	}

	// ammo
	if (m_AmmoDecrease)
	{
		m_pGUI->GetEquipment()->DecreaseStackSize(m_pGUI->GetEquipment()->FindItemSlot(m_AmmoDecrease));
		UpdateAmmo();

		m_AmmoDecrease = NULL;
	}

	if (m_bAmmoUpdated)
	{
		Inventory* hotbar = m_pGUI->GetHotbar();
		for (GLuint i = 0; i < m_Hotbar.size(); ++i) {
			InventoryCell* cell = hotbar->GetCell(i);

			if (m_Hotbar[i])
			{
				const ItemClass* ic = Item::GetItemClass(m_Hotbar[i]->GetIndex());

				if (ic && ic->TypeSub == RANGED)
					cell->SetText(m_CurrentAmmo[i]);
			}
		}

		m_bAmmoUpdated = false;
	}

	// sprinting
	if (bKeyboardFree && pSceneMan->GetKeyState(m_Controls[sprint]) == GLFW_PRESS)
	{
		m_bSprinting = true;
		m_fLookFactor = 0.35f;
	}

	// left click
	if (pSceneMan->GetMouseButton(GLFW_MOUSE_BUTTON_1) && !pGUIMan->IsHovered())
	{
		LeftClick();
		m_bSprinting = false;
	}

	// we will now animate ourself
	if (m_bSprinting)
		m_Animation.SetAnimationSpeed(4.4f);
	else
		m_Animation.SetAnimationSpeed(3.5f);

	Animate(&m_Animation);

	AnimFrame lower, upper;
	GLfloat time;
	m_bSwinging = false;
	if (m_ToolAnimation.Animate(lower, upper, time) == animation_running)
	{
		GetBone(3).rot.Set(Lerp(lower.rotations[3], upper.rotations[3], time));
		GetBone(4).rot.Set(Lerp(lower.rotations[4], upper.rotations[4], time));
		GetBone(13).rot.Set(Lerp(lower.rotations[13], upper.rotations[13], time));

		// set the swinging status
		if (m_SelectedItemClass)
		{
			if ((m_SelectedItemClass->Type == TOOL || m_SelectedItemClass->TypeSub == MELEE) && m_ToolAnimation.GetCurrentPosition() / m_ToolAnimation.GetAnimationLength() <= 0.45f)
				m_bSwinging = true;
			else if (m_SelectedItemClass->TypeSub == RANGED && m_CurrentAmmo > 0)
				m_bSwinging = true;
			else if (m_SelectedItemClass->TypeSub == MAGIC && m_Stats.energy > 0)
				m_bSwinging = true;
		}

		// set our look factor down so we can't swing around wildly
		m_fLookFactor = 0.35f;
	}
	else if (!m_bSprinting)
		Interpolate(m_fLookFactor, 1.f, 20.f, 0.05f);

	// override any current animation to look towards the mouse
	/*vector3df pos = GetPosition();
	plane3df plane(vector3df(0, 1, 0), GetPosition());
	vector3df lookat;
	if (plane.IntersectWithLine(line.start, line.end - line.start, lookat))
	{
		GLfloat _x = lookat.x - pos.x;
		GLfloat _z = lookat.z - pos.z;
		GLfloat angle = atan2(_z, _x);
		angle = -angle - GetRotation().y;
		GetBone(1).rot.Set(0, angle, 0);
	}*/
	GetBone(1).rot.Set(0, 0, (-m_CameraRotation.x + 30) * DEG_TO_RAD);
}

void Player::Render()
{
	// if we are dead, do nothing
	if (m_Stats.health == 0)
		return;
	
	// draw the player
	//GLObject::Render();

	// draw any equipment
	UseShaderProgram(0);
	GetShaderProgram(0)->Bind();
	if (m_Hotbar[m_SelectedItem])
	{
		RenderMesh(m_SelectedItemClass->Meshes[0], GetSceneManager()->GetTextureManager()->GetTexture(m_SelectedItemClass->strTexturePath).gltex, m_SelectedItemMatrix, vector3df(0), true);
	}
	GetShaderProgram(3)->Bind();
	UseShaderProgram(3);
}

void Player::RenderShadows()
{
	// if we are dead, do nothing
	if (m_Stats.health == 0)
		return;
	
	// draw the player
	//glUniform1i(m_ObstructedUni, 0);
	GLObject::RenderShadows();

	// draw any equipment
	UseShadowShaderProgram(2);
	GetShaderProgram(2)->Bind();
	if (m_Hotbar[m_SelectedItem])
	{
		RenderShadowMesh(m_SelectedItemClass->Meshes[0], GetSceneManager()->GetTextureManager()->GetTexture(m_SelectedItemClass->strTexturePath).gltex, m_SelectedItemMatrix, true);
	}
	GetShaderProgram(4)->Bind();
	UseShadowShaderProgram(4);
	//glUniform1i(m_ObstructedUni, ViewObstructed());
}

vector<Item*>* Player::GetInventory()
{
	return &m_Inventory;
}

vector<Item*>* Player::GetHotbar()
{
	return &m_Hotbar;
}

vector<Item*>* Player::GetEquipment()
{
	return &m_Equipment;
}

void Player::RenderMouseOver(GLObject* o)
{
	// draw the held item
	Player* p = dynamic_cast<Player*>(o);

	// if any GUI is hovered, do nothing
	if (dynamic_cast<GUIManager*>(p->GetManager()->GetRoot()->GetManager(gui_manager))->IsHovered())
		return;

	Item* item = p->m_Hotbar[p->m_SelectedItem];
	Inventory* inv = p->m_pGUI->GetInventory();

	if (inv->GetHeldItem())
		item = inv->GetHeldItem();

	if (item)
	{
		const ItemClass* ic = Item::GetItemClass(item->GetName());

		// draw the placement block
		p->UseShaderProgram(0);
		if (p->m_bMouseHit/* && (!p->m_GUICrafting->IsHovered() && !p->m_GUIInventory->IsHovered() && !p->m_GUIHotbar->IsHovered())*/)
		{
			if (ic->Type == BLOCK)
			{
				//glBlendFunc(GL_ONE, GL_ONE);
				//glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
				matrix4f m;
				vector3df pos = p->m_MousePlace;
				pos.x = (int)(pos.x / BLOCK_SIZE) * BLOCK_SIZE - (pos.x < 0) * BLOCK_SIZE + (BLOCK_SIZE / 2.f);
				pos.y = (int)(pos.y / BLOCK_SIZE) * BLOCK_SIZE - (pos.y < 0) * BLOCK_SIZE;
				pos.z = (int)(pos.z / BLOCK_SIZE) * BLOCK_SIZE - (pos.z < 0) * BLOCK_SIZE + (BLOCK_SIZE / 2.f);
				if (ic->BlockSize != vector3di(1))
				{
					int rot = (GLint)(p->m_CameraRotation.y + 45) / 90;
					switch (rot)
					{
					case 0:
					case 4:
						pos.x -= (0.5f * BLOCK_SIZE * (ic->BlockSize.z * 0.5f)) * (1 - (ic->BlockSize.z % 2));
						pos.z -= (0.5f * BLOCK_SIZE * (ic->BlockSize.x * 0.5f)) * (1 - (ic->BlockSize.x % 2));
						break;
					case 1:
						pos.x -= (0.5f * BLOCK_SIZE * (ic->BlockSize.x * 0.5f)) * (1 - (ic->BlockSize.x % 2));
						pos.z -= (0.5f * BLOCK_SIZE * (ic->BlockSize.z * 0.5f)) * (1 - (ic->BlockSize.z % 2));
						break;
					case 2:
						pos.x += (0.5f * BLOCK_SIZE * (ic->BlockSize.z * 0.5f)) * (1 - (ic->BlockSize.z % 2));
						pos.z += (0.5f * BLOCK_SIZE * (ic->BlockSize.x * 0.5f)) * (1 - (ic->BlockSize.x % 2));
						break;
					case 3:
						pos.x += (0.5f * BLOCK_SIZE * (ic->BlockSize.x * 0.5f)) * (1 - (ic->BlockSize.x % 2));
						pos.z += (0.5f * BLOCK_SIZE * (ic->BlockSize.z * 0.5f)) * (1 - (ic->BlockSize.z % 2));
						break;
					}
				}

				m.SetRotation(vector3df(0, (GLint)(-(p->m_CameraRotation.y + 45) / 90) * 90 * DEG_TO_RAD, 0));
				m.SetTranslation(pos);
				p->RenderMesh(ic->Meshes[0], p->GetSceneManager()->GetTextureManager()->GetTexture(p->m_bCanPlace ? "all_white.png" : "all_red.png").gltex, m, vector3df(0), true);
				//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				//glBlendEquation(GL_FUNC_ADD);
			}
			else if (ic->Type == TOOL)
			{
				matrix4f m;

				vector3df pos = p->m_MouseDestroy;
				pos.x = (int)(pos.x / BLOCK_SIZE) * BLOCK_SIZE - (pos.x < 0) * BLOCK_SIZE + (BLOCK_SIZE / 2.f);
				pos.y = (int)(pos.y / BLOCK_SIZE) * BLOCK_SIZE - (pos.y < 0) * BLOCK_SIZE - 0.05f;
				pos.z = (int)(pos.z / BLOCK_SIZE) * BLOCK_SIZE - (pos.z < 0) * BLOCK_SIZE + (BLOCK_SIZE / 2.f);

				vector3df scale(1.01f);
				if (p->m_TargetBlockClass->BlockSize != vector3di(1))
				{
					// block size isn't 1x1x1, calculate stuff
				}

				m.Scale(scale);
				m.SetRotation(vector3df(0, (GLint)(-(p->m_CameraRotation.y + 45) / 90) * 90 * DEG_TO_RAD + PI, 0));
				m.SetTranslation(pos);
				p->RenderMesh(p->m_BlockMesh, p->GetSceneManager()->GetTextureManager()->GetTexture("all_white.png").gltex, m, vector3df(0), true);
			}
		}
		p->UseShaderProgram(3);
	}
}

void Player::RenderBlockBreak(GLObject* o)
{
	// draw the held item
	Player* p = dynamic_cast<Player*>(o);

	// draw the block break
	if (p->m_BlockBreak.current > 0 && p->m_BlockBreak.current < p->m_BlockBreak.full)
	{
		matrix4f m;
		vector3df pos(p->m_BlockBreak.pos.x, p->m_BlockBreak.pos.y, p->m_BlockBreak.pos.z);
		pos *= BLOCK_SIZE;
		pos += vector3df(BLOCK_SIZE * 0.5f, 0, BLOCK_SIZE * 0.5f);

		m.Scale(vector3df(1.015f));
		m.SetTranslation(pos);
		p->UseShaderProgram(0);
		p->RenderMesh(p->m_BreakMesh, p->m_BlockBreakTexture.gltex, m, vector3df(0), true);
		p->UseShaderProgram(3);
	}
}

void Player::LeftClick()
{
	// if we are dead, do nothing
	if (m_Stats.health == 0)
		return;
	
	// handle left click events based off of the currently held item
	Item* item = m_Hotbar[m_SelectedItem];
	Inventory* inv = m_pGUI->GetInventory();
	Inventory* hot = m_pGUI->GetHotbar();
	bool bHeldItem = false;

	if (inv->GetHeldItem())
	{
		item = inv->GetHeldItem();
		bHeldItem = true;
	}

	if (!item)
		return;

	const ItemClass* ic = Item::GetItemClass(item->GetName());

	// animation
	AnimDetails anim = item->GetAnimation();
	auto animate = [&]() {
		m_ToolAnimation.SetAnimation(anim.name);
		m_ToolAnimation.SetAnimationSpeed(anim.speed);
		m_ToolAnimation.Start();
	};
		
	// do stuff based off of the selected item
	if (ic->Type == BLOCK)
	{
		if (m_bMouseHit && m_bCanPlace && m_ToolAnimation.GetAnimationStatus() != animation_running)
		{
			vector4di blockRot;
			int rot = (GLint)(m_CameraRotation.y + 45) / 90;
			switch (rot)
			{
			case 0:
			case 4:
					
				break;
			case 1:
				blockRot.x = 1;
				blockRot.y = 1;
				break;
			case 2:
				blockRot.x = 1;
				break;
			case 3:
				blockRot.y = 1;
				break;
			}
				
			TerrainBlock block(item->GetBlockType(), blockRot.x, blockRot.y, blockRot.z, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ic->bSolid);
			if (m_pTerrain->ChangeBlock(m_MousePlace + m_PlacementOffset, block))
			{
				if (!bHeldItem)
				{
					hot->DecreaseStackSize(m_SelectedItem);
					GetSceneManager()->GetRoot()->CastTo<Game>()->GetNetworkManager()->SendPlayerInventory(player_hotbar_data, GetHotbar());
				}
				else
					inv->DecreaseHeldItemStackSize();

				m_pGUI->ForceUpdateMinimap();
			}

			// do the swing animation
			animate();
		}
	}
	else if (m_bMouseHit && ic->Type == TOOL)
	{
		TerrainBlock block = m_pTerrain->GetBlock(m_MouseDestroy);

		auto mine = [&]() {
			vector3di pos(m_MouseDestroy.x * ONE_OVER_BLOCK_SIZE,
				m_MouseDestroy.y * ONE_OVER_BLOCK_SIZE,
				m_MouseDestroy.z * ONE_OVER_BLOCK_SIZE);
			pos += vector3di(m_MouseDestroy.x < 0 ? -1 : 0, m_MouseDestroy.y < 0 ? -1 : 0, m_MouseDestroy.z < 0 ? -1 : 0);

			if (m_BlockBreak.pos != pos || m_BlockBreak.current <= 0 || m_BlockBreak.full < m_TargetBlockClass->sValues[2])
			{
				m_BlockBreak.pos = pos;
				m_BlockBreak.full = m_TargetBlockClass->sValues[2];
				m_BlockBreak.current = m_BlockBreak.full;
				m_BreakMesh->ChangeAllQuadUV(vector2df(0, 0), vector2df(0.25f, 1));
			}

			if (item->GetDamage() >= m_TargetBlockClass->sValues[1])
			{
				m_BlockBreak.current -= item->GetDamage();
				GLuint x = (GLuint)((1.f - (m_BlockBreak.current / (GLfloat)m_BlockBreak.full)) * 4);
				GLfloat sx = 0.25f;
				m_BreakMesh->ChangeAllQuadUV(vector2df(sx * x, 0), vector2df(sx * x + sx, 1));

				if (m_BlockBreak.current <= 0)
				{
					m_pTerrain->ChangeBlock(m_MouseDestroy, TerrainBlock());

					m_pGUI->ForceUpdateMinimap();
				}
			}
		};

		if (ic->TypeSub == PICKAXE && block.BlockType && m_TargetBlockClass->TypeSub != WOOD && m_TargetBlockClass->TypeSub != TREE)
		{
			if (m_ToolAnimation.GetAnimationStatus() != animation_running)
			{
				// perform the block breaking
				mine();

				// do the swing animation
				animate();
			}
		}
		else if (ic->TypeSub == AXE && block.BlockType && (m_TargetBlockClass->TypeSub == WOOD || m_TargetBlockClass->TypeSub == TREE))
		{
			if (m_ToolAnimation.GetAnimationStatus() != animation_running)
			{
				// perform the block breaking
				mine();

				// do the swing animation
				animate();
			}
		}
	}
	else if (ic->Type == WEAPON)
	{
		if (ic->TypeSub == MELEE)
		{
			// do the animation
			animate();
		}
		else if (item == m_Hotbar[m_SelectedItem] && ic->TypeSub == RANGED && m_CurrentAmmo[m_SelectedItem] > 0)
		{
			// do the animation
			animate();
		}
		else if (ic->TypeSub == MAGIC && m_Stats.energy >= ic->sValues[2])
		{
			// do the animation
			animate();
		}
	}
}

bool Player::RightClick(GLObject* o)
{
	// handle right click events based off of the currently held item and/or the target
	Player* p = dynamic_cast<Player*>(o);
	if (p)
	{
		// if we are dead, do nothing
		if (p->m_Stats.health == 0)
			return false;

		// for now add a block of water
		//TerrainBlock block = p->m_pTerrain->GetBlock(p->m_MousePlace);
		//block.Water = 7;
		//p->m_pTerrain->_DEBUG_SetBlock(p->m_MousePlace, block);
		//p->m_pTerrain->_DEBUG_SetWater(p->m_MousePlace, 1.f);
		//return true;
		
		// right clicking is used mainly in weapons and for interacting with doors, switches etc
		// however, if we are holding an item then drop it!
		Inventory* inv = p->m_pGUI->GetInventory();
		if (inv->GetHeldItem())
		{
			inv->DropHeldItem();
			return true;
		}

		vector3di invPos(p->m_MouseDestroy * ONE_OVER_BLOCK_SIZE);
		if (p->m_MouseDestroy.x < 0)
			invPos.x--;
		if (p->m_MouseDestroy.y < 0)
			invPos.y--;
		if (p->m_MouseDestroy.z < 0)
			invPos.z--;

		if (p->m_TargetBlockClass && p->m_TargetBlockClass->sValues[5] > 0 && (p->m_TargetBlockClass->sValues[4] == 0 || (p->m_BlockInventoryPos != invPos || !p->m_bBlockInventoryOpen)))
		{
			// if we opened a chest (or something with an inventory) previously, send a close packet for that block
			// so that we don't keep every chest we open opened all the time
			p->m_pGUI->DeleteBlockInventory();
			
			// send an interaction packet, the server will figure out what to send us back
			// right-clicking something will always cause an open packet to be sent
			p->m_pTerrain->SendInteraction(p->m_MouseDestroy, true);
			p->m_BlockInteractionPos = p->m_MouseDestroy;

			if (p->m_TargetBlockClass->sValues[4] > 0)
			{
				p->m_BlockInventoryPos = invPos;
				p->m_bBlockInventoryOpen = true;
			}

			return true;
		}

		return false;
	}

	return true;
}

bool Player::Scroll(GLObject* o, double x, double y)
{
	// handle scrolling between items
	Player* p = dynamic_cast<Player*>(o);
	if (p)
	{
		p->m_SelectedItem -= y;
		if (p->m_SelectedItem < 0)
			p->m_SelectedItem = 9;
		else if (p->m_SelectedItem > 9)
			p->m_SelectedItem = 0;

		p->SetSelected(p->m_SelectedItem);
		p->m_pGUI->UpdateSelected();
		return true;
	}

	return false;
}

bool Player::NearCraftingStation(GLulong block)
{
	// check if we are standing near the specified crafting station
	if (block > 0)
	{
		// the station needs to be within a 7x7x3 area around the player
		vector3df pos;
		for (int i = -3; i < 3; ++i) {
			pos.x = GetPosition().x + (i * BLOCK_SIZE);
			for (int j = -3; j < 3; ++j) {
				pos.z = GetPosition().z + (j * BLOCK_SIZE);
				for (int k = -1; k < 1; ++k) {
					pos.y = GetPosition().y + (k * BLOCK_SIZE);

					// if this block is the crafting station return true
					if (m_pTerrain->GetBlock(pos) == block)
						return true;
				}
			}
		}

		// we could not find the crafting station in the vacinity so return false
		return false;
	}

	// we do not need a crafting station so return true
	return true;
}

void Player::AttachGameGUI(GameGUI* gui)
{
	m_pGUI = gui;

	// update the gui
	m_pGUI->GetInventory()->UpdateFromInventory();
	m_pGUI->GetHotbar()->UpdateFromInventory();
	m_pGUI->GetEquipment()->UpdateFromInventory();
	UpdateAmmo();
}

void Player::SetSelected(GLint i)
{
	m_SelectedItem = i;

	if (m_Hotbar[i] != NULL)
	{
		m_SelectedItemClass = const_cast<ItemClass*>(Item::GetItemClass(m_Hotbar[i]->GetName()));
	}
}

GLint Player::GetSelected()
{
	return m_SelectedItem;
}

vector<int>* Player::GetControls()
{
	return &m_Controls;
}

InventoryResult Player::PickUpItem(Item* item)
{
	// try and add this item to an inventory
	Inventory* inv = m_pGUI->GetHotbar();

	// if we are picking up ammo, try and add it to the equipment first
	InventoryResult res = INV_FAIL;
	if (item->GetType() == AMMO)
	{
		inv = m_pGUI->GetEquipment();

		res = inv->AddItem(item);
		UpdateAmmo();

		if (res != INV_SUCCESS)
			inv = m_pGUI->GetHotbar();
		else
			return INV_SUCCESS;
	}

	// if we have this item on our hotbar, try and add it there
	if (inv->FindItemSlot(item) < m_Hotbar.size())
		res = inv->AddItem(item);
	
	// if the item wasn't in our hotbar / we couldn't add it completely to the hotbar then try our inventory
	if (res != INV_SUCCESS)
	{
		inv = m_pGUI->GetInventory();
		res = inv->AddItem(item);
	}

	// we will need to try our hotbar here again as the first check might not be hit
	if (res != INV_SUCCESS)
	{
		Inventory* hot = m_pGUI->GetHotbar();
		InventoryResult hotres = hot->AddItem(item);
		if (hotres != INV_SUCCESS)
			return hotres;
	}

	// return success
	return INV_SUCCESS;
}

vector3df Player::GetMousePlacement() const
{
	return m_MousePlace;
}

vector3df Player::GetBlockInteractionPosition() const
{
	return m_BlockInteractionPos;
}

vector2df Player::GetLookDirection() const
{
	return m_CameraRotation;
}

bool Player::IsSwinging() const
{
	return m_bSwinging;
}

bool Player::IsSprinting() const
{
	return m_bSprinting;
}

const Stats& Player::GetStats() const
{
	return m_Stats;
}

void Player::SetStats(const Stats& stats)
{
	m_Stats = stats;
}

void Player::AttachTerrain(Terrain* t)
{
	m_pTerrain = t;
}

void Player::KnockBack(GLfloat dir, GLfloat speed)
{
	m_fKnockbackDirection = dir;
	m_fKnockbackSpeed = speed;
	m_bRolling = false;
}

void Player::SetBlockInventoryOpen(bool open)
{
	m_bBlockInventoryOpen = open;
}

void Player::UseAmmo(int type)
{
	// we have shot a ranged ammo and it is time to use the ammo!
	const ItemClass* ammo = Item::GetItemClass(type);
	if (ammo)
	{
		// find the first ammo of this equivalent in our equipped items
		Inventory* equipment = m_pGUI->GetEquipment();
		m_AmmoDecrease = equipment->FindItem(ItemFilter(AMMO, ammo->TypeSub));
	}
}

void Player::UpdateAmmo()
{
	// go through our hotbar and update the ammo amount for each ranged weapon
	for (GLuint i = 0; i < 10; ++i) {
		m_CurrentAmmo[i] = 0;

		if (m_Hotbar[i])
		{
			const ItemClass* ic = Item::GetItemClass(m_Hotbar[i]->GetIndex());

			if (ic && ic->TypeSub == RANGED)
				m_CurrentAmmo[i] = m_pGUI->GetEquipment()->GetItemAmount(ItemFilter(AMMO, ic->AmmoType));
		}
	}

	// set the updated flag so we update the GUI in the main thread
	m_bAmmoUpdated = true;
}
